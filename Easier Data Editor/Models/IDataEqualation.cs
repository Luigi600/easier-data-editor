﻿namespace Easier_Data_Editor.Models
{
    public interface IDataEqualation
    {
        bool DataAreEquals(object obj);
    }
}
