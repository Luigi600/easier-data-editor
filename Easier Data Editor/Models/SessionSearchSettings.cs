﻿using System;
using Easier_Data_Editor.Collections;

namespace Easier_Data_Editor.Models
{
    class SessionSearchSettings
    {
        private bool m_matchWholeWord;
        private bool m_matchCase;
        private bool m_wrapAround;
        private bool m_inSelections;

        private ESearchMode m_searchMode;
        private ETransparency m_transparency;
        private int m_opacity = 10;

        private bool m_bookmarkLine;
        private bool m_purge;
        private bool m_subfolders = false;
        private bool m_hiddenFolders = false;

        public LimitedListWithEvents<string> RecentFinds { get; } =
            new LimitedListWithEvents<string>() { Capacity = 10 };
        public LimitedListWithEvents<string> RecentReplaces { get; } =
            new LimitedListWithEvents<string>() { Capacity = 10 };

        public LimitedListWithEvents<string> RecentFilters { get; } =
            new LimitedListWithEvents<string>() { Capacity = 10 };
        public LimitedListWithEvents<string> RecentDirectories { get; } =
            new LimitedListWithEvents<string>() { Capacity = 10 };

        public bool MatchWholeWord
        {
            get => m_matchWholeWord;
            set
            {
                if (m_matchWholeWord == value)
                    return;

                m_matchWholeWord = value;
            }
        }
        public bool MatchCase
        {
            get => m_matchCase;
            set
            {
                if (m_matchCase == value)
                    return;

                m_matchCase = value;
            }
        }
        public bool WrapAround
        {
            get => m_wrapAround;
            set
            {
                if (m_wrapAround == value)
                    return;

                m_wrapAround = value;
            }
        }
        public bool InSelections
        {
            get => m_inSelections;
            set
            {
                if (m_inSelections == value)
                    return;

                m_inSelections = value;
            }
        }
        public ESearchMode SearchMode
        {
            get => m_searchMode;
            set
            {
                if (!Enum.IsDefined(typeof(ESearchMode), value) || m_searchMode == value)
                    return;

                m_searchMode = value;
            }
        }
        public ETransparency TransparencyState
        {
            get => m_transparency;
            set
            {
                if (!Enum.IsDefined(typeof(ETransparency), value) || m_transparency == value)
                    return;

                m_transparency = value;
            }
        }
        public int Opacity
        {
            get => m_opacity;
            set
            {
                if (value < 10 || value > 100 || m_opacity == value)
                    return;

                m_opacity = value;
            }
        }
        public bool BookmarkLine
        {
            get => m_bookmarkLine;
            set
            {
                if (m_bookmarkLine == value)
                    return;

                m_bookmarkLine = value;
            }
        }
        public bool Purge
        {
            get => m_purge;
            set
            {
                if (m_purge == value)
                    return;

                m_purge = value;
            }
        }
        public bool InSubfolders
        {
            get => m_subfolders;
            set
            {
                if (m_subfolders == value)
                    return;

                m_subfolders = value;
            }
        }
        public bool InHiddenFolders
        {
            get => m_hiddenFolders;
            set
            {
                if (m_hiddenFolders == value)
                    return;

                m_hiddenFolders = value;
            }
        }
    }
}
