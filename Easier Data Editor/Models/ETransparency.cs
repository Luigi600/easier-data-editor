﻿namespace Easier_Data_Editor.Models
{
    public enum ETransparency : byte
    {
        None,
        LosingFocus,
        Always,
    }
}
