﻿namespace Easier_Data_Editor.Models
{
    public enum ETextEncoding
    {
        Unknown,
        Ansi,
        ASCII,
        UTF1,
        UTF7,
        UTF8,
        UTF16BE,
        UTF16LE,
        UTF32BE,
        UTF32LE,
    }
}
