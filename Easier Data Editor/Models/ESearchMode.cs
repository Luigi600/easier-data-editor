﻿namespace Easier_Data_Editor.Models
{
    public enum ESearchMode : byte
    {
        Normal,
        Regex,
    }
}
