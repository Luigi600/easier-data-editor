﻿using System;

namespace Easier_Data_Editor.Models
{
    [Flags]
    public enum ENewlineChars : byte
    {
        LF = 1,
        CR = 2,
    }
}
