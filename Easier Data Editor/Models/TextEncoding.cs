﻿using System;
using Easier_Data_Editor.Utilities;

namespace Easier_Data_Editor.Models
{
    public class TextEncoding : ICloneable, IDataEqualation
    {
        public ETextEncoding Encoding { get; set; }
        public bool HasBOM { get; set; }

        public TextEncoding(ETextEncoding encoding, bool hasBOM)
        {
            this.Encoding = encoding;
            this.HasBOM = hasBOM;
        }

        public object Clone() => this.MemberwiseClone();

        public bool DataAreEquals(object obj)
        {
            if (!(obj is TextEncoding encoding))
                return false;

            return this.Encoding == encoding.Encoding && this.HasBOM == encoding.HasBOM;
        }

        public override string ToString()
        {
            string encodingStr = EncodingUtil.ENCODING.TryGetValue(Encoding, out string? value)
                ? value
                : Encoding.ToString();
            if (!HasBOM)
                return encodingStr;

            return $"{encodingStr} (BOM)";
        }
    }
}
