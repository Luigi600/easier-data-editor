﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using Easier_Data_Editor.Attributes;

namespace Easier_Data_Editor.Models.LittleFighter2
{
    [Serializable]
    public class Frame
    {
        [LF2DataRegexProperty(Constants.REGEX_PATTERN_FRAME_NAME, RegexOptions.IgnoreCase)]
        public string Name { get; set; } = "";

        [LF2DataRegexProperty(Constants.REGEX_PATTERN_FRAME_ID, RegexOptions.IgnoreCase)]
        public int ID { get; set; }

        [LF2DataIntegerProperty(@"pic", true)] // to "hide" a frame in frame viewer (see changelog v1.4)
        public int PicID { get; set; }

        [LF2DataIntegerProperty(@"next", true)]
        public int @Next { get; set; }

        [LF2DataIntegerProperty(@"wait")]
        public int @Wait { get; set; }

        [LF2DataThirdPartyIntegerProperty("centerx", true, TargetProperty = nameof(Point.X))]
        [LF2DataThirdPartyIntegerProperty("centery", true, TargetProperty = nameof(Point.Y))]
        public Point Center { get; set; }

        [LF2DataIntegerProperty(@"dvx", true)]
        public int DvX { get; set; }

        [LF2DataIntegerProperty(@"dvy", true)]
        public int DvY { get; set; }

        [LF2DataIntegerProperty(@"dvz", true)]
        public int DvZ { get; set; }

        [LF2DataRegexProperty(@"bdy:.*?bdy_end:", RegexOptions.Singleline)]
        public List<Body> Bodies { get; } = new List<Body>();

        [LF2DataRegexProperty(@"itr:.*?itr_end:", RegexOptions.Singleline)]
        public List<Body> Itrs { get; } = new List<Body>();

        [LF2DataRegexProperty(@"wpoint:.*?wpoint_end:", RegexOptions.Singleline)]
        public WPoint? WPoint { get; set; } = null;

        [LF2DataRegexProperty(@"opoint:.*?opoint_end:", RegexOptions.Singleline)]
        public PointBasic? OPoint { get; set; } = null;

        [LF2DataRegexProperty(@"bpoint:.*?bpoint_end:", RegexOptions.Singleline)]
        public PointBasic? BPoint { get; set; } = null;

        [LF2DataRegexProperty(@"cpoint:.*?cpoint_end:", RegexOptions.Singleline)]
        public PointBasic? CPoint { get; set; } = null;
    }
}
