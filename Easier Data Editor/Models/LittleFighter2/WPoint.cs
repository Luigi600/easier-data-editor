﻿using System;
using Easier_Data_Editor.Attributes;

namespace Easier_Data_Editor.Models.LittleFighter2
{
    [Serializable]
    public class WPoint : PointBasic
    {
        [LF2DataIntegerProperty(@"kind")]
        public virtual int Kind { get; set; } = 0;

        [LF2DataIntegerProperty(@"weaponact")]
        public virtual int Weaponact { get; set; } = 0;

        [LF2DataIntegerProperty(@"attacking")]
        public virtual int Attacking { get; set; } = 0;

        [LF2DataIntegerProperty(@"cover")]
        public virtual bool Cover { get; set; } = true;
    }
}
