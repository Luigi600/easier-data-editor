﻿using System.Collections.Generic;
using System.Text.RegularExpressions;
using Easier_Data_Editor.Attributes;

namespace Easier_Data_Editor.Models.LittleFighter2
{
    public class Object
    {
        [LF2DataRegexProperty(
            Constants.REGEX_PATTERN_FRAME_CONTENT,
            RegexOptions.Singleline | RegexOptions.IgnoreCase
        )]
        public List<Frame> Frames { get; } = new List<Frame>();
    }
}
