﻿using System;
using System.Text.RegularExpressions;
using Easier_Data_Editor.Attributes;

namespace Easier_Data_Editor.Models.LittleFighter2
{
    [Serializable]
    public class BitmapHeaderFileRow
    {
        [LF2DataRegexProperty(Constants.REGEX_PATTERN_BITMAP_HEADER_FILE, RegexOptions.IgnoreCase)]
        public string File { get; set; } = "";

        [LF2DataIntegerProperty(@"w", false)]
        public int Width { get; set; }

        [LF2DataIntegerProperty(@"h", false)]
        public int Height { get; set; }

        [LF2DataIntegerProperty(@"col", false)]
        public int Column { get; set; }

        [LF2DataIntegerProperty(@"row", false)]
        public int Row { get; set; }
    }
}
