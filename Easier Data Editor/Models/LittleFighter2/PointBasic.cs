﻿using System;
using Easier_Data_Editor.Attributes;

namespace Easier_Data_Editor.Models.LittleFighter2
{
    [Serializable]
    public class PointBasic
    {
        [LF2DataIntegerProperty(@"x", true)]
        public virtual int X { get; set; } = 0;

        [LF2DataIntegerProperty(@"y", true)]
        public virtual int Y { get; set; } = 0;
    }
}
