﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Easier_Data_Editor.Attributes;

namespace Easier_Data_Editor.Models.LittleFighter2
{
    [Serializable]
    public class BitmapHeader
    {
        [LF2DataRegexProperty(
            Tools.Reformatters.Constants.REGEX_PATTERN_GETTER_BITMAP_HEADER_FILE,
            RegexOptions.IgnoreCase
        )]
        public List<BitmapHeaderFileRow> Files { get; } = new List<BitmapHeaderFileRow>();
    }
}
