﻿using System;
using System.Drawing;
using System.Text.RegularExpressions;
using Easier_Data_Editor.Attributes;

namespace Easier_Data_Editor.Models.LittleFighter2
{
    [Serializable]
    public class Body
    {
        [LF2DataThirdPartyIntegerProperty(
            "x",
            true,
            RegexOptions.IgnoreCase,
            TargetProperty = nameof(Rectangle.X)
        )]
        [LF2DataThirdPartyIntegerProperty(
            "y",
            true,
            RegexOptions.IgnoreCase,
            TargetProperty = nameof(Rectangle.Y)
        )]
        [LF2DataThirdPartyIntegerProperty(
            "w",
            false,
            RegexOptions.IgnoreCase,
            TargetProperty = nameof(Rectangle.Width)
        )]
        [LF2DataThirdPartyIntegerProperty(
            "h",
            false,
            RegexOptions.IgnoreCase,
            TargetProperty = nameof(Rectangle.Height)
        )]
        public Rectangle Rect { get; set; }

        [LF2DataIntegerProperty("kind", false, RegexOptions.IgnoreCase)]
        public int Kind { get; set; }
    }
}
