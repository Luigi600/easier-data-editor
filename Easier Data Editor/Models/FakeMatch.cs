﻿namespace Easier_Data_Editor.Models
{
    public class FakeMatch
    {
        public int Index { get; }
        public int Length
        {
            get => this.Value.Length;
        }
        public string Value { get; }

        public FakeMatch(int index, string value)
        {
            this.Index = index;
            this.Value = value;
        }
    }
}
