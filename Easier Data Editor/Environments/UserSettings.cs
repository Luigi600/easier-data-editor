﻿using System;
using System.ComponentModel;
using System.Linq;
using Easier_Data_Editor.Attributes;
using Easier_Data_Editor.Environments.Settings;
using Easier_Data_Editor.Utilities;

namespace Easier_Data_Editor.Environments
{
    public sealed class UserSettings : CloneableAndApplyableBaseClass
    {
        [XmlNode("AppSettings"), NestedXmlObject]
        public HiddenSettings HiddenSettings { get; private set; } = new HiddenSettings();

        [NestedXmlObject]
        public General GeneralSettings { get; private set; } = new General();

        [NestedXmlObject]
        public TextEditor TextEditorSettings { get; private set; } = new TextEditor();

        [NestedXmlObject]
        public FrameViewer FrameViewerSettings { get; private set; } = new FrameViewer();

        [NestedXmlObject]
        public Advanced AdvancedSettings { get; private set; } = new Advanced();

        public event EventHandler<PropertyChangedEventArgs>? SettingsChanged;

        private UserSettings() { }

        public static UserSettings GetInstance()
        {
            UserSettings result = new UserSettings();
            result.SetNotifyEvents();
            return result;
        }

        protected override void OnMemberCloned(CloneableAndApplyableBaseClass clone)
        {
            UserSettings deepClone = (UserSettings)clone;
            deepClone.SetNotifyEvents();
            deepClone.SettingsChanged = delegate { }; // doesn't work via Reflection API...
        }

        private void SetNotifyEvents()
        {
            foreach (
                INotifyPropertyChanged notifyPropertyChanged in this.GetType()
                    .GetProperties()
                    .Where(p => typeof(INotifyPropertyChanged).IsAssignableFrom(p.PropertyType))
                    .Select(p => p.GetValue(this))
                    .Cast<INotifyPropertyChanged>()
            )
                notifyPropertyChanged.PropertyChanged += this.NotifyPropertyChanged_PropertyChanged;
        }

        private void NotifyPropertyChanged_PropertyChanged(
            object? sender,
            PropertyChangedEventArgs e
        ) => SettingsChanged?.Invoke(sender, e);
    }
}
