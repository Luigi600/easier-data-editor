﻿using Easier_Data_Editor.Attributes;
using Easier_Data_Editor.Utilities;

namespace Easier_Data_Editor.Environments.FormatBlueprints
{
    public class WeaponStrengthListBlueprint : CloneableBindingBaseClass
    {
        private string? _list;
        private string? _entry;

        [SkipNullSerialization]
        public string? List
        {
            get => this._list;
            set
            {
                this._list = value;

                this.NotifyPropertyChanged();
            }
        }

        [SkipNullSerialization]
        public string? Entry
        {
            get => this._entry;
            set
            {
                this._entry = value;

                this.NotifyPropertyChanged();
            }
        }
    }
}
