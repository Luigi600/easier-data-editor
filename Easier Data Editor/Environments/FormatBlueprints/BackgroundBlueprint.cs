﻿using Easier_Data_Editor.Attributes;
using Easier_Data_Editor.Utilities;

namespace Easier_Data_Editor.Environments.FormatBlueprints
{
    public class BackgroundBlueprint : CloneableBindingBaseClass
    {
        private string? _background;
        private string? _layer;

        [SkipNullSerialization]
        public string? Background
        {
            get => this._background;
            set
            {
                this._background = value;

                this.NotifyPropertyChanged();
            }
        }

        [SkipNullSerialization]
        public string? Layer
        {
            get => this._layer;
            set
            {
                this._layer = value;

                this.NotifyPropertyChanged();
            }
        }
    }
}
