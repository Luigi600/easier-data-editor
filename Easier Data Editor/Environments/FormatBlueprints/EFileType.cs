﻿namespace Easier_Data_Editor.Environments.FormatBlueprints
{
    public enum EFileType
    {
        Background,
        DataTxt,
        Stage,
        NormalData,
    }
}
