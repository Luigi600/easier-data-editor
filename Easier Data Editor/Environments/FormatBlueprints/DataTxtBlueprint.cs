﻿using Easier_Data_Editor.Attributes;
using Easier_Data_Editor.Utilities;

namespace Easier_Data_Editor.Environments.FormatBlueprints
{
    public class DataTxtBlueprint : CloneableBindingBaseClass
    {
        private string? _dataTxt;
        private string? _objects;
        private string? _fileEditing;
        private string? _backgrounds;

        [SkipNullSerialization]
        public string? DataTxt
        {
            get => this._dataTxt;
            set
            {
                this._dataTxt = value;

                this.NotifyPropertyChanged();
            }
        }

        [SkipNullSerialization]
        public string? Objects
        {
            get => this._objects;
            set
            {
                this._objects = value;

                this.NotifyPropertyChanged();
            }
        }

        [SkipNullSerialization]
        public string? FileEditing
        {
            get => this._fileEditing;
            set
            {
                this._fileEditing = value;

                this.NotifyPropertyChanged();
            }
        }

        [SkipNullSerialization]
        public string? Backgrounds
        {
            get => this._backgrounds;
            set
            {
                this._backgrounds = value;

                this.NotifyPropertyChanged();
            }
        }
    }
}
