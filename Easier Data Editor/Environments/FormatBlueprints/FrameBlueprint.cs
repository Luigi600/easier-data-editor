﻿using Easier_Data_Editor.Attributes;
using Easier_Data_Editor.Utilities;

namespace Easier_Data_Editor.Environments.FormatBlueprints
{
    public class FrameBlueprint : CloneableBindingBaseClass
    {
        private string? _frame;
        private string? _bloodPoint;
        private string? _catchPoint;
        private string? _objectPoint;
        private string? _weaponPoint;
        private string? _bodies;
        private string? _itrs;

        [SkipNullSerialization]
        public string? Frame
        {
            get => this._frame;
            set
            {
                this._frame = value;

                this.NotifyPropertyChanged();
            }
        }

        [SkipNullSerialization]
        public string? BloodPoint
        {
            get => this._bloodPoint;
            set
            {
                this._bloodPoint = value;

                this.NotifyPropertyChanged();
            }
        }

        [SkipNullSerialization]
        public string? CatchPoint
        {
            get => this._catchPoint;
            set
            {
                this._catchPoint = value;

                this.NotifyPropertyChanged();
            }
        }

        [SkipNullSerialization]
        public string? ObjectPoint
        {
            get => this._objectPoint;
            set
            {
                this._objectPoint = value;

                this.NotifyPropertyChanged();
            }
        }

        [SkipNullSerialization]
        public string? WeaponPoint
        {
            get => this._weaponPoint;
            set
            {
                this._weaponPoint = value;

                this.NotifyPropertyChanged();
            }
        }

        [SkipNullSerialization]
        public string? Bodies
        {
            get => this._bodies;
            set
            {
                this._bodies = value;

                this.NotifyPropertyChanged();
            }
        }

        [SkipNullSerialization]
        public string? Itrs
        {
            get => this._itrs;
            set
            {
                this._itrs = value;

                this.NotifyPropertyChanged();
            }
        }
    }
}
