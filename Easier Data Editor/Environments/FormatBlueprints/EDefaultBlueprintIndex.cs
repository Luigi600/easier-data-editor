﻿namespace Easier_Data_Editor.Environments.FormatBlueprints
{
    internal enum EDefaultBlueprintIndex
    {
        Frame,
        FrameBdy,
        FrameItr,
        FrameBloodPoint,
        FrameWeaponPoint,
        FrameCatchPoint,
        FrameObjectPoint,
        BitmapHeader,
        BitmapHeaderFileRow,
        WeaponStrengthList,
        WeaponStrengthListEntry,
        Background,
        BackgroundLayer,
        StageStage,
        StagePhase,
        StageSpawn,
        DataTxtDataTxt,
        DataTxtObject,
        DataTxtFileEditing,
        DataTxtBackground,
    }
}
