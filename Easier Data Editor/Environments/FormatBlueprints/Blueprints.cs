﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Text.RegularExpressions;
using Easier_Data_Editor.Attributes;
using Easier_Data_Editor.Properties;
using Easier_Data_Editor.Utilities;

namespace Easier_Data_Editor.Environments.FormatBlueprints
{
    public class Blueprints : CloneableAndApplyableBaseClass
    {
        private const string MAGIC_BLUEPRINT_SEPARATOR = @"\[\]\[\]\[\]";
        private static readonly string[] DEFAULT_BLUEPRINTS = Regex.Split(
            Resources.format_blueprints_default,
            $"(?:\\r\\n|\\r|\\n){MAGIC_BLUEPRINT_SEPARATOR}(?:\\r\\n|\\r|\\n)"
        );

        [NestedXmlObject]
        public FrameBlueprint Frame { get; private set; } = new FrameBlueprint();

        [NestedXmlObject]
        public BitmapBlueprint Bitmap { get; private set; } = new BitmapBlueprint();

        [NestedXmlObject]
        public WeaponStrengthListBlueprint WeaponStrengthList { get; private set; } =
            new WeaponStrengthListBlueprint();

        [NestedXmlObject]
        public BackgroundBlueprint Background { get; private set; } = new BackgroundBlueprint();

        [NestedXmlObject]
        public StageBlueprint Stage { get; private set; } = new StageBlueprint();

        [NestedXmlObject]
        public DataTxtBlueprint DataTxt { get; private set; } = new DataTxtBlueprint();

        public event EventHandler<PropertyChangedEventArgs>? BlueprintsChanged;

        public string GetFrame() =>
            this.Frame.Frame ?? DEFAULT_BLUEPRINTS[(int)EDefaultBlueprintIndex.Frame];

        public string GetFrameBloodPoint() =>
            this.Frame.BloodPoint
            ?? DEFAULT_BLUEPRINTS[(int)EDefaultBlueprintIndex.FrameBloodPoint];

        public string GetFrameCatchPoint() =>
            this.Frame.CatchPoint
            ?? DEFAULT_BLUEPRINTS[(int)EDefaultBlueprintIndex.FrameCatchPoint];

        public string GetFrameObjectPoint() =>
            this.Frame.ObjectPoint
            ?? DEFAULT_BLUEPRINTS[(int)EDefaultBlueprintIndex.FrameObjectPoint];

        public string GetFrameWeaponPoint() =>
            this.Frame.WeaponPoint
            ?? DEFAULT_BLUEPRINTS[(int)EDefaultBlueprintIndex.FrameWeaponPoint];

        public string GetFrameBody() =>
            this.Frame.Bodies ?? DEFAULT_BLUEPRINTS[(int)EDefaultBlueprintIndex.FrameBdy];

        public string GetFrameItr() =>
            this.Frame.Itrs ?? DEFAULT_BLUEPRINTS[(int)EDefaultBlueprintIndex.FrameItr];

        public string GetBitmapHeader() =>
            this.Bitmap.BitmapHeader
            ?? DEFAULT_BLUEPRINTS[(int)EDefaultBlueprintIndex.BitmapHeader];

        public string GetBitmapFileRow() =>
            this.Bitmap.FileRow
            ?? DEFAULT_BLUEPRINTS[(int)EDefaultBlueprintIndex.BitmapHeaderFileRow];

        public string GetWeaponStrengthList() =>
            this.WeaponStrengthList.List
            ?? DEFAULT_BLUEPRINTS[(int)EDefaultBlueprintIndex.WeaponStrengthList];

        public string GetWeaponStrengthListEntry() =>
            this.WeaponStrengthList.Entry
            ?? DEFAULT_BLUEPRINTS[(int)EDefaultBlueprintIndex.WeaponStrengthListEntry];

        public string GetBackground() =>
            this.Background.Background
            ?? DEFAULT_BLUEPRINTS[(int)EDefaultBlueprintIndex.Background];

        public string GetBackgroundLayer() =>
            this.Background.Layer
            ?? DEFAULT_BLUEPRINTS[(int)EDefaultBlueprintIndex.BackgroundLayer];

        public string GetStageStage() =>
            this.Stage.Stage ?? DEFAULT_BLUEPRINTS[(int)EDefaultBlueprintIndex.StageStage];

        public string GetStagePhase() =>
            this.Stage.Phase ?? DEFAULT_BLUEPRINTS[(int)EDefaultBlueprintIndex.StagePhase];

        public string GetStageSpawn() =>
            this.Stage.Spawn ?? DEFAULT_BLUEPRINTS[(int)EDefaultBlueprintIndex.StageSpawn];

        public string GetDataTxtDataTxt() =>
            this.DataTxt.DataTxt ?? DEFAULT_BLUEPRINTS[(int)EDefaultBlueprintIndex.DataTxtDataTxt];

        public string GetDataTxtObject() =>
            this.DataTxt.Objects ?? DEFAULT_BLUEPRINTS[(int)EDefaultBlueprintIndex.DataTxtObject];

        public string GetDataTxtFileEditing() =>
            this.DataTxt.FileEditing
            ?? DEFAULT_BLUEPRINTS[(int)EDefaultBlueprintIndex.DataTxtFileEditing];

        public string GetDataTxtBackground() =>
            this.DataTxt.Backgrounds
            ?? DEFAULT_BLUEPRINTS[(int)EDefaultBlueprintIndex.DataTxtBackground];

        public void SetFrame(string val) =>
            this.Frame.Frame = DEFAULT_BLUEPRINTS[(int)EDefaultBlueprintIndex.Frame].Equals(val)
                ? null
                : val;

        public void SetFrameBloodPoint(string val) =>
            this.Frame.BloodPoint = DEFAULT_BLUEPRINTS[(int)EDefaultBlueprintIndex.FrameBloodPoint]
                .Equals(val)
                ? null
                : val;

        public void SetFrameCatchPoint(string val) =>
            this.Frame.CatchPoint = DEFAULT_BLUEPRINTS[(int)EDefaultBlueprintIndex.FrameCatchPoint]
                .Equals(val)
                ? null
                : val;

        public void SetFrameObjectPoint(string val) =>
            this.Frame.ObjectPoint = DEFAULT_BLUEPRINTS[
                (int)EDefaultBlueprintIndex.FrameObjectPoint
            ]
                .Equals(val)
                ? null
                : val;

        public void SetFrameWeaponPoint(string val) =>
            this.Frame.WeaponPoint = DEFAULT_BLUEPRINTS[
                (int)EDefaultBlueprintIndex.FrameWeaponPoint
            ]
                .Equals(val)
                ? null
                : val;

        public void SetFrameBody(string val) =>
            this.Frame.Bodies = DEFAULT_BLUEPRINTS[(int)EDefaultBlueprintIndex.FrameBdy].Equals(val)
                ? null
                : val;

        public void SetFrameItr(string val) =>
            this.Frame.Itrs = DEFAULT_BLUEPRINTS[(int)EDefaultBlueprintIndex.FrameItr].Equals(val)
                ? null
                : val;

        public void SetBitmapHeader(string val) =>
            this.Bitmap.BitmapHeader = DEFAULT_BLUEPRINTS[(int)EDefaultBlueprintIndex.BitmapHeader]
                .Equals(val)
                ? null
                : val;

        public void SetBitmapFileRow(string val) =>
            this.Bitmap.FileRow = DEFAULT_BLUEPRINTS[
                (int)EDefaultBlueprintIndex.BitmapHeaderFileRow
            ]
                .Equals(val)
                ? null
                : val;

        public void SetWeaponStrengthList(string val) =>
            this.WeaponStrengthList.List = DEFAULT_BLUEPRINTS[
                (int)EDefaultBlueprintIndex.WeaponStrengthList
            ]
                .Equals(val)
                ? null
                : val;

        public void SetWeaponStrengthListEntry(string val) =>
            this.WeaponStrengthList.Entry = DEFAULT_BLUEPRINTS[
                (int)EDefaultBlueprintIndex.WeaponStrengthListEntry
            ]
                .Equals(val)
                ? null
                : val;

        public void SetBackground(string val) =>
            this.Background.Background = DEFAULT_BLUEPRINTS[(int)EDefaultBlueprintIndex.Background]
                .Equals(val)
                ? null
                : val;

        public void SetBackgroundLayer(string val) =>
            this.Background.Layer = DEFAULT_BLUEPRINTS[(int)EDefaultBlueprintIndex.BackgroundLayer]
                .Equals(val)
                ? null
                : val;

        public void SetStageStage(string val) =>
            this.Stage.Stage = DEFAULT_BLUEPRINTS[(int)EDefaultBlueprintIndex.StageStage]
                .Equals(val)
                ? null
                : val;

        public void SetStagePhase(string val) =>
            this.Stage.Phase = DEFAULT_BLUEPRINTS[(int)EDefaultBlueprintIndex.StagePhase]
                .Equals(val)
                ? null
                : val;

        public void SetStageSpawn(string val) =>
            this.Stage.Spawn = DEFAULT_BLUEPRINTS[(int)EDefaultBlueprintIndex.StageSpawn]
                .Equals(val)
                ? null
                : val;

        public void SetDataTxtDataTxt(string val) =>
            this.DataTxt.DataTxt = DEFAULT_BLUEPRINTS[(int)EDefaultBlueprintIndex.DataTxtDataTxt]
                .Equals(val)
                ? null
                : val;

        public void SetDataTxtObject(string val) =>
            this.DataTxt.Objects = DEFAULT_BLUEPRINTS[(int)EDefaultBlueprintIndex.DataTxtObject]
                .Equals(val)
                ? null
                : val;

        public void SetDataTxtFileEditing(string val) =>
            this.DataTxt.FileEditing = DEFAULT_BLUEPRINTS[
                (int)EDefaultBlueprintIndex.DataTxtFileEditing
            ]
                .Equals(val)
                ? null
                : val;

        public void SetDataTxtBackground(string val) =>
            this.DataTxt.Backgrounds = DEFAULT_BLUEPRINTS[
                (int)EDefaultBlueprintIndex.DataTxtBackground
            ]
                .Equals(val)
                ? null
                : val;

        protected override void OnMemberCloned(CloneableAndApplyableBaseClass clone)
        {
            Blueprints deepClone = (Blueprints)clone;
            deepClone.SetNotifyEvents();
            deepClone.BlueprintsChanged = delegate { }; // doesn't work via Reflection API...
        }

        private void SetNotifyEvents()
        {
            foreach (
                INotifyPropertyChanged notifyPropertyChanged in this.GetType()
                    .GetProperties()
                    .Where(p => typeof(INotifyPropertyChanged).IsAssignableFrom(p.PropertyType))
                    .Select(p => p.GetValue(this))
                    .Cast<INotifyPropertyChanged>()
            )
            {
                notifyPropertyChanged.PropertyChanged += this.NotifyPropertyChanged_PropertyChanged;
            }
        }

        private void NotifyPropertyChanged_PropertyChanged(
            object? sender,
            PropertyChangedEventArgs e
        ) => BlueprintsChanged?.Invoke(sender, e);
    }
}
