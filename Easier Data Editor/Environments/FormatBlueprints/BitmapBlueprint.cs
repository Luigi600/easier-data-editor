﻿using Easier_Data_Editor.Attributes;
using Easier_Data_Editor.Utilities;

namespace Easier_Data_Editor.Environments.FormatBlueprints
{
    public class BitmapBlueprint : CloneableBindingBaseClass
    {
        private string? _bitmapHeader;
        private string? _fileRow;

        [SkipNullSerialization]
        public string? BitmapHeader
        {
            get => this._bitmapHeader;
            set
            {
                this._bitmapHeader = value;

                this.NotifyPropertyChanged();
            }
        }

        [SkipNullSerialization]
        public string? FileRow
        {
            get => this._fileRow;
            set
            {
                this._fileRow = value;

                this.NotifyPropertyChanged();
            }
        }
    }
}
