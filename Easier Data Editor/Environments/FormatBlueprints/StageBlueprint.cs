﻿using Easier_Data_Editor.Attributes;
using Easier_Data_Editor.Utilities;

namespace Easier_Data_Editor.Environments.FormatBlueprints
{
    public class StageBlueprint : CloneableBindingBaseClass
    {
        private string? _stage;
        private string? _phase;
        private string? _spawn;

        [SkipNullSerialization]
        public string? Stage
        {
            get => this._stage;
            set
            {
                this._stage = value;

                this.NotifyPropertyChanged();
            }
        }

        [SkipNullSerialization]
        public string? Phase
        {
            get => this._phase;
            set
            {
                this._phase = value;

                this.NotifyPropertyChanged();
            }
        }

        [SkipNullSerialization]
        public string? Spawn
        {
            get => this._spawn;
            set
            {
                this._spawn = value;

                this.NotifyPropertyChanged();
            }
        }
    }
}
