﻿using System;
using System.Drawing;
using System.Windows.Forms;
using Easier_Data_Editor.Utilities;

namespace Easier_Data_Editor.Environments.Settings
{
    public sealed class HiddenSettings : CloneableBindingBaseClass
    {
        private FormWindowState _windowState = FormWindowState.Maximized;
        private Size _windowSize = new Size(1024, 500);

        public FormWindowState WindowState
        {
            get => _windowState;
            set
            {
                if (!Enum.IsDefined(typeof(FormWindowState), value) || _windowState == value)
                    return;

                _windowState = value;
                this.NotifyPropertyChanged();
            }
        }

        public Size WindowSize
        {
            get => _windowSize;
            set
            {
                if (_windowSize.Equals(value))
                    return;

                _windowSize = value;
                this.NotifyPropertyChanged();
            }
        }
    }
}
