﻿using Easier_Data_Editor.Attributes;
using Easier_Data_Editor.Collections;
using Easier_Data_Editor.Extensions.MultiLangSupport;
using Easier_Data_Editor.Models;
using Easier_Data_Editor.Utilities;
using Easier.MultiLangSupport;

namespace Easier_Data_Editor.Environments.Settings
{
    public sealed class General : CloneableBindingBaseClass
    {
        private bool _keepSession = true;
        private bool _multipleInstances = false;
        private bool _multipleFileOpening = false;
        private bool _autoSwitchToFrameViewer = true;
        private string _language = "";
        private ENewlineChars _newline = ENewlineChars.CR | ENewlineChars.LF;
        private LimitedListWithEvents<string> _recentFiles = new LimitedListWithEvents<string>()
        {
            Capacity = 20,
        };

#pragma warning disable S4275 // Getters and setters should access the expected fields
        [SkipSerialization]
        public LimitedListWithEvents<string> RecentFiles
        {
            get => this._recentFiles;
            private set
            {
                if (value == null)
                    return;

                this._recentFiles.Clear();
                this._recentFiles.AddRange(value);
            }
        }
#pragma warning restore S4275 // Getters and setters should access the expected fields

        public string Language
        {
            get => _language;
            set
            {
                if (_language.Equals(value))
                    return;

                if (
                    !Application.IsBuiltInLanguageFileName(value)
                    && Application.GetLanguageFromFileName(value) == null
                )
                    return;

                _language = value;

                this.NotifyPropertyChanged();
            }
        }

        public int RecentFilesCapacity
        {
            get => _recentFiles.Capacity;
            set
            {
                if (value > 100 || _recentFiles.Capacity == value)
                    return;

                _recentFiles.Capacity = value;

                this.NotifyPropertyChanged();
            }
        }

        public bool MultipleInstances
        {
            get => this._multipleInstances;
            set
            {
                if (this._multipleInstances == value)
                    return;

                this._multipleInstances = value;

                this.NotifyPropertyChanged();
            }
        }

        public bool MultipleFileOpening
        {
            get => _multipleFileOpening;
            set
            {
                if (_multipleFileOpening == value)
                    return;

                _multipleFileOpening = value;

                this.NotifyPropertyChanged();
            }
        }

        public bool AutoSwitchToFrameViewer
        {
            get => _autoSwitchToFrameViewer;
            set
            {
                if (_autoSwitchToFrameViewer == value)
                    return;

                _autoSwitchToFrameViewer = value;

                this.NotifyPropertyChanged();
            }
        }

        public bool KeepSession
        {
            get => _keepSession;
            set
            {
                if (_keepSession == value)
                    return;

                _keepSession = value;

                this.NotifyPropertyChanged();
            }
        }

        public ENewlineChars NewlineCharacter
        {
            get => _newline;
            set
            {
                if (_newline == value)
                    return;

                _newline = value;

                this.NotifyPropertyChanged();
            }
        }

        public int NewlineCharacterBindingFriendly
        {
            get => ((int)_newline) - 1;
            set
            {
                value += 1;

                if ((int)_newline == value)
                    return;

                this.NewlineCharacter = (ENewlineChars)value;
                this.NotifyPropertyChanged();
            }
        }

        public void SetLanguage(Language language)
        {
            if (language is LanguageWithFile castedLang)
                this.Language = castedLang.FileName;
            else // hopefully the "default language" => so set language to empty path (built-in language)
                this.Language = "";
        }

        public override object Clone()
        {
            General obj = (General)base.Clone();

            obj._recentFiles = new LimitedListWithEvents<string>();
            obj._recentFiles.AddRange(this.RecentFiles);

            return obj;
        }
    }
}
