﻿using System;
using System.Drawing;
using Easier_Data_Editor.Utilities;

namespace Easier_Data_Editor.Environments.Settings
{
    public sealed class TextEditor : CloneableBindingBaseClass
    {
        private string _fontFamily = "Courier New";
        private uint _fontSize = 9;
        private bool _autocompletion = false;
        private bool _folding = false;
        private bool _subfolding = false;
        private bool _showLineNumbers = true;
        private bool _markCurrentLine = true;
        private Color _markLineColor = Color.FromArgb(235, 235, 255);
        private ScintillaNET.WrapMode _wrapMode = ScintillaNET.WrapMode.None;
        private bool _showWhitespaces = false;
        private bool _showChanges = true;
        private bool _bookmarks = false;
        private bool _indention = false;
        private bool _scrollbarAnnotations = true;
        private bool _autoJumpBySyncID = false;
        private bool _reformattingOnSave = false;

        public string FontFamily
        {
            get => _fontFamily;
            set
            {
                if (_fontFamily.Equals(value))
                    return;

                _fontFamily = value;

                this.NotifyPropertyChanged();
            }
        }

        public uint FontSize
        {
            get => _fontSize;
            set
            {
                if (_fontSize == value)
                    return;

                _fontSize = value;

                this.NotifyPropertyChanged();
            }
        }

        public bool Autocompletion
        {
            get => _autocompletion;
            set
            {
                if (_autocompletion == value)
                    return;

                _autocompletion = value;

                this.NotifyPropertyChanged();
            }
        }

        public bool Folding
        {
            get => _folding;
            set
            {
                if (_folding == value)
                    return;

                _folding = value;

                this.NotifyPropertyChanged();
            }
        }

        public bool Subfolding
        {
            get => _subfolding;
            set
            {
                if (_subfolding == value)
                    return;

                _subfolding = value;

                this.NotifyPropertyChanged();
            }
        }

        public bool Indention
        {
            get => _indention;
            set
            {
                if (_indention == value)
                    return;

                _indention = value;

                this.NotifyPropertyChanged();
            }
        }

        public bool ShowLineNumbers
        {
            get => _showLineNumbers;
            set
            {
                if (_showLineNumbers == value)
                    return;

                _showLineNumbers = value;

                this.NotifyPropertyChanged();
            }
        }

        public bool ShowWhitespaces
        {
            get => _showWhitespaces;
            set
            {
                if (_showWhitespaces == value)
                    return;

                _showWhitespaces = value;

                this.NotifyPropertyChanged();
            }
        }

        public bool Bookmarks
        {
            get => _bookmarks;
            set
            {
                if (_bookmarks == value)
                    return;

                _bookmarks = value;

                this.NotifyPropertyChanged();
            }
        }

        public bool MarkCurrentLine
        {
            get => _markCurrentLine;
            set
            {
                if (_markCurrentLine == value)
                    return;

                _markCurrentLine = value;

                this.NotifyPropertyChanged();
            }
        }

        public Color MarkLineColor
        {
            get => _markLineColor;
            set
            {
                if (_markLineColor.Equals(value))
                    return;

                _markLineColor = value;

                this.NotifyPropertyChanged();
            }
        }

        public ScintillaNET.WrapMode WrapMode
        {
            get => _wrapMode;
            set
            {
                if (!Enum.IsDefined(typeof(ScintillaNET.WrapMode), value) || _wrapMode == value)
                    return;

                _wrapMode = value;

                this.NotifyPropertyChanged();
            }
        }

        public int WrapModeBindingFriendly
        {
            get => (int)_wrapMode;
            set
            {
                WrapMode = (ScintillaNET.WrapMode)value;

                this.NotifyPropertyChanged();
            }
        }

        public bool ShowChanges
        {
            get => _showChanges;
            set
            {
                if (_showChanges == value)
                    return;

                _showChanges = value;

                this.NotifyPropertyChanged();
            }
        }

        public bool ScrollbarAnnotations
        {
            get => _scrollbarAnnotations;
            set
            {
                if (value == _scrollbarAnnotations)
                    return;

                _scrollbarAnnotations = value;

                this.NotifyPropertyChanged();
            }
        }

        public bool AutoJumpBySyncID
        {
            get => _autoJumpBySyncID;
            set
            {
                if (_autoJumpBySyncID == value)
                    return;

                _autoJumpBySyncID = value;

                this.NotifyPropertyChanged();
            }
        }

        public bool ReformattingOnSave
        {
            get => this._reformattingOnSave;
            set
            {
                if (this._reformattingOnSave == value)
                    return;

                this._reformattingOnSave = value;

                this.NotifyPropertyChanged();
            }
        }
    }
}
