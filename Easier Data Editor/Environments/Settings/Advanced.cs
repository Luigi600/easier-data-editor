﻿using Easier_Data_Editor.Utilities;

namespace Easier_Data_Editor.Environments.Settings
{
    public class Advanced : CloneableBindingBaseClass
    {
        private bool _encryptionDetectionFromContent = false;

        public bool EncryptionDetectionFromContent
        {
            get => this._encryptionDetectionFromContent;
            set
            {
                if (this._encryptionDetectionFromContent == value)
                    return;

                this._encryptionDetectionFromContent = value;

                this.NotifyPropertyChanged();
            }
        }
    }
}
