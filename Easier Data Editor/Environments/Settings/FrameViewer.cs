﻿using System.Drawing;
using Easier_Data_Editor.Utilities;

namespace Easier_Data_Editor.Environments.Settings
{
    public sealed class FrameViewer : CloneableBindingBaseClass
    {
        private bool _menuStrip = true;
        private Color _backgroundColor = Color.Transparent;
        private bool _globalView = true;
        private bool _syncFrameID = true;
        private bool _fullRestore = true;
        private string _weaponData = "";
        private string _weaponSprite = "";
        private string _heavyWeaponData = "";
        private string _heavyWeaponSprite = "";
        private bool _changeDirect = false;

        public bool MenuStrip
        {
            get => _menuStrip;
            set
            {
                if (_menuStrip == value)
                    return;

                _menuStrip = value;

                this.NotifyPropertyChanged();
            }
        }

        public Color BackgroundColor
        {
            get => _backgroundColor;
            set
            {
                if (_backgroundColor.Equals(value))
                    return;

                _backgroundColor = value;

                this.NotifyPropertyChanged();
            }
        }

        public bool GlobalView
        {
            get => _globalView;
            set
            {
                if (_globalView == value)
                    return;

                _globalView = value;

                this.NotifyPropertyChanged();
            }
        }

        public bool FullRestore
        {
            get => _fullRestore;
            set
            {
                if (_fullRestore == value)
                    return;

                _fullRestore = value;

                this.NotifyPropertyChanged();
            }
        }

        public bool SyncFrameID
        {
            get => _syncFrameID;
            set
            {
                if (_syncFrameID == value)
                    return;

                _syncFrameID = value;

                this.NotifyPropertyChanged();
            }
        }

        public string WeaponData
        {
            get => _weaponData;
            set
            {
                if (_weaponData.Equals(value))
                    return;

                _weaponData = value;

                this.NotifyPropertyChanged();
            }
        }

        public string WeaponSprite
        {
            get => _weaponSprite;
            set
            {
                if (_weaponSprite.Equals(value))
                    return;

                _weaponSprite = value;

                this.NotifyPropertyChanged();
            }
        }

        public string HeavyWeaponData
        {
            get => _heavyWeaponData;
            set
            {
                if (_heavyWeaponData.Equals(value))
                    return;

                _heavyWeaponData = value;

                this.NotifyPropertyChanged();
            }
        }

        public string HeavyWeaponSprite
        {
            get => _heavyWeaponSprite;
            set
            {
                if (_heavyWeaponSprite.Equals(value))
                    return;

                _heavyWeaponSprite = value;

                this.NotifyPropertyChanged();
            }
        }

        public bool ChangeDirect
        {
            get => _changeDirect;
            set
            {
                if (_changeDirect == value)
                    return;

                _changeDirect = value;

                this.NotifyPropertyChanged();
            }
        }
    }
}
