﻿using System;
using System.IO;
using System.Linq;
using Easier_Data_Editor.Environments.FormatBlueprints;
using Easier_Data_Editor.Extensions.MultiLangSupport;
using Easier_Data_Editor.Extensions.Scintilla.SyntaxHighlighting;
using Easier_Data_Editor.Properties;
using Easier_Data_Editor.Serializers;
using Easier.MultiLangSupport;
using Easier.MultiLangSupport.Environments;

namespace Easier_Data_Editor.Environments
{
    static class Application
    {
        public const string APP_TITLE = "Easier Data Editor (STM93 Version)"; // title (not translatable)

        public static readonly string APP_FOLDER = Path.Combine(
            Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
            "Easier Data Editor"
        );

        public const string BUILD_COMMIT = "000000";

        public static readonly string SETTINGS_FILE_PATH = Path.Combine(APP_FOLDER, "settings.xml");
        public const string SETTINGS_XML_NODE = "Settings";
        public const string SETTINGS_XML_VERSION_ATTRIBUTE = "Version";
        public const int SETTINGS_XML_VERSION = 3;

        public static readonly string FORMAT_BLUEPRINTS_FILE_PATH = Path.Combine(
            APP_FOLDER,
            "blueprints.xml"
        );
        public const string FORMAT_BLUEPRINTS_XML_NODE = "Blueprints";
        public const string FORMAT_BLUEPRINTS_XML_VERSION_ATTRIBUTE = "Version";
        public const int FORMAT_BLUEPRINTS_XML_VERSION = 1;

        public static readonly string RECENT_FILES_FILE_PATH = Path.Combine(
            APP_FOLDER,
            "recentfiles.txt"
        );
        public static readonly string LAYOUT_FILE_PATH = Path.Combine(APP_FOLDER, "layout_v2.xml");
        public static readonly string FILE_PATH_SYNTAX = Path.Combine(APP_FOLDER, "syntax.xml");
        public static readonly string LANGUAGE_DIRECTORY_PATH = Path.Combine(
            APP_FOLDER,
            "languages"
        );

        public static event FileSystemEventHandler? LanguageChanged;

        public static readonly System.Globalization.CultureInfo CultureEN =
            new System.Globalization.CultureInfo("en-US");

        public static readonly string[] SUPPORTED_FILE_EXTENSIONS = new[] { "dat", "txt" };
        public static readonly UserSettings UserSettings = UserSettings.GetInstance();
        public static readonly Blueprints FormatBlueprints = new Blueprints();
        public static readonly MemoryStream DockPanelLayout = new MemoryStream();

        public const string DEFAULT_LANGUAGE = "English";
        public const string DEFAULT_LANGUAGE_FILE_NAME = ""; // empty is "built-in" language
        public static readonly string XML_EXTENSION = ".xml".ToLower();

        public static readonly Random RANDOM_GENERATOR = new Random();

        public static readonly Syntax REFORMATTING_SYNTAX = SyntaxDeserializer.LoadSyntax(
            Resources.syntax_special
        );

        private static FileSystemWatcher? _fileSystemWatcher;
        private static Syntax _defaultSyntax = SyntaxDeserializer.LoadSyntax(Resources.syntax);
        private static Syntax? _customSyntax;

        public static Syntax SYNTAX => _customSyntax ?? _defaultSyntax;

        internal static void LoadCustomSyntax(string? xmlContent)
        {
            if (xmlContent == null)
            {
                _customSyntax = null;
                return;
            }

            _customSyntax = SyntaxDeserializer.LoadSyntax(xmlContent);
        }

        internal static void SetLanguageDirectoryWatcher()
        {
            if (_fileSystemWatcher != null)
                return;

            _fileSystemWatcher = new FileSystemWatcher(LANGUAGE_DIRECTORY_PATH)
            {
                Filter = $"*{XML_EXTENSION}",
                EnableRaisingEvents = true,
                NotifyFilter =
                    NotifyFilters.FileName
                    | NotifyFilters.LastWrite
                    | NotifyFilters.Size
                    | NotifyFilters.CreationTime,
            };

            _fileSystemWatcher.Renamed += (sender, args) => LanguageChanged?.Invoke(sender, args);
            _fileSystemWatcher.Changed += (sender, args) => LanguageChanged?.Invoke(sender, args);
            _fileSystemWatcher.Created += (sender, args) => LanguageChanged?.Invoke(sender, args);
            _fileSystemWatcher.Deleted += (sender, args) => LanguageChanged?.Invoke(sender, args);
        }

        internal static Language? GetLanguageFromFileName(string fileName) =>
            Translation
                .GetLanguages()
                .Where(l => l is LanguageWithFile)
                .Cast<LanguageWithFile>()
                .FirstOrDefault(t => t.FileName.Equals(fileName));

        internal static bool IsBuiltInLanguageFileName(string val) => string.IsNullOrEmpty(val);

        internal static bool IsExtensionSupported(string filePath)
        {
            string ext = Path.GetExtension(filePath).ToLower().Substring(1);
            return SUPPORTED_FILE_EXTENSIONS.Contains(ext);
        }
    }
}
