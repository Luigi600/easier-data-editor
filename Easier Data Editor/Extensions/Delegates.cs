﻿namespace Easier_Data_Editor.Extensions
{
    public static class Delegates
    {
        public delegate void ActionRef<T>(ref T item);
        public delegate void ActionRef<T1, T2>(ref T1 arg1, ref T2 arg2);
    }
}
