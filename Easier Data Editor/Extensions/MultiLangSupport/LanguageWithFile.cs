﻿using System.Collections.Generic;
using Easier.MultiLangSupport;
using Easier.MultiLangSupport.Models;

namespace Easier_Data_Editor.Extensions.MultiLangSupport
{
    public class LanguageWithFile : EditableLanguage
    {
        private readonly string _path;

        public virtual string Path => this._path;

        public virtual string FileName => System.IO.Path.GetFileName(_path);

        public LanguageWithFile(string path)
        {
            this._path = path;
        }

        public LanguageWithFile(
            string path,
            string name,
            string translator,
            Dictionary<string, List<TranslatableMetaItem>> dictionary,
            Dictionary<string, Dictionary<string, string>> dictionaryOfVariables
        )
            : base(name, translator, dictionary, dictionaryOfVariables)
        {
            this._path = path;
        }
    }
}
