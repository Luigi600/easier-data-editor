﻿using System.IO;
using System.Xml;
using Easier.MultiLangSupport;
using Easier.MultiLangSupport.Serializers;

namespace Easier_Data_Editor.Extensions.MultiLangSupport
{
    public class TranslationDeserializerWithPath : TranslationDeserializer
    {
        private readonly string _path;

        private TranslationDeserializerWithPath(string path, FileStream fileStream)
            : base(fileStream)
        {
            this._path = path;
        }

        public static Language GetInstance(string path)
        {
            FileStream input = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read);
            TranslationDeserializerWithPath deserializer = new TranslationDeserializerWithPath(
                path,
                input
            );
            Language lang = deserializer.Read();
            input.Close();

            return lang;
        }

        public override Language Read()
        {
            LanguageWithFile language = new LanguageWithFile(this._path);
            XmlDocument doc = new XmlDocument();
            doc.Load(this.FileStream);

            if (doc.DocumentElement?.LocalName.Equals("Language") != true)
                throw new InvalidDataException();

            this.ReadRootInformation(doc, language);
            this.ReadLanguageChildren(doc, language);

            return language;
        }
    }
}
