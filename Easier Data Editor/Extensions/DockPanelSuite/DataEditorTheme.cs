﻿using Easier_Data_Editor.Properties;
using WeifenLuo.WinFormsUI.ThemeVS2015;

namespace Easier_Data_Editor.Extensions.DockPanelSuite
{
    public class DataEditorTheme : VS2015ThemeBase
    {
        public DataEditorTheme()
            : base(Decompress(Resources.vstheme)) { }
    }
}
