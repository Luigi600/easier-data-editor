﻿namespace Easier_Data_Editor.Extensions.Scintilla
{
    public enum EMarkerIndices : byte
    {
        Saved = 1,
        Changes = 2,
        Bookmark = 4,
    }
}
