﻿using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Easier_Data_Editor.Extensions.Scintilla.SyntaxHighlighting;
using ScintillaNET;

namespace Easier_Data_Editor.Extensions.Scintilla
{
    public class LF2DataLexer
    {
        private const uint FLAG_FOLDING_COLLAPSED =
            (uint)1 << (int)EIndicatorIndices.FoldingCollapsed;

        public virtual Syntax Syntax { get; set; }

        public LF2DataLexer(Syntax syntax)
        {
            this.Syntax = syntax;
        }

        public void Style(Scintilla scintilla, int startPos, int endPos, bool folding)
        {
            StringBuilder range = new StringBuilder(
                scintilla.GetTextRange(startPos, endPos - startPos)
            );
            GetAndSetStyles(scintilla, range, startPos, endPos, folding);
        }

        public ImmutableList<Range> GetStyleRanges(
            Scintilla scintilla,
            StringBuilder range,
            int startPos,
            int endPos,
            bool folding
        )
        {
            List<Range> styles = new List<Range>();
            if (folding)
                styles.AddRange(RemoveFoldedStrings(scintilla, range, startPos, endPos));

            styles.AddRange(GetRangesFromSpaces(range, startPos));
            styles.AddRange(GetRangesFromRegexSpecials(range, startPos));
            styles.AddRange(GetRangesFromSingleWorlds(range, startPos));

            return styles.ToImmutableList();
        }

        protected static Range[] RemoveFoldedStrings(
            Scintilla scintilla,
            StringBuilder range,
            int startPos,
            int endPos
        )
        {
            List<Range> styles = new List<Range>();
            for (
                int lineIndex = scintilla.LineFromPosition(startPos);
                lineIndex <= scintilla.LineFromPosition(endPos);
                lineIndex++
            )
            {
                Line line = scintilla.Lines[lineIndex];
                if (
                    !line.Expanded
                    && (scintilla.IndicatorAllOnFor(line.Position) & FLAG_FOLDING_COLLAPSED)
                        == FLAG_FOLDING_COLLAPSED
                    && line.Position + line.Length < endPos
                )
                {
                    styles.Add(
                        new Range(line.Position, line.Length, StylesIndices.FolderCollapsed)
                    );
                    range = range.Remove(line.Position - startPos, line.Length);
                    range = range.Insert(line.Position - startPos, " ", line.Length);
                }
            }

            return styles.ToArray();
        }

        protected static Range[] GetRangesFromSpaces(StringBuilder range, int startPos)
        {
            List<Range> styles = new List<Range>();
            foreach (Match spaceMatch in Regex.Matches(range.ToString(), @"\s+"))
                styles.Add(
                    new Range(startPos + spaceMatch.Index, spaceMatch.Length, StylesIndices.Default)
                );

            return styles.ToArray();
        }

        protected static void SetRangeStyles(
            Scintilla scintilla,
            ImmutableList<Range> styles,
            int startPos,
            int endPos
        )
        {
            List<Range> sortedList = styles.OrderBy(o => o.Range2.Y).ToList();
            int sortedIndex = 0;

            scintilla.IndicatorCurrent = (int)EIndicatorIndices.SyntaxUnknown;
            scintilla.IndicatorClearRange(startPos, endPos - startPos);
            scintilla.StartStyling(startPos);

            while (startPos < endPos)
            {
                bool found = false;
                int step = 1;
                int style = (int)StylesIndices.Default;

                for (int i = sortedIndex; i <= sortedList.Count - 1; ++i)
                {
                    Range rng = sortedList[i];
                    if (startPos >= rng.Range2.X && startPos < rng.Range2.Y)
                    {
                        step = rng.Range2.Y - startPos;
                        found = true;
                        style = rng.Style;
                        sortedIndex = i;
                        break;
                    }
                }
                if (startPos + step > endPos)
                    step = endPos - startPos;

                scintilla.SetStyling(step, style);

                if (!found)
                {
                    scintilla.IndicatorFillRange(startPos, step);
                    scintilla.AddIncorrectSyntax(startPos, step);
                }
                else
                {
                    scintilla.RemoveIncorrectSyntax(startPos, step);
                }

                startPos += step;
            }
        }

        protected void GetAndSetStyles(
            Scintilla scintilla,
            StringBuilder range,
            int startPos,
            int endPos,
            bool folding
        )
        {
            ImmutableList<Range> styles = GetStyleRanges(
                scintilla,
                range,
                startPos,
                endPos,
                folding
            );

            SetRangeStyles(scintilla, styles, startPos, endPos);
        }

        protected Range[] GetRangesFromRegexSpecials(StringBuilder range, int startPos)
        {
            List<Range> styles = new List<Range>();
            foreach (ItemRegex regexItem in this.Syntax.RegexSpecials)
            {
                foreach (Match mtc in Regex.Matches(range.ToString(), regexItem.Pattern))
                {
                    styles.Add(new Range(startPos + mtc.Index, mtc.Length, regexItem.StyleIndex));

                    range = range.Remove(mtc.Index, mtc.Length);
                    range = range.Insert(mtc.Index, " ", mtc.Length);
                }
            }

            return styles.ToArray();
        }

        protected Range[] GetRangesFromSingleWorlds(StringBuilder range, int startPos)
        {
            List<Range> styles = new List<Range>();
            foreach (Match mtch in Regex.Matches(range.ToString(), @"(?:[^\s]+)"))
            {
                string val = mtch.Value.Trim().ToLower();
                int style = GetStyleIndexFromSingleWordUseRegexList(val);

                if (style < 0)
                    style = GetStyleIndexFromSingleWordUseKeywordList(val);

                if (style >= 0)
                    styles.Add(new Range(startPos + mtch.Index, mtch.Length, (byte)style));
            }

            return styles.ToArray();
        }

        protected int GetStyleIndexFromSingleWordUseRegexList(string val)
        {
            foreach (ItemRegex regexItem in this.Syntax.RegexSingleWord)
            {
                Match mtc = Regex.Match(val, regexItem.Pattern);
                if (mtc.Success && mtc.Index == 0 && mtc.Length == val.Length)
                    return regexItem.StyleIndex;
            }

            return -1;
        }

        protected int GetStyleIndexFromSingleWordUseKeywordList(string val)
        {
            if (
                this.Syntax.KeywordLists.Find(k => k.Keywords.Contains(val)) is ItemKeywordList item
            )
                return item.StyleIndex;

            return -1;
        }
    }
}
