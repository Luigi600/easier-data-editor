﻿namespace Easier_Data_Editor.Extensions.Scintilla
{
    public enum StylesIndices : byte
    {
        @Default = 32,
        LineNumber = 33,
        FolderCollapsed = 100,
    }
}
