﻿namespace Easier_Data_Editor.Extensions.Scintilla
{
    public enum EMarginIndices : byte
    {
        LineNumber = 0,
        TextChangesSpacing = 1,
        TextChanges = 2,
        BookmarkSpacing = 3,
        Bookmark = 4,

        Folding = 6,
    }
}
