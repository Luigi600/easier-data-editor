﻿namespace Easier_Data_Editor.Extensions.Scintilla
{
    public enum EIndicatorIndices : byte
    {
        @Error = 1,
        Highlight = 2,
        ResultInSelection = 3,
        TmpHighlight = 4,
        FoldingCollapsed = 5,

        SyntaxUnknown = 6,
    }
}
