﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Easier_Data_Editor.Environments.Settings;
using Easier_Data_Editor.Extensions.Scintilla.SyntaxHighlighting;
using ScintillaNET;

namespace Easier_Data_Editor.Extensions.Scintilla
{
    public partial class Scintilla : ScintillaNET.Scintilla
    {
        private static Dictionary<Syntax, LF2DataLexer> SYNTAX_TO_LEXER =
            new Dictionary<Syntax, LF2DataLexer>();

        private const int SCI_SETSELECTIONNCARETVIRTUALSPACE = 2580;
        private const int SCI_SETSELECTIONNANCHORVIRTUALSPACE = 2582;
        private const float UNDO_REDO_TEXT_INPUT_MINIMUM_INTERVAL_BETWEEN = 1.0f; // in secs
        private const float UNDO_REDO_TEXT_CLICK_MINIMUM_INTERVAL_BETWEEN = 1.0f; // in secs

        private readonly Syntax _syntax;
        private readonly LF2DataLexer _lexer;
        private readonly TextEditor _textEditorSettings;

        protected DateTime LastTextChangedDate = DateTime.MinValue;
        protected DateTime LastClickOnDocument = DateTime.MinValue;

        protected readonly PictureBox AnnotationsBox = new PictureBox();
        protected readonly List<Tuple<int, int>> IncorrectSyntax = new List<Tuple<int, int>>();

        protected bool _undoActionIsClosed = true;
        protected int _startUndoByLine = -1;
        protected bool _blockUndoRedoActions = false;
        protected bool _blockTextChangedEvent = false;
        protected int _maxLineNumberCharLength = 1;
        protected bool _setText = false;
        protected int _undoCounter = -1;
        protected bool _lastFoldingState = false;
        protected bool _lastSubfoldingState = false;

        public event EventHandler<bool>? UndoRedoStackChanged;

        public virtual bool SuppressFoldingCheck { get; set; } = false;

        // NEW API test:
        // alpha doesn't work
        // int colour2 = 255;
        // colour2 |= 255 << 8;
        // colour2 |= 0 << 16;
        // colour2 |= 5 << 24; // | (System.Drawing.ColorTranslator.ToWin32(Color.FromArgb(5, Color.Green)) << 8);
        // DirectMessage(2753, new IntPtr(50), (IntPtr)colour2); // new IntPtr(colour2));

        public virtual new Document Document
        {
            get => base.Document;
            set
            {
                base.Document = value;
                _setText = true;
                OnTextChanged(EventArgs.Empty);
                _setText = false;
            }
        }

        public virtual bool BlockUndoRedoActions
        {
            get => _blockUndoRedoActions;
            set
            {
                if (value == _blockUndoRedoActions)
                    return;

                _blockUndoRedoActions = value;
                if (value)
                    BeginUndoAction();
                else
                    EndUndoAction();
            }
        }

        public virtual int UndoSize => _undoCounter;

        public virtual bool IsSetText => this._setText;

        public virtual ImmutableList<Tuple<int, int>> IncorrectSyntaxPositions =>
            this.IncorrectSyntax.ToImmutableList();

        public Scintilla()
            : this(
                Environments.Application.SYNTAX,
                Environments.Application.UserSettings.TextEditorSettings
            ) { }

        public Scintilla(Syntax syntax, TextEditor textEditorSettings)
        {
            if (!SYNTAX_TO_LEXER.ContainsKey(syntax))
                SYNTAX_TO_LEXER.Add(syntax, new LF2DataLexer(syntax));

            this._syntax = syntax;
            this._lexer = SYNTAX_TO_LEXER[syntax];
            this._textEditorSettings = textEditorSettings;

            if (DesignMode)
                return;

            ClearCmdKey(Keys.Control | Keys.S);
            ClearCmdKey(Keys.Control | Keys.K);
            ClearCmdKey(Keys.Control | Keys.L);
            ClearCmdKey(Keys.Control | Keys.J);
            ClearCmdKey(Keys.ControlKey | Keys.J);

            SetFirstStyle();
            ReloadStyle();
            NewBeginUndoAction();
        }

        public static string GetTextThreadSafety(Scintilla scin)
        {
            try
            {
                if (!scin.IsDisposed)
                {
                    if (scin.InvokeRequired)
                        return Convert.ToString(
                            scin.Invoke(new Func<string>(() => GetTextThreadSafety(scin)))
                        );
                    else
                        return scin.Text;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
            }

            return "";
        }

        public static int LineFromPositionThreadSafety(Scintilla scin, int pos)
        {
            try
            {
                if (!scin.IsDisposed)
                {
                    if (scin.InvokeRequired)
                        return Convert.ToInt32(
                            scin.Invoke(
                                new Func<int>(() => LineFromPositionThreadSafety(scin, pos))
                            )
                        );
                    else
                        return scin.LineFromPosition(pos);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
            }

            return -1;
        }

        public static bool LineHasMarker(Line line, EMarkerIndices marker)
        {
            uint mask_ = (uint)1 << (int)marker;
            return (line.MarkerGet() & mask_) > 0;
        }

        public virtual Line[] GetVisibleLines() => this.Lines.Where(l => l.Visible).ToArray();

        public new virtual void FoldAll(FoldAction action)
        {
            base.FoldAll(action);
            if (action == FoldAction.Contract)
            {
                IndicatorCurrent = (int)EIndicatorIndices.FoldingCollapsed;
                foreach (
                    Line line in Lines.Where(l =>
                        l.FoldLevelFlags == FoldLevelFlags.Header && l.FoldParent < 0
                    )
                )
                    IndicatorFillRange(line.Position, line.Length);
            }
            else if (action == FoldAction.Expand)
            {
                IndicatorCurrent = (int)EIndicatorIndices.FoldingCollapsed;
                IndicatorClearRange(0, TextLength);
            }

            this._lexer.Style(this, 0, this.TextLength, this._textEditorSettings.Folding);
        }

        public new virtual void BeginUndoAction() => this.NewBeginUndoAction();

        public new virtual void EndUndoAction()
        {
            if (_undoActionIsClosed)
                return;

            _undoCounter += 1;
            base.EndUndoAction();
            _undoActionIsClosed = true;
            Debug.WriteLine("End Undo Action");
            UndoRedoStackChanged?.Invoke(this, true);
        }

        public new virtual void Undo()
        {
            _blockTextChangedEvent = true;
            _undoCounter -= 2;
            BeginUndoAction();
            base.Undo();
            _blockTextChangedEvent = false;
            OnTextChanged(EventArgs.Empty);
            UndoRedoStackChanged?.Invoke(this, true);
        }

        public new virtual void Redo()
        {
            _blockTextChangedEvent = true;
            BeginUndoAction();
            base.Redo();
            _blockTextChangedEvent = false;
            OnTextChanged(EventArgs.Empty);
            UndoRedoStackChanged?.Invoke(this, false);
        }

        public new virtual void Paste()
        {
            BeginUndoAction();
            base.Paste();
            LastTextChangedDate = DateTime.MinValue;
        }

        public new virtual void EmptyUndoBuffer()
        {
            base.EmptyUndoBuffer();
            UndoRedoStackChanged?.Invoke(this, true);
        }

        public new virtual int ReplaceTarget(string text)
        {
            bool oldState = this._undoActionIsClosed;
            this._undoActionIsClosed = true;

            int result = base.ReplaceTarget(text);
            this._undoActionIsClosed = oldState;
            return result;
        }

        public void AddIncorrectSyntax(int startPos, int len)
        {
            int endPos = startPos + len;
            bool found = false;
            for (int i = 0; i < this.IncorrectSyntax.Count; ++i)
            {
                Tuple<int, int> tuple = this.IncorrectSyntax[i];
                if (Utilities.CalUtil.IsInRange(startPos, endPos, tuple.Item1, tuple.Item2))
                {
                    this.IncorrectSyntax[i] = new Tuple<int, int>(
                        Math.Min(tuple.Item1, startPos),
                        Math.Max(tuple.Item2, endPos)
                    );

                    found = true;
                    break;
                }
            }

            if (!found)
                this.IncorrectSyntax.Add(new Tuple<int, int>(startPos, startPos + len));
        }

        public void RemoveIncorrectSyntax(int startPos, int len)
        {
            int endPos = startPos + len;
            int index = 0;
            while (index < this.IncorrectSyntax.Count)
            {
                Tuple<int, int> tuple = this.IncorrectSyntax[index];
                if (Utilities.CalUtil.IsInRange(startPos, endPos, tuple.Item1, tuple.Item2))
                {
                    int oldLen = this.IncorrectSyntax.Count;
                    this.RemoveIncorrectSyntax(index, startPos, endPos, tuple);
                    if (oldLen < this.IncorrectSyntax.Count)
                        ++index;
                    else if (oldLen > this.IncorrectSyntax.Count)
                        continue;
                }

                ++index;
            }
        }

        protected override void OnHandleCreated(EventArgs e)
        {
            // does not work if set via the designer, although according to PR on GitHub the underscore is necessary for this...
            // ( https://github.com/desjarlais/Scintilla.NET/pull/114/files#diff-e89b551bea607ac0ead0991f4b08de3aabfa928462a2f8c50d659c26639eaa44R3281 )
            this._ScintillaManagedDragDrop = true;
            base.OnHandleCreated(e);
        }

        protected override void OnCreateControl()
        {
            base.OnCreateControl();

            this.AnnotationsBox.BackColor = this._syntax.DefaultBackColor;
            this.AnnotationsBox.Anchor =
                AnchorStyles.Top | AnchorStyles.Right | AnchorStyles.Bottom;
            this.AnnotationsBox.Size = new Size(
                ANNOTATION_BAR_WIDTH,
                this.Height
                    - Easier_Data_Editor.Constants.DEFAULT_V_SCROLL_SCROLLBAR_BUTTON_HEIGHT * 2
                    - Easier_Data_Editor.Constants.DEFAULT_V_SCROLL_SCROLLBAR_BUTTON_PADDING * 2
            );
            this.AnnotationsBox.Location = new Point(
                Width - ANNOTATION_BAR_WIDTH - 1,
                Easier_Data_Editor.Constants.DEFAULT_V_SCROLL_SCROLLBAR_BUTTON_HEIGHT
            );
            this.AnnotationsBox.Visible = this._textEditorSettings.ScrollbarAnnotations;
            this.AnnotationsBox.Paint += this.OnDrawAnnotationBox;
            this.Controls.Add(this.AnnotationsBox);
        }

        protected override void OnTextChanged(EventArgs e)
        {
            if (this._blockTextChangedEvent)
                return;

            if (!this._setText)
                this.LastTextChangedDate = DateTime.Now;

            int maxLineNumberCharLength = Lines.Count.ToString().Length;
            if (maxLineNumberCharLength != this._maxLineNumberCharLength)
            {
                this._maxLineNumberCharLength = maxLineNumberCharLength;
                ChangeWidthOfNumberMargin();
            }

            base.OnTextChanged(e);
        }

        protected override void OnStyleNeeded(StyleNeededEventArgs e)
        {
            int startPos = this.GetEndStyled();
            int linePos = LineFromPosition(startPos);
            startPos = Lines[linePos].Position;

            this._lexer.Style(this, startPos, e.Position, this._textEditorSettings.Folding);
        }

        protected override void OnBeforeDelete(BeforeModificationEventArgs e)
        {
            if (e.Source == ModificationSource.User && !this._blockUndoRedoActions)
            {
                double clickDiff = (DateTime.Now - this.LastClickOnDocument).TotalSeconds;
                if (
                    (DateTime.Now - this.LastTextChangedDate).TotalSeconds
                        > UNDO_REDO_TEXT_INPUT_MINIMUM_INTERVAL_BETWEEN
                    || clickDiff >= 0.0
                        && clickDiff <= UNDO_REDO_TEXT_CLICK_MINIMUM_INTERVAL_BETWEEN
                )
                {
                    this.LastClickOnDocument = DateTime.MinValue;
                    this.EndUndoAction();
                }
            }

            base.OnBeforeDelete(e);
        }

        protected override void OnDelete(ModificationEventArgs e)
        {
            int startIndex = LineFromPosition(e.Position);
            Line line = Lines[startIndex];
            line.MarkerAdd((int)EMarkerIndices.Changes);
            SetFoldingInRange(startIndex);

            if (
                !this._blockTextChangedEvent
                && (DateTime.Now - this.LastTextChangedDate).TotalSeconds
                    > UNDO_REDO_TEXT_INPUT_MINIMUM_INTERVAL_BETWEEN
            )
                this.UndoRedoStackChanged?.Invoke(this, true);

            base.OnDelete(e);
        }

        protected override void OnInsertCheck(InsertCheckEventArgs e)
        {
            if (!this._setText)
            {
                bool hasLineBreak = Regex.IsMatch(e.Text, "[\\r\\n]$");

                if (this._textEditorSettings.Folding)
                {
                    Line line = Lines[LineFromPosition(e.Position)];
                    if (hasLineBreak && !line.Expanded)
                    {
                        line.ToggleFold();
                        IndicatorCurrent = (int)EIndicatorIndices.FoldingCollapsed;
                        IndicatorClearRange(line.Position, line.Length);
                    }
                }

                if (this._textEditorSettings.Indention && hasLineBreak)
                {
                    Line curLine = Lines[LineFromPosition(e.Position)];
                    e.Text = string.Format(
                        "{0}{1}",
                        e.Text,
                        Regex.Match(curLine.Text, "^[^\\S\\r\\n]*").Value
                    );
                }
            }

            base.OnInsertCheck(e);
        }

        protected override void OnBeforeInsert(BeforeModificationEventArgs e)
        {
            if (
                !this._setText
                && e.Source == ModificationSource.User
                && !this._blockUndoRedoActions
            )
            {
                IndicatorCurrent = (int)EIndicatorIndices.TmpHighlight;
                IndicatorClearRange(0, TextLength);

                double clickDiff = (DateTime.Now - LastClickOnDocument).TotalSeconds;
                if (
                    (DateTime.Now - this.LastTextChangedDate).TotalSeconds
                        > UNDO_REDO_TEXT_INPUT_MINIMUM_INTERVAL_BETWEEN
                    || clickDiff >= 0 && clickDiff <= UNDO_REDO_TEXT_CLICK_MINIMUM_INTERVAL_BETWEEN
                )
                {
                    this.LastClickOnDocument = DateTime.MinValue;
                    this.BeginUndoAction();
                }
            }

            base.OnBeforeInsert(e);
        }

        protected override void OnInsert(ModificationEventArgs e)
        {
            if (!this._setText)
            {
                if (this._textEditorSettings.Folding)
                {
                    Line line = Lines[LineFromPosition(e.Position)];
                    if (!line.Expanded)
                    {
                        IndicatorCurrent = (int)EIndicatorIndices.FoldingCollapsed;
                        IndicatorFillRange(e.Position, e.Text.Length);
                    }
                }

                int startIndex = LineFromPosition(e.Position);
                for (int i = startIndex; i <= startIndex + e.LinesAdded; ++i)
                    Lines[i].MarkerAdd((int)EMarkerIndices.Changes);

                SetFoldingInRange(startIndex);

                if (
                    !this._blockTextChangedEvent
                    && (DateTime.Now - this.LastTextChangedDate).TotalSeconds
                        > UNDO_REDO_TEXT_INPUT_MINIMUM_INTERVAL_BETWEEN
                )
                    this.UndoRedoStackChanged?.Invoke(this, true);
            }

            base.OnInsert(e);
        }

        protected override void OnZoomChanged(EventArgs e)
        {
            if (!this._setText)
                ChangeWidthOfNumberMargin();

            base.OnZoomChanged(e);
        }

        protected override void OnUpdateUI(UpdateUIEventArgs e)
        {
            if (!this._setText && e.Change == UpdateChange.Selection)
            {
                IndicatorCurrent = (int)EIndicatorIndices.ResultInSelection;
                IndicatorClearRange(0, TextLength);
            }

            base.OnUpdateUI(e);
        }

        protected override void OnMarginClick(MarginClickEventArgs e)
        {
            Line line = Lines[LineFromPosition(e.Position)];
            if (e.Margin == (int)EMarginIndices.Folding)
                ToggleFolding(line);
            else if (e.Margin == (int)EMarginIndices.Bookmark)
                ToggleBookmark(line);
            else
                SetSelection(line.Position, line.Position + line.Length);

            base.OnMarginClick(e);
        }

        protected override void OnMouseClick(MouseEventArgs e)
        {
            LastClickOnDocument = DateTime.Now;

            IndicatorCurrent = (int)EIndicatorIndices.TmpHighlight;
            IndicatorClearRange(0, TextLength);

            Debug.WriteLine(
                "{0} {1} {2}",
                PointXFromPosition(e.X),
                PointYFromPosition(e.Y),
                CurrentLine
            );

            base.OnMouseClick(e);
        }

        protected override void OnDoubleClick(DoubleClickEventArgs e)
        {
            Line line = Lines[CurrentLine];
            if (this._textEditorSettings.Folding && !line.Expanded)
            {
                line.ToggleFold();
                IndicatorCurrent = (int)EIndicatorIndices.FoldingCollapsed;
                IndicatorClearRange(line.Position, line.Length);

                this._lexer.Style(
                    this,
                    line.Position,
                    line.EndPosition,
                    this._textEditorSettings.Folding
                );
                return;
            }

            if (e.Position >= 0)
            {
                string selectedWord = GetWordFromPosition(e.Position);
                if (selectedWord.Trim().Length > 0)
                {
                    IndicatorCurrent = (int)EIndicatorIndices.TmpHighlight;
                    SearchFlags = SearchFlags.WholeWord;
                    TargetStart = 0;
                    TargetEnd = TextLength;

                    while (SearchInTarget(selectedWord) != -1)
                    {
                        IndicatorFillRange(TargetStart, TargetEnd - TargetStart);
                        TargetStart = TargetEnd;
                        TargetEnd = TextLength;
                    }
                }
            }
        }

        public void AddSelection(
            int caret,
            int anchor,
            int caretVirtualSpace,
            int anchorVirtualSpace
        )
        {
            this.AddSelection(caret, anchor);

            DirectMessage(
                SCI_SETSELECTIONNCARETVIRTUALSPACE,
                new IntPtr(this.Selections.Count - 1),
                new IntPtr(caretVirtualSpace)
            );
            DirectMessage(
                SCI_SETSELECTIONNANCHORVIRTUALSPACE,
                new IntPtr(this.Selections.Count - 1),
                new IntPtr(anchorVirtualSpace)
            );
        }

        public void ApplySelections(SelectionCollection selections)
        {
            this.ClearSelections();
            foreach (
                Selection sel in selections.Where(s =>
                    s.Caret != 0
                    || s.Anchor != 0
                    || s.AnchorVirtualSpace != 0
                    || s.CaretVirtualSpace != 0
                )
            )
                this.AddSelection(
                    sel.Caret,
                    sel.Anchor,
                    sel.CaretVirtualSpace,
                    sel.AnchorVirtualSpace
                );
        }

        public void GoToLine(int line)
        {
            if (line < 0)
                line = 0;
            else if (line >= Lines.Count)
                line = Lines.Count - 1;

            GotoPosition(Math.Max(0, Lines[line].Position));
        }

        public bool DeleteKey()
        {
            if (SelectedText.Length == 0)
            {
                string deleteChar = GetTextRange(CurrentPosition, 1);
                int nextChar = CurrentPosition + 1;
                if (
                    nextChar < TextLength
                    && Constants.REGEX_PATTERN_IS_NEW_LINE.IsMatch(deleteChar)
                    && Constants.REGEX_PATTERN_IS_NEW_LINE.IsMatch(GetTextRange(nextChar, 1))
                )
                    deleteChar += "\\n"; // just extend the length

                DeleteRange(CurrentPosition, deleteChar.Length);
                return true;
            }

            return false;
        }

        public void SetStartText(string text)
        {
            bool emptyBuffer = this.Text.Length == 0 && !CanUndo;
            _setText = true;
            EndUndoAction();
            this.Text = text;
            _setText = false;
            if (emptyBuffer)
                EmptyUndoBuffer();
            BeginUndoAction();
            SetFoldRangeList();
            // Folding(0, TextLength, false);
        }

        private void ChangeWidthOfNumberMargin()
        {
            if (!this._textEditorSettings.ShowLineNumbers)
                return;

            Margins[(int)EMarginIndices.LineNumber].Width = this.GetMarginWidthLineNumber();
        }

        private void NewBeginUndoAction()
        {
            // bool oldState = m_undoActionIsClosed;
            EndUndoAction();
            base.BeginUndoAction();
            _undoActionIsClosed = false;
            Debug.WriteLine("Begin Undo Action");
            // If Not oldState Then RaiseEvent UndoRedoStackChanged(Me, True)
            _startUndoByLine = CurrentLine;
        }

        private void RemoveIncorrectSyntax(
            int index,
            int startPos,
            int endPos,
            Tuple<int, int> tuple
        )
        {
            int diffStart = startPos - tuple.Item1;
            int diffEnd = tuple.Item2 - endPos;

            if (diffStart > 0)
                this.IncorrectSyntax[index] = new Tuple<int, int>(
                    tuple.Item1,
                    tuple.Item1 + diffStart
                );

            if (diffEnd > 0)
            {
                Tuple<int, int> newTuple = new Tuple<int, int>(tuple.Item2 - diffEnd, tuple.Item2);
                if (diffStart <= 0)
                    this.IncorrectSyntax[index] = newTuple;
                else
                    this.IncorrectSyntax.Insert(++index, newTuple);
            }

            if (diffStart <= 0 && diffEnd <= 0)
                this.IncorrectSyntax.RemoveAt(index);
        }
    }
}
