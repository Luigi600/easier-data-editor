﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Diagnostics;
using System.Linq;
using ScintillaNET;

namespace Easier_Data_Editor.Extensions.Scintilla
{
    public partial class Scintilla
    {
        private readonly List<FoldRange> _foldRanges = new List<FoldRange>();

        private sealed class FoldRange
        {
            public int StartLine { get; }
            public int EndLine { get; internal set; }
            public Fold.Fold Fold { get; }
            public FoldRange? Parent { get; private set; }
            public ImmutableList<FoldRange> SubRanges => this._ranges.ToImmutableList();

            private readonly List<FoldRange> _ranges = new List<FoldRange>();

            public FoldRange(int startLine, int endLine, Fold.Fold fold)
            {
                StartLine = startLine;
                EndLine = endLine;
                Fold = fold;
            }

            public void AddSubRange(FoldRange subRange)
            {
                subRange.Parent = this;
                this._ranges.Add(subRange);
            }

            public void SetSubRanges(IEnumerable<FoldRange> subRanges)
            {
                this._ranges.Clear();
                foreach (FoldRange range in subRanges)
                    this.AddSubRange(range);
            }
        }

        private sealed class FoldParent
        {
            public FoldRange? Parent { get; }
            public Fold.Fold Fold { get; }

            public FoldParent(FoldRange? parent, Fold.Fold fold)
            {
                this.Parent = parent;
                this.Fold = fold;
            }
        }

        private static List<FoldParent> GetFoldsFromFoldRange(FoldRange? rangeParent)
        {
            List<FoldParent> result = new List<FoldParent>();
            while (rangeParent != null)
            {
                result.AddRange(
                    rangeParent.Fold.SubFolds.Select(f => new FoldParent(rangeParent, f))
                );
                rangeParent = rangeParent.Parent;
            }
            result.AddRange(Constants.DOCUMENT_FOLDS.Select(f => new FoldParent(null, f)));

            return result;
        }

        private static List<Tuple<int, int>> SetFoldLineStartAndEndFromFoldRanges(
            IEnumerable<FoldRange> ranges
        )
        {
            List<Tuple<int, int>> setLineMarkersHeaders = new List<Tuple<int, int>>();
            foreach (FoldRange range in ranges)
            {
                setLineMarkersHeaders.Add(new Tuple<int, int>(range.StartLine, range.EndLine));

                List<Tuple<int, int>> subResult = SetFoldLineStartAndEndFromFoldRanges(
                    range.SubRanges
                );
                setLineMarkersHeaders.AddRange(subResult);
            }

            setLineMarkersHeaders.Sort((i1, i2) => i1.Item1 - i2.Item1);
            return setLineMarkersHeaders;
        }

        private static FoldRange? GetNewFoldRangeFromPossibleFolds(
            Line line,
            FoldRange? rangeParent
        )
        {
            List<FoldParent> subFoldsTest = GetFoldsFromFoldRange(rangeParent);
            if (subFoldsTest.Find(s => s.Fold.Start.IsMatch(line.Text)) is FoldParent fp)
            {
                FoldRange newRange = new FoldRange(line.Index, -1, fp.Fold);
                if (fp.Parent != null)
                    fp.Parent.AddSubRange(newRange);
                return newRange;
            }

            return null;
        }

        public void ToggleFolding(Line line)
        {
            if (line.FoldLevelFlags == FoldLevelFlags.Header)
            {
                if (!line.Expanded)
                {
                    IndicatorCurrent = (int)EIndicatorIndices.FoldingCollapsed;
                    IndicatorClearRange(line.Position, line.Length);

                    this._lexer.Style(
                        this,
                        line.Position,
                        line.EndPosition,
                        this._textEditorSettings.Folding
                    );
                }
                else
                {
                    IndicatorCurrent = (int)EIndicatorIndices.FoldingCollapsed;
                    string lineStr = line.Text.Trim();
                    IndicatorFillRange(
                        line.Position + (line.Length - line.Text.TrimStart().Length),
                        lineStr.Length
                    );

                    StartStyling(line.Position);
                    SetStyling(
                        line.EndPosition - line.Position,
                        (int)StylesIndices.FolderCollapsed
                    );
                }
                line.ToggleFold();
            }
        }

        private void SetFoldRangeList() => SetFoldingInRange(0);

        private void SetFoldingInRange(int lineStart)
        {
            if (!this._textEditorSettings.Folding || SuppressFoldingCheck)
                return;

            this.SetFoldRangeListFromLine(Lines[lineStart]);
            this.SetFoldFromRanges();

            List<FoldRange> foldStack = this._foldRanges.Where(f => f.EndLine < 0).ToList();
            if (foldStack.Count > 0)
                Debug.WriteLine(
                    "Fold stack is not empty (length {0}), that means that the data contains an invalid block (start or stop is missing)",
                    foldStack.Count
                );
        }

        private void SetFoldRangeListFromLine(Line lineStart)
        {
            Line? currentLine = lineStart;
            List<FoldRange> result = new List<FoldRange>();
            // add all valid fold items from previous run, which is BEFORE the area to be inspected
            result.AddRange(
                this._foldRanges.Where(f =>
                    f.EndLine >= 0 && f.EndLine < lineStart.Index && f.StartLine != lineStart.Index
                )
            );

            List<FoldRange> foldStack = this.RestoreFoldRangeFromLastRanges(lineStart);
            if (foldStack.Count > 0)
                currentLine = Lines[foldStack[foldStack.Count - 1].StartLine + 1];

            while (currentLine != null)
            {
                Line line = currentLine;
                currentLine = line.Index + 1 < Lines.Count ? Lines[line.Index + 1] : null;

                FoldRange? rangeParent = foldStack.Count > 0 ? foldStack[^1] : null;

                if (rangeParent != null && rangeParent.Fold.End.IsMatch(line.Text))
                {
                    rangeParent.EndLine = line.Index;
                    foldStack.Remove(rangeParent);
                    if (rangeParent.Parent == null)
                        result.Add(rangeParent);
                    continue;
                }

                if (GetNewFoldRangeFromPossibleFolds(line, rangeParent) is FoldRange newRange)
                    foldStack.Add(newRange);
            }

            this._foldRanges.Clear();

            result.AddRange(foldStack);
            result.Sort((i1, i2) => i1.StartLine - i2.StartLine);

            this._foldRanges.AddRange(result);
        }

        private List<FoldRange> RestoreFoldRangeFromLastRanges(Line lineStart)
        {
            HashSet<FoldRange> foldStack = new HashSet<FoldRange>();
            // add "foldStack" from previous run
            List<FoldRange> ranges = this
                ._foldRanges.Where(f => f.StartLine < lineStart.Index && f.EndLine < 0)
                .ToList();
            ranges.AddRange(
                this._foldRanges.Where(f =>
                    lineStart.Index > f.StartLine && lineStart.Index < f.EndLine
                )
            );
            while (ranges.Count > 0)
            {
                FoldRange rng = ranges[0];

                rng.SetSubRanges(
                    rng.SubRanges.Where(r => r.StartLine <= lineStart.Index && r.EndLine < 0)
                );

                foldStack.Add(rng);
                ranges.AddRange(rng.SubRanges);
                ranges.RemoveAt(0);
            }

            return foldStack.OrderBy(item => item.StartLine).ToList();
        }

#pragma warning disable S3776 // Cognitive Complexity of methods should not be too high
        private void SetFoldFromRanges()
        {
            List<Tuple<int, int>> foldResult = SetFoldLineStartAndEndFromFoldRanges(
                this._foldRanges
            );
            int foldResultIndex = 0;
            List<Tuple<int, int>> foldStack = new List<Tuple<int, int>>();
            for (int i = 0; i < Lines.Count - 1; ++i)
            {
                Line line = Lines[i];
                if (foldStack.Count > 0)
                {
                    line.FoldLevel = 1024 + foldStack.Count;

                    Tuple<int, int> lastItem = foldStack[foldStack.Count - 1];
                    if (line.Index >= lastItem.Item2) // don't include endline coz endline must have the same foldLevel and endline+1 doesn't have the same foldlevel
                    {
                        if (
                            (line.FoldLevelFlags & FoldLevelFlags.Header) == FoldLevelFlags.Header
                            && !line.Expanded
                        )
                            ToggleFolding(line);
                        foldStack.Remove(lastItem);
                        continue;
                    }
                }

                if (foldResultIndex < foldResult.Count - 1)
                {
                    Tuple<int, int> nextItem = foldResult[foldResultIndex];
                    if (line.Index >= nextItem.Item1)
                    {
                        ++foldResultIndex;
                        // should not be happen
                        if (line.Index >= nextItem.Item2)
                        {
                            --i;
                            continue;
                        }

                        line.FoldLevelFlags = FoldLevelFlags.Header;
                        line.FoldLevel = 1024 + foldStack.Count;
                        foldStack.Add(nextItem);
                        continue;
                    }
                }

                if (!line.Expanded)
                    ToggleFolding(line);

                line.FoldLevelFlags = 0;
                line.FoldLevel = 1024 + foldStack.Count;
            }
        }
#pragma warning restore S3776 // Cognitive Complexity of methods should not be too high
    }
}
