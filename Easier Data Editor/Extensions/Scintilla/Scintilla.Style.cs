﻿using System;
using System.Collections.Generic;
using System.Drawing;
using Easier_Data_Editor.Extensions.Scintilla.SyntaxHighlighting;
using ScintillaNET;

namespace Easier_Data_Editor.Extensions.Scintilla
{
    public partial class Scintilla
    {
        private void SetFirstStyle()
        {
            this.SetGeneralStyle();

            // SetFoldMarginColor(True, Color.White)

            // margins
            this.SetMarginLineNumber();
            this.SetMarginTextChangesSpacing();
            this.SetMarginTextChanges();
            this.SetMarginBookmarkSpacing();
            this.SetMarginBookmark();
            this.SetMarkerBookmark();
            this.SetMarginFolding();

            // markers
            this.SetMarkerSaveStateSaved();
            this.SetMarkerSaveStateChanged();
            this.SetMarkerSymbols();
            this.SetMarkerFoldingColors();

            // Indicators
            this.SetIndicatorError();
            this.SetIndicatorUnknown();
            this.SetIndicatorResultInSelection();
            this.SetIndicatorHighlight();
            this.SetIndicatorTmpHighlight();
            this.SetIndicatorFoldingCollapsed();

            IntPtr col = DirectMessage(
                Constants.SCI_GETELEMENTCOLOUR,
                new IntPtr(Constants.SC_ELEMENT_SELECTION_BACK),
                new IntPtr(0)
            );
            DirectMessage(
                Constants.SCI_SETELEMENTCOLOUR,
                new IntPtr(Constants.SC_ELEMENT_SELECTION_INACTIVE_BACK),
                col
            );
        }

        public void ReloadStyle()
        {
            this.HandleFoldingState();
            this.SetStyleDefault();
            this.SetStyleFolderCollapsed();
            this.SetStyleLineNumber();

            this.SetGeneralConfiguration();

            this.SetStyleCaretLine();
            this.SetStyleMarginLineNumber();
            this.SetStyleMarginTextChanges();
            this.SetStyleMarginBookmark();
            this.SetStyleMarginFolding();

            // set styles
            this.SetSyntaxStyles();
        }

        private void SetGeneralStyle()
        {
            Margins.Left = 5;
            Margins.Capacity = 10;
            AutomaticFold = AutomaticFold.None;

            // so scrollbar size...
            ScrollWidth = 10;
            ScrollWidthTracking = true;

            // nice selection feature
            MultipleSelection = true;
            MouseSelectionRectangularSwitch = true;
            AdditionalSelectionTyping = true;
            VirtualSpaceOptions = VirtualSpace.RectangularSelection;

            // whitespace
            WhitespaceSize = 3;
            WhitespaceTextColor = Color.LightGray;
        }

        private void SetMarginLineNumber()
        {
            Margin margin = Margins[(int)EMarginIndices.LineNumber];
            margin.Type = MarginType.Number;
            margin.Sensitive = false;
            margin.Mask = 0;
            margin.Width = 0;
        }

        private void SetMarginTextChangesSpacing()
        {
            // --- spacing between line number and "changes bar"
            Margin margin = Margins[(int)EMarginIndices.TextChangesSpacing];
            margin.Type = MarginType.Symbol;
            margin.Mask = 0;
            margin.Width = 0;
        }

        private void SetMarginTextChanges()
        {
            // --- changes (shows orange/green bar)
            Margin margin = Margins[(int)EMarginIndices.TextChanges];
            margin.Type = MarginType.Symbol;
            margin.Mask =
                (uint)1 << (int)EMarkerIndices.Saved | (uint)1 << (int)EMarkerIndices.Changes;
            margin.Width = 0;
        }

        private void SetMarkerSaveStateSaved()
        {
            Marker marker = Markers[(int)EMarkerIndices.Saved];
            marker.Symbol = MarkerSymbol.FullRect;
            marker.SetBackColor(Constants.COLOR_MARKER_SAVED);
        }

        private void SetMarkerSaveStateChanged()
        {
            Marker marker = Markers[(int)EMarkerIndices.Changes];
            marker.Symbol = MarkerSymbol.FullRect;
            marker.SetBackColor(Constants.COLOR_MARKER_CHANGES);
        }

        private void SetMarginBookmarkSpacing()
        {
            // --- spacing between "change bar" and bookmark
            Margin margin = Margins[(int)EMarginIndices.BookmarkSpacing];
            margin.Type = MarginType.Symbol;
            margin.Mask = 0;
            margin.Width = 0;
        }

        private void SetMarginBookmark()
        {
            Margin margin = Margins[(int)EMarginIndices.Bookmark];
            margin.Type = MarginType.Symbol;
            margin.Sensitive = true;
            margin.Mask = (uint)1 << (int)EMarkerIndices.Bookmark;
            margin.Width = 0;
        }

        private void SetMarkerBookmark()
        {
            Marker marker = Markers[(int)EMarkerIndices.Bookmark];
            marker.Symbol = Constants.MARKER_BOOKMARK;
            marker.SetBackColor(Constants.COLOR_MARKER_BOOKMARK);
            marker.SetForeColor(Color.Black);
        }

        private void SetMarginFolding()
        {
            Margin margin = Margins[(int)EMarginIndices.Folding];
            margin.Type = MarginType.Symbol;
            margin.Sensitive = true;
            margin.Mask = Marker.MaskFolders;
            margin.Width = 0;
            SetFoldMarginColor(true, Color.FromArgb(233, 233, 233));
        }

        private void SetMarkerSymbols()
        {
            Markers[Marker.FolderEnd].Symbol = MarkerSymbol.BoxPlus;
            Markers[Marker.FolderOpenMid].Symbol = MarkerSymbol.BoxMinus;
            Markers[Marker.FolderOpen].Symbol = MarkerSymbol.BoxMinus;
            Markers[Marker.FolderMidTail].Symbol = MarkerSymbol.TCorner;
            Markers[Marker.FolderTail].Symbol = MarkerSymbol.LCorner;
            Markers[Marker.FolderSub].Symbol = MarkerSymbol.VLine;
            Markers[Marker.Folder].Symbol = MarkerSymbol.BoxPlus;
        }

        private void SetIndicatorError()
        {
            Indicator indicator = Indicators[(int)EIndicatorIndices.Error];
            indicator.Style = IndicatorStyle.Squiggle; // StraightBox
            indicator.Under = true;
            indicator.ForeColor = Color.Orange;
            indicator.Alpha = 255;
        }

        private void SetIndicatorUnknown()
        {
            Indicator indicator = Indicators[(int)EIndicatorIndices.SyntaxUnknown];
            indicator.Style = IndicatorStyle.Squiggle; // StraightBox
            indicator.Under = true;
            indicator.ForeColor = Color.Red;
        }

        private void SetIndicatorResultInSelection()
        {
            Indicator indicator = Indicators[(int)EIndicatorIndices.ResultInSelection];
            indicator.Style = IndicatorStyle.StraightBox;
            indicator.Under = false;
            indicator.ForeColor = Color.Blue;
            indicator.Alpha = 50;
        }

        private void SetIndicatorHighlight()
        {
            Indicator indicator = Indicators[(int)EIndicatorIndices.Highlight];
            indicator.Style = IndicatorStyle.StraightBox;
            indicator.Under = false;
            indicator.ForeColor = Color.Red;
            indicator.Alpha = 50;
        }

        private void SetIndicatorTmpHighlight()
        {
            Indicator indicator = Indicators[(int)EIndicatorIndices.TmpHighlight];
            indicator.Style = IndicatorStyle.StraightBox;
            indicator.Under = false;
            indicator.ForeColor = Color.Lime; // Color.FromArgb(155, 255, 155)
            // .HoverForeColor = Color.Black
            indicator.Alpha = 120;
        }

        private void SetIndicatorFoldingCollapsed()
        {
            Indicator indicator = Indicators[(int)EIndicatorIndices.FoldingCollapsed];
            indicator.Style = IndicatorStyle.FullBox;
            indicator.Under = false;
            indicator.ForeColor = Color.Green;
            indicator.HoverForeColor = Color.Blue;
            indicator.OutlineAlpha = 100;
            indicator.Alpha = 0;
        }

        private void SetMarkerFoldingColors()
        {
            for (int i = Marker.FolderEnd; i <= Marker.FolderOpen; ++i)
            {
                Markers[i].SetBackColor(Color.DarkGray);
                Markers[i].SetForeColor(Color.White);
            }
        }

        private void SetGeneralConfiguration()
        {
            WrapMode = this._textEditorSettings.WrapMode;
            ViewWhitespace = this._textEditorSettings.ShowWhitespaces
                ? WhitespaceMode.VisibleAlways
                : WhitespaceMode.Invisible;

            Styles[(int)StylesIndices.LineNumber].Visible =
                this._textEditorSettings.ShowLineNumbers;
        }

        private void HandleFoldingState()
        {
            if (!this._textEditorSettings.Folding)
                FoldAll(FoldAction.Expand);
            else if (
                !_lastFoldingState
                || this._textEditorSettings.Subfolding && !_lastSubfoldingState
            )
                SetFoldingInRange(0);

            _lastFoldingState = this._textEditorSettings.Folding;
            _lastSubfoldingState = this._textEditorSettings.Subfolding;
        }

        private void SetStyleDefault()
        {
            StyleResetDefault();
            Style style = Styles[(int)StylesIndices.Default];
            style.Font = this._textEditorSettings.FontFamily;
            style.Size = (int)this._textEditorSettings.FontSize;

            style.BackColor = this._syntax.DefaultBackColor;
            style.ForeColor = this._syntax.DefaultColor;
            StyleClearAll();
        }

        private void SetStyleFolderCollapsed()
        {
            Styles[(int)StylesIndices.FolderCollapsed].ForeColor = Color.DimGray;
            // Styles[(int)Style.FoldDisplayText].ForeColor = Color.Pink
        }

        private void SetStyleLineNumber()
        {
            Style style = Styles[(int)StylesIndices.LineNumber];
            style.BackColor = Color.FromArgb(228, 228, 228);
            style.ForeColor = Color.FromArgb(128, 128, 128);
        }

        private void SetStyleCaretLine()
        {
            CaretLineBackColor = this._textEditorSettings.MarkCurrentLine
                ? this._textEditorSettings.MarkLineColor
                : Color.FromArgb(0, this._textEditorSettings.MarkLineColor);
        }

        private void SetStyleMarginLineNumber()
        {
            if (this._textEditorSettings.ShowLineNumbers)
                Margins[(int)EMarginIndices.LineNumber].Width = this.GetMarginWidthLineNumber();
            else
                Margins[(int)EMarginIndices.LineNumber].Width = 0;
        }

        private void SetStyleMarginTextChanges()
        {
            if (this._textEditorSettings.ShowLineNumbers)
                Margins[(int)EMarginIndices.TextChangesSpacing].Width = 3;
            else
                Margins[(int)EMarginIndices.TextChangesSpacing].Width = 0;

            if (this._textEditorSettings.ShowChanges)
            {
                Margins[(int)EMarginIndices.TextChanges].Width = 4;
                Markers[(int)EMarkerIndices.Changes].Symbol = MarkerSymbol.FullRect;
                Markers[(int)EMarkerIndices.Saved].Symbol = MarkerSymbol.FullRect;
            }
            else
            {
                Margins[(int)EMarginIndices.TextChanges].Width = 0;
                Markers[(int)EMarkerIndices.Changes].Symbol = MarkerSymbol.Empty;
                Markers[(int)EMarkerIndices.Saved].Symbol = MarkerSymbol.Empty;
            }
        }

        private void SetStyleMarginBookmark()
        {
            // spacing between bookmarks and "change bar"
            if (
                this._textEditorSettings.Bookmarks
                && (
                    this._textEditorSettings.ShowChanges
                    || (
                        this._textEditorSettings.ShowLineNumbers
                        && !this._textEditorSettings.ShowChanges
                    )
                )
            )
                Margins[(int)EMarginIndices.BookmarkSpacing].Width = 2;
            else
                Margins[(int)EMarginIndices.BookmarkSpacing].Width = 0;

            // bookmarks
            if (this._textEditorSettings.Bookmarks)
            {
                Margins[(int)EMarginIndices.Bookmark].Width = 16;
                Markers[(int)EMarkerIndices.Bookmark].Symbol = Constants.MARKER_BOOKMARK;
            }
            else
            {
                Margins[(int)EMarginIndices.Bookmark].Width = 0;
                Markers[(int)EMarkerIndices.Bookmark].Symbol = MarkerSymbol.Empty;
            }
        }

        private void SetStyleMarginFolding()
        {
            if (this._textEditorSettings.Folding)
                Margins[(int)EMarginIndices.Folding].Width = 15;
            else
                Margins[(int)EMarginIndices.Folding].Width = 0;
        }

        private void SetSyntaxStyles()
        {
            byte styleCounter = 0;
            List<Item> items = new List<Item>();
            items.AddRange(this._syntax.RegexSpecials);
            items.AddRange(this._syntax.RegexSingleWord);
            items.AddRange(this._syntax.KeywordLists);
            foreach (Item item in items)
            {
                item.StyleIndex = styleCounter;

                if (!item.BackColor.Equals(Color.Transparent))
                    Styles[styleCounter].BackColor = item.BackColor;

                if (!item.ForeColor.Equals(Color.Transparent))
                    Styles[styleCounter].ForeColor = item.ForeColor;

                Styles[styleCounter].Bold = item.IsBold;
                Styles[styleCounter].Italic = item.IsItalic;

                styleCounter = (byte)(item.StyleIndex + 1); // do that, because perhaps its +2 coz the number exists already in the style enum
            }

            items.Clear();
        }

        private int GetMarginWidthLineNumber() =>
            TextWidth(Style.LineNumber, new string('9', _maxLineNumberCharLength + 1))
            + Constants.PADDING_LINE_NUMBER;
    }
}
