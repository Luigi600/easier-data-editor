﻿using System.Text.RegularExpressions;
using Easier_Data_Editor.Environments.Settings;
using Easier_Data_Editor.Extensions.Scintilla.SyntaxHighlighting;

namespace Easier_Data_Editor.Extensions.Scintilla
{
    public class LF2Scintilla : Scintilla
    {
        public LF2Scintilla() { }

        public LF2Scintilla(Syntax syntax, TextEditor textEditorSettings)
            : base(syntax, textEditorSettings) { }

        public int GetOffsetFromFrame(int frameID)
        {
            Match mtch = Regex.Match(
                Text,
                $"<frame>\\s*?({frameID}$|{frameID}\\s+)",
                RegexOptions.IgnoreCase
            );
            if (mtch.Success)
                return mtch.Index;

            return -1;
        }

        public bool GoToFrame(int frameID)
        {
            int frameOffset = GetOffsetFromFrame(frameID);
            if (frameOffset < 0)
                return false;

            GotoPosition(frameOffset);
            return true;
        }
    }
}
