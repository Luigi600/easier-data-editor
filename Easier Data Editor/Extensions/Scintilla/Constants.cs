﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Drawing;
using System.Text;
using System.Text.RegularExpressions;
using ScintillaNET;

namespace Easier_Data_Editor.Extensions.Scintilla
{
    public static class Constants
    {
        public const int SCI_SETELEMENTCOLOUR = 2753;
        public const int SCI_GETELEMENTCOLOUR = 2754;

        public const int SC_ELEMENT_SELECTION_BACK = 11;
        public const int SC_ELEMENT_SELECTION_INACTIVE_BACK = 17;

        public static readonly Fold.Fold[] DOCUMENT_FOLDS = new Fold.Fold[]
        {
            new Fold.Fold(
                new Regex(@"\s*<frame>[^\n\r]*?"),
                new Regex(@"\s*<frame_end>[^\n\r]*?"),
                new Fold.Fold[]
                {
                    new Fold.Fold(new Regex(@"\s*bdy:"), new Regex(@"\s*bdy_end:")),
                    new Fold.Fold(new Regex(@"\s*itr:"), new Regex(@"\s*itr_end:")),
                    new Fold.Fold(new Regex(@"\s*bpoint:"), new Regex(@"\s*bpoint_end:")),
                    new Fold.Fold(new Regex(@"\s*cpoint:"), new Regex(@"\s*cpoint_end:")),
                    new Fold.Fold(new Regex(@"\s*opoint:"), new Regex(@"\s*opoint_end:")),
                    new Fold.Fold(new Regex(@"\s*wpoint:"), new Regex(@"\s*wpoint_end:")),
                }
            ),
            new Fold.Fold(
                new Regex(@"\s*<bmp_begin>[^\n\r]*?"),
                new Regex(@"\s*<bmp_end>[^\n\r]*?")
            ),
            new Fold.Fold(new Regex(@"\s*layer:[^\n\r]*?"), new Regex(@"\s*layer_end[^\n\r]*?")),
            new Fold.Fold(
                new Regex(@"\s*<stage>[^\n\r]*?"),
                new Regex(@"\s*<stage_end>[^\n\r]*?"),
                new Fold.Fold[]
                {
                    new Fold.Fold(new Regex(@"\s*<phase>"), new Regex(@"\s*<phase_end>")),
                }
            ),
        };

        private static readonly ImmutableArray<Tuple<string, string>> FOLDING_TAGS = new List<
            Tuple<string, string>
        >()
        {
            new Tuple<string, string>("<frame>", "<frame_end>"),
            new Tuple<string, string>("<bmp_begin>", "<bmp_end>"),
            new Tuple<string, string>("layer:", "layer_end"),
            new Tuple<string, string>("<stage>", "<stage_end>"),
        }.ToImmutableArray();

        private static readonly ImmutableArray<Tuple<string, string>> FOLDING_TAGS_TEST = new List<
            Tuple<string, string>
        >()
        {
            new Tuple<string, string>("bdy:", "bdy_end:"),
            new Tuple<string, string>("itr:", "itr_end:"),
            new Tuple<string, string>("bpoint:", "bpoint_end:"),
            new Tuple<string, string>("cpoint:", "cpoint_end:"),
            new Tuple<string, string>("opoint:", "opoint_end:"),
            new Tuple<string, string>("wpoint:", "wpoint_end:"),
            new Tuple<string, string>("<phase>", "<phase_end>"),
        }.ToImmutableArray();

        public static readonly string PATTERN_FOLDING_TAGS = GetPatternStringFromTupleList(
            FOLDING_TAGS
        );
        public static readonly string PATTERN_FOLDING_TAGS_TEST = GetPatternStringFromTupleList(
            FOLDING_TAGS_TEST
        );

        public const int PADDING_LINE_NUMBER = 5;
        public const MarkerSymbol MARKER_BOOKMARK = MarkerSymbol.Circle;
        public const int SCI_SETCARETLINEVISIBLEALWAYS = 2655;

        public static readonly Color COLOR_MARKER_SAVED = Color.FromArgb(108, 226, 108);
        public static readonly Color COLOR_MARKER_CHANGES = Color.FromArgb(255, 238, 98);
        public static readonly Color COLOR_MARKER_BOOKMARK = Color.DeepSkyBlue;

        public static readonly Regex REGEX_PATTERN_IS_NEW_LINE = new Regex("[\\n\\r]");

        static Constants() { }

        private static string GetPatternStringFromTupleList(
            ImmutableArray<Tuple<string, string>> tuples
        )
        {
            StringBuilder patternBuilder = new StringBuilder();
            foreach (Tuple<string, string> tuple in tuples)
            {
                if (patternBuilder.Length > 0)
                    patternBuilder.Append('|');

                patternBuilder.AppendFormat("({0}((?!{0}).)*{1})", tuple.Item1, tuple.Item2);
            }

            return patternBuilder.ToString();
        }
    }
}
