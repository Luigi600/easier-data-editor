﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Drawing;
using System.Windows.Forms;
using ScintillaNET;

namespace Easier_Data_Editor.Extensions.Scintilla
{
    public partial class Scintilla
    {
        private const uint ANNOTATION_MARKERS =
            1 << (int)EMarkerIndices.Bookmark
            | 1 << (int)EMarkerIndices.Changes
            | 1 << (int)EMarkerIndices.Saved;
        private static readonly SolidBrush COLOR_MARKER_SAVED = new SolidBrush(
            Color.FromArgb(108, 226, 108)
        );
        private static readonly SolidBrush COLOR_MARKER_CHANGES = new SolidBrush(
            Color.FromArgb(251, 182, 20)
        );
        private static readonly SolidBrush COLOR_MARKER_BOOKMARK = new SolidBrush(
            Color.DeepSkyBlue
        );

        private static readonly Brush COLOR_CURRENT_CURSOR = Brushes.DarkBlue;

        private const int BOOKMARK_MASK = 1 << (int)EMarkerIndices.Bookmark;
        private const int SAVED_MASK = 1 << (int)EMarkerIndices.Saved;
        private const int CHANGES_MASK = 1 << (int)EMarkerIndices.Changes;

        private const int ANNOTATION_BAR_WIDTH = 12;

        private const float ANNOTATION_LARGE_POSITION_X = 3.0f;
        private const float ANNOTATION_SMALL_POSITION_X = 6.0f;
        private const float ANNOTATION_SIZE = 3.0f;
        private const int ANNOTATION_MINIMUM_LINE_SIZE = 4;
        private const float ANNOTATION_CURRENT_LINE_SIZE = 2;

        private readonly Dictionary<int, uint> _annotations = new Dictionary<int, uint>();

        public ImmutableDictionary<int, uint> Annotations =>
            this._annotations.ToImmutableDictionary();

        public void SetAnnotations()
        {
            this.ClearAnnotations();

            int lineIndex = 0;
            int lastLineIndex = -1;
            // line 0 special case...
            if ((Lines[0].MarkerGet() & ANNOTATION_MARKERS) == 0)
            {
                lastLineIndex = 0;
                lineIndex = Lines[0].MarkerNext(ANNOTATION_MARKERS);
            }

            while (lineIndex >= 0 && lastLineIndex != lineIndex)
            {
                lastLineIndex = lineIndex;
                lineIndex = Lines[lastLineIndex + 1].MarkerNext(ANNOTATION_MARKERS);

                Line line = Lines[lastLineIndex];
                while (!line.Visible && line.FoldParent >= 0)
                    line = Lines[line.FoldParent];

                while (!line.Visible && line.Index > 0)
                    line = Lines[line.Index - 1];

                if (this._annotations.ContainsKey(line.Index))
                    this._annotations[line.Index] |= line.MarkerGet();
                else
                    this._annotations.Add(line.Index, line.MarkerGet());
            }
        }

        public void ClearAnnotations()
        {
            this._annotations.Clear();
        }

        public void DrawAnnotations() => this.AnnotationsBox.Invalidate();

        private void OnDrawAnnotationBox(object? sender, PaintEventArgs e)
        {
            int lineCounter = this.GetVisibleLines().Length;
            float factor = (float)this.AnnotationsBox.Height / lineCounter;
            int lineSize = Math.Max(ANNOTATION_MINIMUM_LINE_SIZE, (int)Math.Ceiling(factor));

            foreach (KeyValuePair<int, uint> entry in this.Annotations)
            {
                float y = factor * entry.Key - lineSize / 2.0f;

                if ((entry.Value & BOOKMARK_MASK) == BOOKMARK_MASK)
                    e.Graphics.FillRectangle(
                        COLOR_MARKER_BOOKMARK,
                        new RectangleF(ANNOTATION_LARGE_POSITION_X, y, ANNOTATION_SIZE, lineSize)
                    );

                if ((entry.Value & SAVED_MASK) == SAVED_MASK)
                    e.Graphics.FillRectangle(
                        COLOR_MARKER_SAVED,
                        new RectangleF(ANNOTATION_SMALL_POSITION_X, y, ANNOTATION_SIZE, lineSize)
                    );
                else if ((entry.Value & CHANGES_MASK) == CHANGES_MASK)
                    e.Graphics.FillRectangle(
                        COLOR_MARKER_CHANGES,
                        new RectangleF(ANNOTATION_SMALL_POSITION_X, y, ANNOTATION_SIZE, lineSize)
                    );
            }

            e.Graphics.FillRectangle(
                COLOR_CURRENT_CURSOR,
                new RectangleF(
                    0,
                    factor * (this.CurrentLine + 1) - (ANNOTATION_CURRENT_LINE_SIZE / 2.0f),
                    e.ClipRectangle.Width,
                    ANNOTATION_CURRENT_LINE_SIZE
                )
            );
        }
    }
}
