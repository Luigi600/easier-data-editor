﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Text.RegularExpressions;

namespace Easier_Data_Editor.Extensions.Scintilla.Fold
{
    public class Fold
    {
        public virtual Regex Start { get; }
        public virtual Regex End { get; }
        public virtual ImmutableList<Fold> SubFolds => _subFolds.ToImmutableList();
        public virtual Fold? Parent => this._parent;

        private readonly List<Fold> _subFolds = new List<Fold>();
        private Fold? _parent;

        public Fold(Regex start, Regex end)
        {
            this.Start = start;
            this.End = end;
        }

        public Fold(Regex start, Regex end, params Tuple<Regex, Regex>[] subFolds)
            : this(start, end)
        {
            foreach (Tuple<Regex, Regex> subFold in subFolds)
                this.AddSubFold(subFold.Item1, subFold.Item2);
        }

        public Fold(Regex start, Regex end, params Fold[] subFolds)
            : this(start, end)
        {
            foreach (Fold subFold in subFolds)
                this.AddSubFold(subFold);
        }

        public Fold(Regex start, Regex end, IEnumerable<Fold> subFolds)
            : this(start, end)
        {
            this._subFolds.AddRange(subFolds);
        }

        public void AddSubFold(Regex start, Regex end) => this.AddSubFold(new Fold(start, end));

        public void AddSubFold(Fold fold)
        {
            fold._parent = this;
            this._subFolds.Add(fold);
        }
    }
}
