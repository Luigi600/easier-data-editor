﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ScintillaNET;

namespace Easier_Data_Editor.Extensions.Scintilla
{
    public partial class Scintilla
    {
        public static void ToggleBookmark(Line line)
        {
            if (LineHasMarker(line, EMarkerIndices.Bookmark))
                line.MarkerDelete((int)EMarkerIndices.Bookmark);
            else
                line.MarkerAdd((int)EMarkerIndices.Bookmark);
        }

        public int[] GetBookmarkedLinesIndices() =>
            GetBookmarkedLines().Select(x => x.Index).ToArray();

        public void GoToNextBookmark()
        {
            for (int i = CurrentLine + 1; i <= Lines.Count + CurrentLine; i++)
            {
                int indexLine = i % Lines.Count;
                Line line = Lines[indexLine];
                if (LineHasMarker(line, EMarkerIndices.Bookmark))
                {
                    line.Goto();
                    break;
                }
            }
        }

        public void GoToPreviousBookmark()
        {
            for (int i = 1; i <= Lines.Count; i++)
            {
                int indexLine = (CurrentLine - i) % Lines.Count;
                if (indexLine < 0)
                    indexLine += Lines.Count;
                Line line = Lines[indexLine];
                if (LineHasMarker(line, EMarkerIndices.Bookmark))
                {
                    line.Goto();
                    break;
                }
            }
        }

        public void CutBookmarkedLines()
        {
            StringBuilder lines = new StringBuilder();
            List<Line> markedLines = GetBookmarkedLines();
            if (markedLines.Count > 0)
            {
                BeginUndoAction();
                for (int i = markedLines.Count - 1; i >= 0; i += -1)
                {
                    Line line = markedLines[i];
                    lines.Insert(0, line.Text);
                    line.MarkerDelete((int)EMarkerIndices.Bookmark);
                    DeleteRange(line.Position, line.Length);
                }
                EndUndoAction();
            }

            Clipboard.SetText(lines.ToString());
        }

        public void CopyBookmarkedLines()
        {
            StringBuilder lines = new StringBuilder();
            foreach (Line line in GetBookmarkedLines())
                lines.Append(line.Text);

            Clipboard.SetText(lines.ToString());
        }

        public void PasteToBookmarkedLines(string replaceTo)
        {
            List<Line> markedLines = GetBookmarkedLines();
            for (int i = markedLines.Count - 1; i >= 0; i += -1)
            {
                Line line = markedLines[i];
                TargetStart = line.Position;
                TargetEnd = line.EndPosition;
                ReplaceTarget(replaceTo);
            }
        }

        public void RemoveBookmarkedLines()
        {
            List<Line> markedLines = GetBookmarkedLines();
            if (markedLines.Count > 0)
            {
                BeginUndoAction();
                for (int i = markedLines.Count - 1; i >= 0; i += -1)
                {
                    Line line = markedLines[i];
                    line.MarkerDelete((int)EMarkerIndices.Bookmark);
                    DeleteRange(line.Position, line.Length);
                }
                EndUndoAction();
            }
        }

        public void RemoveUnmarkedLines()
        {
            BeginUndoAction();
            List<int> unmarked = new List<int>();
            for (int i = 0; i <= Lines.Count - 1; i++)
                unmarked.Add(i);
            unmarked = unmarked.Except(GetBookmarkedLinesIndices()).ToList();
            for (int i = unmarked.Count - 1; i >= 0; i += -1)
            {
                Line line = Lines[unmarked[i]];
                DeleteRange(line.Position, line.Length);
            }
            EndUndoAction();
        }

        public void InvertBookmarks()
        {
            foreach (Line line in Lines)
                ToggleBookmark(line);
        }

        private List<Line> GetBookmarkedLines() =>
            Lines.Where(l => LineHasMarker(l, EMarkerIndices.Bookmark)).ToList();
    }
}
