﻿using System.Collections.Generic;
using System.Collections.Immutable;
using System.Drawing;

namespace Easier_Data_Editor.Extensions.Scintilla.SyntaxHighlighting
{
    public class ItemKeywordList : Item
    {
        private readonly HashSet<string> _keywords = new HashSet<string>();

        public virtual ImmutableHashSet<string> Keywords => this._keywords.ToImmutableHashSet();

        public ItemKeywordList(
            Color? foreColor = null,
            Color? backColor = null,
            bool isBold = false,
            bool isItalic = false
        )
            : base(foreColor, backColor, isBold, isItalic) { }

        public bool AddKeyword(string keyword) => this._keywords.Add(keyword);
    }
}
