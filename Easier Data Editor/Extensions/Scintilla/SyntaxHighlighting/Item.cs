﻿using System;
using System.Drawing;

namespace Easier_Data_Editor.Extensions.Scintilla.SyntaxHighlighting
{
    public abstract class Item
    {
        public virtual Color ForeColor { get; } = Color.Black;
        public virtual Color BackColor { get; } = Color.Transparent;

        public virtual bool IsBold { get; } = false;
        public virtual bool IsItalic { get; } = false;

        private byte m_styleIndex = 0;
        public byte StyleIndex
        {
            get { return m_styleIndex; }
            set
            {
                while (Enum.IsDefined(typeof(StylesIndices), value))
                    value += 1;
                m_styleIndex = value;
            }
        }

        protected Item(
            Color? foreColor = null,
            Color? backColor = null,
            bool isBold = false,
            bool isItalic = false
        )
        {
            if (foreColor != null)
                this.ForeColor = (Color)foreColor;

            if (backColor != null)
                this.BackColor = (Color)backColor;

            this.IsBold = isBold;
            this.IsItalic = isItalic;
        }
    }
}
