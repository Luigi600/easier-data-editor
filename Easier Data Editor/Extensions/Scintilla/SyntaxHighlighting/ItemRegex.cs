﻿using System.Drawing;

namespace Easier_Data_Editor.Extensions.Scintilla.SyntaxHighlighting
{
    public class ItemRegex : Item
    {
        public virtual string Name { get; }
        public virtual string Pattern { get; }

        public ItemRegex(
            string name,
            string pattern,
            Color? foreColor = null,
            Color? backColor = null,
            bool isBold = false,
            bool isItalic = false
        )
            : base(foreColor, backColor, isBold, isItalic)
        {
            this.Name = name;
            this.Pattern = pattern;
        }

        public ItemRegex(
            string pattern,
            Color? foreColor = null,
            Color? backColor = null,
            bool isBold = false,
            bool isItalic = false
        )
            : this("", pattern, foreColor, backColor, isBold, isItalic) { }
    }
}
