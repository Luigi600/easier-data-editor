﻿using System.Drawing;

namespace Easier_Data_Editor.Extensions.Scintilla.SyntaxHighlighting
{
    public sealed class Range
    {
        public Point Range2 { get; }
        public byte Style { get; }

        public Range(int start, int len, byte style)
        {
            this.Range2 = new Point(start, start + len);
            this.Style = style;
        }

        public Range(int start, int len, StylesIndices style)
            : this(start, len, (byte)style) { }

        public Range(int start, int len)
            : this(start, len, (byte)StylesIndices.Default) { }
    }
}
