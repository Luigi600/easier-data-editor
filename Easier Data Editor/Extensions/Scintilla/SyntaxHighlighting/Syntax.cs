﻿using System.Collections.Generic;
using System.Collections.Immutable;
using System.Drawing;

namespace Easier_Data_Editor.Extensions.Scintilla.SyntaxHighlighting
{
    public class Syntax
    {
        private readonly List<ItemRegex> _regexSpecials = new List<ItemRegex>();
        private readonly List<ItemRegex> _regexSingleWord = new List<ItemRegex>();
        private readonly List<ItemKeywordList> _keywordLists = new List<ItemKeywordList>();

        public Color DefaultColor { get; }
        public Color DefaultBackColor { get; }

        public ImmutableList<ItemRegex> RegexSpecials => this._regexSpecials.ToImmutableList();
        public ImmutableList<ItemRegex> RegexSingleWord => this._regexSingleWord.ToImmutableList();
        public ImmutableList<ItemKeywordList> KeywordLists => this._keywordLists.ToImmutableList();

        public Syntax(Color defaultColor, Color defaultBackColor)
        {
            this.DefaultColor = defaultColor;
            this.DefaultBackColor = defaultBackColor;
        }

        public void AddRegexSpecial(ItemRegex item) => this._regexSpecials.Add(item);

        public void AddRegexSpecial(params ItemRegex[] items) =>
            this._regexSpecials.AddRange(items);

        public void AddRegexSpecial(IEnumerable<ItemRegex> items) =>
            this._regexSpecials.AddRange(items);

        public void AddRegexSingleWord(ItemRegex item) => this._regexSingleWord.Add(item);

        public void AddRegexSingleWord(params ItemRegex[] items) =>
            this._regexSingleWord.AddRange(items);

        public void AddRegexSingleWord(IEnumerable<ItemRegex> items) =>
            this._regexSingleWord.AddRange(items);

        public void AddKeywordList(ItemKeywordList item) => this._keywordLists.Add(item);

        public void AddKeywordList(params ItemKeywordList[] items) =>
            this._keywordLists.AddRange(items);

        public void AddKeywordList(IEnumerable<ItemKeywordList> items) =>
            this._keywordLists.AddRange(items);
    }
}
