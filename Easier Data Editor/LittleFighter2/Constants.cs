﻿namespace Easier_Data_Editor.LittleFighter2
{
    internal static class Constants
    {
        internal const string DATA_PASSWORD = "SiuHungIsAGoodBearBecauseHeIsVeryGood";
        internal const string DATA_NOT_ENCRYPTED_AD_TEXT =
            "Created/Edited with Easier Data Editor (STM93 Version) (created by Cookiesoft) - https://cookiesoft.lui.studio.net/";
        internal const int DATA_NOT_ENCRPYTED_BYTE_LENGTH = 123;
        internal const int DATA_INDEX_SHIFT_START = 12;

        internal const int MAXIMUM_FRAMES = 400;
    }
}
