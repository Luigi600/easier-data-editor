﻿using System;

namespace Easier_Data_Editor.LittleFighter2.Ciphers
{
    public static class Encryption
    {
        /// <summary>
        /// Encrypts a string into the format of an LF2 data file.
        /// </summary>
        /// <param name="text">The text to be encrypted.</param>
        /// <param name="password">The password to be used for encryption.</param>
        /// <returns>Returns an array of bytes representing the encrypted data file.</returns>
        public static byte[] EncryptData(string text, string password = Constants.DATA_PASSWORD)
        {
            byte[] encryptedData = new byte[Constants.DATA_NOT_ENCRPYTED_BYTE_LENGTH + text.Length];
            for (
                int i = 0;
                i
                    < Math.Min(
                        Constants.DATA_NOT_ENCRPYTED_BYTE_LENGTH,
                        Constants.DATA_NOT_ENCRYPTED_AD_TEXT.Length
                    );
                ++i
            )
                encryptedData[i] = (byte)Constants.DATA_NOT_ENCRYPTED_AD_TEXT[i];

            int index = Constants.DATA_INDEX_SHIFT_START;
            for (int i = 0; i < text.Length; ++i, ++index)
            {
                if (index == password.Length)
                    index = 0;

                encryptedData[i + Constants.DATA_NOT_ENCRPYTED_BYTE_LENGTH] = (byte)(
                    text[i] + (byte)password[index]
                );
            }

            return encryptedData;
        }
    }
}
