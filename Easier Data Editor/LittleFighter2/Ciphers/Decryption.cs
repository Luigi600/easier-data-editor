﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Easier_Data_Editor.Models;
using Easier_Data_Editor.Utilities;

namespace Easier_Data_Editor.LittleFighter2.Ciphers
{
    public static class Decryption
    {
        private const int READ_BUFFER_SIZE = 1024; // in bytes
        private static readonly byte[] _readBuffer = new byte[READ_BUFFER_SIZE];
        private static readonly char[] _readBufferChar = new char[READ_BUFFER_SIZE];
        private static Regex REGEX_PATTERN_DECRYPTED = new Regex(@":\s+");
        private const int MAGIC_DETECTION_IS_ALREADY_DECRYPTED = 5;

        public static string DetectAndDecrypt(
            string filePath,
            out bool isEncrypted,
            out TextEncoding encoding,
            long readLength = -1,
            string password = Constants.DATA_PASSWORD
        )
        {
            using FileStream fsm = new FileStream(filePath, FileMode.Open, FileAccess.Read);
            using StreamReader sr = new StreamReader(fsm, new UTF8Encoding(false), true);

            int readLen = 0;
            if (sr.Peek() >= 0)
                readLen = sr.Read(_readBufferChar, 0, _readBufferChar.Length);

            encoding = EncodingUtil.GetEncoding(sr);

            isEncrypted = DetectEncryptionState(filePath, sr, readLen);
            if (!isEncrypted)
            {
                sr.DiscardBufferedData();
                fsm.Seek(sr.CurrentEncoding.GetPreamble().Length, SeekOrigin.Begin);
                return sr.ReadToEnd();
            }

            return DecryptData(fsm, readLength, password);
        }

        /// <summary>
        /// Decrypts an LF2 data file which is in the typical LF2 encoding format.
        /// </summary>
        /// <param name="filePath">The path to the file to be decrypted.</param>
        /// <param name="readLength">The length of bytes to be read. A length of less than or equal to 0 means that everything is read.</param>
        /// <param name="password">The password to be used for decryption.</param>
        /// <returns>Returns a string of the decrypted file.</returns>
        public static string DecryptData(
            string filePath,
            long readLength = -1,
            string password = Constants.DATA_PASSWORD
        )
        {
            using FileStream fsm = new FileStream(filePath, FileMode.Open, FileAccess.Read);

            // auto close & dispose of the file stream
            return DecryptData(fsm, readLength, password);
        }

        /// <summary>
        /// Decrypts an LF2 data file that is in the typical LF2 encoding format as a stream.
        /// </summary>
        /// <param name="stream">The stream representing the data file to be decrypted.</param>
        /// <param name="toReadLength">The length of bytes to be read. A length of less than or equal to 0 means that everything is read.</param>
        /// <param name="password">The password to be used for decryption.</param>
        /// <returns>Returns a string of the decrypted file.</returns>
        public static string DecryptData(
            Stream stream,
            long toReadLength = -1,
            string password = Constants.DATA_PASSWORD
        )
        {
            int seek = Constants.DATA_NOT_ENCRPYTED_BYTE_LENGTH;
            if (seek > stream.Length)
                return "";

            stream.Seek(seek, SeekOrigin.Begin);
            if (toReadLength > stream.Length || toReadLength <= 0L)
                toReadLength = stream.Length;

            StringBuilder result = new StringBuilder((int)(toReadLength - stream.Position));
            int index = Constants.DATA_INDEX_SHIFT_START;

            while (stream.Position < toReadLength)
            {
                int readLen = stream.Read(_readBuffer, 0, _readBuffer.Length);
                for (int i = 0; i < readLen; ++i, ++index)
                {
                    if (index == password.Length)
                        index = 0;

                    try
                    {
                        char newByte = (char)(_readBuffer[i] - (byte)password[index]);
                        result.Append(newByte);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("DECRYPTING ERROR: " + ex.ToString());
                    }
                }
            }

            return result.ToString();
        }

        private static bool DetectEncryptionState(string filePath, StreamReader sr, int readLen)
        {
            bool isEncrypted;
            if (
                !Environments
                    .Application
                    .UserSettings
                    .AdvancedSettings
                    .EncryptionDetectionFromContent
            )
            {
                isEncrypted = Path.GetExtension(filePath).ToLower().Equals(".dat");
            }
            else
            {
                byte[] readBytes = sr.CurrentEncoding.GetBytes(_readBufferChar, 0, readLen);
                isEncrypted =
                    REGEX_PATTERN_DECRYPTED.Matches(sr.CurrentEncoding.GetString(readBytes)).Count
                    <= MAGIC_DETECTION_IS_ALREADY_DECRYPTED;
            }

            return isEncrypted;
        }
    }
}
