﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DarkModeForms;
using Easier_Data_Editor.Extensions.MultiLangSupport;
using Easier_Data_Editor.Forms;
using Easier_Data_Editor.Forms.TextEditors;
using Easier_Data_Editor.Models;
using Easier_Data_Editor.Natives;
using Easier_Data_Editor.Properties;
using Easier_Data_Editor.Serializers;
using Easier_Data_Editor.Utilities;
using Easier.MultiLangSupport;
using Easier.MultiLangSupport.Environments;
using Easier.MultiLangSupport.Forms;
using Easier.MultiLangSupport.Serializers;
using WeifenLuo.WinFormsUI.Docking;

namespace Easier_Data_Editor
{
    public partial class Main : TranslatableForm, IMainEditor
    {
        private readonly DarkModeCS ThemeRenderer;
        private readonly List<ToolStripItem> _toolStripItemsEditorRequired =
            new List<ToolStripItem>();
        private static readonly List<Keys> SHIFT_KEYS = new List<Keys>()
        {
            Keys.Shift,
            Keys.LShiftKey,
            (Keys.Shift | Keys.LShiftKey),
        };

        private const int RETRY_LANGUAGE_FILE_READING = 250; // in ms
        private const int MAX_RETRY_LANGUAGE_READING = 3;
        private const int MESSAGE_CODE_MOUSE_WHEEL = 0x20A;

        private bool _isActive = true;
        private bool _isSettingsActive = false;
        private IEditor? _currentEditor = null;
        private TextEditor? _currentTextEditor = null;

        public event EventHandler<TextEditor?>? TextEditorChanged;

        private bool IsActive
        {
            get => this._isActive;
            set
            {
                if (this._isActive == value)
                    return;

                this._isActive = value;
                this.timAnnotations.Enabled = value;
                // work around to support drag & drop of files in a scintilla text editor
                foreach (TextEditor editor in this.dockPalMain.Contents.OfType<TextEditor>())
                    editor.SetActiveState(this._isActive);
            }
        }

        private TextEditor? CurrentTextEditor
        {
            get => this._currentTextEditor;
            set
            {
                if (this._currentTextEditor == value)
                    return;

                this._currentTextEditor = value;

                if (value == null)
                {
                    this.ClearTextEditorStats();
                    this.SetIconsEnableStateForEditorRequiredButtons(false);
                    this.SetIconsForNoneSelectedTextEditor();
                }
                else
                {
                    this.SetTextEditorStats(value);
                    this.SetIconsEnableStateDependsOnTextEditor(value);
                }

                this.TextEditorChanged?.Invoke(this, value);
            }
        }

        public Main(bool systemStart)
        {
            this.VariablesList.Add(
                "CHANGE_ENCRYPTION_TITLE",
                $"{Environments.Application.APP_TITLE}    -   Change encryption state?"
            );
            this.VariablesList.Add(
                "CHANGE_ENCRYPTION",
                "By changing the encryption state, the file may be saved in such a way that other programs or LF2 can no longer read the file properly. Furthermore, \"Encryption detection from file content\" must be activated for this application so that the file is readable after a restart."
            );

            DarkModeCS.IsDarkModeCSEnabled = false;
            InitializeComponent();
            this.ThemeRenderer = new DarkModeCS(this, false, false);
            Debug.WriteLine(this.ThemeRenderer.IsDarkMode);
            this.ThemeRenderer.ApplyTheme(true);
            this._toolStripItemsEditorRequired.AddRange(
                new ToolStripItem[]
                {
                    // save, undo, redo will set in another function
                    this.tsmiSaveAll,
                    this.tsmiSaveAs,
                    this.tsmiExportHTML,
                    this.tsmiCloseFile,
                    this.tsmiCloseAllFiles,
                    this.tsmiIconSaveAll,
                    this.tsmiIconClose,
                    this.tsCombFrames,
                    this.tsCombLF,
                    this.tsmiRun,
                    this.tsmiEdit,
                    this.tsmiSearch,
                    this.tsmiView,
                    this.tsmiTools,
                    this.tsslTextEncryption,
                    this.tsslTextEncryptionVal,
                    this.tsslTextLength,
                    this.tsslTextLengthVal,
                    this.tsslTextLines,
                    this.tsslTextLinesVal,
                    this.tsslTextPos,
                    this.tsslTextPosVal,
                    this.tsslTextSel,
                    this.tsslTextSelVal,
                    this.tsslTextEncoding,
                    this.tsslTextEncodingVal,
                }
            );

            this.InitDockPanel();

            if (!systemStart)
                return;
            Environments.Application.LanguageChanged += Application_LanguageChanged;
#if DEBUG
            this.GenerateLanguageFile();
            Models.LittleFighter2.Object frame = new Models.LittleFighter2.Object();
            LF2DataDeserializer.ParseObjectFromAttributes(
                frame,
                new StringBuilder(Resources.weapon_normal)
            );
#endif
            this.AfterControlsInit();
            this.SetBindingsTextEditorView();
            this.SetSettingsEvents();
            this.SetIconsEnableStateDependsOnOpenedDocuments();
            this.ClearTextEditorStats();
        }

        public Main()
            : this(false) { }

        protected override void SetNotTranslatableThings()
        {
            this.Text = "";
            this.NotTranslatablePrefix = Environments.Application.APP_TITLE;
            this.NotTranslatableControlsList.AddRange(
                new Component[]
                {
                    this.tsCombFrames,
                    this.tsCombLF,
                    this.tsslTextLengthVal,
                    this.tsslTextLinesVal,
                    this.tsslTextPosVal,
                    this.tsslTextSelVal,
                    this.linkCookiesoft,
                }
            );
        }

        protected override void WndProc(ref Message m)
        {
            if (
                m.Msg == MESSAGE_CODE_MOUSE_WHEEL
                && SHIFT_KEYS.Exists(k => KeyUtil.IsKeyPressed(k))
            )
            {
                TextEditor? textEditor = this
                    .dockPalMain.Contents.OfType<TextEditor>()
                    .FirstOrDefault(e =>
                        e.Visible
                        && new Rectangle(Point.Empty, e.Size).Contains(
                            e.PointToClient(MousePosition)
                        )
                    );

                textEditor?.IncreaseOrDecreaseSelectedNumber(((int)m.WParam) > 0);
            }
            else if (m.Msg == Natives.Communications.Instance.WM_SETTEXT)
            {
                string? filePath = System.Runtime.InteropServices.Marshal.PtrToStringAuto(m.LParam);
                if (
                    filePath == null
                    || !File.Exists(filePath)
                    || Environments.Application.IsExtensionSupported(filePath)
                )
                    return;

                OpenFile(filePath);

                this.Show();
                this.BringToFront();
                this.Focus();
                this.Activate();
                Natives.Communications.Instance.SetForegroundWindow(this.Handle);
                if (WindowState == FormWindowState.Minimized)
                    Natives.Communications.Instance.ShowWindow(
                        this.Handle,
                        Natives.Communications.Instance.SW_RESTORE
                    );
            }

            base.WndProc(ref m);
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.Delete && this.CurrentTextEditor != null)
                return this.CurrentTextEditor.DeleteKey();

            return base.ProcessCmdKey(ref msg, keyData);
        }

        protected override void OnDeactivate(EventArgs e)
        {
            this.IsActive = false;

            base.OnDeactivate(e);
        }

        protected override void OnActivated(EventArgs e)
        {
            this.IsActive = true;

            base.OnActivated(e);
        }

        private void InitDockPanel()
        {
            this.dockPalMain.Theme = new Extensions.DockPanelSuite.DataEditorTheme();
            //this.dockPalMain.Theme = new VS2015DarkTheme();
            this.dockPalMain.DocumentStyle = DocumentStyle.DockingWindow;
            this.dockPalMain.ShowDocumentIcon = true;

            this.dockPalMain.ContentAdded += this.DockPalMain_ContentAdded;
            this.dockPalMain.ContentRemoved += this.DockPalMain_ContentRemoved;
            this.dockPalMain.ContentAdded += this.DockPalMain_ContentChanged;
            this.dockPalMain.ContentRemoved += this.DockPalMain_ContentChanged;
            this.dockPalMain.ActiveContentChanged += this.DockPalMain_ActiveContentChanged;

            this.dockPalMain.ActiveContentChanged += this.DockPalMain_LayoutChanged;
            this.dockPalMain.ActiveDocumentChanged += this.DockPalMain_LayoutChanged;
            // this.dockPalMain.ActivePaneChanged += this.DockPalMain_LayoutChanged;
            this.dockPalMain.ContentAdded += this.DockPalMain_LayoutChanged;
            this.dockPalMain.ContentRemoved += this.DockPalMain_LayoutChanged;
        }

        private void SetBindingsTextEditorView()
        {
            this.tsmiLineNumbers.DataBindings.Add(
                Constants.BINDING_CHECK_BOX_VALUE,
                Environments.Application.UserSettings.TextEditorSettings,
                "ShowLineNumbers",
                true,
                DataSourceUpdateMode.OnPropertyChanged
            );

            this.tsmiMarkLine.DataBindings.Add(
                Constants.BINDING_CHECK_BOX_VALUE,
                Environments.Application.UserSettings.TextEditorSettings,
                "MarkCurrentLine",
                true,
                DataSourceUpdateMode.OnPropertyChanged
            );

            this.tsmiWhitespaces.DataBindings.Add(
                Constants.BINDING_CHECK_BOX_VALUE,
                Environments.Application.UserSettings.TextEditorSettings,
                "ShowWhitespaces",
                true,
                DataSourceUpdateMode.OnPropertyChanged
            );

            this.tsCombWrapMode.DataBindings.Add(
                Constants.BINDING_COMBO_BOX_SELECTED_INDEX,
                Environments.Application.UserSettings.TextEditorSettings,
                "WrapModeBindingFriendly",
                true,
                DataSourceUpdateMode.OnPropertyChanged
            );
        }

        private void SetSettingsEvents()
        {
            Environments.Application.UserSettings.SettingsChanged += (sender, e) =>
            {
                foreach (TextEditor editor in this.dockPalMain.Contents.OfType<TextEditor>())
                    editor.ReloadStyle();

                if (this._isSettingsActive)
                    return; // changes was apply from settings

                SettingsSerializer.WriteToXML(Environments.Application.UserSettings);
            };
        }

        #region Events

        // MUST in main thread... and I don't how to get the main thread in entry point class
        private async void Application_LanguageChanged(object sender, FileSystemEventArgs e)
        {
            Debug.WriteLine(e.ChangeType);
            if (e is RenamedEventArgs eventArgs)
            {
                string oldExt = Path.GetExtension(eventArgs.OldFullPath).ToLower();
                string newExt = Path.GetExtension(eventArgs.FullPath).ToLower();
                if (
                    !oldExt.Equals(Environments.Application.XML_EXTENSION)
                    && newExt.Equals(Environments.Application.XML_EXTENSION)
                )
                    await this.AddLanguage(e.FullPath);
                else if (
                    oldExt.Equals(Environments.Application.XML_EXTENSION)
                    && !newExt.Equals(Environments.Application.XML_EXTENSION)
                )
                    this.RemoveLanguage(eventArgs.OldFullPath);
                else
                    await this.ReloadLanguageAndReplace(eventArgs.OldFullPath, e.FullPath);
            }
            else if ((e.ChangeType & WatcherChangeTypes.Deleted) == WatcherChangeTypes.Deleted)
            {
                this.RemoveLanguage(e.FullPath);
            }
            else if ((e.ChangeType & WatcherChangeTypes.Created) == WatcherChangeTypes.Created)
            {
                await this.AddLanguage(e.FullPath);
            }
            else
            {
                // reload file
                await this.ReloadLanguageAndReplace(e.FullPath, e.FullPath);
            }
        }

        private void RemoveLanguage(string fullPath)
        {
            Language? lang = Translation
                .GetLanguages()
                .Where(l => l is LanguageWithFile)
                .Cast<LanguageWithFile>()
                .FirstOrDefault(l => l.Path.Equals(fullPath));
            if (lang != null)
                ThreadUtil.RunInMainThread(this, Translation.RemoveLanguage, lang);
        }

        private async Task AddLanguage(string fullPath)
        {
            Language? lang = await this.GetLanguageFromFile(fullPath);
            if (lang != null)
                ThreadUtil.RunInMainThread(this, Translation.AddLanguage, lang);
        }

        private async Task ReloadLanguageAndReplace(string oldPath, string newPath)
        {
            Language? oldLang = Translation
                .GetLanguages()
                .Where(l => l is LanguageWithFile)
                .Cast<LanguageWithFile>()
                .FirstOrDefault(l => l.Path.Equals(oldPath));
            if (oldLang == null)
                return;

            Language? newLang = await this.GetLanguageFromFile(newPath);
            if (newLang != null)
                ThreadUtil.RunInMainThread(this, Translation.ReplaceLanguage, oldLang, newLang);
        }

        private void DockPalMain_ContentAdded(object? sender, DockContentEventArgs e)
        {
            if (e.Content is Control control)
            {
                control.DragEnter += this.DragEnterOnApp;
                control.DragOver += this.DragOverOnApp;
                control.DragDrop += this.DragDropOnApp;
            }

            if (e.Content is IEditor editor)
                editor.HasFocusChanged += this.Editor_HasFocusChanged;
        }

        private void DockPalMain_ContentRemoved(object? sender, DockContentEventArgs e)
        {
            if (e.Content is Control control)
            {
                control.DragEnter -= this.DragEnterOnApp;
                control.DragOver -= this.DragOverOnApp;
                control.DragDrop -= this.DragDropOnApp;
            }

            if (e.Content is IEditor editor && this._currentEditor == editor)
            {
                this._currentEditor = null;
                editor.SetHasFocus(false, true);
            }

            if (e.Content is TextEditor textEditor && this.CurrentTextEditor == textEditor)
                this.CurrentTextEditor = null;
        }

        private void DockPalMain_ContentChanged(object? sender, DockContentEventArgs e)
        {
            this.SetIconsEnableStateDependsOnOpenedDocuments();
            // TODO: CheckEXEFiles
        }

        private void Editor_HasFocusChanged(object? sender, bool e)
        {
            foreach (
                IEditor editor in this
                    .dockPalMain.Contents.OfType<IEditor>()
                    .Where(e => e != this._currentEditor)
            )
                editor.SetHasFocus(false, false);
        }

        private void DockPalMain_LayoutChanged(object? sender, EventArgs e)
        {
            Environments.Application.DockPanelLayout.SetLength(0);
            // TODO: Chinese characters works?
            this.dockPalMain.SaveAsXml(Environments.Application.DockPanelLayout, Encoding.Unicode);
        }

        private void DockPalMain_ActiveContentChanged(object? sender, EventArgs e)
        {
            if (this.dockPalMain.ActiveContent is IEditor editor)
            {
                this._currentEditor = editor;
                editor.SetHasFocus(true, true);
            }

            if (this.dockPalMain.ActiveContent is TextEditor textEditor)
                this.CurrentTextEditor = textEditor;
        }

        #endregion



#if DEBUG
        private void GenerateLanguageFile()
        {
            TranslationCreator creator = new TranslationCreator("English", "Luigi600");
            creator.AddAssembly(System.Reflection.Assembly.GetExecutingAssembly());
            TranslationSerializer serializer = new TranslationSerializer(creator);

            foreach (Type type in System.Reflection.Assembly.GetExecutingAssembly().GetTypes())
                Console.WriteLine(
                    "{0} {1}",
                    type.FullName,
                    typeof(ITranslatable).IsAssignableFrom(type)
                        && typeof(ContainerControl).IsAssignableFrom(type)
                );

            var output = new StreamWriter(new FileStream("test.test", FileMode.Create));
            serializer.Write(output);
            output.Close();
            creator.Dispose();
        }
#endif

        private async Task<Language?> GetLanguageFromFile(string file)
        {
            int @try = 0;
            while (@try <= MAX_RETRY_LANGUAGE_READING)
            {
                try
                {
                    return TranslationDeserializerWithPath.GetInstance(file);
                }
                catch (Exception exception)
                {
                    Console.WriteLine(exception);
                    await Task.Delay(RETRY_LANGUAGE_FILE_READING);
                }
                finally
                {
                    ++@try;
                }
            }

            return null;
        }

        private void OpenFile(string file)
        {
            if (!File.Exists(file))
                return;
            Environments.Application.UserSettings.GeneralSettings.RecentFiles.Add(file);

            TextEditor editor = new TextEditor(
                LittleFighter2.Ciphers.Decryption.DetectAndDecrypt(
                    file,
                    out bool isEncrypted,
                    out TextEncoding encoding
                ),
                file,
                isEncrypted,
                encoding
            );
            editor.TextLengthChanged += this.Editor_StatsChanged;
            editor.TextPositionChanged += this.Editor_StatsChanged;
            editor.SelectionChanged += this.Editor_StatsChanged;
            editor.DocumentPropertiesChanged += this.Editor_StatsChanged;
            editor.UndoRedoStackChanged += this.Editor_StateChanged;
            editor.SaveStateChanged += this.Editor_StateChanged;
            editor.Show(this.dockPalMain, DockState.Document);
            editor.SetActiveState(this.IsActive);
        }

        private void Editor_StateChanged(object? sender, EventArgs e)
        {
            if (!(sender is TextEditor textEditor) || textEditor != this._currentTextEditor)
                return;

            this.SetIconsEnableStateDependsOnTextEditor(textEditor);
        }

        private void Editor_StatsChanged(object? sender, EventArgs e)
        {
            if (!(sender is TextEditor textEditor) || textEditor != this._currentTextEditor)
                return;

            this.SetTextEditorStats(textEditor);
        }

        private void SetTextEditorStats(TextEditor editor)
        {
            this.tsslTextEncryptionVal.Text = editor.DocumentEncrypted.ToString();
            this.tsslTextLengthVal.Text = editor.GetTextLength().ToString("N0");
            this.tsslTextLinesVal.Text = editor.GetLinesCount().ToString("N0");
            this.tsslTextPosVal.Text = editor.GetPosition().ToString("N0");
            this.tsslTextSelVal.Text =
                $"{editor.GetLengthOfSelectedCharacters().ToString("N0")} | {editor.GetLengthOfSelectedLines().ToString("N0")}";
            this.tsslTextEncodingVal.Text = editor.Encoding.ToString();
            this.tsslTextEncoding.Enabled = !editor.DocumentEncrypted;
            this.tsslTextEncodingVal.Enabled = this.tsslTextEncoding.Enabled;
        }

        private void ClearTextEditorStats()
        {
            this.tsslTextEncryptionVal.Text = "-";
            this.tsslTextLengthVal.Text = "-";
            this.tsslTextLinesVal.Text = "-";
            this.tsslTextPosVal.Text = "-";
            this.tsslTextSelVal.Text = "-";
            this.tsslTextEncryptionVal.Text = "-";
            this.tsslTextEncodingVal.Text = "-";
        }

        private void SetIconsEnableStateDependsOnOpenedDocuments() =>
            SetIconsEnableStateForEditorRequiredButtons(
                this.dockPalMain.Contents.OfType<IEditor>().Any()
            );

        private void SetIconsEnableStateForEditorRequiredButtons(bool state)
        {
            foreach (ToolStripItem item in this._toolStripItemsEditorRequired)
                item.Enabled = state;
        }

        private void SetIconsEnableStateDependsOnTextEditor(TextEditor editor)
        {
            this.tsmiUndo.Enabled = editor.CanUndo();
            this.tsmiRedo.Enabled = editor.CanRedo();

            this.tsmiSave.Enabled = !editor.IsSave;
        }

        private void SetIconsForNoneSelectedTextEditor()
        {
            this.tsmiUndo.Enabled = false;
            this.tsmiRedo.Enabled = false;

            this.tsmiSave.Enabled = false;
        }

        private void DragDropOnApp(object? sender, DragEventArgs e)
        {
            if (e.Data is null || !(e.Data.GetData(DataFormats.FileDrop) is string[] files))
                return;

            foreach (
                string file in files.Where(f => Environments.Application.IsExtensionSupported(f))
            )
                this.OpenFile(file);
        }

        private void DragEnterOnApp(object? sender, DragEventArgs e) =>
            e.Effect = e.Data.GetDataPresent(DataFormats.FileDrop)
                ? DragDropEffects.Copy
                : DragDropEffects.None;

        private void DragOverOnApp(object? sender, DragEventArgs e)
        {
            if (e.Data is null || !(e.Data.GetData(DataFormats.FileDrop) is string[] files))
                return;

            e.Effect = files.Any(f => Environments.Application.IsExtensionSupported(f))
                ? DragDropEffects.Copy
                : DragDropEffects.None;
        }

        private void TimAnnotations_Tick(object sender, EventArgs e)
        {
            if (!(this.dockPalMain.ActiveDocument is TextEditor textEditor))
                return;

            textEditor.SetAnnotations();
        }
    }
}
