﻿using System.Windows.Forms;
using Easier.MultiLangSupport;
using Easier.MultiLangSupport.Forms;

namespace Easier_Data_Editor.Windows
{
    [SkipComponentTranslation]
    public class EscapeCloseableWindow : TranslatableForm
    {
        /// <inheritdoc/>
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }
    }
}
