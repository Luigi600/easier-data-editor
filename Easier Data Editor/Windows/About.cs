﻿using System;
using System.Diagnostics;
using System.Windows.Forms;

namespace Easier_Data_Editor.Windows
{
    public partial class About : EscapeCloseableWindow
    {
#pragma warning disable S1075 // URIs should not be hardcoded
        private const string URL_LUI_STUDIO = "https://www.lui-studio.net/";
        private const string URL_GITLAB = "https://gitlab.com/Luigi600/easier-data-editor";
        private const string URL_SCINTILLA_NET = "https://github.com/jacobslusser/ScintillaNET";
        private const string URL_DOCK_PANEL_SUITE =
            "https://github.com/dockpanelsuite/dockpanelsuite";
#pragma warning restore S1075 // URIs should not be hardcoded

        public About()
        {
            InitializeComponent();

            rtbStory.SelectionStart = rtbStory.TextLength;

            System.Reflection.AssemblyName oAssembly = System
                .Reflection.Assembly.GetExecutingAssembly()
                .GetName();
            lblVersionValue.Text = oAssembly.Version?.ToString() + " (STM93 Version)";
            lblVersionValue2.Text =
                $"{Application.ProductVersion} (Commit {Environments.Application.BUILD_COMMIT})";

            this.AfterControlsInit();
        }

        protected override void SetNotTranslatableThings()
        {
            this.NotTranslatableControlsList.AddRange(
                new[]
                {
                    this.lblCreatorValue,
                    this.linkLuiStudio,
                    this.linkLibrary1,
                    this.linkLibrary2,
                    this.linkGitLab,
                    this.lblVersionValue,
                    this.lblVersionValue2,
                }
            );
        }

        private void LinkLuiStudio_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e) =>
            Process.Start(new ProcessStartInfo(URL_LUI_STUDIO) { UseShellExecute = true });

        private void LinkGitLab_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e) =>
            Process.Start(new ProcessStartInfo(URL_GITLAB) { UseShellExecute = true });

        private void LinkLibrary1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e) =>
            Process.Start(new ProcessStartInfo(URL_SCINTILLA_NET) { UseShellExecute = true });

        private void LinkLibrary2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e) =>
            Process.Start(new ProcessStartInfo(URL_DOCK_PANEL_SUITE) { UseShellExecute = true });

        private void BtnOkay_Click(object sender, EventArgs e) => this.Close();
    }
}
