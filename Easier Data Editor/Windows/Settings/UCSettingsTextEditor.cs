﻿using System;
using System.Windows.Forms;
using ScintillaNET;

namespace Easier_Data_Editor.Windows.Settings
{
    public partial class UCSettingsTextEditor : UCSettingsBase
    {
        private readonly Environments.Settings.TextEditor _settings;

        public UCSettingsTextEditor(Environments.Settings.TextEditor settings)
            : base()
        {
            this._settings = settings;
            this.VariablesList.Add("DISPLAY_NAME", "Text Editor");

            InitializeComponent();

            this.combWrapMode.Items.Clear();
            foreach (string wrapModeName in Enum.GetNames(typeof(WrapMode)))
                this.combWrapMode.Items.Add(wrapModeName);

            this.SetBindings();
            this.AfterControlsInit();
        }

        // necessary for the designer (private) and translation creation (public)
        public UCSettingsTextEditor()
            : this(new Environments.Settings.TextEditor()) { }

        protected override Control MainControl() => this.tlpMain;

        private void SetBindings()
        {
            this.txtFontFamily.DataBindings.Add(
                Constants.BINDING_TEXT_BOX_VALUE,
                this._settings,
                "FontFamily",
                true,
                DataSourceUpdateMode.OnPropertyChanged
            );

            this.nudFontSize.DataBindings.Add(
                Constants.BINDING_NUMERIC_UP_DOWN_VALUE,
                this._settings,
                "FontSize",
                true,
                DataSourceUpdateMode.OnPropertyChanged
            );

            this.cbLineNumbers.DataBindings.Add(
                Constants.BINDING_CHECK_BOX_VALUE,
                this._settings,
                "ShowLineNumbers",
                true,
                DataSourceUpdateMode.OnPropertyChanged
            );

            this.cbWhitespaces.DataBindings.Add(
                Constants.BINDING_CHECK_BOX_VALUE,
                this._settings,
                "ShowWhitespaces",
                true,
                DataSourceUpdateMode.OnPropertyChanged
            );

            this.cbMarkCurrentLine.DataBindings.Add(
                Constants.BINDING_CHECK_BOX_VALUE,
                this._settings,
                "MarkCurrentLine",
                true,
                DataSourceUpdateMode.OnPropertyChanged
            );

            this.picCurrentLineColorPreview.DataBindings.Add(
                Constants.BINDING_PICTURE_BOX_BACK_COLOR,
                this._settings,
                "MarkLineColor",
                true,
                DataSourceUpdateMode.OnPropertyChanged
            );

            this.combWrapMode.DataBindings.Add(
                Constants.BINDING_COMBO_BOX_SELECTED_INDEX,
                this._settings,
                "WrapModeBindingFriendly",
                true,
                DataSourceUpdateMode.OnPropertyChanged
            );

            this.cbAutocompletion.DataBindings.Add(
                Constants.BINDING_CHECK_BOX_VALUE,
                this._settings,
                "Autocompletion",
                true,
                DataSourceUpdateMode.OnPropertyChanged
            );

            this.cbFolding.DataBindings.Add(
                Constants.BINDING_CHECK_BOX_VALUE,
                this._settings,
                "Folding",
                true,
                DataSourceUpdateMode.OnPropertyChanged
            );

            this.cbSubfolding.DataBindings.Add(
                Constants.BINDING_CHECK_BOX_VALUE,
                this._settings,
                "Subfolding",
                true,
                DataSourceUpdateMode.OnPropertyChanged
            );

            this.cbBookmarks.DataBindings.Add(
                Constants.BINDING_CHECK_BOX_VALUE,
                this._settings,
                "Bookmarks",
                true,
                DataSourceUpdateMode.OnPropertyChanged
            );

            this.cbChangeBar.DataBindings.Add(
                Constants.BINDING_CHECK_BOX_VALUE,
                this._settings,
                "ShowChanges",
                true,
                DataSourceUpdateMode.OnPropertyChanged
            );

            this.cbScrollbarAnnotations.DataBindings.Add(
                Constants.BINDING_CHECK_BOX_VALUE,
                this._settings,
                "ScrollbarAnnotations",
                true,
                DataSourceUpdateMode.OnPropertyChanged
            );

            this.cbIndention.DataBindings.Add(
                Constants.BINDING_CHECK_BOX_VALUE,
                this._settings,
                "Indention",
                true,
                DataSourceUpdateMode.OnPropertyChanged
            );

            this.cbAutoJumpBySyncID.DataBindings.Add(
                Constants.BINDING_CHECK_BOX_VALUE,
                this._settings,
                "AutoJumpBySyncID",
                true,
                DataSourceUpdateMode.OnPropertyChanged
            );
        }

        private void BtnChangeLineColor_Click(object sender, System.EventArgs e)
        {
            ColorDialog2.ColorDialog dialog = new ColorDialog2.ColorDialog(
                this.picCurrentLineColorPreview.BackColor
            );
            if (dialog.ShowDialog() == DialogResult.OK)
                this._settings.MarkLineColor = dialog.SelectedColor;
        }

        private void BtnChangeFontFamily_Click(object sender, EventArgs e)
        {
            if (this.fdFontFamily.ShowDialog() == DialogResult.OK)
            {
                this.txtFontFamily.Text = this.fdFontFamily.Font.Name;
                this.nudFontSize.Value = (decimal)this.fdFontFamily.Font.Size;
            }
        }
    }
}
