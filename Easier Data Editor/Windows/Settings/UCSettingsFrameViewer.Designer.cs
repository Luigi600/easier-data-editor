﻿
namespace Easier_Data_Editor.Windows.Settings
{
    partial class UCSettingsFrameViewer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ofdSprite = new System.Windows.Forms.OpenFileDialog();
            this.lblDefaultBG = new System.Windows.Forms.Label();
            this.lblMenuStrip = new System.Windows.Forms.Label();
            this.txtHeavyWeaponSprite = new System.Windows.Forms.TextBox();
            this.btnChangePathHWeaponSprite = new System.Windows.Forms.Button();
            this.txtHeavyWeaponData = new System.Windows.Forms.TextBox();
            this.btnChangePathHWeaponData = new System.Windows.Forms.Button();
            this.ofdData = new System.Windows.Forms.OpenFileDialog();
            this.txtWeaponSprite = new System.Windows.Forms.TextBox();
            this.btnChangePathWeaponSprite = new System.Windows.Forms.Button();
            this.txtWeaponData = new System.Windows.Forms.TextBox();
            this.btnChangePathWeaponData = new System.Windows.Forms.Button();
            this.cbMenuStrip = new System.Windows.Forms.CheckBox();
            this.tlpWeaponSprite = new System.Windows.Forms.TableLayoutPanel();
            this.btnResetWeaponSprite = new System.Windows.Forms.Button();
            this.tlpHeavyWeaponData = new System.Windows.Forms.TableLayoutPanel();
            this.btnResetHeavyWeaponData = new System.Windows.Forms.Button();
            this.tlpMain = new System.Windows.Forms.TableLayoutPanel();
            this.lblGlobalView = new System.Windows.Forms.Label();
            this.lblFullRestore = new System.Windows.Forms.Label();
            this.cbFullRestore = new System.Windows.Forms.CheckBox();
            this.cbGlobalView = new System.Windows.Forms.CheckBox();
            this.tlpDefaultBG = new System.Windows.Forms.TableLayoutPanel();
            this.btnChangeDefaultBG = new System.Windows.Forms.Button();
            this.picDefaultBGPreview = new System.Windows.Forms.PictureBox();
            this.btnImage = new System.Windows.Forms.Button();
            this.tlpWeaponData = new System.Windows.Forms.TableLayoutPanel();
            this.btnResetWeaponData = new System.Windows.Forms.Button();
            this.tlpHeavyWeaponSprite = new System.Windows.Forms.TableLayoutPanel();
            this.btnResetHeavyWeaponSprite = new System.Windows.Forms.Button();
            this.lblHeavyWeaponSprite = new System.Windows.Forms.Label();
            this.lblHeavyWeaponData = new System.Windows.Forms.Label();
            this.lblWeaponSprite = new System.Windows.Forms.Label();
            this.lblWeaponData = new System.Windows.Forms.Label();
            this.lblHeavyWeaponSpriteFound = new System.Windows.Forms.Label();
            this.lblHeavyWeaponDataFound = new System.Windows.Forms.Label();
            this.lblWeaponSpriteFound = new System.Windows.Forms.Label();
            this.lblDirect = new System.Windows.Forms.Label();
            this.cbDirect = new System.Windows.Forms.CheckBox();
            this.lblWeaponDataFound = new System.Windows.Forms.Label();
            this.lblDirectInformation = new System.Windows.Forms.Label();
            this.lblSyncSelectedFrame = new System.Windows.Forms.Label();
            this.cbSyncSelectedFrame = new System.Windows.Forms.CheckBox();
            this.tlpWeaponSprite.SuspendLayout();
            this.tlpHeavyWeaponData.SuspendLayout();
            this.tlpMain.SuspendLayout();
            this.tlpDefaultBG.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picDefaultBGPreview)).BeginInit();
            this.tlpWeaponData.SuspendLayout();
            this.tlpHeavyWeaponSprite.SuspendLayout();
            this.SuspendLayout();
            // 
            // ofdSprite
            // 
            this.ofdSprite.Filter = "Sprite sheet|*.bmp";
            this.ofdSprite.Title = "Select a sprite sheet...";
            // 
            // lblDefaultBG
            // 
            this.lblDefaultBG.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDefaultBG.AutoSize = true;
            this.lblDefaultBG.Location = new System.Drawing.Point(3, 34);
            this.lblDefaultBG.Margin = new System.Windows.Forms.Padding(3);
            this.lblDefaultBG.Name = "lblDefaultBG";
            this.lblDefaultBG.Padding = new System.Windows.Forms.Padding(0, 6, 0, 6);
            this.lblDefaultBG.Size = new System.Drawing.Size(86, 51);
            this.lblDefaultBG.TabIndex = 1;
            this.lblDefaultBG.Text = "Default Background Color:";
            this.lblDefaultBG.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMenuStrip
            // 
            this.lblMenuStrip.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMenuStrip.AutoSize = true;
            this.lblMenuStrip.Location = new System.Drawing.Point(3, 3);
            this.lblMenuStrip.Margin = new System.Windows.Forms.Padding(3);
            this.lblMenuStrip.Name = "lblMenuStrip";
            this.lblMenuStrip.Padding = new System.Windows.Forms.Padding(0, 6, 0, 6);
            this.lblMenuStrip.Size = new System.Drawing.Size(86, 25);
            this.lblMenuStrip.TabIndex = 0;
            this.lblMenuStrip.Text = "Menu Strip:";
            this.lblMenuStrip.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtHeavyWeaponSprite
            // 
            this.txtHeavyWeaponSprite.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtHeavyWeaponSprite.Location = new System.Drawing.Point(3, 11);
            this.txtHeavyWeaponSprite.Margin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.txtHeavyWeaponSprite.Name = "txtHeavyWeaponSprite";
            this.txtHeavyWeaponSprite.Size = new System.Drawing.Size(5, 20);
            this.txtHeavyWeaponSprite.TabIndex = 9;
            this.txtHeavyWeaponSprite.TextChanged += new System.EventHandler(this.TxtWeaponDataOrSprite_TextChanged);
            // 
            // btnChangePathHWeaponSprite
            // 
            this.btnChangePathHWeaponSprite.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.btnChangePathHWeaponSprite.Location = new System.Drawing.Point(11, 10);
            this.btnChangePathHWeaponSprite.Margin = new System.Windows.Forms.Padding(3, 3, 2, 3);
            this.btnChangePathHWeaponSprite.Name = "btnChangePathHWeaponSprite";
            this.btnChangePathHWeaponSprite.Size = new System.Drawing.Size(26, 22);
            this.btnChangePathHWeaponSprite.TabIndex = 10;
            this.btnChangePathHWeaponSprite.Text = "...";
            this.btnChangePathHWeaponSprite.UseVisualStyleBackColor = true;
            this.btnChangePathHWeaponSprite.Click += new System.EventHandler(this.BtnChangePathHWeaponSprite_Click);
            // 
            // txtHeavyWeaponData
            // 
            this.txtHeavyWeaponData.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtHeavyWeaponData.Location = new System.Drawing.Point(3, 11);
            this.txtHeavyWeaponData.Margin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.txtHeavyWeaponData.Name = "txtHeavyWeaponData";
            this.txtHeavyWeaponData.Size = new System.Drawing.Size(5, 20);
            this.txtHeavyWeaponData.TabIndex = 9;
            this.txtHeavyWeaponData.TextChanged += new System.EventHandler(this.TxtWeaponDataOrSprite_TextChanged);
            // 
            // btnChangePathHWeaponData
            // 
            this.btnChangePathHWeaponData.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.btnChangePathHWeaponData.Location = new System.Drawing.Point(11, 10);
            this.btnChangePathHWeaponData.Margin = new System.Windows.Forms.Padding(3, 3, 2, 3);
            this.btnChangePathHWeaponData.Name = "btnChangePathHWeaponData";
            this.btnChangePathHWeaponData.Size = new System.Drawing.Size(26, 22);
            this.btnChangePathHWeaponData.TabIndex = 10;
            this.btnChangePathHWeaponData.Text = "...";
            this.btnChangePathHWeaponData.UseVisualStyleBackColor = true;
            this.btnChangePathHWeaponData.Click += new System.EventHandler(this.BtnChangePathHWeaponData_Click);
            // 
            // ofdData
            // 
            this.ofdData.Filter = "Data|*.dat";
            this.ofdData.Title = "Select a data file...";
            // 
            // txtWeaponSprite
            // 
            this.txtWeaponSprite.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtWeaponSprite.Location = new System.Drawing.Point(3, 5);
            this.txtWeaponSprite.Margin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.txtWeaponSprite.Name = "txtWeaponSprite";
            this.txtWeaponSprite.Size = new System.Drawing.Size(5, 20);
            this.txtWeaponSprite.TabIndex = 9;
            this.txtWeaponSprite.TextChanged += new System.EventHandler(this.TxtWeaponDataOrSprite_TextChanged);
            // 
            // btnChangePathWeaponSprite
            // 
            this.btnChangePathWeaponSprite.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.btnChangePathWeaponSprite.Location = new System.Drawing.Point(11, 4);
            this.btnChangePathWeaponSprite.Margin = new System.Windows.Forms.Padding(3, 3, 2, 3);
            this.btnChangePathWeaponSprite.Name = "btnChangePathWeaponSprite";
            this.btnChangePathWeaponSprite.Size = new System.Drawing.Size(26, 22);
            this.btnChangePathWeaponSprite.TabIndex = 10;
            this.btnChangePathWeaponSprite.Text = "...";
            this.btnChangePathWeaponSprite.UseVisualStyleBackColor = true;
            this.btnChangePathWeaponSprite.Click += new System.EventHandler(this.BtnChangePathWeaponSprite_Click);
            // 
            // txtWeaponData
            // 
            this.txtWeaponData.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtWeaponData.Location = new System.Drawing.Point(3, 5);
            this.txtWeaponData.Margin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.txtWeaponData.Name = "txtWeaponData";
            this.txtWeaponData.Size = new System.Drawing.Size(5, 20);
            this.txtWeaponData.TabIndex = 9;
            this.txtWeaponData.TextChanged += new System.EventHandler(this.TxtWeaponDataOrSprite_TextChanged);
            // 
            // btnChangePathWeaponData
            // 
            this.btnChangePathWeaponData.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.btnChangePathWeaponData.Location = new System.Drawing.Point(11, 4);
            this.btnChangePathWeaponData.Margin = new System.Windows.Forms.Padding(3, 3, 2, 3);
            this.btnChangePathWeaponData.Name = "btnChangePathWeaponData";
            this.btnChangePathWeaponData.Size = new System.Drawing.Size(26, 22);
            this.btnChangePathWeaponData.TabIndex = 10;
            this.btnChangePathWeaponData.Text = "...";
            this.btnChangePathWeaponData.UseVisualStyleBackColor = true;
            this.btnChangePathWeaponData.Click += new System.EventHandler(this.BtnChangePathWeaponData_Click);
            // 
            // cbMenuStrip
            // 
            this.cbMenuStrip.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbMenuStrip.AutoSize = true;
            this.cbMenuStrip.Location = new System.Drawing.Point(95, 8);
            this.cbMenuStrip.Name = "cbMenuStrip";
            this.cbMenuStrip.Size = new System.Drawing.Size(86, 14);
            this.cbMenuStrip.TabIndex = 0;
            this.cbMenuStrip.UseVisualStyleBackColor = true;
            // 
            // tlpWeaponSprite
            // 
            this.tlpWeaponSprite.AutoSize = true;
            this.tlpWeaponSprite.ColumnCount = 3;
            this.tlpWeaponSprite.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpWeaponSprite.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tlpWeaponSprite.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tlpWeaponSprite.Controls.Add(this.txtWeaponSprite, 0, 0);
            this.tlpWeaponSprite.Controls.Add(this.btnChangePathWeaponSprite, 1, 0);
            this.tlpWeaponSprite.Controls.Add(this.btnResetWeaponSprite, 2, 0);
            this.tlpWeaponSprite.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpWeaponSprite.Location = new System.Drawing.Point(93, 227);
            this.tlpWeaponSprite.Margin = new System.Windows.Forms.Padding(1, 1, 0, 0);
            this.tlpWeaponSprite.Name = "tlpWeaponSprite";
            this.tlpWeaponSprite.RowCount = 1;
            this.tlpWeaponSprite.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpWeaponSprite.Size = new System.Drawing.Size(91, 30);
            this.tlpWeaponSprite.TabIndex = 12;
            // 
            // btnResetWeaponSprite
            // 
            this.btnResetWeaponSprite.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.btnResetWeaponSprite.Location = new System.Drawing.Point(42, 4);
            this.btnResetWeaponSprite.Margin = new System.Windows.Forms.Padding(3, 3, 2, 3);
            this.btnResetWeaponSprite.Name = "btnResetWeaponSprite";
            this.btnResetWeaponSprite.Size = new System.Drawing.Size(47, 22);
            this.btnResetWeaponSprite.TabIndex = 10;
            this.btnResetWeaponSprite.Text = "built-in";
            this.btnResetWeaponSprite.UseVisualStyleBackColor = true;
            this.btnResetWeaponSprite.Click += new System.EventHandler(this.BtnResetWeaponSprite_Click);
            // 
            // tlpHeavyWeaponData
            // 
            this.tlpHeavyWeaponData.AutoSize = true;
            this.tlpHeavyWeaponData.ColumnCount = 3;
            this.tlpHeavyWeaponData.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpHeavyWeaponData.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tlpHeavyWeaponData.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tlpHeavyWeaponData.Controls.Add(this.txtHeavyWeaponData, 0, 0);
            this.tlpHeavyWeaponData.Controls.Add(this.btnChangePathHWeaponData, 1, 0);
            this.tlpHeavyWeaponData.Controls.Add(this.btnResetHeavyWeaponData, 2, 0);
            this.tlpHeavyWeaponData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpHeavyWeaponData.Location = new System.Drawing.Point(93, 258);
            this.tlpHeavyWeaponData.Margin = new System.Windows.Forms.Padding(1, 1, 0, 0);
            this.tlpHeavyWeaponData.Name = "tlpHeavyWeaponData";
            this.tlpHeavyWeaponData.RowCount = 1;
            this.tlpHeavyWeaponData.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpHeavyWeaponData.Size = new System.Drawing.Size(91, 43);
            this.tlpHeavyWeaponData.TabIndex = 17;
            // 
            // btnResetHeavyWeaponData
            // 
            this.btnResetHeavyWeaponData.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.btnResetHeavyWeaponData.Location = new System.Drawing.Point(42, 10);
            this.btnResetHeavyWeaponData.Margin = new System.Windows.Forms.Padding(3, 3, 2, 3);
            this.btnResetHeavyWeaponData.Name = "btnResetHeavyWeaponData";
            this.btnResetHeavyWeaponData.Size = new System.Drawing.Size(47, 22);
            this.btnResetHeavyWeaponData.TabIndex = 10;
            this.btnResetHeavyWeaponData.Text = "built-in";
            this.btnResetHeavyWeaponData.UseVisualStyleBackColor = true;
            this.btnResetHeavyWeaponData.Click += new System.EventHandler(this.BtnResetHeavyWeaponData_Click);
            // 
            // tlpMain
            // 
            this.tlpMain.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tlpMain.AutoSize = true;
            this.tlpMain.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tlpMain.ColumnCount = 3;
            this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlpMain.Controls.Add(this.lblDefaultBG, 0, 1);
            this.tlpMain.Controls.Add(this.lblMenuStrip, 0, 0);
            this.tlpMain.Controls.Add(this.cbMenuStrip, 1, 0);
            this.tlpMain.Controls.Add(this.lblGlobalView, 0, 2);
            this.tlpMain.Controls.Add(this.lblFullRestore, 0, 3);
            this.tlpMain.Controls.Add(this.cbFullRestore, 1, 3);
            this.tlpMain.Controls.Add(this.cbGlobalView, 1, 2);
            this.tlpMain.Controls.Add(this.tlpDefaultBG, 1, 1);
            this.tlpMain.Controls.Add(this.tlpWeaponData, 1, 5);
            this.tlpMain.Controls.Add(this.tlpWeaponSprite, 1, 6);
            this.tlpMain.Controls.Add(this.tlpHeavyWeaponData, 1, 7);
            this.tlpMain.Controls.Add(this.tlpHeavyWeaponSprite, 1, 8);
            this.tlpMain.Controls.Add(this.lblHeavyWeaponSprite, 0, 8);
            this.tlpMain.Controls.Add(this.lblHeavyWeaponData, 0, 7);
            this.tlpMain.Controls.Add(this.lblWeaponSprite, 0, 6);
            this.tlpMain.Controls.Add(this.lblWeaponData, 0, 5);
            this.tlpMain.Controls.Add(this.lblHeavyWeaponSpriteFound, 2, 8);
            this.tlpMain.Controls.Add(this.lblHeavyWeaponDataFound, 2, 7);
            this.tlpMain.Controls.Add(this.lblWeaponSpriteFound, 2, 6);
            this.tlpMain.Controls.Add(this.lblDirect, 0, 4);
            this.tlpMain.Controls.Add(this.cbDirect, 1, 4);
            this.tlpMain.Controls.Add(this.lblWeaponDataFound, 2, 5);
            this.tlpMain.Controls.Add(this.lblDirectInformation, 2, 4);
            this.tlpMain.Controls.Add(this.lblSyncSelectedFrame, 0, 9);
            this.tlpMain.Controls.Add(this.cbSyncSelectedFrame, 1, 9);
            this.tlpMain.Location = new System.Drawing.Point(22, 22);
            this.tlpMain.Name = "tlpMain";
            this.tlpMain.RowCount = 10;
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpMain.Size = new System.Drawing.Size(276, 389);
            this.tlpMain.TabIndex = 2;
            // 
            // lblGlobalView
            // 
            this.lblGlobalView.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblGlobalView.AutoSize = true;
            this.lblGlobalView.Location = new System.Drawing.Point(3, 91);
            this.lblGlobalView.Margin = new System.Windows.Forms.Padding(3);
            this.lblGlobalView.Name = "lblGlobalView";
            this.lblGlobalView.Padding = new System.Windows.Forms.Padding(0, 6, 0, 6);
            this.lblGlobalView.Size = new System.Drawing.Size(86, 25);
            this.lblGlobalView.TabIndex = 3;
            this.lblGlobalView.Text = "Global View:";
            this.lblGlobalView.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblFullRestore
            // 
            this.lblFullRestore.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblFullRestore.AutoSize = true;
            this.lblFullRestore.Location = new System.Drawing.Point(3, 122);
            this.lblFullRestore.Margin = new System.Windows.Forms.Padding(3);
            this.lblFullRestore.Name = "lblFullRestore";
            this.lblFullRestore.Padding = new System.Windows.Forms.Padding(0, 6, 0, 6);
            this.lblFullRestore.Size = new System.Drawing.Size(86, 25);
            this.lblFullRestore.TabIndex = 4;
            this.lblFullRestore.Text = "Full Restore:";
            this.lblFullRestore.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cbFullRestore
            // 
            this.cbFullRestore.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbFullRestore.AutoSize = true;
            this.cbFullRestore.Location = new System.Drawing.Point(95, 127);
            this.cbFullRestore.Name = "cbFullRestore";
            this.cbFullRestore.Size = new System.Drawing.Size(86, 14);
            this.cbFullRestore.TabIndex = 7;
            this.cbFullRestore.UseVisualStyleBackColor = true;
            // 
            // cbGlobalView
            // 
            this.cbGlobalView.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbGlobalView.AutoSize = true;
            this.cbGlobalView.Location = new System.Drawing.Point(95, 96);
            this.cbGlobalView.Name = "cbGlobalView";
            this.cbGlobalView.Size = new System.Drawing.Size(86, 14);
            this.cbGlobalView.TabIndex = 6;
            this.cbGlobalView.UseVisualStyleBackColor = true;
            // 
            // tlpDefaultBG
            // 
            this.tlpDefaultBG.AutoSize = true;
            this.tlpDefaultBG.ColumnCount = 3;
            this.tlpDefaultBG.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpDefaultBG.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tlpDefaultBG.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tlpDefaultBG.Controls.Add(this.btnChangeDefaultBG, 1, 0);
            this.tlpDefaultBG.Controls.Add(this.picDefaultBGPreview, 0, 0);
            this.tlpDefaultBG.Controls.Add(this.btnImage, 2, 0);
            this.tlpDefaultBG.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpDefaultBG.Location = new System.Drawing.Point(93, 32);
            this.tlpDefaultBG.Margin = new System.Windows.Forms.Padding(1, 1, 0, 0);
            this.tlpDefaultBG.Name = "tlpDefaultBG";
            this.tlpDefaultBG.RowCount = 1;
            this.tlpDefaultBG.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpDefaultBG.Size = new System.Drawing.Size(91, 56);
            this.tlpDefaultBG.TabIndex = 11;
            // 
            // btnChangeDefaultBG
            // 
            this.btnChangeDefaultBG.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.btnChangeDefaultBG.Location = new System.Drawing.Point(-10, 17);
            this.btnChangeDefaultBG.Margin = new System.Windows.Forms.Padding(3, 0, 2, 0);
            this.btnChangeDefaultBG.Name = "btnChangeDefaultBG";
            this.btnChangeDefaultBG.Size = new System.Drawing.Size(26, 22);
            this.btnChangeDefaultBG.TabIndex = 10;
            this.btnChangeDefaultBG.Text = "...";
            this.btnChangeDefaultBG.UseVisualStyleBackColor = true;
            this.btnChangeDefaultBG.Click += new System.EventHandler(this.BtnChangeDefaultBG_Click);
            // 
            // picDefaultBGPreview
            // 
            this.picDefaultBGPreview.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.picDefaultBGPreview.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picDefaultBGPreview.Location = new System.Drawing.Point(2, 16);
            this.picDefaultBGPreview.Margin = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.picDefaultBGPreview.Name = "picDefaultBGPreview";
            this.picDefaultBGPreview.Size = new System.Drawing.Size(1, 23);
            this.picDefaultBGPreview.TabIndex = 11;
            this.picDefaultBGPreview.TabStop = false;
            // 
            // btnImage
            // 
            this.btnImage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.btnImage.Location = new System.Drawing.Point(21, 17);
            this.btnImage.Margin = new System.Windows.Forms.Padding(3, 0, 2, 0);
            this.btnImage.Name = "btnImage";
            this.btnImage.Size = new System.Drawing.Size(68, 22);
            this.btnImage.TabIndex = 12;
            this.btnImage.Text = "no color";
            this.btnImage.UseVisualStyleBackColor = true;
            this.btnImage.Click += new System.EventHandler(this.BtnImage_Click);
            // 
            // tlpWeaponData
            // 
            this.tlpWeaponData.AutoSize = true;
            this.tlpWeaponData.ColumnCount = 3;
            this.tlpWeaponData.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpWeaponData.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tlpWeaponData.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tlpWeaponData.Controls.Add(this.txtWeaponData, 0, 0);
            this.tlpWeaponData.Controls.Add(this.btnChangePathWeaponData, 1, 0);
            this.tlpWeaponData.Controls.Add(this.btnResetWeaponData, 2, 0);
            this.tlpWeaponData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpWeaponData.Location = new System.Drawing.Point(93, 196);
            this.tlpWeaponData.Margin = new System.Windows.Forms.Padding(1, 1, 0, 0);
            this.tlpWeaponData.Name = "tlpWeaponData";
            this.tlpWeaponData.RowCount = 1;
            this.tlpWeaponData.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpWeaponData.Size = new System.Drawing.Size(91, 30);
            this.tlpWeaponData.TabIndex = 13;
            // 
            // btnResetWeaponData
            // 
            this.btnResetWeaponData.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.btnResetWeaponData.Location = new System.Drawing.Point(42, 4);
            this.btnResetWeaponData.Margin = new System.Windows.Forms.Padding(3, 3, 2, 3);
            this.btnResetWeaponData.Name = "btnResetWeaponData";
            this.btnResetWeaponData.Size = new System.Drawing.Size(47, 22);
            this.btnResetWeaponData.TabIndex = 10;
            this.btnResetWeaponData.Text = "built-in";
            this.btnResetWeaponData.UseVisualStyleBackColor = true;
            this.btnResetWeaponData.Click += new System.EventHandler(this.BtnResetWeaponData_Click);
            // 
            // tlpHeavyWeaponSprite
            // 
            this.tlpHeavyWeaponSprite.AutoSize = true;
            this.tlpHeavyWeaponSprite.ColumnCount = 3;
            this.tlpHeavyWeaponSprite.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpHeavyWeaponSprite.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tlpHeavyWeaponSprite.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tlpHeavyWeaponSprite.Controls.Add(this.txtHeavyWeaponSprite, 0, 0);
            this.tlpHeavyWeaponSprite.Controls.Add(this.btnChangePathHWeaponSprite, 1, 0);
            this.tlpHeavyWeaponSprite.Controls.Add(this.btnResetHeavyWeaponSprite, 2, 0);
            this.tlpHeavyWeaponSprite.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpHeavyWeaponSprite.Location = new System.Drawing.Point(93, 302);
            this.tlpHeavyWeaponSprite.Margin = new System.Windows.Forms.Padding(1, 1, 0, 0);
            this.tlpHeavyWeaponSprite.Name = "tlpHeavyWeaponSprite";
            this.tlpHeavyWeaponSprite.RowCount = 1;
            this.tlpHeavyWeaponSprite.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpHeavyWeaponSprite.Size = new System.Drawing.Size(91, 43);
            this.tlpHeavyWeaponSprite.TabIndex = 18;
            // 
            // btnResetHeavyWeaponSprite
            // 
            this.btnResetHeavyWeaponSprite.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.btnResetHeavyWeaponSprite.Location = new System.Drawing.Point(42, 10);
            this.btnResetHeavyWeaponSprite.Margin = new System.Windows.Forms.Padding(3, 3, 2, 3);
            this.btnResetHeavyWeaponSprite.Name = "btnResetHeavyWeaponSprite";
            this.btnResetHeavyWeaponSprite.Size = new System.Drawing.Size(47, 22);
            this.btnResetHeavyWeaponSprite.TabIndex = 10;
            this.btnResetHeavyWeaponSprite.Text = "built-in";
            this.btnResetHeavyWeaponSprite.UseVisualStyleBackColor = true;
            this.btnResetHeavyWeaponSprite.Click += new System.EventHandler(this.BtnResetHeavyWeaponSprite_Click);
            // 
            // lblHeavyWeaponSprite
            // 
            this.lblHeavyWeaponSprite.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblHeavyWeaponSprite.AutoSize = true;
            this.lblHeavyWeaponSprite.Location = new System.Drawing.Point(3, 304);
            this.lblHeavyWeaponSprite.Margin = new System.Windows.Forms.Padding(3);
            this.lblHeavyWeaponSprite.Name = "lblHeavyWeaponSprite";
            this.lblHeavyWeaponSprite.Padding = new System.Windows.Forms.Padding(0, 6, 0, 6);
            this.lblHeavyWeaponSprite.Size = new System.Drawing.Size(86, 38);
            this.lblHeavyWeaponSprite.TabIndex = 14;
            this.lblHeavyWeaponSprite.Text = "Heavy Weapon Sprite:";
            this.lblHeavyWeaponSprite.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblHeavyWeaponData
            // 
            this.lblHeavyWeaponData.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblHeavyWeaponData.AutoSize = true;
            this.lblHeavyWeaponData.Location = new System.Drawing.Point(3, 260);
            this.lblHeavyWeaponData.Margin = new System.Windows.Forms.Padding(3);
            this.lblHeavyWeaponData.Name = "lblHeavyWeaponData";
            this.lblHeavyWeaponData.Padding = new System.Windows.Forms.Padding(0, 6, 0, 6);
            this.lblHeavyWeaponData.Size = new System.Drawing.Size(86, 38);
            this.lblHeavyWeaponData.TabIndex = 16;
            this.lblHeavyWeaponData.Text = "Heavy Weapon Data:";
            this.lblHeavyWeaponData.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblWeaponSprite
            // 
            this.lblWeaponSprite.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblWeaponSprite.AutoSize = true;
            this.lblWeaponSprite.Location = new System.Drawing.Point(3, 229);
            this.lblWeaponSprite.Margin = new System.Windows.Forms.Padding(3);
            this.lblWeaponSprite.Name = "lblWeaponSprite";
            this.lblWeaponSprite.Padding = new System.Windows.Forms.Padding(0, 6, 0, 6);
            this.lblWeaponSprite.Size = new System.Drawing.Size(86, 25);
            this.lblWeaponSprite.TabIndex = 9;
            this.lblWeaponSprite.Text = "Weapon Sprite:";
            this.lblWeaponSprite.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblWeaponData
            // 
            this.lblWeaponData.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblWeaponData.AutoSize = true;
            this.lblWeaponData.Location = new System.Drawing.Point(3, 198);
            this.lblWeaponData.Margin = new System.Windows.Forms.Padding(3);
            this.lblWeaponData.Name = "lblWeaponData";
            this.lblWeaponData.Padding = new System.Windows.Forms.Padding(0, 6, 0, 6);
            this.lblWeaponData.Size = new System.Drawing.Size(86, 25);
            this.lblWeaponData.TabIndex = 5;
            this.lblWeaponData.Text = "Weapon Data:";
            this.lblWeaponData.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblHeavyWeaponSpriteFound
            // 
            this.lblHeavyWeaponSpriteFound.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblHeavyWeaponSpriteFound.AutoSize = true;
            this.lblHeavyWeaponSpriteFound.ForeColor = System.Drawing.Color.DarkRed;
            this.lblHeavyWeaponSpriteFound.Location = new System.Drawing.Point(187, 316);
            this.lblHeavyWeaponSpriteFound.Margin = new System.Windows.Forms.Padding(3);
            this.lblHeavyWeaponSpriteFound.Name = "lblHeavyWeaponSpriteFound";
            this.lblHeavyWeaponSpriteFound.Size = new System.Drawing.Size(86, 13);
            this.lblHeavyWeaponSpriteFound.TabIndex = 21;
            this.lblHeavyWeaponSpriteFound.Text = "File not found!";
            // 
            // lblHeavyWeaponDataFound
            // 
            this.lblHeavyWeaponDataFound.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblHeavyWeaponDataFound.AutoSize = true;
            this.lblHeavyWeaponDataFound.ForeColor = System.Drawing.Color.DarkRed;
            this.lblHeavyWeaponDataFound.Location = new System.Drawing.Point(187, 272);
            this.lblHeavyWeaponDataFound.Margin = new System.Windows.Forms.Padding(3);
            this.lblHeavyWeaponDataFound.Name = "lblHeavyWeaponDataFound";
            this.lblHeavyWeaponDataFound.Size = new System.Drawing.Size(86, 13);
            this.lblHeavyWeaponDataFound.TabIndex = 21;
            this.lblHeavyWeaponDataFound.Text = "File not found!";
            // 
            // lblWeaponSpriteFound
            // 
            this.lblWeaponSpriteFound.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblWeaponSpriteFound.AutoSize = true;
            this.lblWeaponSpriteFound.ForeColor = System.Drawing.Color.DarkRed;
            this.lblWeaponSpriteFound.Location = new System.Drawing.Point(187, 235);
            this.lblWeaponSpriteFound.Margin = new System.Windows.Forms.Padding(3);
            this.lblWeaponSpriteFound.Name = "lblWeaponSpriteFound";
            this.lblWeaponSpriteFound.Size = new System.Drawing.Size(86, 13);
            this.lblWeaponSpriteFound.TabIndex = 20;
            this.lblWeaponSpriteFound.Text = "File not found!";
            // 
            // lblDirect
            // 
            this.lblDirect.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDirect.AutoSize = true;
            this.lblDirect.Location = new System.Drawing.Point(3, 153);
            this.lblDirect.Margin = new System.Windows.Forms.Padding(3);
            this.lblDirect.Name = "lblDirect";
            this.lblDirect.Padding = new System.Windows.Forms.Padding(0, 6, 0, 6);
            this.lblDirect.Size = new System.Drawing.Size(86, 38);
            this.lblDirect.TabIndex = 5;
            this.lblDirect.Text = "Runtime text editing:";
            this.lblDirect.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cbDirect
            // 
            this.cbDirect.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbDirect.AutoSize = true;
            this.cbDirect.Location = new System.Drawing.Point(95, 165);
            this.cbDirect.Name = "cbDirect";
            this.cbDirect.Size = new System.Drawing.Size(86, 14);
            this.cbDirect.TabIndex = 7;
            this.cbDirect.UseVisualStyleBackColor = true;
            // 
            // lblWeaponDataFound
            // 
            this.lblWeaponDataFound.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblWeaponDataFound.AutoSize = true;
            this.lblWeaponDataFound.ForeColor = System.Drawing.Color.DarkRed;
            this.lblWeaponDataFound.Location = new System.Drawing.Point(187, 204);
            this.lblWeaponDataFound.Margin = new System.Windows.Forms.Padding(3);
            this.lblWeaponDataFound.Name = "lblWeaponDataFound";
            this.lblWeaponDataFound.Size = new System.Drawing.Size(86, 13);
            this.lblWeaponDataFound.TabIndex = 19;
            this.lblWeaponDataFound.Text = "File not found!";
            // 
            // lblDirectInformation
            // 
            this.lblDirectInformation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDirectInformation.AutoSize = true;
            this.lblDirectInformation.Location = new System.Drawing.Point(187, 153);
            this.lblDirectInformation.Margin = new System.Windows.Forms.Padding(3);
            this.lblDirectInformation.Name = "lblDirectInformation";
            this.lblDirectInformation.Size = new System.Drawing.Size(86, 39);
            this.lblDirectInformation.TabIndex = 5;
            this.lblDirectInformation.Text = "Perhaps long undo/redo duration!";
            this.lblDirectInformation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSyncSelectedFrame
            // 
            this.lblSyncSelectedFrame.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSyncSelectedFrame.AutoSize = true;
            this.lblSyncSelectedFrame.Location = new System.Drawing.Point(3, 348);
            this.lblSyncSelectedFrame.Margin = new System.Windows.Forms.Padding(3);
            this.lblSyncSelectedFrame.Name = "lblSyncSelectedFrame";
            this.lblSyncSelectedFrame.Padding = new System.Windows.Forms.Padding(0, 6, 0, 6);
            this.lblSyncSelectedFrame.Size = new System.Drawing.Size(86, 38);
            this.lblSyncSelectedFrame.TabIndex = 14;
            this.lblSyncSelectedFrame.Text = "Synchronize selected frame:";
            this.lblSyncSelectedFrame.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cbSyncSelectedFrame
            // 
            this.cbSyncSelectedFrame.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbSyncSelectedFrame.AutoSize = true;
            this.cbSyncSelectedFrame.Location = new System.Drawing.Point(95, 360);
            this.cbSyncSelectedFrame.Name = "cbSyncSelectedFrame";
            this.cbSyncSelectedFrame.Size = new System.Drawing.Size(86, 14);
            this.cbSyncSelectedFrame.TabIndex = 7;
            this.cbSyncSelectedFrame.UseVisualStyleBackColor = true;
            // 
            // UCSettingsFrameViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Controls.Add(this.tlpMain);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.MinimumSize = new System.Drawing.Size(300, 0);
            this.Name = "UCSettingsFrameViewer";
            this.Padding = new System.Windows.Forms.Padding(16);
            this.Size = new System.Drawing.Size(300, 430);
            this.tlpWeaponSprite.ResumeLayout(false);
            this.tlpWeaponSprite.PerformLayout();
            this.tlpHeavyWeaponData.ResumeLayout(false);
            this.tlpHeavyWeaponData.PerformLayout();
            this.tlpMain.ResumeLayout(false);
            this.tlpMain.PerformLayout();
            this.tlpDefaultBG.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picDefaultBGPreview)).EndInit();
            this.tlpWeaponData.ResumeLayout(false);
            this.tlpWeaponData.PerformLayout();
            this.tlpHeavyWeaponSprite.ResumeLayout(false);
            this.tlpHeavyWeaponSprite.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        internal System.Windows.Forms.OpenFileDialog ofdSprite;
        internal System.Windows.Forms.Label lblDefaultBG;
        internal System.Windows.Forms.Label lblMenuStrip;
        internal System.Windows.Forms.TextBox txtHeavyWeaponSprite;
        internal System.Windows.Forms.Button btnChangePathHWeaponSprite;
        internal System.Windows.Forms.TextBox txtHeavyWeaponData;
        internal System.Windows.Forms.Button btnChangePathHWeaponData;
        internal System.Windows.Forms.OpenFileDialog ofdData;
        internal System.Windows.Forms.TextBox txtWeaponSprite;
        internal System.Windows.Forms.Button btnChangePathWeaponSprite;
        internal System.Windows.Forms.TextBox txtWeaponData;
        internal System.Windows.Forms.Button btnChangePathWeaponData;
        internal System.Windows.Forms.CheckBox cbMenuStrip;
        internal System.Windows.Forms.TableLayoutPanel tlpWeaponSprite;
        internal System.Windows.Forms.TableLayoutPanel tlpHeavyWeaponData;
        internal System.Windows.Forms.TableLayoutPanel tlpMain;
        internal System.Windows.Forms.Label lblGlobalView;
        internal System.Windows.Forms.Label lblFullRestore;
        internal System.Windows.Forms.CheckBox cbFullRestore;
        internal System.Windows.Forms.CheckBox cbGlobalView;
        internal System.Windows.Forms.TableLayoutPanel tlpDefaultBG;
        internal System.Windows.Forms.Button btnChangeDefaultBG;
        internal System.Windows.Forms.PictureBox picDefaultBGPreview;
        internal System.Windows.Forms.Button btnImage;
        internal System.Windows.Forms.TableLayoutPanel tlpWeaponData;
        internal System.Windows.Forms.TableLayoutPanel tlpHeavyWeaponSprite;
        internal System.Windows.Forms.Label lblHeavyWeaponSprite;
        internal System.Windows.Forms.Label lblHeavyWeaponData;
        internal System.Windows.Forms.Label lblWeaponSprite;
        internal System.Windows.Forms.Label lblWeaponData;
        internal System.Windows.Forms.Label lblHeavyWeaponSpriteFound;
        internal System.Windows.Forms.Label lblHeavyWeaponDataFound;
        internal System.Windows.Forms.Label lblWeaponSpriteFound;
        internal System.Windows.Forms.Label lblDirect;
        internal System.Windows.Forms.CheckBox cbDirect;
        internal System.Windows.Forms.Label lblWeaponDataFound;
        internal System.Windows.Forms.Label lblDirectInformation;
        internal System.Windows.Forms.Label lblSyncSelectedFrame;
        internal System.Windows.Forms.CheckBox cbSyncSelectedFrame;
        internal System.Windows.Forms.Button btnResetWeaponSprite;
        internal System.Windows.Forms.Button btnResetHeavyWeaponData;
        internal System.Windows.Forms.Button btnResetWeaponData;
        internal System.Windows.Forms.Button btnResetHeavyWeaponSprite;
    }
}
