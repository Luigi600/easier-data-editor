﻿
namespace Easier_Data_Editor.Windows.Settings
{
    partial class UCSettingsAdvanced
    {
        /// <summary> 
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Komponenten-Designer generierter Code

        /// <summary> 
        /// Erforderliche Methode für die Designerunterstützung. 
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.tlpMain = new System.Windows.Forms.TableLayoutPanel();
            this.lblEncryptionDetectionFromContent = new System.Windows.Forms.Label();
            this.lblResetRecentFiles = new System.Windows.Forms.Label();
            this.lblResetLayout = new System.Windows.Forms.Label();
            this.btnResetRecentFiles = new System.Windows.Forms.Button();
            this.btnModifyRecentFiles = new System.Windows.Forms.Button();
            this.lblSettings = new System.Windows.Forms.Label();
            this.btnResetLayout = new System.Windows.Forms.Button();
            this.btnResetSettings = new System.Windows.Forms.Button();
            this.cbEncryptionDetectionFromContent = new System.Windows.Forms.CheckBox();
            this.lblEncryptionDetectionFromContentInformation = new System.Windows.Forms.Label();
            this.tlpMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // tlpMain
            // 
            this.tlpMain.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tlpMain.AutoSize = true;
            this.tlpMain.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tlpMain.ColumnCount = 3;
            this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlpMain.Controls.Add(this.lblEncryptionDetectionFromContent, 0, 3);
            this.tlpMain.Controls.Add(this.lblResetRecentFiles, 0, 2);
            this.tlpMain.Controls.Add(this.lblResetLayout, 0, 1);
            this.tlpMain.Controls.Add(this.btnResetRecentFiles, 1, 2);
            this.tlpMain.Controls.Add(this.btnModifyRecentFiles, 2, 2);
            this.tlpMain.Controls.Add(this.lblSettings, 0, 0);
            this.tlpMain.Controls.Add(this.btnResetLayout, 1, 1);
            this.tlpMain.Controls.Add(this.btnResetSettings, 1, 0);
            this.tlpMain.Controls.Add(this.cbEncryptionDetectionFromContent, 1, 3);
            this.tlpMain.Controls.Add(this.lblEncryptionDetectionFromContentInformation, 2, 3);
            this.tlpMain.Location = new System.Drawing.Point(22, 22);
            this.tlpMain.Name = "tlpMain";
            this.tlpMain.RowCount = 4;
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpMain.Size = new System.Drawing.Size(234, 163);
            this.tlpMain.TabIndex = 3;
            // 
            // lblEncryptionDetectionFromContent
            // 
            this.lblEncryptionDetectionFromContent.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblEncryptionDetectionFromContent.AutoSize = true;
            this.lblEncryptionDetectionFromContent.Location = new System.Drawing.Point(3, 102);
            this.lblEncryptionDetectionFromContent.Margin = new System.Windows.Forms.Padding(3);
            this.lblEncryptionDetectionFromContent.Name = "lblEncryptionDetectionFromContent";
            this.lblEncryptionDetectionFromContent.Padding = new System.Windows.Forms.Padding(0, 6, 0, 6);
            this.lblEncryptionDetectionFromContent.Size = new System.Drawing.Size(72, 51);
            this.lblEncryptionDetectionFromContent.TabIndex = 2;
            this.lblEncryptionDetectionFromContent.Text = "Encryption detection from content:";
            this.lblEncryptionDetectionFromContent.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblResetRecentFiles
            // 
            this.lblResetRecentFiles.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblResetRecentFiles.AutoSize = true;
            this.lblResetRecentFiles.Location = new System.Drawing.Point(3, 65);
            this.lblResetRecentFiles.Margin = new System.Windows.Forms.Padding(3);
            this.lblResetRecentFiles.Name = "lblResetRecentFiles";
            this.lblResetRecentFiles.Padding = new System.Windows.Forms.Padding(0, 6, 0, 6);
            this.lblResetRecentFiles.Size = new System.Drawing.Size(72, 25);
            this.lblResetRecentFiles.TabIndex = 0;
            this.lblResetRecentFiles.Text = "Recent files:";
            this.lblResetRecentFiles.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblResetLayout
            // 
            this.lblResetLayout.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblResetLayout.AutoSize = true;
            this.lblResetLayout.Location = new System.Drawing.Point(3, 34);
            this.lblResetLayout.Margin = new System.Windows.Forms.Padding(3);
            this.lblResetLayout.Name = "lblResetLayout";
            this.lblResetLayout.Padding = new System.Windows.Forms.Padding(0, 6, 0, 6);
            this.lblResetLayout.Size = new System.Drawing.Size(72, 25);
            this.lblResetLayout.TabIndex = 0;
            this.lblResetLayout.Text = "Layout:";
            this.lblResetLayout.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnResetRecentFiles
            // 
            this.btnResetRecentFiles.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnResetRecentFiles.Location = new System.Drawing.Point(81, 66);
            this.btnResetRecentFiles.Name = "btnResetRecentFiles";
            this.btnResetRecentFiles.Size = new System.Drawing.Size(71, 22);
            this.btnResetRecentFiles.TabIndex = 1;
            this.btnResetRecentFiles.Text = "reset";
            this.btnResetRecentFiles.UseVisualStyleBackColor = true;
            this.btnResetRecentFiles.Click += new System.EventHandler(this.BtnResetRecentFiles_Click);
            // 
            // btnModifyRecentFiles
            // 
            this.btnModifyRecentFiles.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnModifyRecentFiles.Location = new System.Drawing.Point(159, 66);
            this.btnModifyRecentFiles.Name = "btnModifyRecentFiles";
            this.btnModifyRecentFiles.Size = new System.Drawing.Size(72, 22);
            this.btnModifyRecentFiles.TabIndex = 1;
            this.btnModifyRecentFiles.Text = "modify";
            this.btnModifyRecentFiles.UseVisualStyleBackColor = true;
            this.btnModifyRecentFiles.Click += new System.EventHandler(this.BtnModifyRecentFiles_Click);
            // 
            // lblSettings
            // 
            this.lblSettings.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSettings.AutoSize = true;
            this.lblSettings.Location = new System.Drawing.Point(3, 3);
            this.lblSettings.Margin = new System.Windows.Forms.Padding(3);
            this.lblSettings.Name = "lblSettings";
            this.lblSettings.Padding = new System.Windows.Forms.Padding(0, 6, 0, 6);
            this.lblSettings.Size = new System.Drawing.Size(72, 25);
            this.lblSettings.TabIndex = 0;
            this.lblSettings.Text = "Settings:";
            this.lblSettings.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnResetLayout
            // 
            this.btnResetLayout.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnResetLayout.Location = new System.Drawing.Point(81, 35);
            this.btnResetLayout.Name = "btnResetLayout";
            this.btnResetLayout.Size = new System.Drawing.Size(71, 22);
            this.btnResetLayout.TabIndex = 1;
            this.btnResetLayout.Text = "reset";
            this.btnResetLayout.UseVisualStyleBackColor = true;
            this.btnResetLayout.Click += new System.EventHandler(this.BtnResetLayout_Click);
            // 
            // btnResetSettings
            // 
            this.btnResetSettings.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnResetSettings.Location = new System.Drawing.Point(81, 4);
            this.btnResetSettings.Name = "btnResetSettings";
            this.btnResetSettings.Size = new System.Drawing.Size(71, 22);
            this.btnResetSettings.TabIndex = 1;
            this.btnResetSettings.Text = "reset";
            this.btnResetSettings.UseVisualStyleBackColor = true;
            this.btnResetSettings.Click += new System.EventHandler(this.BtnResetSettings_Click);
            // 
            // cbEncryptionDetectionFromContent
            // 
            this.cbEncryptionDetectionFromContent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cbEncryptionDetectionFromContent.Location = new System.Drawing.Point(81, 96);
            this.cbEncryptionDetectionFromContent.Name = "cbEncryptionDetectionFromContent";
            this.cbEncryptionDetectionFromContent.Size = new System.Drawing.Size(72, 64);
            this.cbEncryptionDetectionFromContent.TabIndex = 3;
            this.cbEncryptionDetectionFromContent.UseVisualStyleBackColor = true;
            // 
            // lblEncryptionDetectionFromContentInformation
            // 
            this.lblEncryptionDetectionFromContentInformation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblEncryptionDetectionFromContentInformation.AutoSize = true;
            this.lblEncryptionDetectionFromContentInformation.Location = new System.Drawing.Point(159, 96);
            this.lblEncryptionDetectionFromContentInformation.Margin = new System.Windows.Forms.Padding(3);
            this.lblEncryptionDetectionFromContentInformation.Name = "lblEncryptionDetectionFromContentInformation";
            this.lblEncryptionDetectionFromContentInformation.Padding = new System.Windows.Forms.Padding(0, 6, 0, 6);
            this.lblEncryptionDetectionFromContentInformation.Size = new System.Drawing.Size(72, 64);
            this.lblEncryptionDetectionFromContentInformation.TabIndex = 4;
            this.lblEncryptionDetectionFromContentInformation.Text = "Otherwise the file extension will be used as detection";
            this.lblEncryptionDetectionFromContentInformation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // UCSettingsAdvanced
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Controls.Add(this.tlpMain);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.MinimumSize = new System.Drawing.Size(300, 0);
            this.Name = "UCSettingsAdvanced";
            this.Padding = new System.Windows.Forms.Padding(16);
            this.Size = new System.Drawing.Size(300, 204);
            this.tlpMain.ResumeLayout(false);
            this.tlpMain.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.TableLayoutPanel tlpMain;
        internal System.Windows.Forms.Label lblResetLayout;
        internal System.Windows.Forms.Label lblResetRecentFiles;
        private System.Windows.Forms.Button btnResetLayout;
        private System.Windows.Forms.Button btnResetRecentFiles;
        private System.Windows.Forms.Button btnModifyRecentFiles;
        internal System.Windows.Forms.Label lblSettings;
        private System.Windows.Forms.Button btnResetSettings;
        internal System.Windows.Forms.Label lblEncryptionDetectionFromContent;
        private System.Windows.Forms.CheckBox cbEncryptionDetectionFromContent;
        internal System.Windows.Forms.Label lblEncryptionDetectionFromContentInformation;
    }
}
