﻿using System;
using System.Diagnostics;
using System.Windows.Forms;
using Easier_Data_Editor.Extensions.MultiLangSupport;
using Easier.MultiLangSupport;
using Easier.MultiLangSupport.Environments;

namespace Easier_Data_Editor.Windows.Settings
{
    public partial class UCSettingsGeneral : UCSettingsBase
    {
        private readonly Environments.Settings.General _settings;
        private bool _isUpdatingLanguages = false;

        public UCSettingsGeneral(Environments.Settings.General settings)
            : base()
        {
            this._settings = settings;
            this.VariablesList.Add("DISPLAY_NAME", "General");

            InitializeComponent();

            this.SetBindings();
            this.InitLanguageSelectionAndSetEvents();
            this.AfterControlsInit();
        }

        // necessary for the designer (private) and translation creation (public)
        public UCSettingsGeneral()
            : this(new Environments.Settings.General()) { }

        protected override void SetNotTranslatableThings()
        {
            this.NotTranslatableControlsList.Add(this.combLanguage);
            this.NotTranslatableControlsList.Add(this.combNewlineChar);
        }

        protected override Control MainControl() => this.tlpMain;

        private void SetBindings()
        {
            this.nudRecentFiles.DataBindings.Add(
                Constants.BINDING_NUMERIC_UP_DOWN_VALUE,
                this._settings,
                "RecentFilesCapacity",
                true,
                DataSourceUpdateMode.OnPropertyChanged
            );

            this.cbKeepSession.DataBindings.Add(
                Constants.BINDING_CHECK_BOX_VALUE,
                this._settings,
                "KeepSession",
                true,
                DataSourceUpdateMode.OnPropertyChanged
            );

            this.cbMultipleInstances.DataBindings.Add(
                Constants.BINDING_CHECK_BOX_VALUE,
                this._settings,
                "MultipleInstances",
                true,
                DataSourceUpdateMode.OnPropertyChanged
            );

            this.cbMultipleFileOpening.DataBindings.Add(
                Constants.BINDING_CHECK_BOX_VALUE,
                this._settings,
                "MultipleFileOpening",
                true,
                DataSourceUpdateMode.OnPropertyChanged
            );

            this.cbChangeFrameViewer.DataBindings.Add(
                Constants.BINDING_CHECK_BOX_VALUE,
                this._settings,
                "AutoSwitchToFrameViewer",
                true,
                DataSourceUpdateMode.OnPropertyChanged
            );

            this.combNewlineChar.DataBindings.Add(
                Constants.BINDING_COMBO_BOX_SELECTED_INDEX,
                this._settings,
                "NewlineCharacterBindingFriendly",
                true,
                DataSourceUpdateMode.OnPropertyChanged
            );
        }

        private void InitLanguageSelectionAndSetEvents()
        {
            this.SetLanguageItems();
            int index = GetLanguageComboIndexFromFileName(this._settings.Language);
            this.combLanguage.SelectedIndex = index < 0 ? 0 : index;

            this.combLanguage.SelectedIndexChanged += this.CombLanguage_SelectedIndexChanged;
            Translation.AvailableLanguagesChanged += this.Translation_AvailableLanguagesChanged;
        }

        private void SetLanguageItems()
        {
            this.combLanguage.Items.Clear();
            this.combLanguage.Items.Add(Translation.DefaultLanguage);
            foreach (Language lang in Translation.GetLanguages())
                this.combLanguage.Items.Add(lang);
        }

        private int GetLanguageComboIndexFromFileName(string fileName)
        {
            for (int i = 0; i < this.combLanguage.Items.Count; ++i)
            {
                if (
                    this.combLanguage.Items[i] is LanguageWithFile lang
                    && lang.FileName.Equals(fileName)
                )
                    return i;
            }

            return -1;
        }

        private void Translation_AvailableLanguagesChanged(object? sender, EventArgs e)
        {
            this._isUpdatingLanguages = true;

            this.SetLanguageItems();
            int index = GetLanguageComboIndexFromFileName(this._settings.Language);
            this.combLanguage.SelectedIndex = index < 0 ? 0 : index;

            this._isUpdatingLanguages = false;
        }

        private void CombLanguage_SelectedIndexChanged(object? sender, EventArgs e)
        {
            if (this._isUpdatingLanguages)
                return;

            Language? lang = (Language?)this.combLanguage.SelectedItem;
            if (lang == null)
                return;

            this._settings.SetLanguage(lang);
            Translation.SelectedLanguage = lang;
        }

        private void BtnOpenLangDirectory_Click(object sender, EventArgs e) =>
            Process.Start(
                new ProcessStartInfo(Environments.Application.LANGUAGE_DIRECTORY_PATH)
                {
                    UseShellExecute = true,
                }
            );
    }
}
