﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using Easier_Data_Editor.Environments;
using Easier_Data_Editor.Environments.FormatBlueprints;
using Easier_Data_Editor.Extensions.MultiLangSupport;
using Easier_Data_Editor.Serializers;
using Easier.MultiLangSupport;
using Easier.MultiLangSupport.Environments;
using Easier.MultiLangSupport.Forms;

namespace Easier_Data_Editor.Windows.Settings
{
    public partial class Settings : TranslatableForm
    {
        private readonly UserSettings _originalUserSettings;
        private readonly UserSettings _userSettings;
        private readonly Blueprints _originalBlueprints;
        private readonly Blueprints _blueprints;
        private readonly List<UCSettingsBase> _settingPages = new List<UCSettingsBase>();
        private readonly Padding _settingPagePadding = new Padding(16);
        private int _selectedIndex = -1;

        public Settings(
            UserSettings userSettings,
            Blueprints blueprints,
            string? startSettingPage = null
        )
        {
            this._originalUserSettings = userSettings;
            this._userSettings = (UserSettings)userSettings.Clone();
            this._originalBlueprints = blueprints;
            this._blueprints = (Blueprints)blueprints.Clone();
            this._userSettings.SettingsChanged += (_, __) => this.CheckSettingsEqualationState();
            this._userSettings.GeneralSettings.RecentFiles.ItemAdded += (_, __) =>
                this.CheckSettingsEqualationState();
            this._userSettings.GeneralSettings.RecentFiles.ItemRemoved += (_, __) =>
                this.CheckSettingsEqualationState();
            this._userSettings.GeneralSettings.RecentFiles.ItemsCleared += (_, __) =>
                this.CheckSettingsEqualationState();

            this._blueprints.BlueprintsChanged += (_, __) => this.CheckSettingsEqualationState();

            Translation.LanguageReplaced += this.Translation_LanguageReplaced;

            this.VariablesList.Add(
                "SAVE_TITLE",
                $"{Environments.Application.APP_TITLE}    -   Save settings?"
            );
            this.VariablesList.Add("SAVE", "Do you want save the settings?");
            this.VariablesList.Add(
                "SAVED_TITLE",
                $"{Environments.Application.APP_TITLE}    -   Settings saved?"
            );
            this.VariablesList.Add("SAVED", "Settings successfully applied and saved.");

            UCSettingsAdvanced advanced = new UCSettingsAdvanced(
                ref this._originalUserSettings,
                ref this._userSettings
            );
            advanced.WillSaveSettings += (sender, args) => ApplyAndSaveSettings();
            this._settingPages.Add(new UCSettingsGeneral(this._userSettings.GeneralSettings));
            this._settingPages.Add(new UCSettingsTextEditor(this._userSettings.TextEditorSettings));
            this._settingPages.Add(
                new UCSettingsTextEditorReformatter(
                    this._blueprints,
                    this._userSettings.TextEditorSettings
                )
            );
            this._settingPages.Add(
                new UCSettingsFrameViewer(this._userSettings.FrameViewerSettings)
            );
            this._settingPages.Add(advanced);

            InitializeComponent();

            this.btnApply.Enabled = false;
            Translation.ApplicationLanguageChanged +=
                this.TranslationEnvironment_ApplicationLanguageChanged;
            this.RefreshNodeNames();
            this.AfterControlsInit();

            int pageIndex =
                startSettingPage != null
                    ? this._settingPages.FindIndex(p => p.GetType().Name.Equals(startSettingPage))
                    : 0;
            this.SetStartPage(pageIndex);
        }

        // for translatable lib
        private Settings()
            : this(Environments.Application.UserSettings, new Blueprints()) { }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            if (this.HasChanges())
            {
                DialogResult buttonResult = MessageBox.Show(
                    GetVariable("SAVE"),
                    GetVariable("SAVE_TITLE"),
                    MessageBoxButtons.YesNoCancel,
                    MessageBoxIcon.Question
                );
                if (buttonResult == DialogResult.Cancel)
                    e.Cancel = true;
                else if (buttonResult == DialogResult.No)
                    CancelSettings();
                else
                    this.ApplyAndSaveSettings();
            }

            base.OnFormClosing(e);
        }

        protected override void OnFormClosed(FormClosedEventArgs e)
        {
            base.OnFormClosed(e);

            Translation.ApplicationLanguageChanged -=
                this.TranslationEnvironment_ApplicationLanguageChanged;
        }

        protected override void SetNotTranslatableThings()
        {
            this.NotTranslatableControlsRecursiveList.Add(this.treeSettings);
        }

        #region Events
        private void TranslationEnvironment_ApplicationLanguageChanged(
            object? sender,
            System.EventArgs e
        ) => this.RefreshNodeNames();

        private void Translation_LanguageReplaced(object? sender, LanguageReplaceEventArgs e)
        {
            if (
                e.OldLanguage is LanguageWithFile oldLangWithFile
                && e.NewLanguage is LanguageWithFile newLangWithFile
            )
            {
                if (
                    this._originalUserSettings.GeneralSettings.Language.Equals(
                        oldLangWithFile.FileName
                    )
                )
                    this._originalUserSettings.GeneralSettings.Language = newLangWithFile.FileName;

                if (this._userSettings.GeneralSettings.Language.Equals(oldLangWithFile.FileName))
                    this._userSettings.GeneralSettings.Language = newLangWithFile.FileName;
            }
        }

        private void CheckSettingsEqualationState() => this.btnApply.Enabled = this.HasChanges();

        private void BtnSave_Click(object sender, System.EventArgs e)
        {
            this.ApplyAndSaveSettings();
            this.Close();
        }

        private void TreeSettings_AfterSelect(object sender, TreeViewEventArgs e)
        {
            int index = e.Node is null ? -1 : this.GetOrderedNodes().IndexOf(e.Node);

            if (index >= 0)
                this.SetSelectedPageIndex(index);
        }

        private void BtnApply_Click(object sender, System.EventArgs e)
        {
            this.ApplyAndSaveSettings();
            MessageBox.Show(
                GetVariable("SAVED"),
                GetVariable("SAVED_TITLE"),
                MessageBoxButtons.OK,
                MessageBoxIcon.Information
            );
        }

        private void BtnCancel_Click(object sender, System.EventArgs e)
        {
            this.CancelSettings();
            // work around to suppress "do you want save" question
            this._userSettings.Apply(this._originalUserSettings);
            this._blueprints.Apply(this._originalBlueprints);
            this.Close();
        }
        #endregion

        private void SetStartPage(int index)
        {
            List<TreeNode> nodes = this.GetOrderedNodes();
            if (index < 0)
                index = 0;
            else if (index >= nodes.Count)
                index = nodes.Count - 1;

            this.treeSettings.SelectedNode = nodes[index];
        }

        private void SetSelectedPageIndex(int value)
        {
            if (value >= this._settingPages.Count)
                value = this._settingPages.Count - 1;
            else if (value < 0)
                value = 0;

            if (this._selectedIndex == value)
                return;

            this._selectedIndex = value;
            this.palSettingPage.Controls.Clear();
            this._settingPages[this._selectedIndex].Dock = DockStyle.Fill;
            this._settingPages[this._selectedIndex].SetMainLayout(this._settingPagePadding);
            this.palSettingPage.Controls.Add(this._settingPages[this._selectedIndex]);
        }

        private void RefreshNodeNames()
        {
            if (this.treeSettings == null)
                return; // translation can be too fast???

            this.treeSettings.Nodes.Clear();
            TreeNode? lastNode = null;
            foreach (UCSettingsBase settingPage in this._settingPages)
            {
                TreeNode newNode = new TreeNode(settingPage.GetDisplayName());
                if (settingPage.IsChildOfPreviousElement && lastNode != null)
                {
                    lastNode.Nodes.Add(newNode);
                }
                else
                {
                    this.treeSettings.Nodes.Add(newNode);
                    lastNode = newNode;
                }
            }

            this.treeSettings.ExpandAll();
        }

        private void CancelSettings()
        {
            Translation.SelectedLanguage =
                Environments.Application.GetLanguageFromFileName(
                    this._originalUserSettings.GeneralSettings.Language
                ) ?? Translation.DefaultLanguage;
        }

        private void ApplyAndSaveSettings()
        {
            this._originalUserSettings.Apply(this._userSettings);
            SettingsSerializer.WriteToXML(this._userSettings);
            File.WriteAllLines(
                Environments.Application.RECENT_FILES_FILE_PATH,
                this._userSettings.GeneralSettings.RecentFiles.ToArray()
            );

            this._originalBlueprints.Apply(this._blueprints);
            BlueprintsSerializer.WriteToXML(this._blueprints);
            // trigger to set "enable" state
            this.CheckSettingsEqualationState();
        }

        private bool HasChanges() =>
            !this._userSettings.DataAreEquals(this._originalUserSettings)
            || !this._blueprints.DataAreEquals(this._originalBlueprints);

        private List<TreeNode> GetOrderedNodes()
        {
            List<TreeNode> nodes = new List<TreeNode>();
            foreach (TreeNode node in this.treeSettings.Nodes)
            {
                nodes.Add(node);
                nodes.AddRange(node.Nodes.OfType<TreeNode>());
            }

            return nodes;
        }
    }
}
