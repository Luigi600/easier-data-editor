﻿
namespace Easier_Data_Editor.Windows.Settings
{
    partial class UCSettingsGeneral
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblFrameViewer = new System.Windows.Forms.Label();
            this.lblMultipleFileOpening = new System.Windows.Forms.Label();
            this.lblSaveSession = new System.Windows.Forms.Label();
            this.lblRecentFiles = new System.Windows.Forms.Label();
            this.cbChangeFrameViewer = new System.Windows.Forms.CheckBox();
            this.cbMultipleFileOpening = new System.Windows.Forms.CheckBox();
            this.cbKeepSession = new System.Windows.Forms.CheckBox();
            this.nudRecentFiles = new System.Windows.Forms.NumericUpDown();
            this.tlpMain = new System.Windows.Forms.TableLayoutPanel();
            this.lblLanguage = new System.Windows.Forms.Label();
            this.combLanguage = new System.Windows.Forms.ComboBox();
            this.btnOpenLangDirectory = new System.Windows.Forms.Button();
            this.lblNewlineChar = new System.Windows.Forms.Label();
            this.combNewlineChar = new System.Windows.Forms.ComboBox();
            this.lblMultipleInstances = new System.Windows.Forms.Label();
            this.cbMultipleInstances = new System.Windows.Forms.CheckBox();
            this.lblMultipleInstancesDescription = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.nudRecentFiles)).BeginInit();
            this.tlpMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblFrameViewer
            // 
            this.lblFrameViewer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblFrameViewer.AutoSize = true;
            this.lblFrameViewer.Location = new System.Drawing.Point(3, 275);
            this.lblFrameViewer.Margin = new System.Windows.Forms.Padding(3);
            this.lblFrameViewer.Name = "lblFrameViewer";
            this.lblFrameViewer.Padding = new System.Windows.Forms.Padding(0, 6, 0, 6);
            this.lblFrameViewer.Size = new System.Drawing.Size(74, 64);
            this.lblFrameViewer.TabIndex = 0;
            this.lblFrameViewer.Text = "Change Frame Viewer to appropriate data file:";
            this.lblFrameViewer.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMultipleFileOpening
            // 
            this.lblMultipleFileOpening.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMultipleFileOpening.AutoSize = true;
            this.lblMultipleFileOpening.Location = new System.Drawing.Point(3, 231);
            this.lblMultipleFileOpening.Margin = new System.Windows.Forms.Padding(3);
            this.lblMultipleFileOpening.Name = "lblMultipleFileOpening";
            this.lblMultipleFileOpening.Padding = new System.Windows.Forms.Padding(0, 6, 0, 6);
            this.lblMultipleFileOpening.Size = new System.Drawing.Size(74, 38);
            this.lblMultipleFileOpening.TabIndex = 0;
            this.lblMultipleFileOpening.Text = "Multiple File Opening:";
            this.lblMultipleFileOpening.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSaveSession
            // 
            this.lblSaveSession.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSaveSession.AutoSize = true;
            this.lblSaveSession.Location = new System.Drawing.Point(3, 78);
            this.lblSaveSession.Margin = new System.Windows.Forms.Padding(3);
            this.lblSaveSession.Name = "lblSaveSession";
            this.lblSaveSession.Padding = new System.Windows.Forms.Padding(0, 6, 0, 6);
            this.lblSaveSession.Size = new System.Drawing.Size(74, 38);
            this.lblSaveSession.TabIndex = 0;
            this.lblSaveSession.Text = "Keep Session:";
            this.lblSaveSession.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblRecentFiles
            // 
            this.lblRecentFiles.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblRecentFiles.AutoSize = true;
            this.lblRecentFiles.Location = new System.Drawing.Point(3, 34);
            this.lblRecentFiles.Margin = new System.Windows.Forms.Padding(3);
            this.lblRecentFiles.Name = "lblRecentFiles";
            this.lblRecentFiles.Padding = new System.Windows.Forms.Padding(0, 6, 0, 6);
            this.lblRecentFiles.Size = new System.Drawing.Size(74, 38);
            this.lblRecentFiles.TabIndex = 0;
            this.lblRecentFiles.Text = "Recent Files Capacity:";
            this.lblRecentFiles.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cbChangeFrameViewer
            // 
            this.cbChangeFrameViewer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbChangeFrameViewer.AutoSize = true;
            this.cbChangeFrameViewer.Location = new System.Drawing.Point(83, 300);
            this.cbChangeFrameViewer.Name = "cbChangeFrameViewer";
            this.cbChangeFrameViewer.Size = new System.Drawing.Size(75, 14);
            this.cbChangeFrameViewer.TabIndex = 2;
            this.cbChangeFrameViewer.UseVisualStyleBackColor = true;
            // 
            // cbMultipleFileOpening
            // 
            this.cbMultipleFileOpening.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbMultipleFileOpening.AutoSize = true;
            this.cbMultipleFileOpening.Location = new System.Drawing.Point(83, 243);
            this.cbMultipleFileOpening.Name = "cbMultipleFileOpening";
            this.cbMultipleFileOpening.Size = new System.Drawing.Size(75, 14);
            this.cbMultipleFileOpening.TabIndex = 2;
            this.cbMultipleFileOpening.UseVisualStyleBackColor = true;
            // 
            // cbKeepSession
            // 
            this.cbKeepSession.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbKeepSession.AutoSize = true;
            this.cbKeepSession.Location = new System.Drawing.Point(83, 90);
            this.cbKeepSession.Name = "cbKeepSession";
            this.cbKeepSession.Size = new System.Drawing.Size(75, 14);
            this.cbKeepSession.TabIndex = 2;
            this.cbKeepSession.UseVisualStyleBackColor = true;
            // 
            // nudRecentFiles
            // 
            this.nudRecentFiles.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.nudRecentFiles.Location = new System.Drawing.Point(83, 43);
            this.nudRecentFiles.Name = "nudRecentFiles";
            this.nudRecentFiles.Size = new System.Drawing.Size(75, 20);
            this.nudRecentFiles.TabIndex = 1;
            // 
            // tlpMain
            // 
            this.tlpMain.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tlpMain.AutoSize = true;
            this.tlpMain.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tlpMain.ColumnCount = 3;
            this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33332F));
            this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tlpMain.Controls.Add(this.lblLanguage, 0, 0);
            this.tlpMain.Controls.Add(this.lblSaveSession, 0, 2);
            this.tlpMain.Controls.Add(this.lblRecentFiles, 0, 1);
            this.tlpMain.Controls.Add(this.combLanguage, 1, 0);
            this.tlpMain.Controls.Add(this.cbKeepSession, 1, 2);
            this.tlpMain.Controls.Add(this.nudRecentFiles, 1, 1);
            this.tlpMain.Controls.Add(this.btnOpenLangDirectory, 2, 0);
            this.tlpMain.Controls.Add(this.lblNewlineChar, 0, 7);
            this.tlpMain.Controls.Add(this.lblFrameViewer, 0, 6);
            this.tlpMain.Controls.Add(this.combNewlineChar, 1, 7);
            this.tlpMain.Controls.Add(this.cbChangeFrameViewer, 1, 6);
            this.tlpMain.Controls.Add(this.lblMultipleFileOpening, 0, 4);
            this.tlpMain.Controls.Add(this.lblMultipleInstances, 0, 3);
            this.tlpMain.Controls.Add(this.cbMultipleFileOpening, 1, 4);
            this.tlpMain.Controls.Add(this.cbMultipleInstances, 1, 3);
            this.tlpMain.Controls.Add(this.lblMultipleInstancesDescription, 2, 3);
            this.tlpMain.Location = new System.Drawing.Point(22, 22);
            this.tlpMain.Name = "tlpMain";
            this.tlpMain.RowCount = 8;
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpMain.Size = new System.Drawing.Size(243, 360);
            this.tlpMain.TabIndex = 4;
            // 
            // lblLanguage
            // 
            this.lblLanguage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblLanguage.AutoSize = true;
            this.lblLanguage.Location = new System.Drawing.Point(3, 3);
            this.lblLanguage.Margin = new System.Windows.Forms.Padding(3);
            this.lblLanguage.Name = "lblLanguage";
            this.lblLanguage.Padding = new System.Windows.Forms.Padding(0, 6, 0, 6);
            this.lblLanguage.Size = new System.Drawing.Size(74, 25);
            this.lblLanguage.TabIndex = 3;
            this.lblLanguage.Text = "Language:";
            // 
            // combLanguage
            // 
            this.combLanguage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.combLanguage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.combLanguage.FormattingEnabled = true;
            this.combLanguage.Location = new System.Drawing.Point(83, 5);
            this.combLanguage.Name = "combLanguage";
            this.combLanguage.Size = new System.Drawing.Size(75, 21);
            this.combLanguage.TabIndex = 4;
            // 
            // btnOpenLangDirectory
            // 
            this.btnOpenLangDirectory.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOpenLangDirectory.Location = new System.Drawing.Point(164, 4);
            this.btnOpenLangDirectory.Name = "btnOpenLangDirectory";
            this.btnOpenLangDirectory.Size = new System.Drawing.Size(76, 23);
            this.btnOpenLangDirectory.TabIndex = 5;
            this.btnOpenLangDirectory.Text = "Open Directory";
            this.btnOpenLangDirectory.UseVisualStyleBackColor = true;
            this.btnOpenLangDirectory.Click += new System.EventHandler(this.BtnOpenLangDirectory_Click);
            // 
            // lblNewlineChar
            // 
            this.lblNewlineChar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblNewlineChar.AutoSize = true;
            this.lblNewlineChar.Location = new System.Drawing.Point(3, 345);
            this.lblNewlineChar.Margin = new System.Windows.Forms.Padding(3);
            this.lblNewlineChar.Name = "lblNewlineChar";
            this.lblNewlineChar.Padding = new System.Windows.Forms.Padding(0, 6, 0, 6);
            this.lblNewlineChar.Size = new System.Drawing.Size(74, 25);
            this.lblNewlineChar.TabIndex = 6;
            this.lblNewlineChar.Text = "Newline Char:";
            // 
            // combNewlineChar
            // 
            this.combNewlineChar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.combNewlineChar.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.combNewlineChar.FormattingEnabled = true;
            this.combNewlineChar.Items.AddRange(new object[] {
            "LF (=Line Feed ; Linux etc)",
            "CR (=Carriage Return ; Mac OS)",
            "CR LF (CR & LF ; Windows)"});
            this.combNewlineChar.Location = new System.Drawing.Point(83, 347);
            this.combNewlineChar.Name = "combNewlineChar";
            this.combNewlineChar.Size = new System.Drawing.Size(75, 21);
            this.combNewlineChar.TabIndex = 7;
            // 
            // lblMultipleInstances
            // 
            this.lblMultipleInstances.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMultipleInstances.AutoSize = true;
            this.lblMultipleInstances.Location = new System.Drawing.Point(3, 154);
            this.lblMultipleInstances.Margin = new System.Windows.Forms.Padding(3);
            this.lblMultipleInstances.Name = "lblMultipleInstances";
            this.lblMultipleInstances.Padding = new System.Windows.Forms.Padding(0, 6, 0, 6);
            this.lblMultipleInstances.Size = new System.Drawing.Size(74, 38);
            this.lblMultipleInstances.TabIndex = 0;
            this.lblMultipleInstances.Text = "Multiple Instances:";
            this.lblMultipleInstances.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cbMultipleInstances
            // 
            this.cbMultipleInstances.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbMultipleInstances.AutoSize = true;
            this.cbMultipleInstances.Location = new System.Drawing.Point(83, 166);
            this.cbMultipleInstances.Name = "cbMultipleInstances";
            this.cbMultipleInstances.Size = new System.Drawing.Size(75, 14);
            this.cbMultipleInstances.TabIndex = 2;
            this.cbMultipleInstances.UseVisualStyleBackColor = true;
            // 
            // lblMultipleInstancesDescription
            // 
            this.lblMultipleInstancesDescription.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMultipleInstancesDescription.AutoSize = true;
            this.lblMultipleInstancesDescription.Location = new System.Drawing.Point(164, 128);
            this.lblMultipleInstancesDescription.Margin = new System.Windows.Forms.Padding(3);
            this.lblMultipleInstancesDescription.Name = "lblMultipleInstancesDescription";
            this.lblMultipleInstancesDescription.Padding = new System.Windows.Forms.Padding(0, 6, 0, 6);
            this.lblMultipleInstancesDescription.Size = new System.Drawing.Size(76, 90);
            this.lblMultipleInstancesDescription.TabIndex = 3;
            this.lblMultipleInstancesDescription.Text = "Allows multiple opening of this application (several windows at the same time)";
            // 
            // UCSettingsGeneral
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Controls.Add(this.tlpMain);
            this.MinimumSize = new System.Drawing.Size(300, 0);
            this.Name = "UCSettingsGeneral";
            this.Padding = new System.Windows.Forms.Padding(16);
            this.Size = new System.Drawing.Size(300, 414);
            ((System.ComponentModel.ISupportInitialize)(this.nudRecentFiles)).EndInit();
            this.tlpMain.ResumeLayout(false);
            this.tlpMain.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Label lblFrameViewer;
        internal System.Windows.Forms.Label lblMultipleFileOpening;
        internal System.Windows.Forms.Label lblSaveSession;
        internal System.Windows.Forms.Label lblRecentFiles;
        internal System.Windows.Forms.CheckBox cbChangeFrameViewer;
        internal System.Windows.Forms.CheckBox cbMultipleFileOpening;
        internal System.Windows.Forms.CheckBox cbKeepSession;
        internal System.Windows.Forms.NumericUpDown nudRecentFiles;
        internal System.Windows.Forms.TableLayoutPanel tlpMain;
        internal System.Windows.Forms.Label lblLanguage;
        internal System.Windows.Forms.ComboBox combLanguage;
        internal System.Windows.Forms.Button btnOpenLangDirectory;
        internal System.Windows.Forms.Label lblNewlineChar;
        internal System.Windows.Forms.ComboBox combNewlineChar;
        internal System.Windows.Forms.Label lblMultipleInstances;
        internal System.Windows.Forms.CheckBox cbMultipleInstances;
        internal System.Windows.Forms.Label lblMultipleInstancesDescription;
    }
}
