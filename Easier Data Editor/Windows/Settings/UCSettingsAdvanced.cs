﻿using System;
using System.IO;
using System.Windows.Forms;
using Easier_Data_Editor.Environments;
using Easier_Data_Editor.Windows.Settings.ModifyRecentFiles;
using Easier.MultiLangSupport.Environments;

namespace Easier_Data_Editor.Windows.Settings
{
    public partial class UCSettingsAdvanced : UCSettingsBase
    {
        private readonly UserSettings _originalUserSettings;
        private readonly UserSettings _userSettings;

        public event EventHandler? WillSaveSettings;

        public UCSettingsAdvanced(
            ref UserSettings originalUserSettings,
            ref UserSettings userSettings
        )
        {
            this._originalUserSettings = originalUserSettings;
            this._userSettings = userSettings;

            this.VariablesList.Add("DISPLAY_NAME", "Advanced");
            this.VariablesList.Add(
                "RESET_TITLE",
                $"{Environments.Application.APP_TITLE}    -   Reset?"
            );
            this.VariablesList.Add(
                "RESET",
                "Do you really want to reset \"{0}\"? This operation cannot be rolled back and should only be used if the program is not working properly."
            );
            this.VariablesList.Add("RESET_PLACEHOLDER_NAME_SETTINGS", "Settings");
            this.VariablesList.Add("RESET_PLACEHOLDER_NAME_LAYOUT", "Layout");
            this.VariablesList.Add("RESET_PLACEHOLDER_NAME_RECENT_FILES", "Recent files");
            this.VariablesList.Add(
                "AFTER_RESET_TITLE",
                $"{Environments.Application.APP_TITLE}    -   Reset"
            );
            this.VariablesList.Add("AFTER_RESET", "\"{0}\" have been successfully reset.");

            InitializeComponent();

            this.SetBindings();
            this.AfterControlsInit();
        }

        protected override Control MainControl() => this.tlpMain;

        private void SetBindings()
        {
            this.cbEncryptionDetectionFromContent.DataBindings.Add(
                Constants.BINDING_CHECK_BOX_VALUE,
                this._userSettings.AdvancedSettings,
                "EncryptionDetectionFromContent",
                true,
                DataSourceUpdateMode.OnPropertyChanged
            );
        }

        private void BtnResetSettings_Click(object sender, EventArgs e)
        {
            if (
                MessageBox.Show(
                    GetVariable("RESET", GetVariable("RESET_PLACEHOLDER_NAME_SETTINGS")),
                    GetVariable("RESET_TITLE"),
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Exclamation
                ) == DialogResult.No
            )
                return;

            this._originalUserSettings.Apply(UserSettings.GetInstance());
            this._userSettings.Apply(UserSettings.GetInstance());
            Translation.SelectedLanguage = Translation.DefaultLanguage;
            // save
            WillSaveSettings?.Invoke(this, EventArgs.Empty);

            this.OpenSuccessfulResetDialog("RESET_PLACEHOLDER_NAME_SETTINGS");
        }

        private void BtnResetLayout_Click(object sender, EventArgs e)
        {
            if (
                MessageBox.Show(
                    GetVariable("RESET", GetVariable("RESET_PLACEHOLDER_NAME_LAYOUT")),
                    GetVariable("RESET_TITLE"),
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Exclamation
                ) == DialogResult.No
            )
                return;

            if (File.Exists(Environments.Application.LAYOUT_FILE_PATH))
                File.Delete(Environments.Application.LAYOUT_FILE_PATH);

            this.OpenSuccessfulResetDialog("RESET_PLACEHOLDER_NAME_LAYOUT");
        }

        private void BtnResetRecentFiles_Click(object sender, EventArgs e)
        {
            if (
                MessageBox.Show(
                    GetVariable("RESET", GetVariable("RESET_PLACEHOLDER_NAME_RECENT_FILES")),
                    GetVariable("RESET_TITLE"),
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Exclamation
                ) == DialogResult.No
            )
                return;

            if (File.Exists(Environments.Application.RECENT_FILES_FILE_PATH))
                File.Delete(Environments.Application.RECENT_FILES_FILE_PATH);

            this._originalUserSettings.GeneralSettings.RecentFiles.Clear();
            this._userSettings.GeneralSettings.RecentFiles.Clear();

            this.OpenSuccessfulResetDialog("RESET_PLACEHOLDER_NAME_RECENT_FILES");
        }

        private void BtnModifyRecentFiles_Click(object sender, EventArgs e)
        {
            DialogModifyRecentFiles modifyDialog = new DialogModifyRecentFiles(
                this._userSettings.GeneralSettings.RecentFiles
            );
            modifyDialog.WillChangeRecentFiles += (sender, e) =>
            {
                this._userSettings.GeneralSettings.RecentFiles.Clear();
                this._userSettings.GeneralSettings.RecentFiles.AddRange(e);
            };
            modifyDialog.ShowDialog(this);
        }

        private void OpenSuccessfulResetDialog(string varPlaceholderName)
        {
            MessageBox.Show(
                GetVariable("AFTER_RESET", GetVariable(varPlaceholderName)),
                GetVariable("AFTER_RESET_TITLE"),
                MessageBoxButtons.OK,
                MessageBoxIcon.Information
            );
        }
    }
}
