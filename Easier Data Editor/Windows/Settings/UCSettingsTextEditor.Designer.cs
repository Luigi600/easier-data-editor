﻿
namespace Easier_Data_Editor.Windows.Settings
{
    partial class UCSettingsTextEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.fdFontFamily = new System.Windows.Forms.FontDialog();
            this.btnChangeLineColor = new System.Windows.Forms.Button();
            this.picCurrentLineColorPreview = new System.Windows.Forms.PictureBox();
            this.tlpFontFamily = new System.Windows.Forms.TableLayoutPanel();
            this.btnChangeFontFamily = new System.Windows.Forms.Button();
            this.txtFontFamily = new System.Windows.Forms.TextBox();
            this.tlpLineColor = new System.Windows.Forms.TableLayoutPanel();
            this.lblChangeBar = new System.Windows.Forms.Label();
            this.lblSubfolding = new System.Windows.Forms.Label();
            this.picExampleBookmark = new System.Windows.Forms.PictureBox();
            this.cbSubfolding = new System.Windows.Forms.CheckBox();
            this.lblSubfoldingInformation = new System.Windows.Forms.Label();
            this.lblFoldingInformation = new System.Windows.Forms.Label();
            this.lblIndention = new System.Windows.Forms.Label();
            this.picExampleIndention = new System.Windows.Forms.PictureBox();
            this.lblScrollbarAnnotations = new System.Windows.Forms.Label();
            this.cbIndention = new System.Windows.Forms.CheckBox();
            this.cbScrollbarAnnotations = new System.Windows.Forms.CheckBox();
            this.tlpMain = new System.Windows.Forms.TableLayoutPanel();
            this.combWrapMode = new System.Windows.Forms.ComboBox();
            this.lblFontFamily = new System.Windows.Forms.Label();
            this.lblFontSize = new System.Windows.Forms.Label();
            this.cbFolding = new System.Windows.Forms.CheckBox();
            this.lblWrapMode = new System.Windows.Forms.Label();
            this.cbAutocompletion = new System.Windows.Forms.CheckBox();
            this.nudFontSize = new System.Windows.Forms.NumericUpDown();
            this.lblFolding = new System.Windows.Forms.Label();
            this.lblLineNumbers = new System.Windows.Forms.Label();
            this.lblAutocompletion = new System.Windows.Forms.Label();
            this.lblLineColor = new System.Windows.Forms.Label();
            this.cbMarkCurrentLine = new System.Windows.Forms.CheckBox();
            this.cbWhitespaces = new System.Windows.Forms.CheckBox();
            this.cbLineNumbers = new System.Windows.Forms.CheckBox();
            this.lblWhitespaces = new System.Windows.Forms.Label();
            this.lblCurrentLine = new System.Windows.Forms.Label();
            this.lblBookmarks = new System.Windows.Forms.Label();
            this.cbChangeBar = new System.Windows.Forms.CheckBox();
            this.cbBookmarks = new System.Windows.Forms.CheckBox();
            this.cbAutoJumpBySyncID = new System.Windows.Forms.CheckBox();
            this.lblAutoJumpBySyncID = new System.Windows.Forms.Label();
            this.lblAutoJumpBySyncIDInformation = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.picCurrentLineColorPreview)).BeginInit();
            this.tlpFontFamily.SuspendLayout();
            this.tlpLineColor.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picExampleBookmark)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picExampleIndention)).BeginInit();
            this.tlpMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudFontSize)).BeginInit();
            this.SuspendLayout();
            // 
            // fdFontFamily
            // 
            this.fdFontFamily.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F);
            // 
            // btnChangeLineColor
            // 
            this.btnChangeLineColor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.btnChangeLineColor.Location = new System.Drawing.Point(47, 10);
            this.btnChangeLineColor.Margin = new System.Windows.Forms.Padding(3, 3, 2, 3);
            this.btnChangeLineColor.Name = "btnChangeLineColor";
            this.btnChangeLineColor.Size = new System.Drawing.Size(32, 22);
            this.btnChangeLineColor.TabIndex = 10;
            this.btnChangeLineColor.Text = "...";
            this.btnChangeLineColor.UseVisualStyleBackColor = true;
            this.btnChangeLineColor.Click += new System.EventHandler(this.BtnChangeLineColor_Click);
            // 
            // picCurrentLineColorPreview
            // 
            this.picCurrentLineColorPreview.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.picCurrentLineColorPreview.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picCurrentLineColorPreview.Location = new System.Drawing.Point(3, 11);
            this.picCurrentLineColorPreview.Name = "picCurrentLineColorPreview";
            this.picCurrentLineColorPreview.Size = new System.Drawing.Size(38, 20);
            this.picCurrentLineColorPreview.TabIndex = 11;
            this.picCurrentLineColorPreview.TabStop = false;
            // 
            // tlpFontFamily
            // 
            this.tlpFontFamily.AutoSize = true;
            this.tlpFontFamily.ColumnCount = 2;
            this.tlpFontFamily.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpFontFamily.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tlpFontFamily.Controls.Add(this.btnChangeFontFamily, 1, 0);
            this.tlpFontFamily.Controls.Add(this.txtFontFamily, 0, 0);
            this.tlpFontFamily.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpFontFamily.Location = new System.Drawing.Point(83, 1);
            this.tlpFontFamily.Margin = new System.Windows.Forms.Padding(1, 1, 0, 0);
            this.tlpFontFamily.Name = "tlpFontFamily";
            this.tlpFontFamily.RowCount = 1;
            this.tlpFontFamily.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpFontFamily.Size = new System.Drawing.Size(81, 30);
            this.tlpFontFamily.TabIndex = 16;
            // 
            // btnChangeFontFamily
            // 
            this.btnChangeFontFamily.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.btnChangeFontFamily.Location = new System.Drawing.Point(47, 4);
            this.btnChangeFontFamily.Margin = new System.Windows.Forms.Padding(3, 3, 2, 3);
            this.btnChangeFontFamily.Name = "btnChangeFontFamily";
            this.btnChangeFontFamily.Size = new System.Drawing.Size(32, 22);
            this.btnChangeFontFamily.TabIndex = 10;
            this.btnChangeFontFamily.Text = "...";
            this.btnChangeFontFamily.UseVisualStyleBackColor = true;
            this.btnChangeFontFamily.Click += new System.EventHandler(this.BtnChangeFontFamily_Click);
            // 
            // txtFontFamily
            // 
            this.txtFontFamily.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFontFamily.Location = new System.Drawing.Point(3, 5);
            this.txtFontFamily.Margin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.txtFontFamily.Name = "txtFontFamily";
            this.txtFontFamily.ReadOnly = true;
            this.txtFontFamily.Size = new System.Drawing.Size(41, 20);
            this.txtFontFamily.TabIndex = 1;
            // 
            // tlpLineColor
            // 
            this.tlpLineColor.AutoSize = true;
            this.tlpLineColor.ColumnCount = 2;
            this.tlpLineColor.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpLineColor.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tlpLineColor.Controls.Add(this.btnChangeLineColor, 1, 0);
            this.tlpLineColor.Controls.Add(this.picCurrentLineColorPreview, 0, 0);
            this.tlpLineColor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpLineColor.Location = new System.Drawing.Point(83, 195);
            this.tlpLineColor.Margin = new System.Windows.Forms.Padding(1, 1, 0, 0);
            this.tlpLineColor.Name = "tlpLineColor";
            this.tlpLineColor.RowCount = 1;
            this.tlpLineColor.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpLineColor.Size = new System.Drawing.Size(81, 43);
            this.tlpLineColor.TabIndex = 14;
            // 
            // lblChangeBar
            // 
            this.lblChangeBar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblChangeBar.AutoSize = true;
            this.lblChangeBar.Location = new System.Drawing.Point(3, 454);
            this.lblChangeBar.Margin = new System.Windows.Forms.Padding(3);
            this.lblChangeBar.Name = "lblChangeBar";
            this.lblChangeBar.Padding = new System.Windows.Forms.Padding(0, 6, 0, 6);
            this.lblChangeBar.Size = new System.Drawing.Size(76, 38);
            this.lblChangeBar.TabIndex = 15;
            this.lblChangeBar.Text = "Show \"Change Bar\":";
            // 
            // lblSubfolding
            // 
            this.lblSubfolding.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSubfolding.AutoSize = true;
            this.lblSubfolding.Location = new System.Drawing.Point(3, 368);
            this.lblSubfolding.Margin = new System.Windows.Forms.Padding(3);
            this.lblSubfolding.Name = "lblSubfolding";
            this.lblSubfolding.Padding = new System.Windows.Forms.Padding(0, 6, 0, 6);
            this.lblSubfolding.Size = new System.Drawing.Size(76, 25);
            this.lblSubfolding.TabIndex = 23;
            this.lblSubfolding.Text = "Subfolding:";
            // 
            // picExampleBookmark
            // 
            this.picExampleBookmark.Image = global::Easier_Data_Editor.Properties.Resources.manual_bookmark;
            this.picExampleBookmark.Location = new System.Drawing.Point(167, 419);
            this.picExampleBookmark.Name = "picExampleBookmark";
            this.picExampleBookmark.Size = new System.Drawing.Size(78, 29);
            this.picExampleBookmark.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picExampleBookmark.TabIndex = 17;
            this.picExampleBookmark.TabStop = false;
            // 
            // cbSubfolding
            // 
            this.cbSubfolding.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbSubfolding.AutoSize = true;
            this.cbSubfolding.Location = new System.Drawing.Point(85, 373);
            this.cbSubfolding.Name = "cbSubfolding";
            this.cbSubfolding.Size = new System.Drawing.Size(76, 14);
            this.cbSubfolding.TabIndex = 24;
            this.cbSubfolding.UseVisualStyleBackColor = true;
            // 
            // lblSubfoldingInformation
            // 
            this.lblSubfoldingInformation.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSubfoldingInformation.AutoSize = true;
            this.lblSubfoldingInformation.Location = new System.Drawing.Point(167, 348);
            this.lblSubfoldingInformation.Margin = new System.Windows.Forms.Padding(3);
            this.lblSubfoldingInformation.Name = "lblSubfoldingInformation";
            this.lblSubfoldingInformation.Size = new System.Drawing.Size(78, 65);
            this.lblSubfoldingInformation.TabIndex = 25;
            this.lblSubfoldingInformation.Text = "Includes bdy/itr/b-, c-, o-, wpoint/phases";
            // 
            // lblFoldingInformation
            // 
            this.lblFoldingInformation.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblFoldingInformation.AutoSize = true;
            this.lblFoldingInformation.Location = new System.Drawing.Point(167, 316);
            this.lblFoldingInformation.Margin = new System.Windows.Forms.Padding(3);
            this.lblFoldingInformation.Name = "lblFoldingInformation";
            this.lblFoldingInformation.Size = new System.Drawing.Size(78, 26);
            this.lblFoldingInformation.TabIndex = 26;
            this.lblFoldingInformation.Text = "Needs more loading time!";
            // 
            // lblIndention
            // 
            this.lblIndention.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblIndention.AutoSize = true;
            this.lblIndention.Location = new System.Drawing.Point(3, 542);
            this.lblIndention.Margin = new System.Windows.Forms.Padding(3);
            this.lblIndention.Name = "lblIndention";
            this.lblIndention.Padding = new System.Windows.Forms.Padding(0, 6, 0, 6);
            this.lblIndention.Size = new System.Drawing.Size(76, 25);
            this.lblIndention.TabIndex = 18;
            this.lblIndention.Text = "Indention:";
            // 
            // picExampleIndention
            // 
            this.picExampleIndention.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.picExampleIndention.Image = global::Easier_Data_Editor.Properties.Resources.indention;
            this.picExampleIndention.Location = new System.Drawing.Point(167, 542);
            this.picExampleIndention.Name = "picExampleIndention";
            this.picExampleIndention.Size = new System.Drawing.Size(78, 25);
            this.picExampleIndention.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picExampleIndention.TabIndex = 20;
            this.picExampleIndention.TabStop = false;
            // 
            // lblScrollbarAnnotations
            // 
            this.lblScrollbarAnnotations.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblScrollbarAnnotations.AutoSize = true;
            this.lblScrollbarAnnotations.Location = new System.Drawing.Point(3, 498);
            this.lblScrollbarAnnotations.Margin = new System.Windows.Forms.Padding(3);
            this.lblScrollbarAnnotations.Name = "lblScrollbarAnnotations";
            this.lblScrollbarAnnotations.Padding = new System.Windows.Forms.Padding(0, 6, 0, 6);
            this.lblScrollbarAnnotations.Size = new System.Drawing.Size(76, 38);
            this.lblScrollbarAnnotations.TabIndex = 27;
            this.lblScrollbarAnnotations.Text = "Scrollbar Annotations:";
            // 
            // cbIndention
            // 
            this.cbIndention.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbIndention.AutoSize = true;
            this.cbIndention.Location = new System.Drawing.Point(85, 547);
            this.cbIndention.Name = "cbIndention";
            this.cbIndention.Size = new System.Drawing.Size(76, 14);
            this.cbIndention.TabIndex = 19;
            this.cbIndention.UseVisualStyleBackColor = true;
            // 
            // cbScrollbarAnnotations
            // 
            this.cbScrollbarAnnotations.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbScrollbarAnnotations.AutoSize = true;
            this.cbScrollbarAnnotations.Location = new System.Drawing.Point(85, 510);
            this.cbScrollbarAnnotations.Name = "cbScrollbarAnnotations";
            this.cbScrollbarAnnotations.Size = new System.Drawing.Size(76, 14);
            this.cbScrollbarAnnotations.TabIndex = 28;
            this.cbScrollbarAnnotations.UseVisualStyleBackColor = true;
            // 
            // tlpMain
            // 
            this.tlpMain.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tlpMain.AutoSize = true;
            this.tlpMain.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tlpMain.ColumnCount = 3;
            this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33332F));
            this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tlpMain.Controls.Add(this.tlpLineColor, 1, 5);
            this.tlpMain.Controls.Add(this.combWrapMode, 1, 6);
            this.tlpMain.Controls.Add(this.lblFontFamily, 0, 0);
            this.tlpMain.Controls.Add(this.lblFontSize, 0, 1);
            this.tlpMain.Controls.Add(this.cbFolding, 1, 8);
            this.tlpMain.Controls.Add(this.lblWrapMode, 0, 6);
            this.tlpMain.Controls.Add(this.cbAutocompletion, 1, 7);
            this.tlpMain.Controls.Add(this.nudFontSize, 1, 1);
            this.tlpMain.Controls.Add(this.lblFolding, 0, 8);
            this.tlpMain.Controls.Add(this.lblLineNumbers, 0, 2);
            this.tlpMain.Controls.Add(this.lblAutocompletion, 0, 7);
            this.tlpMain.Controls.Add(this.lblLineColor, 0, 5);
            this.tlpMain.Controls.Add(this.cbMarkCurrentLine, 1, 4);
            this.tlpMain.Controls.Add(this.cbWhitespaces, 1, 3);
            this.tlpMain.Controls.Add(this.cbLineNumbers, 1, 2);
            this.tlpMain.Controls.Add(this.lblWhitespaces, 0, 3);
            this.tlpMain.Controls.Add(this.lblCurrentLine, 0, 4);
            this.tlpMain.Controls.Add(this.tlpFontFamily, 1, 0);
            this.tlpMain.Controls.Add(this.lblChangeBar, 0, 11);
            this.tlpMain.Controls.Add(this.lblBookmarks, 0, 10);
            this.tlpMain.Controls.Add(this.lblSubfolding, 0, 9);
            this.tlpMain.Controls.Add(this.cbChangeBar, 1, 11);
            this.tlpMain.Controls.Add(this.cbBookmarks, 1, 10);
            this.tlpMain.Controls.Add(this.picExampleBookmark, 2, 10);
            this.tlpMain.Controls.Add(this.cbSubfolding, 1, 9);
            this.tlpMain.Controls.Add(this.lblSubfoldingInformation, 2, 9);
            this.tlpMain.Controls.Add(this.lblFoldingInformation, 2, 8);
            this.tlpMain.Controls.Add(this.lblIndention, 0, 13);
            this.tlpMain.Controls.Add(this.cbAutoJumpBySyncID, 1, 14);
            this.tlpMain.Controls.Add(this.lblScrollbarAnnotations, 0, 12);
            this.tlpMain.Controls.Add(this.cbIndention, 1, 13);
            this.tlpMain.Controls.Add(this.cbScrollbarAnnotations, 1, 12);
            this.tlpMain.Controls.Add(this.lblAutoJumpBySyncID, 0, 14);
            this.tlpMain.Controls.Add(this.lblAutoJumpBySyncIDInformation, 2, 14);
            this.tlpMain.Controls.Add(this.picExampleIndention, 2, 13);
            this.tlpMain.Location = new System.Drawing.Point(22, 22);
            this.tlpMain.Margin = new System.Windows.Forms.Padding(16);
            this.tlpMain.Name = "tlpMain";
            this.tlpMain.RowCount = 15;
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpMain.Size = new System.Drawing.Size(248, 641);
            this.tlpMain.TabIndex = 8;
            // 
            // combWrapMode
            // 
            this.combWrapMode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.combWrapMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.combWrapMode.FormattingEnabled = true;
            this.combWrapMode.Location = new System.Drawing.Point(85, 243);
            this.combWrapMode.Name = "combWrapMode";
            this.combWrapMode.Size = new System.Drawing.Size(76, 21);
            this.combWrapMode.TabIndex = 6;
            // 
            // lblFontFamily
            // 
            this.lblFontFamily.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblFontFamily.AutoSize = true;
            this.lblFontFamily.Location = new System.Drawing.Point(3, 3);
            this.lblFontFamily.Margin = new System.Windows.Forms.Padding(3);
            this.lblFontFamily.Name = "lblFontFamily";
            this.lblFontFamily.Padding = new System.Windows.Forms.Padding(0, 6, 0, 6);
            this.lblFontFamily.Size = new System.Drawing.Size(76, 25);
            this.lblFontFamily.TabIndex = 0;
            this.lblFontFamily.Text = "Font Family:";
            // 
            // lblFontSize
            // 
            this.lblFontSize.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblFontSize.AutoSize = true;
            this.lblFontSize.Location = new System.Drawing.Point(3, 34);
            this.lblFontSize.Margin = new System.Windows.Forms.Padding(3);
            this.lblFontSize.Name = "lblFontSize";
            this.lblFontSize.Padding = new System.Windows.Forms.Padding(0, 6, 0, 6);
            this.lblFontSize.Size = new System.Drawing.Size(76, 25);
            this.lblFontSize.TabIndex = 0;
            this.lblFontSize.Text = "Font Size:";
            // 
            // cbFolding
            // 
            this.cbFolding.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbFolding.AutoSize = true;
            this.cbFolding.Location = new System.Drawing.Point(85, 322);
            this.cbFolding.Name = "cbFolding";
            this.cbFolding.Size = new System.Drawing.Size(76, 14);
            this.cbFolding.TabIndex = 4;
            this.cbFolding.UseVisualStyleBackColor = true;
            // 
            // lblWrapMode
            // 
            this.lblWrapMode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblWrapMode.AutoSize = true;
            this.lblWrapMode.Location = new System.Drawing.Point(3, 241);
            this.lblWrapMode.Margin = new System.Windows.Forms.Padding(3);
            this.lblWrapMode.Name = "lblWrapMode";
            this.lblWrapMode.Padding = new System.Windows.Forms.Padding(0, 6, 0, 6);
            this.lblWrapMode.Size = new System.Drawing.Size(76, 25);
            this.lblWrapMode.TabIndex = 0;
            this.lblWrapMode.Text = "Wrap Mode:";
            // 
            // cbAutocompletion
            // 
            this.cbAutocompletion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbAutocompletion.AutoSize = true;
            this.cbAutocompletion.Enabled = false;
            this.cbAutocompletion.Location = new System.Drawing.Point(85, 284);
            this.cbAutocompletion.Name = "cbAutocompletion";
            this.cbAutocompletion.Size = new System.Drawing.Size(76, 14);
            this.cbAutocompletion.TabIndex = 3;
            this.cbAutocompletion.UseVisualStyleBackColor = true;
            // 
            // nudFontSize
            // 
            this.nudFontSize.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.nudFontSize.Location = new System.Drawing.Point(85, 36);
            this.nudFontSize.Name = "nudFontSize";
            this.nudFontSize.Size = new System.Drawing.Size(76, 20);
            this.nudFontSize.TabIndex = 2;
            // 
            // lblFolding
            // 
            this.lblFolding.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblFolding.AutoSize = true;
            this.lblFolding.Location = new System.Drawing.Point(3, 316);
            this.lblFolding.Margin = new System.Windows.Forms.Padding(3);
            this.lblFolding.Name = "lblFolding";
            this.lblFolding.Padding = new System.Windows.Forms.Padding(0, 6, 0, 6);
            this.lblFolding.Size = new System.Drawing.Size(76, 25);
            this.lblFolding.TabIndex = 0;
            this.lblFolding.Text = "Folding:";
            // 
            // lblLineNumbers
            // 
            this.lblLineNumbers.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblLineNumbers.AutoSize = true;
            this.lblLineNumbers.Location = new System.Drawing.Point(3, 65);
            this.lblLineNumbers.Margin = new System.Windows.Forms.Padding(3);
            this.lblLineNumbers.Name = "lblLineNumbers";
            this.lblLineNumbers.Padding = new System.Windows.Forms.Padding(0, 6, 0, 6);
            this.lblLineNumbers.Size = new System.Drawing.Size(76, 38);
            this.lblLineNumbers.TabIndex = 0;
            this.lblLineNumbers.Text = "Show Line Numbers:";
            // 
            // lblAutocompletion
            // 
            this.lblAutocompletion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblAutocompletion.AutoSize = true;
            this.lblAutocompletion.Enabled = false;
            this.lblAutocompletion.Location = new System.Drawing.Point(3, 272);
            this.lblAutocompletion.Margin = new System.Windows.Forms.Padding(3);
            this.lblAutocompletion.Name = "lblAutocompletion";
            this.lblAutocompletion.Padding = new System.Windows.Forms.Padding(0, 6, 0, 6);
            this.lblAutocompletion.Size = new System.Drawing.Size(76, 38);
            this.lblAutocompletion.TabIndex = 0;
            this.lblAutocompletion.Text = "Autocompletion:";
            // 
            // lblLineColor
            // 
            this.lblLineColor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblLineColor.AutoSize = true;
            this.lblLineColor.Location = new System.Drawing.Point(3, 197);
            this.lblLineColor.Margin = new System.Windows.Forms.Padding(3);
            this.lblLineColor.Name = "lblLineColor";
            this.lblLineColor.Padding = new System.Windows.Forms.Padding(0, 6, 0, 6);
            this.lblLineColor.Size = new System.Drawing.Size(76, 38);
            this.lblLineColor.TabIndex = 0;
            this.lblLineColor.Text = "Current Line Color:";
            // 
            // cbMarkCurrentLine
            // 
            this.cbMarkCurrentLine.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbMarkCurrentLine.AutoSize = true;
            this.cbMarkCurrentLine.Location = new System.Drawing.Point(85, 165);
            this.cbMarkCurrentLine.Name = "cbMarkCurrentLine";
            this.cbMarkCurrentLine.Size = new System.Drawing.Size(76, 14);
            this.cbMarkCurrentLine.TabIndex = 4;
            this.cbMarkCurrentLine.UseVisualStyleBackColor = true;
            // 
            // cbWhitespaces
            // 
            this.cbWhitespaces.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbWhitespaces.AutoSize = true;
            this.cbWhitespaces.Location = new System.Drawing.Point(85, 121);
            this.cbWhitespaces.Name = "cbWhitespaces";
            this.cbWhitespaces.Size = new System.Drawing.Size(76, 14);
            this.cbWhitespaces.TabIndex = 4;
            this.cbWhitespaces.UseVisualStyleBackColor = true;
            // 
            // cbLineNumbers
            // 
            this.cbLineNumbers.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbLineNumbers.AutoSize = true;
            this.cbLineNumbers.Location = new System.Drawing.Point(85, 77);
            this.cbLineNumbers.Name = "cbLineNumbers";
            this.cbLineNumbers.Size = new System.Drawing.Size(76, 14);
            this.cbLineNumbers.TabIndex = 4;
            this.cbLineNumbers.UseVisualStyleBackColor = true;
            // 
            // lblWhitespaces
            // 
            this.lblWhitespaces.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblWhitespaces.AutoSize = true;
            this.lblWhitespaces.Location = new System.Drawing.Point(3, 109);
            this.lblWhitespaces.Margin = new System.Windows.Forms.Padding(3);
            this.lblWhitespaces.Name = "lblWhitespaces";
            this.lblWhitespaces.Padding = new System.Windows.Forms.Padding(0, 6, 0, 6);
            this.lblWhitespaces.Size = new System.Drawing.Size(76, 38);
            this.lblWhitespaces.TabIndex = 0;
            this.lblWhitespaces.Text = "Show Whitespaces:";
            // 
            // lblCurrentLine
            // 
            this.lblCurrentLine.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCurrentLine.AutoSize = true;
            this.lblCurrentLine.Location = new System.Drawing.Point(3, 153);
            this.lblCurrentLine.Margin = new System.Windows.Forms.Padding(3);
            this.lblCurrentLine.Name = "lblCurrentLine";
            this.lblCurrentLine.Padding = new System.Windows.Forms.Padding(0, 6, 0, 6);
            this.lblCurrentLine.Size = new System.Drawing.Size(76, 38);
            this.lblCurrentLine.TabIndex = 0;
            this.lblCurrentLine.Text = "Mark Current Line:";
            // 
            // lblBookmarks
            // 
            this.lblBookmarks.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblBookmarks.AutoSize = true;
            this.lblBookmarks.Location = new System.Drawing.Point(3, 421);
            this.lblBookmarks.Margin = new System.Windows.Forms.Padding(3);
            this.lblBookmarks.Name = "lblBookmarks";
            this.lblBookmarks.Padding = new System.Windows.Forms.Padding(0, 6, 0, 6);
            this.lblBookmarks.Size = new System.Drawing.Size(76, 25);
            this.lblBookmarks.TabIndex = 0;
            this.lblBookmarks.Text = "Bookmarks:";
            // 
            // cbChangeBar
            // 
            this.cbChangeBar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbChangeBar.AutoSize = true;
            this.cbChangeBar.Location = new System.Drawing.Point(85, 466);
            this.cbChangeBar.Name = "cbChangeBar";
            this.cbChangeBar.Size = new System.Drawing.Size(76, 14);
            this.cbChangeBar.TabIndex = 4;
            this.cbChangeBar.UseVisualStyleBackColor = true;
            // 
            // cbBookmarks
            // 
            this.cbBookmarks.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbBookmarks.AutoSize = true;
            this.cbBookmarks.Location = new System.Drawing.Point(85, 426);
            this.cbBookmarks.Name = "cbBookmarks";
            this.cbBookmarks.Size = new System.Drawing.Size(76, 14);
            this.cbBookmarks.TabIndex = 4;
            this.cbBookmarks.UseVisualStyleBackColor = true;
            // 
            // cbAutoJumpBySyncID
            // 
            this.cbAutoJumpBySyncID.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbAutoJumpBySyncID.AutoSize = true;
            this.cbAutoJumpBySyncID.Location = new System.Drawing.Point(85, 611);
            this.cbAutoJumpBySyncID.Name = "cbAutoJumpBySyncID";
            this.cbAutoJumpBySyncID.Size = new System.Drawing.Size(76, 14);
            this.cbAutoJumpBySyncID.TabIndex = 4;
            this.cbAutoJumpBySyncID.UseVisualStyleBackColor = true;
            // 
            // lblAutoJumpBySyncID
            // 
            this.lblAutoJumpBySyncID.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblAutoJumpBySyncID.AutoSize = true;
            this.lblAutoJumpBySyncID.Location = new System.Drawing.Point(3, 593);
            this.lblAutoJumpBySyncID.Margin = new System.Windows.Forms.Padding(3);
            this.lblAutoJumpBySyncID.Name = "lblAutoJumpBySyncID";
            this.lblAutoJumpBySyncID.Padding = new System.Windows.Forms.Padding(0, 6, 0, 6);
            this.lblAutoJumpBySyncID.Size = new System.Drawing.Size(76, 51);
            this.lblAutoJumpBySyncID.TabIndex = 0;
            this.lblAutoJumpBySyncID.Text = "Synchronize selected frame offset:";
            // 
            // lblAutoJumpBySyncIDInformation
            // 
            this.lblAutoJumpBySyncIDInformation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblAutoJumpBySyncIDInformation.AutoSize = true;
            this.lblAutoJumpBySyncIDInformation.Location = new System.Drawing.Point(167, 579);
            this.lblAutoJumpBySyncIDInformation.Margin = new System.Windows.Forms.Padding(3);
            this.lblAutoJumpBySyncIDInformation.Name = "lblAutoJumpBySyncIDInformation";
            this.lblAutoJumpBySyncIDInformation.Size = new System.Drawing.Size(78, 78);
            this.lblAutoJumpBySyncIDInformation.TabIndex = 0;
            this.lblAutoJumpBySyncIDInformation.Text = "Scrolls to the same frame ID in each text editor. The frame viewer is not affecte" +
    "d.";
            // 
            // UCSettingsTextEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Controls.Add(this.tlpMain);
            this.MinimumSize = new System.Drawing.Size(300, 0);
            this.Name = "UCSettingsTextEditor";
            this.Padding = new System.Windows.Forms.Padding(16);
            this.Size = new System.Drawing.Size(300, 721);
            ((System.ComponentModel.ISupportInitialize)(this.picCurrentLineColorPreview)).EndInit();
            this.tlpFontFamily.ResumeLayout(false);
            this.tlpFontFamily.PerformLayout();
            this.tlpLineColor.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picExampleBookmark)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picExampleIndention)).EndInit();
            this.tlpMain.ResumeLayout(false);
            this.tlpMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudFontSize)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.FontDialog fdFontFamily;
        internal System.Windows.Forms.Button btnChangeLineColor;
        internal System.Windows.Forms.PictureBox picCurrentLineColorPreview;
        internal System.Windows.Forms.TableLayoutPanel tlpFontFamily;
        internal System.Windows.Forms.Button btnChangeFontFamily;
        internal System.Windows.Forms.TextBox txtFontFamily;
        internal System.Windows.Forms.TableLayoutPanel tlpLineColor;
        internal System.Windows.Forms.Label lblChangeBar;
        internal System.Windows.Forms.Label lblSubfolding;
        internal System.Windows.Forms.PictureBox picExampleBookmark;
        internal System.Windows.Forms.CheckBox cbSubfolding;
        internal System.Windows.Forms.Label lblSubfoldingInformation;
        internal System.Windows.Forms.Label lblFoldingInformation;
        internal System.Windows.Forms.Label lblIndention;
        internal System.Windows.Forms.PictureBox picExampleIndention;
        internal System.Windows.Forms.Label lblScrollbarAnnotations;
        internal System.Windows.Forms.CheckBox cbIndention;
        internal System.Windows.Forms.CheckBox cbScrollbarAnnotations;
        internal System.Windows.Forms.TableLayoutPanel tlpMain;
        internal System.Windows.Forms.ComboBox combWrapMode;
        internal System.Windows.Forms.Label lblFontFamily;
        internal System.Windows.Forms.Label lblFontSize;
        internal System.Windows.Forms.CheckBox cbFolding;
        internal System.Windows.Forms.Label lblWrapMode;
        internal System.Windows.Forms.CheckBox cbAutocompletion;
        internal System.Windows.Forms.NumericUpDown nudFontSize;
        internal System.Windows.Forms.Label lblFolding;
        internal System.Windows.Forms.Label lblLineNumbers;
        internal System.Windows.Forms.Label lblAutocompletion;
        internal System.Windows.Forms.Label lblLineColor;
        internal System.Windows.Forms.CheckBox cbMarkCurrentLine;
        internal System.Windows.Forms.CheckBox cbWhitespaces;
        internal System.Windows.Forms.CheckBox cbLineNumbers;
        internal System.Windows.Forms.Label lblWhitespaces;
        internal System.Windows.Forms.Label lblCurrentLine;
        internal System.Windows.Forms.Label lblBookmarks;
        internal System.Windows.Forms.CheckBox cbChangeBar;
        internal System.Windows.Forms.CheckBox cbBookmarks;
        internal System.Windows.Forms.CheckBox cbAutoJumpBySyncID;
        internal System.Windows.Forms.Label lblAutoJumpBySyncID;
        internal System.Windows.Forms.Label lblAutoJumpBySyncIDInformation;
    }
}
