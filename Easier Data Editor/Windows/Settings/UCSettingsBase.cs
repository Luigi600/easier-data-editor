﻿using System.Drawing;
using System.Windows.Forms;
using Easier.MultiLangSupport.Forms;

namespace Easier_Data_Editor.Windows.Settings
{
    // can't be abstract because designer...
    public class UCSettingsBase : TranslatableUserControl
    {
        public virtual bool IsChildOfPreviousElement { get; } = false;

        protected UCSettingsBase() { }

        protected virtual Control MainControl() => null;

        public string GetDisplayName() => this.GetVariable("DISPLAY_NAME");

        public void SetMainLayout(Padding padding)
        {
            Control mainControl = this.MainControl();

            mainControl.Padding = new Padding(0, 0, padding.Right, padding.Bottom);
            mainControl.Margin = padding;
            mainControl.Dock = DockStyle.None;
            mainControl.Location = new Point(padding.Left, padding.Top);
            mainControl.Size = new Size(
                this.Width - padding.Left - padding.Right,
                this.Height - padding.Top - padding.Bottom
            );
            mainControl.Anchor = AnchorStyles.Left | AnchorStyles.Top | AnchorStyles.Right;
        }
    }
}
