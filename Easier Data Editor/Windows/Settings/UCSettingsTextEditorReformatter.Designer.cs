﻿
namespace Easier_Data_Editor.Windows.Settings
{
    partial class UCSettingsTextEditorReformatter
    {
        /// <summary> 
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Komponenten-Designer generierter Code

        /// <summary> 
        /// Erforderliche Methode für die Designerunterstützung. 
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.tlpMain = new System.Windows.Forms.TableLayoutPanel();
            this.tlpTop = new System.Windows.Forms.TableLayoutPanel();
            this.lblMultipleInstancesDescription = new System.Windows.Forms.Label();
            this.lblStructure = new System.Windows.Forms.Label();
            this.lblHelp = new System.Windows.Forms.Label();
            this.lblOnSave = new System.Windows.Forms.Label();
            this.btnHelp = new System.Windows.Forms.Button();
            this.cbOnSave = new System.Windows.Forms.CheckBox();
            this.combStructure = new System.Windows.Forms.ComboBox();
            this.scinBlueprint = new Easier_Data_Editor.Extensions.Scintilla.LF2Scintilla(Environments.Application.REFORMATTING_SYNTAX, this._syntaxSettings);
            this.tlpMain.SuspendLayout();
            this.tlpTop.SuspendLayout();
            this.SuspendLayout();
            // 
            // tlpMain
            // 
            this.tlpMain.AutoSize = true;
            this.tlpMain.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tlpMain.ColumnCount = 1;
            this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpMain.Controls.Add(this.tlpTop, 0, 0);
            this.tlpMain.Controls.Add(this.scinBlueprint, 0, 1);
            this.tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpMain.Location = new System.Drawing.Point(16, 16);
            this.tlpMain.Name = "tlpMain";
            this.tlpMain.RowCount = 2;
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlpMain.Size = new System.Drawing.Size(512, 379);
            this.tlpMain.TabIndex = 4;
            // 
            // tlpTop
            // 
            this.tlpTop.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tlpTop.AutoSize = true;
            this.tlpTop.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tlpTop.ColumnCount = 3;
            this.tlpTop.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlpTop.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlpTop.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlpTop.Controls.Add(this.lblMultipleInstancesDescription, 2, 0);
            this.tlpTop.Controls.Add(this.lblStructure, 0, 2);
            this.tlpTop.Controls.Add(this.lblHelp, 0, 1);
            this.tlpTop.Controls.Add(this.lblOnSave, 0, 0);
            this.tlpTop.Controls.Add(this.btnHelp, 1, 1);
            this.tlpTop.Controls.Add(this.cbOnSave, 1, 0);
            this.tlpTop.Controls.Add(this.combStructure, 1, 2);
            this.tlpTop.Location = new System.Drawing.Point(3, 3);
            this.tlpTop.Name = "tlpTop";
            this.tlpTop.RowCount = 3;
            this.tlpTop.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpTop.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpTop.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpTop.Size = new System.Drawing.Size(506, 131);
            this.tlpTop.TabIndex = 4;
            // 
            // lblMultipleInstancesDescription
            // 
            this.lblMultipleInstancesDescription.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMultipleInstancesDescription.AutoSize = true;
            this.lblMultipleInstancesDescription.Location = new System.Drawing.Point(339, 3);
            this.lblMultipleInstancesDescription.Margin = new System.Windows.Forms.Padding(3);
            this.lblMultipleInstancesDescription.Name = "lblMultipleInstancesDescription";
            this.lblMultipleInstancesDescription.Padding = new System.Windows.Forms.Padding(0, 6, 0, 6);
            this.lblMultipleInstancesDescription.Size = new System.Drawing.Size(164, 64);
            this.lblMultipleInstancesDescription.TabIndex = 5;
            this.lblMultipleInstancesDescription.Text = "Automatically triggers formatting as soon as the file is saved. Otherwise, the fo" +
    "rmatting must be executed manually each time.";
            // 
            // lblStructure
            // 
            this.lblStructure.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblStructure.AutoSize = true;
            this.lblStructure.Location = new System.Drawing.Point(3, 105);
            this.lblStructure.Margin = new System.Windows.Forms.Padding(3);
            this.lblStructure.Name = "lblStructure";
            this.lblStructure.Padding = new System.Windows.Forms.Padding(0, 6, 0, 6);
            this.lblStructure.Size = new System.Drawing.Size(162, 25);
            this.lblStructure.TabIndex = 4;
            this.lblStructure.Text = "Structure:";
            this.lblStructure.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblHelp
            // 
            this.lblHelp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblHelp.AutoSize = true;
            this.lblHelp.Location = new System.Drawing.Point(3, 73);
            this.lblHelp.Margin = new System.Windows.Forms.Padding(3);
            this.lblHelp.Name = "lblHelp";
            this.lblHelp.Padding = new System.Windows.Forms.Padding(0, 6, 0, 6);
            this.lblHelp.Size = new System.Drawing.Size(162, 25);
            this.lblHelp.TabIndex = 0;
            this.lblHelp.Text = "Help:";
            this.lblHelp.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblOnSave
            // 
            this.lblOnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblOnSave.AutoSize = true;
            this.lblOnSave.Location = new System.Drawing.Point(3, 22);
            this.lblOnSave.Margin = new System.Windows.Forms.Padding(3);
            this.lblOnSave.Name = "lblOnSave";
            this.lblOnSave.Padding = new System.Windows.Forms.Padding(0, 6, 0, 6);
            this.lblOnSave.Size = new System.Drawing.Size(162, 25);
            this.lblOnSave.TabIndex = 0;
            this.lblOnSave.Text = "On save:";
            this.lblOnSave.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnHelp
            // 
            this.btnHelp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.btnHelp.Location = new System.Drawing.Point(171, 74);
            this.btnHelp.Name = "btnHelp";
            this.btnHelp.Size = new System.Drawing.Size(162, 22);
            this.btnHelp.TabIndex = 1;
            this.btnHelp.Text = "open instructions";
            this.btnHelp.UseVisualStyleBackColor = true;
            // 
            // cbOnSave
            // 
            this.cbOnSave.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cbOnSave.Location = new System.Drawing.Point(171, 3);
            this.cbOnSave.Name = "cbOnSave";
            this.cbOnSave.Size = new System.Drawing.Size(162, 64);
            this.cbOnSave.TabIndex = 3;
            this.cbOnSave.UseVisualStyleBackColor = true;
            // 
            // combStructure
            // 
            this.combStructure.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.combStructure.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.combStructure.FormattingEnabled = true;
            this.combStructure.Items.AddRange(new object[] {
            "Bitmap",
            "   File Row",
            "Weapon Strength List",
            "   Entry",
            "Frame",
            "   Blood Point",
            "   Catch Point",
            "   Object Point",
            "   Weapon Point",
            "   Body",
            "   Itr",
            "Background",
            "   Layer",
            "Stage",
            "   Phase",
            "      Spawn",
            "Data.txt",
            "   Objects",
            "   File Editings",
            "   Backgrounds"});
            this.combStructure.Location = new System.Drawing.Point(172, 107);
            this.combStructure.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.combStructure.Name = "combStructure";
            this.combStructure.Size = new System.Drawing.Size(160, 21);
            this.combStructure.TabIndex = 5;
            this.combStructure.SelectedIndexChanged += new System.EventHandler(this.CombStructure_SelectedIndexChanged);
            // 
            // scinBlueprint
            // 
            this.scinBlueprint.AdditionalSelectionTyping = true;
            this.scinBlueprint.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.scinBlueprint.AutoCMaxHeight = 9;
            this.scinBlueprint.BiDirectionality = ScintillaNET.BiDirectionalDisplayType.Disabled;
            this.scinBlueprint.BlockUndoRedoActions = false;
            this.scinBlueprint.CaretLineBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(235)))), ((int)(((byte)(255)))));
            this.scinBlueprint.CaretLineVisible = true;
            this.scinBlueprint.CaretLineVisibleAlways = true;
            this.scinBlueprint.LexerName = null;
            this.scinBlueprint.Location = new System.Drawing.Point(3, 140);
            this.scinBlueprint.Margins.Capacity = 10;
            this.scinBlueprint.Margins.Left = 5;
            this.scinBlueprint.MinimumSize = new System.Drawing.Size(4, 50);
            this.scinBlueprint.MouseSelectionRectangularSwitch = true;
            this.scinBlueprint.MultipleSelection = true;
            this.scinBlueprint.Name = "scinBlueprint";
            this.scinBlueprint.ScrollWidth = 91;
            this.scinBlueprint.Size = new System.Drawing.Size(506, 236);
            this.scinBlueprint.SuppressFoldingCheck = false;
            this.scinBlueprint.TabIndents = true;
            this.scinBlueprint.TabIndex = 5;
            //this.scinBlueprint.UseRightToLeftReadingLayout = false;
            this.scinBlueprint.VirtualSpaceOptions = ScintillaNET.VirtualSpace.RectangularSelection;
            this.scinBlueprint.WhitespaceSize = 3;
            this.scinBlueprint.WrapMode = ScintillaNET.WrapMode.None;
            // 
            // UCSettingsTextEditorReformatter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Controls.Add(this.tlpMain);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.MinimumSize = new System.Drawing.Size(300, 0);
            this.Name = "UCSettingsTextEditorReformatter";
            this.Padding = new System.Windows.Forms.Padding(16);
            this.Size = new System.Drawing.Size(544, 411);
            this.tlpMain.ResumeLayout(false);
            this.tlpMain.PerformLayout();
            this.tlpTop.ResumeLayout(false);
            this.tlpTop.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.TableLayoutPanel tlpMain;
        internal System.Windows.Forms.TableLayoutPanel tlpTop;
        internal System.Windows.Forms.Label lblHelp;
        internal System.Windows.Forms.Label lblOnSave;
        private System.Windows.Forms.Button btnHelp;
        private System.Windows.Forms.CheckBox cbOnSave;
        private Extensions.Scintilla.LF2Scintilla scinBlueprint;
        internal System.Windows.Forms.Label lblStructure;
        private System.Windows.Forms.ComboBox combStructure;
        internal System.Windows.Forms.Label lblMultipleInstancesDescription;
    }
}
