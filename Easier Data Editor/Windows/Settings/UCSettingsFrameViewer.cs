﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Easier_Data_Editor.Windows.Settings
{
    public partial class UCSettingsFrameViewer : UCSettingsBase
    {
        private readonly Environments.Settings.FrameViewer _settings;
        private readonly Dictionary<TextBox, Label> _fileNotFoundMapper =
            new Dictionary<TextBox, Label>();

        public UCSettingsFrameViewer(Environments.Settings.FrameViewer settings)
            : base()
        {
            this._settings = settings;
            this.VariablesList.Add("DISPLAY_NAME", "Frame Viewer");

            InitializeComponent();

            this._fileNotFoundMapper.Add(this.txtWeaponData, this.lblWeaponDataFound);
            this._fileNotFoundMapper.Add(this.txtWeaponSprite, this.lblWeaponSpriteFound);
            this._fileNotFoundMapper.Add(this.txtHeavyWeaponData, this.lblHeavyWeaponDataFound);
            this._fileNotFoundMapper.Add(this.txtHeavyWeaponSprite, this.lblHeavyWeaponSpriteFound);

            // trigger one time the event to set the visibility of "file not found" labels
            foreach (KeyValuePair<TextBox, Label> entry in this._fileNotFoundMapper)
                this.TxtWeaponDataOrSprite_TextChanged(entry.Key, EventArgs.Empty);

            this.SetBindings();
            this.AfterControlsInit();
        }

        // necessary for the designer and translation creation
        private UCSettingsFrameViewer()
            : this(new Environments.Settings.FrameViewer()) { }

        protected override Control MainControl() => this.tlpMain;

        private void SetBindings()
        {
            this.cbMenuStrip.DataBindings.Add(
                Constants.BINDING_CHECK_BOX_VALUE,
                this._settings,
                "MenuStrip",
                true,
                DataSourceUpdateMode.OnPropertyChanged
            );

            this.picDefaultBGPreview.DataBindings.Add(
                Constants.BINDING_PICTURE_BOX_BACK_COLOR,
                this._settings,
                "BackgroundColor",
                true,
                DataSourceUpdateMode.OnPropertyChanged
            );

            this.cbGlobalView.DataBindings.Add(
                Constants.BINDING_CHECK_BOX_VALUE,
                this._settings,
                "GlobalView",
                true,
                DataSourceUpdateMode.OnPropertyChanged
            );

            this.cbFullRestore.DataBindings.Add(
                Constants.BINDING_CHECK_BOX_VALUE,
                this._settings,
                "FullRestore",
                true,
                DataSourceUpdateMode.OnPropertyChanged
            );

            this.cbDirect.DataBindings.Add(
                Constants.BINDING_CHECK_BOX_VALUE,
                this._settings,
                "ChangeDirect",
                true,
                DataSourceUpdateMode.OnPropertyChanged
            );

            this.txtWeaponData.DataBindings.Add(
                Constants.BINDING_TEXT_BOX_VALUE,
                this._settings,
                "WeaponData",
                true,
                DataSourceUpdateMode.OnPropertyChanged
            );

            this.txtWeaponSprite.DataBindings.Add(
                Constants.BINDING_TEXT_BOX_VALUE,
                this._settings,
                "WeaponSprite",
                true,
                DataSourceUpdateMode.OnPropertyChanged
            );

            this.txtHeavyWeaponData.DataBindings.Add(
                Constants.BINDING_TEXT_BOX_VALUE,
                this._settings,
                "HeavyWeaponData",
                true,
                DataSourceUpdateMode.OnPropertyChanged
            );

            this.lblHeavyWeaponSprite.DataBindings.Add(
                Constants.BINDING_TEXT_BOX_VALUE,
                this._settings,
                "HeavyWeaponSprite",
                true,
                DataSourceUpdateMode.OnPropertyChanged
            );

            this.cbSyncSelectedFrame.DataBindings.Add(
                Constants.BINDING_CHECK_BOX_VALUE,
                this._settings,
                "SyncFrameID",
                true,
                DataSourceUpdateMode.OnPropertyChanged
            );
        }

        private void BtnChangeDefaultBG_Click(object sender, EventArgs e)
        {
            ColorDialog2.ColorDialog dialog = new ColorDialog2.ColorDialog(
                this.picDefaultBGPreview.BackColor
            );
            if (dialog.ShowDialog() == DialogResult.OK)
                this._settings.BackgroundColor = dialog.SelectedColor;
        }

        private void BtnImage_Click(object sender, EventArgs e) =>
            this._settings.BackgroundColor = Color.Transparent;

        private void BtnChangePathWeaponData_Click(object sender, EventArgs e)
        {
            if (this.ofdData.ShowDialog() == DialogResult.OK)
                this.txtWeaponData.Text = this.ofdData.FileName;
        }

        private void BtnChangePathWeaponSprite_Click(object sender, EventArgs e)
        {
            if (this.ofdSprite.ShowDialog() == DialogResult.OK)
                this.txtWeaponSprite.Text = this.ofdSprite.FileName;
        }

        private void BtnChangePathHWeaponData_Click(object sender, EventArgs e)
        {
            if (this.ofdData.ShowDialog() == DialogResult.OK)
                this.txtHeavyWeaponData.Text = this.ofdData.FileName;
        }

        private void BtnChangePathHWeaponSprite_Click(object sender, EventArgs e)
        {
            if (this.ofdSprite.ShowDialog() == DialogResult.OK)
                this.txtHeavyWeaponSprite.Text = this.ofdSprite.FileName;
        }

        private void BtnResetWeaponData_Click(object sender, EventArgs e) =>
            this.txtWeaponData.Text = "";

        private void BtnResetWeaponSprite_Click(object sender, EventArgs e) =>
            this.txtWeaponSprite.Text = "";

        private void BtnResetHeavyWeaponData_Click(object sender, EventArgs e) =>
            this.txtHeavyWeaponData.Text = "";

        private void BtnResetHeavyWeaponSprite_Click(object sender, EventArgs e) =>
            this.txtHeavyWeaponSprite.Text = "";

        private void TxtWeaponDataOrSprite_TextChanged(object sender, EventArgs e)
        {
            // I used "pattern matching" then SonarQube said: use "as"
            // now the IDE says: use "pattern matching"...
            TextBox? txtbox = sender as TextBox;
            if (txtbox == null)
                return;

            if (!this._fileNotFoundMapper.TryGetValue(txtbox, out Label? value))
                return;

            value.Visible =
                !string.IsNullOrEmpty(txtbox.Text) && !System.IO.File.Exists(txtbox.Text);
        }
    }
}
