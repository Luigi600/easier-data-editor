﻿
namespace Easier_Data_Editor.Windows.Settings.ModifyRecentFiles
{
    partial class UCRecentFile
    {
        /// <summary> 
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Komponenten-Designer generierter Code

        /// <summary> 
        /// Erforderliche Methode für die Designerunterstützung. 
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblPath = new System.Windows.Forms.Label();
            this.btnRemove = new System.Windows.Forms.Button();
            this.palHr = new System.Windows.Forms.Panel();
            this.btnUp = new Easier_Data_Editor.Forms.FakeButton();
            this.btnDown = new Easier_Data_Editor.Forms.FakeButton();
            this.SuspendLayout();
            // 
            // lblPath
            // 
            this.lblPath.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPath.AutoEllipsis = true;
            this.lblPath.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPath.Location = new System.Drawing.Point(48, 4);
            this.lblPath.Name = "lblPath";
            this.lblPath.Size = new System.Drawing.Size(321, 43);
            this.lblPath.TabIndex = 0;
            this.lblPath.Text = "label1";
            this.lblPath.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnRemove
            // 
            this.btnRemove.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRemove.BackgroundImage = global::Easier_Data_Editor.Properties.Resources.icon_delete;
            this.btnRemove.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnRemove.FlatAppearance.BorderSize = 0;
            this.btnRemove.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRemove.Location = new System.Drawing.Point(375, 14);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(22, 22);
            this.btnRemove.TabIndex = 1;
            this.btnRemove.UseVisualStyleBackColor = true;
            this.btnRemove.Click += new System.EventHandler(this.BtnRemove_Click);
            // 
            // palHr
            // 
            this.palHr.BackColor = System.Drawing.Color.DarkGray;
            this.palHr.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.palHr.Location = new System.Drawing.Point(0, 49);
            this.palHr.Name = "palHr";
            this.palHr.Size = new System.Drawing.Size(412, 1);
            this.palHr.TabIndex = 3;
            // 
            // btnUp
            // 
            this.btnUp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnUp.ImageDefault = null;
            this.btnUp.ImageDisabled = null;
            this.btnUp.ImageHover = null;
            this.btnUp.Location = new System.Drawing.Point(6, 3);
            this.btnUp.Name = "btnUp";
            this.btnUp.Size = new System.Drawing.Size(22, 22);
            this.btnUp.TabIndex = 4;
            this.btnUp.MouseClick += new System.Windows.Forms.MouseEventHandler(this.BtnUp_MouseClick);
            // 
            // btnDown
            // 
            this.btnDown.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnDown.ImageDefault = null;
            this.btnDown.ImageDisabled = null;
            this.btnDown.ImageHover = null;
            this.btnDown.Location = new System.Drawing.Point(6, 25);
            this.btnDown.Name = "btnDown";
            this.btnDown.Size = new System.Drawing.Size(22, 22);
            this.btnDown.TabIndex = 4;
            this.btnDown.MouseClick += new System.Windows.Forms.MouseEventHandler(this.BtnDown_MouseClick);
            // 
            // UCRecentFile
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.btnDown);
            this.Controls.Add(this.btnUp);
            this.Controls.Add(this.palHr);
            this.Controls.Add(this.btnRemove);
            this.Controls.Add(this.lblPath);
            this.Name = "UCRecentFile";
            this.Size = new System.Drawing.Size(412, 50);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblPath;
        private System.Windows.Forms.Button btnRemove;
        private System.Windows.Forms.Panel palHr;
        private Forms.FakeButton btnUp;
        private Forms.FakeButton btnDown;
    }
}
