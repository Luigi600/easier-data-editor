﻿using System.Drawing;
using Easier_Data_Editor.Properties;
using Easier_Data_Editor.Utilities;

namespace Easier_Data_Editor.Windows.Settings.ModifyRecentFiles
{
    internal static class Constants
    {
        private const float HOVER_IMAGE_BRIGHTNESS = (float)0.4;
        private const float DISABLED_IMAGE_BRIGHTNESS = (float)0.7;

        public static readonly Bitmap DEFAULT_IMAGE = (Bitmap)Resources.icon_up.Clone();
        public static readonly Bitmap DEFAULT_IMAGE_INVERTED = DrawUtil.InvertImage(
            Resources.icon_up
        );
        public static readonly Bitmap HOVER_IMAGE = DrawUtil.ChangeBrightness(
            DrawUtil.InvertImage(Resources.icon_up),
            HOVER_IMAGE_BRIGHTNESS
        );
        public static readonly Bitmap DISABLED_IMAGE = DrawUtil.ChangeBrightness(
            DrawUtil.InvertImage(Resources.icon_up),
            DISABLED_IMAGE_BRIGHTNESS
        );

        public static readonly Bitmap DEFAULT_IMAGE_ROTATED = Rotate(DEFAULT_IMAGE);
        public static readonly Bitmap HOVER_IMAGE_ROTATED = Rotate(HOVER_IMAGE);
        public static readonly Bitmap DISABLED_IMAGE_ROTATED = Rotate(DISABLED_IMAGE);

        private static Bitmap Rotate(Bitmap bitmap)
        {
            Bitmap bit = (Bitmap)bitmap.Clone();
            bit.RotateFlip(RotateFlipType.Rotate180FlipX);
            return bit;
        }
    }
}
