﻿using System;
using System.Windows.Forms;
using Easier.MultiLangSupport;
using Easier.MultiLangSupport.Forms;

namespace Easier_Data_Editor.Windows.Settings.ModifyRecentFiles
{
    [SkipComponentTranslation]
    public partial class UCRecentFile : TranslatableUserControl
    {
        public event EventHandler? WillRemove;
        public event EventHandler? WillMoveUp;
        public event EventHandler? WillMoveDown;

        public string Path { get; }

        public bool CanUp
        {
            get => this.btnUp.Enabled;
            set => this.btnUp.Enabled = value;
        }

        public bool CanDown
        {
            get => this.btnDown.Enabled;
            set
            {
                this.btnDown.Enabled = value;
                this.palHr.Visible = value;
            }
        }

        public UCRecentFile(string path, bool canUp = false, bool canDown = false)
        {
            this.Path = path;

            InitializeComponent();

            this.CanUp = canUp;
            this.CanDown = canDown;
            this.lblPath.Text = path;

            this.btnUp.BackgroundImageLayout = ImageLayout.Center;
            this.btnUp.ImageDefault = Constants.DEFAULT_IMAGE;
            this.btnUp.ImageDisabled = Constants.DISABLED_IMAGE;
            this.btnUp.ImageHover = Constants.HOVER_IMAGE;

            this.btnDown.BackgroundImageLayout = ImageLayout.Center;
            this.btnDown.ImageDefault = Constants.DEFAULT_IMAGE_ROTATED;
            this.btnDown.ImageDisabled = Constants.DISABLED_IMAGE_ROTATED;
            this.btnDown.ImageHover = Constants.HOVER_IMAGE_ROTATED;

            this.AfterControlsInit();
        }

        private void BtnRemove_Click(object sender, EventArgs e) =>
            this.WillRemove?.Invoke(this, EventArgs.Empty);

        private void BtnUp_MouseClick(object sender, System.Windows.Forms.MouseEventArgs e) =>
            this.WillMoveUp?.Invoke(this, EventArgs.Empty);

        private void BtnDown_MouseClick(object sender, System.Windows.Forms.MouseEventArgs e) =>
            this.WillMoveDown?.Invoke(this, EventArgs.Empty);
    }
}
