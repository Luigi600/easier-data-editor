﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Easier_Data_Editor.Windows.Settings.ModifyRecentFiles
{
    public partial class DialogModifyRecentFiles : EscapeCloseableWindow
    {
        public event EventHandler<List<string>>? WillChangeRecentFiles;

        private readonly List<string> _originalList = new List<string>();
        private bool _cancelOrSafeExit;

        // for translatable lib
        private DialogModifyRecentFiles(bool initComponents = true)
        {
            this.VariablesList.Add(
                "APPLY_TITLE",
                $"{Environments.Application.APP_TITLE}    -   Apply changes?"
            );
            this.VariablesList.Add("APPLY", "Do you want apply the changes?");

            if (initComponents)
                this.InitializeComponent();
        }

        public DialogModifyRecentFiles(List<string> recentFiles)
            : this(false)
        {
            this._originalList.AddRange(recentFiles);
            List<string> list = new List<string>();
            list.AddRange(recentFiles);

            this.InitializeComponent();

            this.GenerateList(list);
            this.AfterControlsInit();
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            if (this._cancelOrSafeExit)
            {
                base.OnFormClosing(e);
                return;
            }

            List<string> list = this.GetNewRecentList();
            if (!this._originalList.SequenceEqual(list))
            {
                DialogResult buttonResult = MessageBox.Show(
                    GetVariable("APPLY"),
                    GetVariable("APPLY_TITLE"),
                    MessageBoxButtons.YesNoCancel,
                    MessageBoxIcon.Question
                );
                if (buttonResult == DialogResult.Cancel)
                    e.Cancel = true;
                else if (buttonResult == DialogResult.Yes)
                    this.WillChangeRecentFiles?.Invoke(this, list);
            }

            base.OnFormClosing(e);
        }

        private void GenerateList(List<string> list)
        {
            foreach (Control control in this.palFiles.Controls)
            {
                if (control is UCRecentFile ucFile)
                {
                    ucFile.WillRemove -= this.UcFile_WillRemove;
                    ucFile.WillMoveUp -= this.UcFile_WillMoveUp;
                    ucFile.WillMoveDown -= this.UcFile_WillMoveDown;
                }
            }

            this.palFiles.Controls.Clear();

            bool isFirst = true;
            UCRecentFile? last = null;
            foreach (string file in list)
            {
                if (last != null)
                    last.CanDown = true;

                UCRecentFile ucFile = new UCRecentFile(file, !isFirst);
                ucFile.Dock = DockStyle.Top;
                ucFile.WillRemove += this.UcFile_WillRemove;
                ucFile.WillMoveUp += this.UcFile_WillMoveUp;
                ucFile.WillMoveDown += this.UcFile_WillMoveDown;

                this.palFiles.Controls.Add(ucFile);
                ucFile.BringToFront();

                last = ucFile;
                isFirst = false;
            }
        }

        private void UcFile_WillMoveDown(object? sender, EventArgs e)
        {
            if (!(sender is UCRecentFile ucFile))
                return;

            List<Tuple<int, UCRecentFile>> controls = this
                .palFiles.Controls.Cast<Control>()
                .Where(c => c is UCRecentFile)
                .Cast<UCRecentFile>()
                .Select(c => new Tuple<int, UCRecentFile>(c.Location.Y, c))
                .ToList();
            controls.Sort((c1, c2) => c2.Item1 - c1.Item1); // reverse order
            int index = controls.FindIndex(e => e.Item1 == ucFile.Location.Y);
            for (int i = index - 1; i < controls.Count; ++i)
            {
                if (i == index)
                    continue; // skip the current item (coz we want to exchange the pos)

                Tuple<int, UCRecentFile> file = controls[i];
                file.Item2.SendToBack();
            }

            this.ResetButtonStates();
        }

        private void UcFile_WillMoveUp(object? sender, EventArgs e)
        {
            if (!(sender is UCRecentFile ucFile))
                return;

            List<Tuple<int, UCRecentFile>> controls = this
                .palFiles.Controls.Cast<Control>()
                .Where(c => c is UCRecentFile)
                .Cast<UCRecentFile>()
                .Select(c => new Tuple<int, UCRecentFile>(c.Location.Y, c))
                .ToList();
            controls.Sort((c1, c2) => c2.Item1 - c1.Item1); // reverse order
            int oldY = ucFile.Location.Y;
            int index = controls.FindIndex(e => e.Item1 == oldY);
            ucFile.SendToBack();
            for (int i = 0; i < controls.Count; ++i)
            {
                if (i == index + 1)
                    continue; // skip the prev item (coz we want to exchange the pos) - +1 and not -1 coz reverse

                Tuple<int, UCRecentFile> file = controls[i];
                if (file.Item1 < oldY)
                    file.Item2.SendToBack();
            }

            this.ResetButtonStates();
        }

        private void UcFile_WillRemove(object? sender, EventArgs e)
        {
            if (!(sender is UCRecentFile ucFile))
                return;

            ucFile.Parent?.Controls.Remove(ucFile);
            this.ResetButtonStates();
        }

        private void BtnApply_Click(object sender, EventArgs e)
        {
            this.WillChangeRecentFiles?.Invoke(this, this.GetNewRecentList());
            this._cancelOrSafeExit = true;
            this.Close();
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this._cancelOrSafeExit = true;
            this.Close();
        }

        private void ResetButtonStates()
        {
            List<UCRecentFile> controls = this
                .palFiles.Controls.Cast<Control>()
                .Where(c => c is UCRecentFile)
                .Cast<UCRecentFile>()
                .ToList();
            controls.Sort((c1, c2) => c1.Location.Y - c2.Location.Y);

            if (controls.Count == 0)
                return;

            foreach (UCRecentFile file in controls)
            {
                file.CanUp = true;
                file.CanDown = true;
            }

            controls[0].CanUp = false;
            controls[controls.Count - 1].CanDown = false;
        }

        private List<string> GetNewRecentList()
        {
            List<string> list = new List<string>();
            List<UCRecentFile> controls = this
                .palFiles.Controls.Cast<Control>()
                .Where(c => c is UCRecentFile)
                .Cast<UCRecentFile>()
                .ToList();
            controls.Sort((c1, c2) => c1.Location.Y - c2.Location.Y); // reverse coz recent files are reverse

            foreach (UCRecentFile ucFile in controls)
                list.Add(ucFile.Path);

            return list;
        }

        private void TimDrawer_Tick(object sender, EventArgs e)
        {
            foreach (Control con in this.palFiles.Controls)
                con.Invalidate();

            this.palFiles.Invalidate();
        }
    }
}
