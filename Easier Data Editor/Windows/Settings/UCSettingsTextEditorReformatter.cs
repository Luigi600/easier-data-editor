﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Easier_Data_Editor.Environments.FormatBlueprints;
using Easier_Data_Editor.Environments.Settings;
using ScintillaNET;

namespace Easier_Data_Editor.Windows.Settings
{
    public partial class UCSettingsTextEditorReformatter : UCSettingsBase
    {
        private readonly TextEditor _settings;
        private readonly Blueprints _blueprints;

        private readonly TextEditor _syntaxSettings;

        private readonly Dictionary<int, Func<string>> _combIndexToGetterResolver =
            new Dictionary<int, Func<string>>();
        private readonly Dictionary<int, Action<string>> _combIndexToSetterResolver =
            new Dictionary<int, Action<string>>();

        private bool _ignoreTextChange = true;

        public override bool IsChildOfPreviousElement { get; } = true;

        public UCSettingsTextEditorReformatter(Blueprints blueprints, TextEditor settings)
            : base()
        {
            this._blueprints = blueprints;
            this._settings = settings;
            this._syntaxSettings = new TextEditor()
            {
                AutoJumpBySyncID = false,
                Autocompletion = false,
                Bookmarks = false,
                Folding = false,
                ReformattingOnSave = false,
                ScrollbarAnnotations = false,
            };
            this.VariablesList.Add("DISPLAY_NAME", "Reformatting");

            InitializeComponent();

            this._combIndexToGetterResolver.Add(
                this._combIndexToGetterResolver.Count,
                () => this._blueprints.GetBitmapHeader()
            );
            this._combIndexToGetterResolver.Add(
                this._combIndexToGetterResolver.Count,
                () => this._blueprints.GetBitmapFileRow()
            );
            this._combIndexToGetterResolver.Add(
                this._combIndexToGetterResolver.Count,
                () => this._blueprints.GetWeaponStrengthList()
            );
            this._combIndexToGetterResolver.Add(
                this._combIndexToGetterResolver.Count,
                () => this._blueprints.GetWeaponStrengthListEntry()
            );
            this._combIndexToGetterResolver.Add(
                this._combIndexToGetterResolver.Count,
                () => this._blueprints.GetFrame()
            );
            this._combIndexToGetterResolver.Add(
                this._combIndexToGetterResolver.Count,
                () => this._blueprints.GetFrameBloodPoint()
            );
            this._combIndexToGetterResolver.Add(
                this._combIndexToGetterResolver.Count,
                () => this._blueprints.GetFrameCatchPoint()
            );
            this._combIndexToGetterResolver.Add(
                this._combIndexToGetterResolver.Count,
                () => this._blueprints.GetFrameObjectPoint()
            );
            this._combIndexToGetterResolver.Add(
                this._combIndexToGetterResolver.Count,
                () => this._blueprints.GetFrameWeaponPoint()
            );
            this._combIndexToGetterResolver.Add(
                this._combIndexToGetterResolver.Count,
                () => this._blueprints.GetFrameBody()
            );
            this._combIndexToGetterResolver.Add(
                this._combIndexToGetterResolver.Count,
                () => this._blueprints.GetFrameItr()
            );
            this._combIndexToGetterResolver.Add(
                this._combIndexToGetterResolver.Count,
                () => this._blueprints.GetBackground()
            );
            this._combIndexToGetterResolver.Add(
                this._combIndexToGetterResolver.Count,
                () => this._blueprints.GetBackgroundLayer()
            );
            this._combIndexToGetterResolver.Add(
                this._combIndexToGetterResolver.Count,
                () => this._blueprints.GetStageStage()
            );
            this._combIndexToGetterResolver.Add(
                this._combIndexToGetterResolver.Count,
                () => this._blueprints.GetStagePhase()
            );
            this._combIndexToGetterResolver.Add(
                this._combIndexToGetterResolver.Count,
                () => this._blueprints.GetStageSpawn()
            );
            this._combIndexToGetterResolver.Add(
                this._combIndexToGetterResolver.Count,
                () => this._blueprints.GetDataTxtDataTxt()
            );
            this._combIndexToGetterResolver.Add(
                this._combIndexToGetterResolver.Count,
                () => this._blueprints.GetDataTxtObject()
            );
            this._combIndexToGetterResolver.Add(
                this._combIndexToGetterResolver.Count,
                () => this._blueprints.GetDataTxtFileEditing()
            );
            this._combIndexToGetterResolver.Add(
                this._combIndexToGetterResolver.Count,
                () => this._blueprints.GetDataTxtBackground()
            );

            this._combIndexToSetterResolver.Add(
                this._combIndexToSetterResolver.Count,
                (val) => this._blueprints.SetBitmapHeader(val)
            );
            this._combIndexToSetterResolver.Add(
                this._combIndexToSetterResolver.Count,
                (val) => this._blueprints.SetBitmapFileRow(val)
            );
            this._combIndexToSetterResolver.Add(
                this._combIndexToSetterResolver.Count,
                (val) => this._blueprints.SetWeaponStrengthList(val)
            );
            this._combIndexToSetterResolver.Add(
                this._combIndexToSetterResolver.Count,
                (val) => this._blueprints.SetWeaponStrengthListEntry(val)
            );
            this._combIndexToSetterResolver.Add(
                this._combIndexToSetterResolver.Count,
                (val) => this._blueprints.SetFrame(val)
            );
            this._combIndexToSetterResolver.Add(
                this._combIndexToSetterResolver.Count,
                (val) => this._blueprints.SetFrameBloodPoint(val)
            );
            this._combIndexToSetterResolver.Add(
                this._combIndexToSetterResolver.Count,
                (val) => this._blueprints.SetFrameCatchPoint(val)
            );
            this._combIndexToSetterResolver.Add(
                this._combIndexToSetterResolver.Count,
                (val) => this._blueprints.SetFrameObjectPoint(val)
            );
            this._combIndexToSetterResolver.Add(
                this._combIndexToSetterResolver.Count,
                (val) => this._blueprints.SetFrameWeaponPoint(val)
            );
            this._combIndexToSetterResolver.Add(
                this._combIndexToSetterResolver.Count,
                (val) => this._blueprints.SetFrameBody(val)
            );
            this._combIndexToSetterResolver.Add(
                this._combIndexToSetterResolver.Count,
                (val) => this._blueprints.SetFrameItr(val)
            );
            this._combIndexToSetterResolver.Add(
                this._combIndexToSetterResolver.Count,
                (val) => this._blueprints.SetBackground(val)
            );
            this._combIndexToSetterResolver.Add(
                this._combIndexToSetterResolver.Count,
                (val) => this._blueprints.SetBackgroundLayer(val)
            );
            this._combIndexToSetterResolver.Add(
                this._combIndexToSetterResolver.Count,
                (val) => this._blueprints.SetStageStage(val)
            );
            this._combIndexToSetterResolver.Add(
                this._combIndexToSetterResolver.Count,
                (val) => this._blueprints.SetStagePhase(val)
            );
            this._combIndexToSetterResolver.Add(
                this._combIndexToSetterResolver.Count,
                (val) => this._blueprints.SetStageSpawn(val)
            );
            this._combIndexToSetterResolver.Add(
                this._combIndexToSetterResolver.Count,
                (val) => this._blueprints.SetDataTxtDataTxt(val)
            );
            this._combIndexToSetterResolver.Add(
                this._combIndexToSetterResolver.Count,
                (val) => this._blueprints.SetDataTxtObject(val)
            );
            this._combIndexToSetterResolver.Add(
                this._combIndexToSetterResolver.Count,
                (val) => this._blueprints.SetDataTxtFileEditing(val)
            );
            this._combIndexToSetterResolver.Add(
                this._combIndexToSetterResolver.Count,
                (val) => this._blueprints.SetDataTxtBackground(val)
            );

            this.scinBlueprint.TextChanged += this.ScinBlueprint_TextChanged;

            this.SetBindings();
            this.AfterControlsInit();

            this.combStructure.SelectedIndex = 0;
        }

        // necessary for the designer (private) and translation creation (public)
        public UCSettingsTextEditorReformatter()
            : this(new Blueprints(), new Environments.Settings.TextEditor()) { }

        protected override Control MainControl() => this.tlpMain;

        private void SetBindings()
        {
            this.cbOnSave.DataBindings.Add(
                Constants.BINDING_CHECK_BOX_VALUE,
                this._settings,
                nameof(Environments.Settings.TextEditor.ReformattingOnSave),
                true,
                DataSourceUpdateMode.OnPropertyChanged
            );
        }

        private void CombStructure_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (
                !this._combIndexToGetterResolver.TryGetValue(
                    this.combStructure.SelectedIndex,
                    out Func<string>? value
                )
            )
                return;

            this._ignoreTextChange = true;
            this.scinBlueprint.Document = new Document(); // suppress undo/redo of the old text
            this.scinBlueprint.SetStartText(value());
            this._ignoreTextChange = false;
        }

        private void ScinBlueprint_TextChanged(object? sender, EventArgs e)
        {
            if (this._ignoreTextChange)
                return;

            if (
                !this._combIndexToSetterResolver.TryGetValue(
                    this.combStructure.SelectedIndex,
                    out Action<string>? value
                )
            )
                return;

            value(this.scinBlueprint.Text);
        }
    }
}
