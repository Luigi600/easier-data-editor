﻿
namespace Easier_Data_Editor.Windows
{
    partial class About
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(About));
            this.btnOkay = new System.Windows.Forms.Button();
            this.linkLibrary2 = new System.Windows.Forms.LinkLabel();
            this.linkLibrary1 = new System.Windows.Forms.LinkLabel();
            this.linkLuiStudio = new System.Windows.Forms.LinkLabel();
            this.lblVersionValue2 = new System.Windows.Forms.Label();
            this.lblVersionValue = new System.Windows.Forms.Label();
            this.lblCreatorValue = new System.Windows.Forms.Label();
            this.lblVersion = new System.Windows.Forms.Label();
            this.lblLibraries = new System.Windows.Forms.Label();
            this.lblCreator = new System.Windows.Forms.Label();
            this.rtbStory = new System.Windows.Forms.RichTextBox();
            this.picLogo = new System.Windows.Forms.PictureBox();
            this.lblSourceCode = new System.Windows.Forms.Label();
            this.linkGitLab = new System.Windows.Forms.LinkLabel();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // btnOkay
            // 
            this.btnOkay.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOkay.Location = new System.Drawing.Point(439, 286);
            this.btnOkay.Name = "btnOkay";
            this.btnOkay.Size = new System.Drawing.Size(167, 30);
            this.btnOkay.TabIndex = 23;
            this.btnOkay.Text = "Good guy this \"STM93\"";
            this.btnOkay.UseVisualStyleBackColor = true;
            this.btnOkay.Click += new System.EventHandler(this.BtnOkay_Click);
            // 
            // linkLibrary2
            // 
            this.linkLibrary2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.linkLibrary2.AutoSize = true;
            this.linkLibrary2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLibrary2.Location = new System.Drawing.Point(259, 220);
            this.linkLibrary2.Name = "linkLibrary2";
            this.linkLibrary2.Size = new System.Drawing.Size(230, 16);
            this.linkLibrary2.TabIndex = 22;
            this.linkLibrary2.TabStop = true;
            this.linkLibrary2.Text = "WeifenLuo.WinFormsUI.Docking";
            this.linkLibrary2.VisitedLinkColor = System.Drawing.Color.Blue;
            this.linkLibrary2.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LinkLibrary2_LinkClicked);
            // 
            // linkLibrary1
            // 
            this.linkLibrary1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.linkLibrary1.AutoSize = true;
            this.linkLibrary1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLibrary1.Location = new System.Drawing.Point(125, 220);
            this.linkLibrary1.Name = "linkLibrary1";
            this.linkLibrary1.Size = new System.Drawing.Size(94, 16);
            this.linkLibrary1.TabIndex = 21;
            this.linkLibrary1.TabStop = true;
            this.linkLibrary1.Text = "ScintillaNET";
            this.linkLibrary1.VisitedLinkColor = System.Drawing.Color.Blue;
            this.linkLibrary1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LinkLibrary1_LinkClicked);
            // 
            // linkLuiStudio
            // 
            this.linkLuiStudio.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.linkLuiStudio.AutoSize = true;
            this.linkLuiStudio.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(124)))), ((int)(((byte)(89)))));
            this.linkLuiStudio.Location = new System.Drawing.Point(217, 170);
            this.linkLuiStudio.Name = "linkLuiStudio";
            this.linkLuiStudio.Size = new System.Drawing.Size(134, 13);
            this.linkLuiStudio.TabIndex = 20;
            this.linkLuiStudio.TabStop = true;
            this.linkLuiStudio.Text = "https://www.lui-studio.net/";
            this.linkLuiStudio.VisitedLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(124)))), ((int)(((byte)(89)))));
            this.linkLuiStudio.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LinkLuiStudio_LinkClicked);
            // 
            // lblVersionValue2
            // 
            this.lblVersionValue2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblVersionValue2.AutoSize = true;
            this.lblVersionValue2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVersionValue2.Location = new System.Drawing.Point(125, 262);
            this.lblVersionValue2.Name = "lblVersionValue2";
            this.lblVersionValue2.Size = new System.Drawing.Size(112, 13);
            this.lblVersionValue2.TabIndex = 18;
            this.lblVersionValue2.Text = "1.0.0.0 (Build Number)";
            // 
            // lblVersionValue
            // 
            this.lblVersionValue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblVersionValue.AutoSize = true;
            this.lblVersionValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVersionValue.Location = new System.Drawing.Point(125, 246);
            this.lblVersionValue.Name = "lblVersionValue";
            this.lblVersionValue.Size = new System.Drawing.Size(114, 16);
            this.lblVersionValue.TabIndex = 19;
            this.lblVersionValue.Text = "1.0.0.0 (STM93)";
            // 
            // lblCreatorValue
            // 
            this.lblCreatorValue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblCreatorValue.AutoSize = true;
            this.lblCreatorValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCreatorValue.Location = new System.Drawing.Point(125, 168);
            this.lblCreatorValue.Name = "lblCreatorValue";
            this.lblCreatorValue.Size = new System.Drawing.Size(65, 16);
            this.lblCreatorValue.TabIndex = 17;
            this.lblCreatorValue.Text = "Luigi600";
            // 
            // lblVersion
            // 
            this.lblVersion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblVersion.AutoSize = true;
            this.lblVersion.Location = new System.Drawing.Point(40, 248);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(45, 13);
            this.lblVersion.TabIndex = 16;
            this.lblVersion.Text = "Version:";
            // 
            // lblLibraries
            // 
            this.lblLibraries.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblLibraries.AutoSize = true;
            this.lblLibraries.Location = new System.Drawing.Point(40, 222);
            this.lblLibraries.Name = "lblLibraries";
            this.lblLibraries.Size = new System.Drawing.Size(49, 13);
            this.lblLibraries.TabIndex = 15;
            this.lblLibraries.Text = "Libraries:";
            // 
            // lblCreator
            // 
            this.lblCreator.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblCreator.AutoSize = true;
            this.lblCreator.Location = new System.Drawing.Point(40, 170);
            this.lblCreator.Name = "lblCreator";
            this.lblCreator.Size = new System.Drawing.Size(44, 13);
            this.lblCreator.TabIndex = 14;
            this.lblCreator.Text = "Creator:";
            // 
            // rtbStory
            // 
            this.rtbStory.BackColor = System.Drawing.SystemColors.Control;
            this.rtbStory.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtbStory.Location = new System.Drawing.Point(40, 72);
            this.rtbStory.Name = "rtbStory";
            this.rtbStory.ReadOnly = true;
            this.rtbStory.Size = new System.Drawing.Size(566, 76);
            this.rtbStory.TabIndex = 13;
            this.rtbStory.Text = resources.GetString("rtbStory.Text");
            // 
            // picLogo
            // 
            this.picLogo.Image = global::Easier_Data_Editor.Properties.Resources.logo_v2;
            this.picLogo.Location = new System.Drawing.Point(40, 35);
            this.picLogo.Name = "picLogo";
            this.picLogo.Size = new System.Drawing.Size(210, 30);
            this.picLogo.TabIndex = 24;
            this.picLogo.TabStop = false;
            // 
            // lblSourceCode
            // 
            this.lblSourceCode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblSourceCode.AutoSize = true;
            this.lblSourceCode.Location = new System.Drawing.Point(40, 196);
            this.lblSourceCode.Name = "lblSourceCode";
            this.lblSourceCode.Size = new System.Drawing.Size(72, 13);
            this.lblSourceCode.TabIndex = 15;
            this.lblSourceCode.Text = "Source Code:";
            // 
            // linkGitLab
            // 
            this.linkGitLab.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.linkGitLab.AutoSize = true;
            this.linkGitLab.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkGitLab.Location = new System.Drawing.Point(125, 194);
            this.linkGitLab.Name = "linkGitLab";
            this.linkGitLab.Size = new System.Drawing.Size(386, 16);
            this.linkGitLab.TabIndex = 21;
            this.linkGitLab.TabStop = true;
            this.linkGitLab.Text = "GitLab ( https://gitlab.com/Luigi600/easier-data-editor )";
            this.linkGitLab.VisitedLinkColor = System.Drawing.Color.Blue;
            this.linkGitLab.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LinkGitLab_LinkClicked);
            // 
            // About
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(641, 341);
            this.Controls.Add(this.picLogo);
            this.Controls.Add(this.btnOkay);
            this.Controls.Add(this.linkLibrary2);
            this.Controls.Add(this.linkGitLab);
            this.Controls.Add(this.linkLibrary1);
            this.Controls.Add(this.linkLuiStudio);
            this.Controls.Add(this.lblVersionValue);
            this.Controls.Add(this.lblCreatorValue);
            this.Controls.Add(this.lblSourceCode);
            this.Controls.Add(this.lblVersion);
            this.Controls.Add(this.lblLibraries);
            this.Controls.Add(this.lblCreator);
            this.Controls.Add(this.rtbStory);
            this.Controls.Add(this.lblVersionValue2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "About";
            this.Padding = new System.Windows.Forms.Padding(32, 32, 32, 22);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "About";
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Button btnOkay;
        internal System.Windows.Forms.LinkLabel linkLibrary2;
        internal System.Windows.Forms.LinkLabel linkLibrary1;
        internal System.Windows.Forms.LinkLabel linkLuiStudio;
        internal System.Windows.Forms.Label lblVersionValue2;
        internal System.Windows.Forms.Label lblVersionValue;
        internal System.Windows.Forms.Label lblCreatorValue;
        internal System.Windows.Forms.Label lblVersion;
        internal System.Windows.Forms.Label lblLibraries;
        internal System.Windows.Forms.Label lblCreator;
        internal System.Windows.Forms.RichTextBox rtbStory;
        private System.Windows.Forms.PictureBox picLogo;
        internal System.Windows.Forms.Label lblSourceCode;
        internal System.Windows.Forms.LinkLabel linkGitLab;
    }
}