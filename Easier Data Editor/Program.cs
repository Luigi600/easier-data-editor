﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using System.Xml;
using Easier_Data_Editor.Collections;
using Easier_Data_Editor.Extensions.MultiLangSupport;
using Easier_Data_Editor.Serializers;
using Easier.MultiLangSupport;
using Easier.MultiLangSupport.Environments;

namespace Easier_Data_Editor
{
    static class Program
    {
        private static readonly Mutex MUTEX = new Mutex(true, "Easier_Data_Editor");
        private static LimitedListWithEvents<string>? _initRecentFiles = null;

        [STAThread]
        static void Main()
        {
            CreateApplicationDirectoryAndMigrate();
            CreateLanguageDirectory();
            LoadLanguages();
            LoadUserSettings();

            if (Environments.Application.UserSettings.GeneralSettings.MultipleInstances)
            {
                InitInstance();
            }
            else if (MUTEX.WaitOne(TimeSpan.Zero, true))
            {
                InitInstance();
                MUTEX.ReleaseMutex();
            }
            else
            {
                HandleMultipleInstance();
            }
        }

        private static void Application_ApplicationExit(object? sender, EventArgs e)
        {
            // null, if the application was not really started
            if (
                _initRecentFiles != null
                && !_initRecentFiles.SequenceEqual(
                    Environments.Application.UserSettings.GeneralSettings.RecentFiles
                )
            )
                File.WriteAllLines(
                    Environments.Application.RECENT_FILES_FILE_PATH,
                    Environments.Application.UserSettings.GeneralSettings.RecentFiles.ToArray()
                );

            if (
                Environments.Application.DockPanelLayout.CanRead
                && Environments.Application.DockPanelLayout.Length > 0
            )
            {
                using (
                    FileStream file = new FileStream(
                        Environments.Application.LAYOUT_FILE_PATH,
                        FileMode.Create,
                        FileAccess.Write
                    )
                )
                    Environments.Application.DockPanelLayout.WriteTo(file);

                Environments.Application.DockPanelLayout.Close();
            }
        }

        private static Process? GetInstance(string name, int currentPID) =>
            Array.Find(
                Process.GetProcesses(),
                (
                    p =>
                    {
                        try
                        {
                            return p.MainWindowHandle != IntPtr.Zero
                                && p.Id != currentPID
                                && (p.MainModule?.FileName.Equals(name) ?? false);
                        }
                        catch (Exception exception)
                        {
                            Debug.WriteLine(
                                $"Error while searching the same instance ({exception.Message})"
                            );
                        }

                        return false;
                    }
                )
            );

        private static void InitInstance()
        {
            LoadFormatBlueprints();
            ReadRecentFiles();
            CloneRecentFiles();
            LoadSyntax();
            Environments.Application.SetLanguageDirectoryWatcher();

            Application.ThreadException += new ThreadExceptionEventHandler(
                ExceptionHandler.UIThreadException
            );
            Application.ApplicationExit += Application_ApplicationExit;
            Application.EnableVisualStyles();
            Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Main(true));
            AppDomain.CurrentDomain.UnhandledException += ExceptionHandler.UnhandledException;
        }

        private static void HandleMultipleInstance()
        {
            Process currentProcess = Process.GetCurrentProcess();
            Process? instance = currentProcess.MainModule is null
                ? null
                : GetInstance(currentProcess.MainModule.FileName, currentProcess.Id);
            if (instance == null)
            {
                InitInstance();
                return;
            }

            foreach (string arg in Environment.GetCommandLineArgs())
            {
                string ext = Path.GetExtension(arg).ToLower().Substring(1);
                if (Environments.Application.SUPPORTED_FILE_EXTENSIONS.Contains<string>(ext))
                    Natives.Communications.Instance.SendMessageToProcess(
                        instance.MainWindowHandle,
                        arg
                    );
            }
        }

        private static void CreateApplicationDirectoryAndMigrate()
        {
            if (Directory.Exists(Environments.Application.APP_FOLDER))
                return;

            Directory.CreateDirectory(Environments.Application.APP_FOLDER);
            // TODO: start migration
        }

        private static void CreateLanguageDirectory()
        {
            if (!Directory.Exists(Environments.Application.LANGUAGE_DIRECTORY_PATH))
            {
                Directory.CreateDirectory(Environments.Application.LANGUAGE_DIRECTORY_PATH);
                // TODO: copy zh-tw to languages
            }
        }

        private static void LoadUserSettings()
        {
            Translation.DefaultLanguage.SetAuthor("Luigi600");
            Translation.DefaultLanguage.SetName(Environments.Application.DEFAULT_LANGUAGE);
            Environments.Application.UserSettings.GeneralSettings.SetLanguage(
                Translation.DefaultLanguage
            );

            if (!File.Exists(Environments.Application.SETTINGS_FILE_PATH))
                return;

            try
            {
                FileStream inputSettings = new FileStream(
                    Environments.Application.SETTINGS_FILE_PATH,
                    FileMode.Open,
                    FileAccess.Read,
                    FileShare.Read
                );
                XmlDocument doc = new XmlDocument();
                doc.Load(inputSettings);
                inputSettings.Close();

                if (
                    doc.DocumentElement?.Name.Equals(Environments.Application.SETTINGS_XML_NODE)
                    != true
                )
                {
                    ShowInvalidSettingsDialog();
                    return;
                }

                if (
                    !doc.DocumentElement.HasAttribute(
                        Environments.Application.SETTINGS_XML_VERSION_ATTRIBUTE
                    )
                    || !doc
                        .DocumentElement.GetAttribute(
                            Environments.Application.SETTINGS_XML_VERSION_ATTRIBUTE
                        )
                        .Equals(Environments.Application.SETTINGS_XML_VERSION.ToString())
                )
                {
                    // TODO: check if version lower
                    // TODO: migrate
                    MessageBox.Show(
                        "The settings file appears to be so outdated that migration is not possible. The default settings are used.",
                        $"{Environments.Application.APP_TITLE}  -   Error",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Warning
                    );
                }

                XmlDeserializer.LoadFromXML(
                    Environments.Application.UserSettings,
                    doc.DocumentElement
                );
                if (
                    Environments.Application.GetLanguageFromFileName(
                        Environments.Application.UserSettings.GeneralSettings.Language
                    )
                    is Language userLang
                )
                    Translation.SelectedLanguage = userLang;
            }
            catch (Exception e)
            {
                ShowInvalidSettingsDialog();
                Console.WriteLine(e);
                throw;
            }
        }

        private static void LoadFormatBlueprints()
        {
            if (!File.Exists(Environments.Application.FORMAT_BLUEPRINTS_FILE_PATH))
                return;

            try
            {
                FileStream blueprints = new FileStream(
                    Environments.Application.FORMAT_BLUEPRINTS_FILE_PATH,
                    FileMode.Open,
                    FileAccess.Read,
                    FileShare.Read
                );
                XmlDocument doc = new XmlDocument();
                doc.Load(blueprints);
                blueprints.Close();

                if (
                    doc.DocumentElement?.Name.Equals(
                        Environments.Application.FORMAT_BLUEPRINTS_XML_NODE
                    ) != true
                )
                {
                    ShowInvalidFormatBlueprintsDialog();
                    return;
                }

                XmlDeserializer.LoadFromXML(
                    Environments.Application.FormatBlueprints,
                    doc.DocumentElement
                );
            }
            catch (Exception e)
            {
                ShowInvalidFormatBlueprintsDialog();
                Console.WriteLine(e);
                throw;
            }
        }

        private static void ShowInvalidSettingsDialog()
        {
            MessageBox.Show(
                "The settings file seems to be corrupted. File could not be loaded and default settings are used.",
                $"{Environments.Application.APP_TITLE}  -   Error",
                MessageBoxButtons.OK,
                MessageBoxIcon.Warning
            );
        }

        private static void ShowInvalidFormatBlueprintsDialog()
        {
            MessageBox.Show(
                "The format blueprints file seems to be corrupted. File could not be loaded and default blueprints are used.",
                $"{Environments.Application.APP_TITLE}  -   Error",
                MessageBoxButtons.OK,
                MessageBoxIcon.Warning
            );
        }

        private static void LoadLanguages()
        {
            foreach (
                string file in Directory
                    .GetFiles(Environments.Application.LANGUAGE_DIRECTORY_PATH)
                    .Where(f =>
                        Path.GetExtension(f)
                            .ToLower()
                            .Equals(Environments.Application.XML_EXTENSION)
                    )
            )
            {
                try
                {
                    Language lang = TranslationDeserializerWithPath.GetInstance(file);
                    Translation.AddLanguage(lang);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }
        }

        private static void ReadRecentFiles()
        {
            if (!File.Exists(Environments.Application.RECENT_FILES_FILE_PATH))
                return;

            foreach (
                string path in File.ReadAllLines(Environments.Application.RECENT_FILES_FILE_PATH)
                    .Where(l => !string.IsNullOrEmpty(l))
                    .Reverse()
            )
            {
                try
                {
                    if (File.Exists(path))
                        Environments.Application.UserSettings.GeneralSettings.RecentFiles.Add(path);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }
            }
        }

        private static void CloneRecentFiles()
        {
            _initRecentFiles = new LimitedListWithEvents<string>();
            _initRecentFiles.AddRange(
                Environments.Application.UserSettings.GeneralSettings.RecentFiles
            );
        }

        private static void LoadSyntax()
        {
            if (!File.Exists(Environments.Application.FILE_PATH_SYNTAX))
                return;

            Environments.Application.LoadCustomSyntax(
                File.ReadAllText(Environments.Application.FILE_PATH_SYNTAX)
            );
        }
    }
}
