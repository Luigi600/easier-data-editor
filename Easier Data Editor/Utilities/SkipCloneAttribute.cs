﻿using System;

namespace Easier_Data_Editor.Utilities
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class SkipCloneAttribute : Attribute { }
}
