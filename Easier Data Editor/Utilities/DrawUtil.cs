﻿using System.Drawing;
using System.Drawing.Imaging;

namespace Easier_Data_Editor.Utilities
{
    public static class DrawUtil
    {
        public static Bitmap ChangeOpacity(Image img, float opacity)
        {
            Bitmap bitmap = new Bitmap(img.Width, img.Height);
            Graphics graphics = Graphics.FromImage(bitmap);
            ColorMatrix colorMatrix = new ColorMatrix() { Matrix33 = opacity };
            ImageAttributes imgAttributes = new ImageAttributes();
            imgAttributes.SetColorMatrix(
                colorMatrix,
                ColorMatrixFlag.Default,
                ColorAdjustType.Bitmap
            );
            graphics.DrawImage(
                img,
                new Rectangle(0, 0, bitmap.Width, bitmap.Height),
                0,
                0,
                img.Width,
                img.Height,
                GraphicsUnit.Pixel,
                imgAttributes
            );
            graphics.Dispose();
            return bitmap;
        }

        public static Bitmap ChangeBrightness(Image img, float scale)
        {
            Bitmap bitmap = new Bitmap(img.Width, img.Height);
            Graphics graphics = Graphics.FromImage(bitmap);
            ColorMatrix colorMatrix = new ColorMatrix();
            colorMatrix[0, 0] = scale;
            colorMatrix[1, 1] = scale;
            colorMatrix[2, 2] = scale;
            ImageAttributes imgAttributes = new ImageAttributes();
            imgAttributes.SetColorMatrix(
                colorMatrix,
                ColorMatrixFlag.Default,
                ColorAdjustType.Bitmap
            );
            graphics.DrawImage(
                img,
                new Rectangle(0, 0, bitmap.Width, bitmap.Height),
                0,
                0,
                img.Width,
                img.Height,
                GraphicsUnit.Pixel,
                imgAttributes
            );
            graphics.Dispose();
            return bitmap;
        }

        public static Bitmap InvertImage(Image img)
        {
            Bitmap bitmap = new Bitmap(img);
            for (int y = 0; y < bitmap.Height; ++y)
            {
                for (int x = 0; x < bitmap.Width; ++x)
                    bitmap.SetPixel(x, y, InvertColor(bitmap.GetPixel(x, y)));
            }
            return bitmap;
        }

        public static Color InvertColor(Color color) =>
            Color.FromArgb(color.A, 255 - color.R, 255 - color.G, 255 - color.B);
    }
}
