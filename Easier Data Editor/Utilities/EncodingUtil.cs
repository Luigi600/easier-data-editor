﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.IO;
using System.Linq;
using System.Text;
using Easier_Data_Editor.Models;

namespace Easier_Data_Editor.Utilities
{
    public static class EncodingUtil
    {
        private const int BYTE_ORDER_MARK_LENGTH_UTF7 = 4;

        // see https://en.wikipedia.org/wiki/Byte_order_mark#Byte_order_marks_by_encoding

        private static byte[] BYTE_ORDER_MARK_UTF1 = new byte[] { 0xF7, 0x64, 0x4C };
        private static byte[] BYTE_ORDER_MARK_UTF7 = new byte[] { 0x2B, 0x2F, 0x76 };
        private static byte[] BYTE_ORDER_MARK_UTF7_ADDITION = new byte[] { 0x38, 0x39, 0x2B, 0x2F };
        private static byte[] BYTE_ORDER_MARK_UTF8 = new byte[] { 0xEF, 0xBB, 0xBF };
        private static byte[] BYTE_ORDER_MARK_UTF16_BE = new byte[] { 0xFE, 0xFF };
        private static byte[] BYTE_ORDER_MARK_UTF16_LE = new byte[] { 0xFF, 0xFE };
        private static byte[] BYTE_ORDER_MARK_UTF32_BE = new byte[] { 0x00, 0x00, 0xFE, 0xFF };
        private static byte[] BYTE_ORDER_MARK_UTF32_LE = new byte[] { 0xFF, 0xFE, 0x00, 0x00 };

        public static readonly ImmutableDictionary<ETextEncoding, string> ENCODING = new Dictionary<
            ETextEncoding,
            string
        >()
        {
            { ETextEncoding.ASCII, "ASCII" },
            { ETextEncoding.Ansi, "Ansi" },
            { ETextEncoding.UTF1, "UTF-1" },
            { ETextEncoding.UTF7, "UTF-7" },
            { ETextEncoding.UTF8, "UTF-8" },
            { ETextEncoding.UTF16BE, "UTF-16 BE" },
            { ETextEncoding.UTF16LE, "UTF-16 LE" },
            { ETextEncoding.UTF32BE, "UTF-32 BE" },
            { ETextEncoding.UTF32LE, "UTF-32 LE" },
        }.ToImmutableDictionary();

        public static TextEncoding GetEncoding(StreamReader sr)
        {
            Encoding encoding = sr.CurrentEncoding;
            if (encoding is UTF8Encoding)
            {
                return new TextEncoding(
                    ETextEncoding.UTF8,
                    encoding.GetPreamble().SequenceEqual(BYTE_ORDER_MARK_UTF8)
                );
            }
            else if (encoding is UnicodeEncoding)
            {
                if (encoding.GetPreamble().SequenceEqual(BYTE_ORDER_MARK_UTF16_BE))
                    return new TextEncoding(ETextEncoding.UTF16BE, true);
                else if (encoding.GetPreamble().SequenceEqual(BYTE_ORDER_MARK_UTF16_LE))
                    return new TextEncoding(ETextEncoding.UTF16LE, true);
            }
            else if (encoding is UTF32Encoding)
            {
                if (encoding.GetPreamble().SequenceEqual(BYTE_ORDER_MARK_UTF32_BE))
                    return new TextEncoding(ETextEncoding.UTF32BE, true);
                else if (encoding.GetPreamble().SequenceEqual(BYTE_ORDER_MARK_UTF32_LE))
                    return new TextEncoding(ETextEncoding.UTF32LE, true);
            }
            else if (encoding is UTF7Encoding)
            {
                byte[] bom = encoding.GetPreamble();
                byte lastByte = 0;
                if (bom.Length == BYTE_ORDER_MARK_LENGTH_UTF7)
                {
                    lastByte = bom[BYTE_ORDER_MARK_LENGTH_UTF7 - 1];
                    Array.Resize(ref bom, BYTE_ORDER_MARK_LENGTH_UTF7 - 1);
                }

                return new TextEncoding(
                    ETextEncoding.UTF7,
                    bom.SequenceEqual(BYTE_ORDER_MARK_UTF7)
                        && BYTE_ORDER_MARK_UTF7_ADDITION.Contains(lastByte)
                );
            }
            else if (encoding is ASCIIEncoding)
            {
                return new TextEncoding(ETextEncoding.ASCII, false);
            }

            return new TextEncoding(ETextEncoding.Unknown, false);
        }

        public static Encoding GetSystemEncodingFromTextEncoding(TextEncoding encoding)
        {
            if (encoding.Encoding == ETextEncoding.UTF7)
                return new UTF7Encoding();
            else if (encoding.Encoding == ETextEncoding.UTF8)
                return new UTF8Encoding(encoding.HasBOM);
            else if (encoding.Encoding == ETextEncoding.UTF16BE)
                return new UnicodeEncoding(true, encoding.HasBOM);
            else if (encoding.Encoding == ETextEncoding.UTF16LE)
                return new UnicodeEncoding(false, encoding.HasBOM);
            else if (encoding.Encoding == ETextEncoding.UTF32BE)
                return new UTF32Encoding(true, encoding.HasBOM);
            else if (encoding.Encoding == ETextEncoding.UTF32LE)
                return new UTF32Encoding(false, encoding.HasBOM);

            return new UTF8Encoding(false);
        }
    }
}
