﻿namespace Easier_Data_Editor.Utilities
{
    public static class CalUtil
    {
        public static bool IsInRange(int startX1, int endX1, int startX2, int endX2)
        {
            return (
                //   |____________|    OR       |____________|
                //        |___|        OR              |_________|
                startX1 >= startX2
                    && startX1 <= endX2
                ||
                //   |____________|    OR       |____________|
                // |_______|           OR     |________________|
                startX1 < startX2
                    && endX1 >= startX2
            );
        }
    }
}
