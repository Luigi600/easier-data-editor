﻿using System;
using System.Drawing;
using System.Reflection;
using static Easier_Data_Editor.Environments.Application;

namespace Easier_Data_Editor.Utilities
{
    static class SerializationUtil
    {
        public static string GetStringValue(object? obj)
        {
            if (obj is Size size)
                return $"{size.Width}x{size.Height}";
            else if (obj is Point poi)
                return $"{poi.X}x{poi.Y}";
            else if (obj is SizeF sizeF)
                return $"{sizeF.Width.ToString("N2", CultureEN)}x{sizeF.Height.ToString("N2", CultureEN)}";
            else if (obj is PointF poiF)
                return $"{poiF.X.ToString("N2", CultureEN)}x{poiF.Y.ToString("N2", CultureEN)}";
            else if (obj is bool)
                return Convert.ToInt16(obj).ToString();
            else if (obj is Enum)
                return Convert.ToInt16(obj).ToString();
            else if (obj is Color col)
                return ColorTranslator.ToHtml(col);
            else if (obj is float)
                return Convert.ToSingle(obj).ToString("N2", CultureEN);

            return obj?.ToString() ?? "";
        }

        public static string GetXMLName(PropertyInfo property)
        {
            if (
                property.GetCustomAttribute(typeof(Attributes.XmlNodeAttribute))
                is Attributes.XmlNodeAttribute attribute
            )
                return attribute.XMLNodeName;

            return property.Name;
        }
    }
}
