﻿using System;
using System.Windows.Forms;

namespace Easier_Data_Editor.Utilities
{
    public static class ThreadUtil
    {
        public static void RunInMainThread(Control control, Action func)
        {
            if (control.InvokeRequired)
                control.Invoke(new Action(() => func()));
            else
                func();
        }

        public static void RunInMainThread<TP1>(Control control, Action<TP1> func, TP1 param1)
        {
            if (control.InvokeRequired)
                control.Invoke(new Action(() => func(param1)));
            else
                func(param1);
        }

        public static TR RunInMainThread<TR>(Control control, Func<TR> func)
        {
            if (control.InvokeRequired)
                return (TR)control.Invoke(new Func<TR>(() => func()));
            else
                return func();
        }

        public static TR RunInMainThread<TP1, TR>(Control control, Func<TP1, TR> func, TP1 param1)
        {
            if (control.InvokeRequired)
                return (TR)control.Invoke(new Func<TR>(() => func(param1)));
            else
                return func(param1);
        }

        public static TR RunInMainThread<TP1, TP2, TR>(
            Control control,
            Func<TP1, TP2, TR> func,
            TP1 param1,
            TP2 param2
        )
        {
            if (control.InvokeRequired)
                return (TR)control.Invoke(new Func<TR>(() => func(param1, param2)));
            else
                return func(param1, param2);
        }
    }
}
