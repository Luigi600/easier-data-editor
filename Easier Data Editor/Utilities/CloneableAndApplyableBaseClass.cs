﻿using System;
using System.Linq;
using System.Reflection;
using Easier_Data_Editor.Models;

namespace Easier_Data_Editor.Utilities
{
    public abstract class CloneableAndApplyableBaseClass : ICloneable, IDataEqualation
    {
        protected abstract void OnMemberCloned(CloneableAndApplyableBaseClass clone);

        public object Clone()
        {
            CloneableAndApplyableBaseClass deepClone = (CloneableAndApplyableBaseClass)
                this.MemberwiseClone();

            foreach (
                PropertyInfo propInfo in deepClone
                    .GetType()
                    .GetProperties()
                    .Where(p => typeof(ICloneable).IsAssignableFrom(p.PropertyType))
            )
            {
                if (!(propInfo.GetValue(this) is ICloneable cloneObj))
                    continue;

                propInfo.SetValue(deepClone, cloneObj.Clone());
            }

            this.OnMemberCloned(deepClone);
            return deepClone;
        }

        public void Apply(CloneableAndApplyableBaseClass settings)
        {
            foreach (PropertyInfo propInfo in settings.GetType().GetProperties())
            {
                if (typeof(CloneableBindingBaseClass).IsAssignableFrom(propInfo.PropertyType))
                {
                    CloneableBindingBaseClass? obj = (CloneableBindingBaseClass?)
                        propInfo.GetValue(this);
                    CloneableBindingBaseClass? obj2 = (CloneableBindingBaseClass?)
                        propInfo.GetValue(settings);

                    if (obj is not null && obj2 is not null)
                        obj.Apply(obj2);
                }
                else
                {
                    propInfo.SetValue(this, propInfo.GetValue(settings));
                }
            }
        }

        public bool DataAreEquals(object obj) => ClassUtil.IsEquals(this, obj);
    }
}
