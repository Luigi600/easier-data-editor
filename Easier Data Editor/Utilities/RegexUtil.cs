﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Easier_Data_Editor.Models;

namespace Easier_Data_Editor.Utilities
{
    public static class RegexUtil
    {
        public static List<FakeMatch> GetLinesWithEmptyLines(string input)
        {
            List<FakeMatch> result = new List<FakeMatch>();

            int index = 0;
            foreach (string line in Constants.REGEX_NEW_LINE_SPLITTER.Split(input))
            {
                index = input.IndexOf(line);

                if (index < 0)
                    break;

                result.Add(new FakeMatch(index, line));
            }

            return result;
        }

        public static string GetRegexValue(
            string text,
            string pattern,
            RegexOptions regOptions = RegexOptions.IgnoreCase,
            object? optionalerReturn = null
        )
        {
            if (text != null)
            {
                Match match = Regex.Match(text, pattern, regOptions);
                if (match.Success)
                    return match.Value;
            }

            return Convert.ToString(optionalerReturn) ?? "";
        }

        public static int GetRegexValueInteger(
            string text,
            string pattern,
            RegexOptions regOptions = RegexOptions.IgnoreCase,
            int optionalerReturn = -1
        )
        {
            string result = GetRegexValue(text, pattern, regOptions);
            if (result != null)
            {
                result = result.Trim();
                if (NumberShitUtil.IsNumeric(result))
                    return Convert.ToInt32(result);
            }

            return optionalerReturn;
        }

        public static Regex GetSingleWordIntegerNegativeRegexFromWordAndValuePattern(
            string word,
            RegexOptions options = RegexOptions.IgnoreCase
        ) => GetSingleWordRegexFromWordAndValuePattern(word, @"-?\d+", options);

        public static Regex GetSingleWordIntegerRegexFromWordAndValuePattern(
            string word,
            RegexOptions options = RegexOptions.IgnoreCase
        ) => GetSingleWordRegexFromWordAndValuePattern(word, @"\d+", options);

        public static Regex GetSingleWordRegexFromWordAndValuePattern(
            string word,
            string valuePattern,
            RegexOptions options = RegexOptions.IgnoreCase
        ) => new Regex($"(?<={GetSingleWordPatternFromWord(word)}:\\s*){valuePattern}", options);

        public static string GetSingleWordPatternFromWord(string word) => $"(\\s+{word}|^{word})";
    }
}
