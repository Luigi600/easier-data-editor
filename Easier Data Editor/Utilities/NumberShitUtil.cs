﻿using System;
using System.ComponentModel;
using System.Globalization;

namespace Easier_Data_Editor.Utilities
{
#pragma warning disable S3776 // Cognitive Complexity of methods should not be too high
    internal static class NumberShitUtil
    {
        public static object? ParseStringToNumberOrBoolean<T>(string input) =>
            ParseStringToNumberOrBoolean(typeof(T), input);

        public static object? ParseStringToNumberOrBoolean(Type genericType, string input)
        {
            TypeConverter conv = TypeDescriptor.GetConverter(genericType);
            if (conv is EnumConverter)
                return conv.ConvertFromString(input);

            // GetConverter doesn't can handle culture...

            // so I MUST do that... :( (source: https://stackoverflow.com/a/2689929)

            bool isDouble = double.TryParse(
                input,
                NumberStyles.Float,
                Environments.Application.CultureEN,
                out double parsedD
            );
            if (isDouble)
                return ParseShitToT(genericType, parsedD);

            bool isInt = int.TryParse(
                input,
                NumberStyles.Integer,
                Environments.Application.CultureEN,
                out int parsedI
            );
            if (isInt)
                return ParseShitToT(genericType, parsedI);

            return null;
        }

        public static int ConvertToInt(bool input) => input ? 1 : 0;

        public static int ConvertToInt(byte input) => input;

        public static int ConvertToInt(sbyte input) => input;

        public static int ConvertToInt(char input) => input;

        public static int ConvertToInt(decimal input) => Convert.ToInt32(input);

        public static int ConvertToInt(double input) => Convert.ToInt32(input);

        public static int ConvertToInt(float input) => Convert.ToInt32(input);

        public static int ConvertToInt(uint input) => (int)input;

        public static int ConvertToInt(IntPtr input) => input.ToInt32();

        public static int ConvertToInt(long input) => (int)input;

        public static int ConvertToInt(ulong input) => (int)input;

        public static int ConvertToInt(short input) => input;

        public static int ConvertToInt(ushort input) => input;

        public static int ConvertToInt(string input, bool isHex = false) =>
            Convert.ToInt32(input, isHex ? 0x10 : 10);

        public static bool IsNumeric(string str) => int.TryParse(str, out _);

        private static object? ParseShitToT(Type originalType, double parsedVal)
        {
            if (originalType == typeof(double))
                return parsedVal;
            else if (originalType == typeof(float))
                return (float)parsedVal;
            else if (originalType == typeof(uint))
                return (uint)parsedVal;
            else if (originalType == typeof(int))
                return (int)parsedVal;
            else if (originalType == typeof(ushort))
                return (ushort)parsedVal;
            else if (originalType == typeof(short))
                return (short)parsedVal;
            else if (originalType == typeof(byte))
                return (byte)parsedVal;
            else if (originalType == typeof(sbyte))
                return (sbyte)parsedVal;
            else if (originalType == typeof(decimal))
                return (decimal)parsedVal;
            else if (originalType == typeof(bool))
                return !(parsedVal >= (-0.9d) && parsedVal <= 0.9d);

            return null;
        }

        private static object? ParseShitToT(Type originalType, int parsedVal)
        {
            if (originalType == typeof(double))
                return (double)parsedVal;
            else if (originalType == typeof(float))
                return (float)parsedVal;
            else if (originalType == typeof(uint))
                return (uint)parsedVal;
            else if (originalType == typeof(int))
                return parsedVal;
            else if (originalType == typeof(ushort))
                return (ushort)parsedVal;
            else if (originalType == typeof(short))
                return (short)parsedVal;
            else if (originalType == typeof(byte))
                return (byte)parsedVal;
            else if (originalType == typeof(sbyte))
                return (sbyte)parsedVal;
            else if (originalType == typeof(decimal))
                return (decimal)parsedVal;
            else if (originalType == typeof(bool))
                return parsedVal != 0;

            return null;
        }
    }
#pragma warning restore S3776 // Cognitive Complexity of methods should not be too high
}
