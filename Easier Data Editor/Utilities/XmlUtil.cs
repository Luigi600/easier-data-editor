﻿using System.Drawing;
using System.Xml;

namespace Easier_Data_Editor.Utilities
{
    public static class XmlUtil
    {
        public static string? GetAttributeValue(
            XmlAttributeCollection xmlAttributes,
            string attrName
        )
        {
            if (xmlAttributes != null)
            {
                XmlAttribute? attr = xmlAttributes[attrName];
                if (attr != null)
                    return attr.Value;
            }

            return null;
        }

        public static void GetAttributeValueAndSet(
            XmlAttributeCollection xmlAttributes,
            string attrName,
            ref string? val
        )
        {
            if (xmlAttributes != null)
            {
                XmlAttribute? attr = xmlAttributes[attrName];
                if (attr != null)
                    val = attr.Value;
            }
        }

        public static void GetAttributeValueAndSet(
            XmlAttributeCollection xmlAttributes,
            string attrName,
            ref bool val
        )
        {
            if (xmlAttributes != null)
            {
                XmlAttribute? attr = xmlAttributes[attrName];
                if (attr != null && DeserializationUtil.ParseBoolean(attr.Value) is bool boolVal)
                    val = boolVal;
            }
        }

        public static void GetAttributeValueAndSet(
            XmlAttributeCollection xmlAttributes,
            string attrName,
            ref Color? val
        )
        {
            string? xmlStr = null;
            GetAttributeValueAndSet(xmlAttributes, attrName, ref xmlStr);
            if (xmlStr != null && DeserializationUtil.GetColor(xmlStr) is Color col)
                val = col;
        }
    }
}
