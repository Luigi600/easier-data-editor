﻿using System;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using static Easier_Data_Editor.Environments.Application;

namespace Easier_Data_Editor.Utilities
{
    static class DeserializationUtil
    {
        private const string BOOLEAN_STRING_TRUE = "true";
        private const string BOOLEAN_STRING_FALSE = "false";

        public static object ParseStringValue(Type obj, string val)
        {
            if (obj.Equals(typeof(Size)) || obj.Equals(typeof(Point)))
            {
                object? result = ParseIntegerPointOrSize(val, obj.Equals(typeof(Size)));
                if (result != null)
                    return result;
            }
            else if (obj.Equals(typeof(SizeF)) || obj.Equals(typeof(PointF)))
            {
                object? result = ParseFloatPointOrSize(val, obj.Equals(typeof(SizeF)));
                if (result != null)
                    return result;
            }
            else if (obj.Equals(typeof(bool)) && ParseBoolean(val) is bool boolVal)
                return boolVal;
            else if (obj.Equals(typeof(Color)))
                return ColorTranslator.FromHtml(val);
            else if (obj.Equals(typeof(int)) || obj.Equals(typeof(FormWindowState)))
                return int.Parse(val);
            else if (obj.Equals(typeof(uint)))
                return uint.Parse(val);
            else if (obj.IsEnum)
                return byte.Parse(val);

            return val;
        }

        public static bool? ParseBoolean(string val)
        {
            string valLower = val.ToLower();
            if (valLower.Equals(BOOLEAN_STRING_TRUE))
                return true;
            else if (valLower.Equals(BOOLEAN_STRING_FALSE))
                return false;
            else if (Regex.IsMatch(val, "\\d+"))
                return int.Parse(val) > 0;

            return null;
        }

        public static Color? GetColor(string colStr)
        {
            try
            {
                if (colStr.StartsWith('#'))
                    return ColorTranslator.FromHtml(colStr);
                else
                    return Color.FromName(colStr);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            return null;
        }

        private static object? ParseIntegerPointOrSize(string val, bool isSize)
        {
            if (!Regex.IsMatch(val, @"-?\d+x-?\d+"))
                return null;

            string[] splitter = val.Split('x');
            if (splitter.Length == 2)
            {
                if (isSize)
                    return new Size(Convert.ToInt32(splitter[0]), Convert.ToInt32(splitter[1]));
                else
                    return new Point(Convert.ToInt32(splitter[0]), Convert.ToInt32(splitter[1]));
            }

            return null;
        }

        private static object? ParseFloatPointOrSize(string val, bool isSize)
        {
            if (!Regex.IsMatch(val, @"-?\d+(\.\d+)?x-?\d+(\.\d+)?"))
                return null;

            string[] splitter = val.Split('x');
            if (splitter.Length == 2)
            {
                if (isSize)
                    return new SizeF(
                        Convert.ToSingle(splitter[0], CultureEN),
                        Convert.ToSingle(splitter[1], CultureEN)
                    );
                else
                    return new PointF(
                        Convert.ToSingle(splitter[0], CultureEN),
                        Convert.ToSingle(splitter[1], CultureEN)
                    );
            }

            return null;
        }
    }
}
