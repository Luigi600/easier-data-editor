﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Easier_Data_Editor.Utilities
{
    public static class UiUtil
    {
        public static void RefreshListView<T>(
            ListView control,
            List<T> newItems,
            Func<T, ListViewItem> lvItemGenerator
        )
            where T : IComparable<T>
        {
            RemoveNotDeprecatedItemsFromList(control, newItems);
            AddNewItemsToList(control, newItems, lvItemGenerator);
        }

        private static void RemoveNotDeprecatedItemsFromList<T>(ListView control, List<T> newItems)
        {
            int index = 0;
            while (index < control.Items.Count)
            {
                T? item = (T?)control.Items[index].Tag;
                if (
                    EqualityComparer<T>.Default.Equals(item, default)
                    || !newItems.Any(i => item!.Equals(i))
                )
                {
                    control.Items.RemoveAt(index);
                    continue;
                }

                ++index;
            }
        }

        private static void AddNewItemsToList<T>(
            ListView control,
            List<T> newItems,
            Func<T, ListViewItem> lvItemGenerator
        )
            where T : IComparable<T>
        {
            List<T> lvItems = control
                .Items.Cast<ListViewItem>()
                .Where(i => i.Tag is not null)
                .Select(i => (T)i.Tag!)
                .ToList();
            List<T> items = newItems.Except(lvItems).ToList();
            for (int i = 0; i < items.Count; ++i)
            {
                T item = items[i];
                bool found = false;
                for (int j = 0; j < lvItems.Count - 1; ++j)
                {
                    T lvItem = lvItems[j];
                    T nextLvItem = lvItems[j + 1];

                    if (item.CompareTo(lvItem) > 0 && item.CompareTo(nextLvItem) < 0)
                    {
                        control.Items.Insert(j + 1, lvItemGenerator(item));
                        found = true;
                        break;
                    }
                }

                if (!found)
                    control.Items.Add(lvItemGenerator(item));
            }
        }
    }
}
