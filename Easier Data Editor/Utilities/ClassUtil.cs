﻿using System.Linq;
using System.Reflection;
using Easier_Data_Editor.Models;

namespace Easier_Data_Editor.Utilities
{
    public static class ClassUtil
    {
        public const int HASH_SEED = 1400;
        public const int HASH_FACTOR = 5;

        public static bool IsEquals(object obj1, object obj2)
        {
            if (!obj1.GetType().IsInstanceOfType(obj2))
                return false;

            foreach (PropertyInfo propInfo in obj1.GetType().GetProperties())
            {
                object? val1 = propInfo.GetValue(obj2);
                object? val2 = propInfo.GetValue(obj1);
                if (val1 is not null && val2 is not null)
                {
                    if ((val1 is IDataEqualation dataEq1) && (val2 is IDataEqualation))
                    {
                        if (!dataEq1.DataAreEquals(val2))
                            return false;
                    }
                    else if (!val1.Equals(val2))
                    {
                        return false;
                    }
                }
                else if (val1 != null) // val2 is null and val1 => NOT EQUALS
                {
                    return false;
                }
            }

            return true;
        }

        /**
         * @source https://stackoverflow.com/a/60960259
         */
        public static int Hash(params object[] values) =>
            CustomHash(HASH_SEED, HASH_FACTOR, values.Select(v => v.GetHashCode()).ToArray());

        // @source https://stackoverflow.com/a/60960259 https://stackoverflow.com/a/34006336/25338
        public static int CustomHash(int seed, int factor, params int[] vals)
        {
            int hash = seed;
            foreach (int i in vals)
                hash = (hash * factor) + i;

            return hash;
        }
    }
}
