﻿using System;
using System.ComponentModel;
using System.Reflection;
using System.Runtime.CompilerServices;
using Easier_Data_Editor.Models;

namespace Easier_Data_Editor.Utilities
{
    public abstract class CloneableBindingBaseClass
        : ICloneable,
            INotifyPropertyChanged,
            IDataEqualation
    {
        public event PropertyChangedEventHandler? PropertyChanged;

        public virtual object Clone()
        {
            CloneableBindingBaseClass clone = (CloneableBindingBaseClass)this.MemberwiseClone();
            clone.PropertyChanged = delegate { }; // doesn't work via Reflection API...
            return clone;
        }

        public void Apply(CloneableBindingBaseClass settings)
        {
            foreach (PropertyInfo propInfo in settings.GetType().GetProperties())
                propInfo.SetValue(this, propInfo.GetValue(settings));
        }

        public bool DataAreEquals(object obj) => ClassUtil.IsEquals(this, obj);

        protected void NotifyPropertyChanged([CallerMemberName] string propertyName = "") =>
            this.OnPropertyChanged(propertyName);

        protected void OnPropertyChanged(string propertyName) =>
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }
}
