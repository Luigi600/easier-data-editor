﻿using System.Drawing;
using System.Text.RegularExpressions;

namespace Easier_Data_Editor
{
    internal static class Constants
    {
        public const string BINDING_NUMERIC_UP_DOWN_VALUE = "Value";
        public const string BINDING_CHECK_BOX_VALUE = "Checked";
        public const string BINDING_TEXT_BOX_VALUE = "Text";
        public const string BINDING_COMBO_BOX_SELECTED_INDEX = "SelectedIndex";
        public const string BINDING_PICTURE_BOX_BACK_COLOR = "BackColor";

        public const string REGEX_PATTERN_BITMAP_HEADER_FILE = @"(?<=file\([^()]*\):\s*)[^\s\n\r]+";

        public const string REGEX_PATTERN_FRAME_CONTENT = @"<frame>.*?<frame_end>";
        public const string REGEX_PATTERN_FRAME_ID = @"(?<=\<frame\>\s*)\d+";
        public const string REGEX_PATTERN_FRAME_NAME = @"(?<=<frame>\s*\d+\s+)[^\s]+";

        public const char CHAR_NEW_LINE_CR = '\r';
        public const char CHAR_NEW_LINE_LF = '\n';
        public const char CHAR_SPACE = ' ';
        public static readonly Regex REGEX_IS_NEW_LINE = new Regex(
            @"[\n\r]",
            RegexOptions.IgnoreCase
        );
        public static readonly Regex REGEX_NEW_LINE_SPLITTER = new Regex(
            @"\r\n|\r|\n",
            RegexOptions.IgnoreCase
        );
        public static readonly Regex REGEX_IS_SPACE_TAB_CHAR = new Regex(
            @"[^\S\n\r]",
            RegexOptions.IgnoreCase
        );
        public static readonly Regex REGEX_PATH = new Regex(
            @"[^\s@]+\.[A-z\d\-_]{1,5}",
            RegexOptions.IgnoreCase
        );

        public static readonly Color COLOR_ERROR = Color.DarkRed;

        public const int DEFAULT_V_SCROLL_SCROLLBAR_BUTTON_WIDTH = 15;
        public const int DEFAULT_V_SCROLL_SCROLLBAR_BUTTON_HEIGHT = 17;
        public const int DEFAULT_V_SCROLL_SCROLLBAR_BUTTON_PADDING = 2;
    }
}
