﻿using System;
using Easier_Data_Editor.Models;

namespace Easier_Data_Editor.Forms
{
    public interface ISavableEditor
    {
        event EventHandler SaveStateChanged;
        event EventHandler UndoRedoStackChanged;

        string Path { get; }
        bool IsSave { get; }
        TextEncoding Encoding { get; }

        string GetContentToSave();
        void DocumentSaved();

        bool CanUndo();
        bool CanRedo();

        void Undo();
        void Redo();
    }
}
