﻿using System;
using Easier_Data_Editor.Forms.TextEditors;

namespace Easier_Data_Editor.Forms
{
    public interface IMainEditor
    {
        event EventHandler<TextEditor?> TextEditorChanged;
    }
}
