﻿using System;
using System.Collections.Generic;

namespace Easier_Data_Editor.Forms.ErrorDetections
{
    public sealed class ErrorItem : IComparer<ErrorItem>, IComparable<ErrorItem>
    {
        public int Line { get; }
        public int Position { get; }
        public int Length { get; }
        public string Message { get; }
        public string File { get; }

        public ErrorItem(int line, int pos, int len, string message, string file)
        {
            this.Line = line;
            this.Position = pos;
            this.Length = len;
            this.Message = message;
            this.File = file;
        }

        public int Compare(ErrorItem? x, ErrorItem? y)
        {
            if (x is null && y is null)
                return 0;

            // both are NOT null (/\, so y can be 'cast' to not-null)
            return x?.CompareTo(y) ?? y!.CompareTo(x);
        }

        public int CompareTo(ErrorItem? other) => this.Position - (other?.Position ?? 0);

        public static bool operator >(ErrorItem left, ErrorItem right) => left.CompareTo(right) > 0;

        public static bool operator <(ErrorItem left, ErrorItem right) => left.CompareTo(right) < 0;

        public static bool operator >=(ErrorItem left, ErrorItem right) =>
            left.CompareTo(right) >= 0;

        public static bool operator <=(ErrorItem left, ErrorItem right) =>
            left.CompareTo(right) <= 0;

        public static bool operator !=(ErrorItem left, ErrorItem right) =>
            left.CompareTo(right) != 0;

        public static bool operator ==(ErrorItem left, ErrorItem right) =>
            left.CompareTo(right) == 0;

        public override bool Equals(object? obj)
        {
            if (!(obj is ErrorItem objItem))
                return false;

            return this.File.Equals(objItem.File)
                && this.Message.Equals(objItem.Message)
                && this.Position == objItem.Position
                && this.Length == objItem.Length
                && this.Line == objItem.Line;
        }

        public override int GetHashCode() =>
            this.File.GetHashCode()
            + this.Message.GetHashCode()
            + this.Line.GetHashCode()
            + this.Position.GetHashCode()
            + this.Length.GetHashCode();
    }
}
