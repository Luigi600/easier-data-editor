﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Easier_Data_Editor.Forms.TextEditors;
using Easier_Data_Editor.Serializers;
using Easier.MultiLangSupport;
using Easier.MultiLangSupport.DockPanel.Forms;

namespace Easier_Data_Editor.Forms.ErrorDetections
{
    public partial class ErrorList : TranslatableDockContent
    {
        private const int REFRESH_ERROR_LIST_INTERVAL = 1000 * 5; // in ms; every 5 secs
        private static readonly Regex REGEX_PATTERN_PIC_NUMBER = new Regex(
            @"(?<=(\s+|^)pic:\s*)\d+",
            RegexOptions.IgnoreCase
        );

        private TextEditor? _editor;

        public ErrorList(IMainEditor main, TextEditor? editor)
            : this()
        {
            main.TextEditorChanged += (sender, e) => SetTextEditor(e);
            this.SetTextEditor(editor);
            this.backgroundWorker1.DoWork += this.BackgroundWorker1_DoWork;
        }

        // for translatable lib
        private ErrorList()
        {
            this.VariablesList.Add("ERROR_MESSAGE_ATTRIBUTE", "Undefined attribute '[ATTR]'");
            this.VariablesList.Add("ERROR_MESSAGE_ID_DUPLICATE", "Duplicate frame [ID]");
            this.VariablesList.Add(
                "ERROR_MESSAGE_ID_LIMIT",
                "Frame ID '[ID]' out of the maximum range"
            );
            this.VariablesList.Add(
                "ERROR_MESSAGE_PIC_OUT_OF_RANGE",
                "'pic' value [PIC] is out of range of [MAX]"
            );

            InitializeComponent();
        }

        public override void Translate(Language language)
        {
            base.Translate(language);

            this.SetStatsValue();
        }

        protected override void OnActivated(EventArgs e)
        {
            base.OnActivated(e);

            this.SetStatsValue();
        }

        protected override void OnShown(EventArgs e)
        {
            base.OnShown(e);

            this.backgroundWorker1.RunWorkerAsync();
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            this.backgroundWorker1.CancelAsync();

            base.OnClosing(e);
        }

        private void BackgroundWorker1_DoWork(object? sender, DoWorkEventArgs e)
        {
            if (!(sender is BackgroundWorker worker))
                return;

            while (!worker.CancellationPending)
            {
                this.SetStatsValue();

                System.Threading.Thread.Sleep(REFRESH_ERROR_LIST_INTERVAL);
            }
        }

        private void SetTextEditor(TextEditor? editor)
        {
            this._editor = editor;
            this.SetStatsValue();
        }

        private void SetStatsValue()
        {
            if (this._editor == null)
            {
                if (this.InvokeRequired)
                    this.Invoke(new Action(() => this.lvErrors.Items.Clear()));
                else
                    this.lvErrors.Items.Clear();

                return;
            }

            List<ErrorItem> oldItems = Utilities.ThreadUtil.RunInMainThread(
                this,
                this.lvErrors.Items.Cast<ListViewItem>()
                    .Where(i => i.Tag is not null)
                    .Select(i => (ErrorItem)i.Tag!)
                    .ToList
            );
            List<ErrorItem> errorItems = this.GetErrorItems();

            Utilities.ThreadUtil.RunInMainThread(
                this,
                new Action(() =>
                {
                    Utilities.UiUtil.RefreshListView(
                        this.lvErrors,
                        errorItems,
                        this.GetListViewItemFromErrorItem
                    );
                    this._editor.RemoveErrors(oldItems.Except(errorItems));
                    this._editor.AddErrors(errorItems.Except(oldItems));
                })
            );
        }

        private ListViewItem GetListViewItemFromErrorItem(ErrorItem item)
        {
            ListViewItem lvItem = new ListViewItem(item.Line.ToString());
            lvItem.SubItems.Add(item.Message);
            lvItem.SubItems.Add(item.Position.ToString());
            lvItem.SubItems.Add(item.File);
            lvItem.Tag = item;
            lvItem.ForeColor = Constants.COLOR_ERROR;

            return lvItem;
        }

        private List<ErrorItem> GetErrorItems()
        {
            List<ErrorItem> result = new List<ErrorItem>();

            if (this._editor == null)
                return result;

            string editorText = this.InvokeRequired
                ? this.Invoke(new Func<string>(() => this._editor.EditorText))
                : this._editor.EditorText;
            string path = this._editor.Path;
            Dictionary<int, int> usedIDs = Easier_Data_Editor.Tools.IdsAnalyser.GetUsedFrameIDs(
                editorText
            );

            foreach (Tuple<int, int> tuple in this._editor.IncorrectSyntaxPositions)
            {
                result.Add(
                    new ErrorItem(
                        Utilities.ThreadUtil.RunInMainThread(
                            this._editor,
                            this._editor.GetLineNumberFromPosition,
                            tuple.Item1
                        ) + 1, // +1 coz editor line count is 0-based
                        tuple.Item1,
                        tuple.Item2 - tuple.Item1,
                        this.GetVariable("ERROR_MESSAGE_ATTRIBUTE")
                            .Replace(
                                "[ATTR]",
                                editorText.Substring(tuple.Item1, tuple.Item2 - tuple.Item1)
                            ),
                        path
                    )
                );
            }

            foreach (
                KeyValuePair<int, int> entry in usedIDs
                    .GroupBy(x => x.Value)
                    .Where(g => g.Count() > 1)
                    .SelectMany(g => g)
                    .ToList()
            )
            {
                result.Add(
                    new ErrorItem(
                        Utilities.ThreadUtil.RunInMainThread(
                            this._editor,
                            this._editor.GetLineNumberFromPosition,
                            entry.Key
                        ) + 1, // +1 coz editor line count is 0-based
                        entry.Key,
                        entry.Value.ToString().Length,
                        this.GetVariable("ERROR_MESSAGE_ID_DUPLICATE")
                            .Replace("[ID]", entry.Value.ToString()),
                        path
                    )
                );
            }

            foreach (
                KeyValuePair<int, int> entry in usedIDs.Where(e =>
                    e.Value >= LittleFighter2.Constants.MAXIMUM_FRAMES
                )
            )
            {
                result.Add(
                    new ErrorItem(
                        Utilities.ThreadUtil.RunInMainThread(
                            this._editor,
                            this._editor.GetLineNumberFromPosition,
                            entry.Key
                        ) + 1, // +1 coz editor line count is 0-based
                        entry.Key,
                        entry.Value.ToString().Length,
                        this.GetVariable("ERROR_MESSAGE_ID_LIMIT")
                            .Replace("[ID]", entry.Value.ToString()),
                        path
                    )
                );
            }

            result.AddRange(this.GetInvalidPictureNumbers(this._editor, ref editorText, path));

            result.Sort();
            return result;
        }

        private List<ErrorItem> GetInvalidPictureNumbers(
            TextEditor editor,
            ref string editorText,
            string path
        )
        {
            List<ErrorItem> result = new List<ErrorItem>();

            Models.LittleFighter2.BitmapHeader header = new Models.LittleFighter2.BitmapHeader();
            LF2DataDeserializer.ParseObjectFromAttributes(header, new StringBuilder(editorText));

            int maxPicNumber = 0;
            foreach (Models.LittleFighter2.BitmapHeaderFileRow row in header.Files)
                maxPicNumber += row.Column * row.Row;

            foreach (Match mtch in REGEX_PATTERN_PIC_NUMBER.Matches(editorText))
            {
                try
                {
                    if (!int.TryParse(mtch.Value, out int pic) || pic <= maxPicNumber)
                        continue;

                    result.Add(
                        new ErrorItem(
                            Utilities.ThreadUtil.RunInMainThread(
                                editor,
                                editor.GetLineNumberFromPosition,
                                mtch.Index
                            ) + 1, // +1 coz editor line count is 0-based
                            mtch.Index,
                            mtch.Value.Length,
                            this.GetVariable("ERROR_MESSAGE_PIC_OUT_OF_RANGE")
                                .Replace("[PIC]", mtch.Value)
                                .Replace("[MAX]", maxPicNumber.ToString()),
                            path
                        )
                    );
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }

            return result;
        }

        private void LvErrors_DoubleClick(object sender, EventArgs e)
        {
            ListView senderObj = (ListView)sender;

            if (this._editor == null || senderObj.SelectedItems.Count == 0)
                return;

            ErrorItem? item = (ErrorItem?)senderObj.SelectedItems[0]?.Tag;
            if (item is null)
                return;

            this._editor.GoToPosition(item.Position);
            this._editor.FocusTextEditor();
        }
    }
}
