﻿
namespace Easier_Data_Editor.Forms.Tools
{
    partial class Reformatter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Reformatter));
            this.btnApplySelected = new System.Windows.Forms.Button();
            this.lblTitle = new System.Windows.Forms.Label();
            this.lvFiles = new System.Windows.Forms.ListView();
            this.colFile = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colPath = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnApply = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.rtbDescription = new System.Windows.Forms.RichTextBox();
            this.lblLinkDescription = new System.Windows.Forms.Label();
            this.linkSettings = new System.Windows.Forms.LinkLabel();
            this.SuspendLayout();
            // 
            // btnApplySelected
            // 
            this.btnApplySelected.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnApplySelected.Location = new System.Drawing.Point(304, 364);
            this.btnApplySelected.Name = "btnApplySelected";
            this.btnApplySelected.Size = new System.Drawing.Size(147, 30);
            this.btnApplySelected.TabIndex = 26;
            this.btnApplySelected.Text = "Change Only Selected Text";
            this.btnApplySelected.UseVisualStyleBackColor = true;
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.Location = new System.Drawing.Point(25, 24);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(135, 25);
            this.lblTitle.TabIndex = 1;
            this.lblTitle.Text = "Reformatter";
            // 
            // lvFiles
            // 
            this.lvFiles.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvFiles.CheckBoxes = true;
            this.lvFiles.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colFile,
            this.colPath});
            this.lvFiles.FullRowSelect = true;
            this.lvFiles.GridLines = true;
            this.lvFiles.HideSelection = false;
            this.lvFiles.Location = new System.Drawing.Point(27, 125);
            this.lvFiles.Name = "lvFiles";
            this.lvFiles.Size = new System.Drawing.Size(580, 215);
            this.lvFiles.TabIndex = 0;
            this.lvFiles.UseCompatibleStateImageBehavior = false;
            this.lvFiles.View = System.Windows.Forms.View.Details;
            // 
            // colFile
            // 
            this.colFile.Name = "colFile";
            this.colFile.Text = "File Name";
            this.colFile.Width = 182;
            // 
            // colPath
            // 
            this.colPath.Name = "colPath";
            this.colPath.Text = "Path";
            this.colPath.Width = 351;
            // 
            // btnApply
            // 
            this.btnApply.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnApply.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnApply.ForeColor = System.Drawing.Color.DarkGreen;
            this.btnApply.Location = new System.Drawing.Point(482, 364);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(125, 30);
            this.btnApply.TabIndex = 21;
            this.btnApply.Text = "Apply";
            this.btnApply.UseVisualStyleBackColor = true;
            this.btnApply.Click += new System.EventHandler(this.BtnApply_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnClose.Location = new System.Drawing.Point(27, 364);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(125, 30);
            this.btnClose.TabIndex = 20;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.BtnClose_Click);
            // 
            // rtbDescription
            // 
            this.rtbDescription.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rtbDescription.BackColor = System.Drawing.SystemColors.Control;
            this.rtbDescription.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtbDescription.Location = new System.Drawing.Point(30, 57);
            this.rtbDescription.Name = "rtbDescription";
            this.rtbDescription.Size = new System.Drawing.Size(572, 40);
            this.rtbDescription.TabIndex = 27;
            this.rtbDescription.Text = resources.GetString("rtbDescription.Text");
            // 
            // lblLinkDescription
            // 
            this.lblLinkDescription.AutoSize = true;
            this.lblLinkDescription.Location = new System.Drawing.Point(27, 103);
            this.lblLinkDescription.Name = "lblLinkDescription";
            this.lblLinkDescription.Size = new System.Drawing.Size(102, 13);
            this.lblLinkDescription.TabIndex = 28;
            this.lblLinkDescription.Text = "Reformatter options:";
            // 
            // linkSettings
            // 
            this.linkSettings.AutoSize = true;
            this.linkSettings.Location = new System.Drawing.Point(127, 103);
            this.linkSettings.Name = "linkSettings";
            this.linkSettings.Size = new System.Drawing.Size(178, 13);
            this.linkSettings.TabIndex = 29;
            this.linkSettings.TabStop = true;
            this.linkSettings.Text = "Settings -> Text Editor > Reformatter";
            this.linkSettings.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LinkSettings_LinkClicked);
            // 
            // Reformatter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(634, 421);
            this.Controls.Add(this.linkSettings);
            this.Controls.Add(this.lblLinkDescription);
            this.Controls.Add(this.rtbDescription);
            this.Controls.Add(this.lblTitle);
            this.Controls.Add(this.lvFiles);
            this.Controls.Add(this.btnApplySelected);
            this.Controls.Add(this.btnApply);
            this.Controls.Add(this.btnClose);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(650, 460);
            this.Name = "Reformatter";
            this.Padding = new System.Windows.Forms.Padding(24);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Reformatter";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Button btnApplySelected;
        internal System.Windows.Forms.Label lblTitle;
        internal System.Windows.Forms.ListView lvFiles;
        internal System.Windows.Forms.ColumnHeader colFile;
        internal System.Windows.Forms.ColumnHeader colPath;
        internal System.Windows.Forms.Button btnApply;
        internal System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.RichTextBox rtbDescription;
        private System.Windows.Forms.Label lblLinkDescription;
        private System.Windows.Forms.LinkLabel linkSettings;
    }
}