﻿using System.Linq;
using System.Windows.Forms;
using Easier_Data_Editor.Forms.TextEditors;
using Easier_Data_Editor.Windows.Settings;
using Easier.MultiLangSupport.Forms;

namespace Easier_Data_Editor.Forms.Tools
{
    public partial class Reformatter : TranslatableForm
    {
        public Reformatter()
            : this(System.Array.Empty<TextEditor>()) { }

        public Reformatter(TextEditor[] editors)
        {
            InitializeComponent();

            this.Init(editors);
        }

        #region Events


        private void BtnClose_Click(object sender, System.EventArgs e) => this.Close();

        private void LinkSettings_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            new Settings(
                Environments.Application.UserSettings,
                Environments.Application.FormatBlueprints,
                nameof(UCSettingsTextEditorReformatter)
            ).ShowDialog();
        }

        #endregion

        private void Init(TextEditor[] editors)
        {
            foreach (TextEditor editor in editors)
            {
                ListViewItem lvItem = new ListViewItem(editor.FileName);
                lvItem.SubItems.Add(editor.Path);
                lvItem.Tag = editor;

                this.lvFiles.Items.Add(lvItem);
            }
        }

        private void BtnApply_Click(object sender, System.EventArgs e)
        {
            foreach (
                TextEditor editor in this
                    .lvFiles.CheckedItems.OfType<ListViewItem>()
                    .Where(l => l.Tag is TextEditor)
                    .Select(l => (TextEditor)l.Tag!)
            )
            {
                editor.Reformat(false);
            }
        }
    }
}
