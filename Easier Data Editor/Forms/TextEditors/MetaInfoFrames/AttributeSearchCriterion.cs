﻿using System.Text.RegularExpressions;

namespace Easier_Data_Editor.Forms.TextEditors.MetaInfoFrames
{
    public sealed class AttributeSearchCriterion
    {
        public Regex Pattern { get; }
        public LF2Attribute Attribute { get; }
        public LF2ValueTypes ValueType { get; }

        public AttributeSearchCriterion(Regex pattern, LF2Attribute attr, LF2ValueTypes valType)
        {
            this.Pattern = pattern;
            this.Attribute = attr;
            this.ValueType = valType;
        }
    }
}
