﻿namespace Easier_Data_Editor.Forms.TextEditors.MetaInfoFrames
{
    public enum LF2Attribute
    {
        Center,
        Body,
        Itr,
        BloodPoint,
        CatchPoint,
        ObjectPoint,
        WeaponPoint,
    }
}
