﻿using System;

namespace Easier_Data_Editor.Forms.TextEditors.MetaInfoFrames
{
    [Flags]
    public enum LF2ValueTypes
    {
        X = 1,
        Y = 2,
        Z = 4,
        Width = 8,
        Height = 16,
        Depth = 32,
    }
}
