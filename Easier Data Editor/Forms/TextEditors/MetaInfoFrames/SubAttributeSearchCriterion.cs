﻿using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Easier_Data_Editor.Forms.TextEditors.MetaInfoFrames
{
    public sealed class SubAttributeSearchCriterion
    {
        private readonly List<AttributeSearchCriterion> _criteria =
            new List<AttributeSearchCriterion>();

        public Regex Pattern { get; }
        public bool IsArray { get; }
        public AttributeSearchCriterion[] Criteria => this._criteria.ToArray();

        public SubAttributeSearchCriterion(
            Regex pattern,
            IEnumerable<AttributeSearchCriterion> criteria,
            bool isArray = false
        )
        {
            this.Pattern = pattern;
            this.IsArray = isArray;
            this._criteria.AddRange(criteria);
        }
    }
}
