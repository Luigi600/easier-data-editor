﻿using System.Collections.Generic;
using System.Linq;

namespace Easier_Data_Editor.Forms.TextEditors.MetaInfoFrames
{
    public class MetaInfoFrame
    {
        public int Offset { get; set; }
        public int Length { get; set; }

        private readonly List<LF2AttributeValueOffset> _attributeValueOffsets =
            new List<LF2AttributeValueOffset>();

        public MetaInfoFrame(int offset, int len)
        {
            this.Offset = offset;
            this.Length = len;
        }

        public void AddOffset(LF2AttributeValueOffset offset)
        {
            this._attributeValueOffsets.Add(offset);

            offset.LengthChanged += this.PropertyLengthChanged;
        }

        public LF2AttributeValueOffset? GetOffset(LF2Attribute attr, LF2ValueTypes prop) =>
            this._attributeValueOffsets.Find(
                (p) => p.Attribute == attr && (p.ValueType & prop) == p.ValueType
            );

        public LF2AttributeValueInArrayOffset? GetOffset(
            LF2Attribute attr,
            LF2ValueTypes prop,
            int arrayIndex
        ) =>
            this
                ._attributeValueOffsets.Where(p => p is LF2AttributeValueInArrayOffset)
                .Cast<LF2AttributeValueInArrayOffset>()
                .FirstOrDefault(
                    (p) =>
                        p.ArrayIndex == arrayIndex
                        && p.Attribute == attr
                        && (p.ValueType & prop) == p.ValueType
                );

        public void ChangeLength(LF2Attribute attr, LF2ValueTypes prop, int newLength)
        {
            LF2AttributeValueOffset? property = this._attributeValueOffsets.Find(p =>
                p.Attribute == attr && (p.ValueType & prop) == p.ValueType
            );

            if (property != null)
                property.Length = newLength;
        }

        public void ChangeLength(
            LF2Attribute attr,
            LF2ValueTypes prop,
            int arrayIndex,
            int newLength
        )
        {
            LF2AttributeValueInArrayOffset? property = this
                ._attributeValueOffsets.Where(p => p is LF2AttributeValueInArrayOffset)
                .Cast<LF2AttributeValueInArrayOffset>()
                .FirstOrDefault(p =>
                    p.ArrayIndex == arrayIndex
                    && p.Attribute == attr
                    && (p.ValueType & prop) == p.ValueType
                );

            if (property != null)
                property.Length = newLength;
        }

        private void PropertyLengthChanged(object? sender, int e)
        {
            if (!(sender is LF2AttributeValueOffset property))
                return;

            foreach (
                LF2AttributeValueOffset propOffset in this._attributeValueOffsets.Where(p =>
                    p.Offset > property.Offset
                )
            )
                propOffset.Offset += e;

            this.Length += e;
        }
    }
}
