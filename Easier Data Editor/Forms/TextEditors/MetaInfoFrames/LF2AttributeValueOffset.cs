﻿using System;

namespace Easier_Data_Editor.Forms.TextEditors.MetaInfoFrames
{
    public class LF2AttributeValueOffset
    {
        private int _length = 0;

        public event EventHandler<int>? LengthChanged;

        public virtual LF2Attribute Attribute { get; }
        public virtual LF2ValueTypes ValueType { get; }
        public virtual int Offset { get; set; }

        public virtual int Length
        {
            get => this._length;
            set
            {
                if (this._length == value)
                    return;

                int diff = this._length - value;
                this._length = value;
                this.LengthChanged?.Invoke(this, diff);
            }
        }

        public LF2AttributeValueOffset(
            LF2Attribute attr,
            LF2ValueTypes valType,
            int offset,
            int len
        )
        {
            this.Attribute = attr;
            this.ValueType = valType;
            this.Offset = offset;
            this.Length = len;
        }
    }
}
