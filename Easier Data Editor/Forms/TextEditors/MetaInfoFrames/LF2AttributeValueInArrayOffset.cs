﻿namespace Easier_Data_Editor.Forms.TextEditors.MetaInfoFrames
{
    public class LF2AttributeValueInArrayOffset : LF2AttributeValueOffset
    {
        public int ArrayIndex { get; set; } = 0;

        public LF2AttributeValueInArrayOffset(
            LF2Attribute attr,
            LF2ValueTypes valType,
            int offset,
            int len,
            int arrayIndex = 0
        )
            : base(attr, valType, offset, len)
        {
            this.ArrayIndex = arrayIndex;
        }
    }
}
