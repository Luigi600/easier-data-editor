﻿using System.Collections.Generic;
using System.Text.RegularExpressions;
using Easier_Data_Editor.Utilities;

namespace Easier_Data_Editor.Forms.TextEditors.MetaInfoFrames
{
    public static class Creator
    {
        private static List<AttributeSearchCriterion> ROOT_ATTRIBUTES =
            new List<AttributeSearchCriterion>
            {
                new AttributeSearchCriterion(
                    RegexUtil.GetSingleWordIntegerNegativeRegexFromWordAndValuePattern("centerx"),
                    LF2Attribute.Center,
                    LF2ValueTypes.X
                ),
                new AttributeSearchCriterion(
                    RegexUtil.GetSingleWordIntegerNegativeRegexFromWordAndValuePattern("centery"),
                    LF2Attribute.Center,
                    LF2ValueTypes.Y
                ),
            };

        private static List<SubAttributeSearchCriterion> SUB_ATTRIBUTES =
            new List<SubAttributeSearchCriterion>()
            {
                new SubAttributeSearchCriterion(
                    new Regex(
                        @"bdy:.*?bdy_end:",
                        RegexOptions.IgnoreCase | RegexOptions.Singleline
                    ),
                    new List<AttributeSearchCriterion>()
                    {
                        new AttributeSearchCriterion(
                            RegexUtil.GetSingleWordIntegerNegativeRegexFromWordAndValuePattern("x"),
                            LF2Attribute.Body,
                            LF2ValueTypes.X
                        ),
                        new AttributeSearchCriterion(
                            RegexUtil.GetSingleWordIntegerNegativeRegexFromWordAndValuePattern("y"),
                            LF2Attribute.Body,
                            LF2ValueTypes.Y
                        ),
                        new AttributeSearchCriterion(
                            RegexUtil.GetSingleWordIntegerNegativeRegexFromWordAndValuePattern("w"),
                            LF2Attribute.Body,
                            LF2ValueTypes.Width
                        ),
                        new AttributeSearchCriterion(
                            RegexUtil.GetSingleWordIntegerNegativeRegexFromWordAndValuePattern("h"),
                            LF2Attribute.Body,
                            LF2ValueTypes.Height
                        ),
                    },
                    true
                ),
                new SubAttributeSearchCriterion(
                    new Regex(
                        @"itr:.*?itr_end:",
                        RegexOptions.IgnoreCase | RegexOptions.Singleline
                    ),
                    new List<AttributeSearchCriterion>()
                    {
                        new AttributeSearchCriterion(
                            RegexUtil.GetSingleWordIntegerNegativeRegexFromWordAndValuePattern("x"),
                            LF2Attribute.Itr,
                            LF2ValueTypes.X
                        ),
                        new AttributeSearchCriterion(
                            RegexUtil.GetSingleWordIntegerNegativeRegexFromWordAndValuePattern("y"),
                            LF2Attribute.Itr,
                            LF2ValueTypes.Y
                        ),
                        new AttributeSearchCriterion(
                            RegexUtil.GetSingleWordIntegerNegativeRegexFromWordAndValuePattern("w"),
                            LF2Attribute.Itr,
                            LF2ValueTypes.Width
                        ),
                        new AttributeSearchCriterion(
                            RegexUtil.GetSingleWordIntegerNegativeRegexFromWordAndValuePattern("h"),
                            LF2Attribute.Itr,
                            LF2ValueTypes.Height
                        ),
                        new AttributeSearchCriterion(
                            RegexUtil.GetSingleWordIntegerNegativeRegexFromWordAndValuePattern(
                                "zwidth"
                            ),
                            LF2Attribute.Itr,
                            LF2ValueTypes.Depth
                        ),
                    },
                    true
                ),
                new SubAttributeSearchCriterion(
                    new Regex(
                        @"bpoint:.*?bpoint_end:",
                        RegexOptions.IgnoreCase | RegexOptions.Singleline
                    ),
                    new List<AttributeSearchCriterion>()
                    {
                        new AttributeSearchCriterion(
                            RegexUtil.GetSingleWordIntegerNegativeRegexFromWordAndValuePattern("x"),
                            LF2Attribute.BloodPoint,
                            LF2ValueTypes.X
                        ),
                        new AttributeSearchCriterion(
                            RegexUtil.GetSingleWordIntegerNegativeRegexFromWordAndValuePattern("y"),
                            LF2Attribute.BloodPoint,
                            LF2ValueTypes.Y
                        ),
                    },
                    false
                ),
                new SubAttributeSearchCriterion(
                    new Regex(
                        @"cpoint:.*?cpoint_end:",
                        RegexOptions.IgnoreCase | RegexOptions.Singleline
                    ),
                    new List<AttributeSearchCriterion>()
                    {
                        new AttributeSearchCriterion(
                            RegexUtil.GetSingleWordIntegerNegativeRegexFromWordAndValuePattern("x"),
                            LF2Attribute.CatchPoint,
                            LF2ValueTypes.X
                        ),
                        new AttributeSearchCriterion(
                            RegexUtil.GetSingleWordIntegerNegativeRegexFromWordAndValuePattern("y"),
                            LF2Attribute.CatchPoint,
                            LF2ValueTypes.Y
                        ),
                    },
                    false
                ),
                new SubAttributeSearchCriterion(
                    new Regex(
                        @"opoint:.*?opoint_end:",
                        RegexOptions.IgnoreCase | RegexOptions.Singleline
                    ),
                    new List<AttributeSearchCriterion>()
                    {
                        new AttributeSearchCriterion(
                            RegexUtil.GetSingleWordIntegerNegativeRegexFromWordAndValuePattern("x"),
                            LF2Attribute.ObjectPoint,
                            LF2ValueTypes.X
                        ),
                        new AttributeSearchCriterion(
                            RegexUtil.GetSingleWordIntegerNegativeRegexFromWordAndValuePattern("y"),
                            LF2Attribute.ObjectPoint,
                            LF2ValueTypes.Y
                        ),
                    },
                    false
                ),
                new SubAttributeSearchCriterion(
                    new Regex(
                        @"wpoint:.*?wpoint_end:",
                        RegexOptions.IgnoreCase | RegexOptions.Singleline
                    ),
                    new List<AttributeSearchCriterion>()
                    {
                        new AttributeSearchCriterion(
                            RegexUtil.GetSingleWordIntegerNegativeRegexFromWordAndValuePattern("x"),
                            LF2Attribute.WeaponPoint,
                            LF2ValueTypes.X
                        ),
                        new AttributeSearchCriterion(
                            RegexUtil.GetSingleWordIntegerNegativeRegexFromWordAndValuePattern("y"),
                            LF2Attribute.WeaponPoint,
                            LF2ValueTypes.Y
                        ),
                    },
                    false
                ),
            };

        public static MetaInfoFrame GetMetaInfoFrameFromText(string frame, int frameOffset)
        {
            MetaInfoFrame metaFrame = new MetaInfoFrame(frameOffset, frame.Length);

            AddMetaInfoPropertiesToMetaFrame(frame, frameOffset, metaFrame, ROOT_ATTRIBUTES);
            AddSubMetaInfoPropertiesToMetaFrame(frame, frameOffset, metaFrame, SUB_ATTRIBUTES);

            return metaFrame;
        }

        private static void AddSubMetaInfoPropertiesToMetaFrame(
            string frame,
            int frameOffset,
            MetaInfoFrame metaFrame,
            IEnumerable<SubAttributeSearchCriterion> props
        )
        {
            foreach (SubAttributeSearchCriterion prop in props)
            {
                MatchCollection matches = prop.Pattern.Matches(frame);
                int matchIndex = 0;
                foreach (Match match in matches)
                {
                    if (!prop.IsArray)
                    {
                        AddMetaInfoPropertiesToMetaFrame(
                            match.Value,
                            frameOffset + match.Index,
                            metaFrame,
                            prop.Criteria
                        );
                        break;
                    }
                    else
                    {
                        AddMetaInfoArrayPropertiesToMetaFrame(
                            match.Value,
                            frameOffset + match.Index,
                            matchIndex,
                            metaFrame,
                            prop.Criteria
                        );
                    }

                    ++matchIndex;
                }
            }
        }

        private static void AddMetaInfoPropertiesToMetaFrame(
            string frame,
            int frameOffset,
            MetaInfoFrame metaFrame,
            IEnumerable<AttributeSearchCriterion> props
        )
        {
            foreach (AttributeSearchCriterion prop in props)
            {
                Match match = prop.Pattern.Match(frame);
                if (!match.Success)
                    continue;

                metaFrame.AddOffset(
                    new LF2AttributeValueOffset(
                        prop.Attribute,
                        prop.ValueType,
                        frameOffset + match.Index,
                        match.Value.Length
                    )
                );
            }
        }

        private static void AddMetaInfoArrayPropertiesToMetaFrame(
            string frame,
            int frameOffset,
            int arrayIndex,
            MetaInfoFrame metaFrame,
            IEnumerable<AttributeSearchCriterion> props
        )
        {
            foreach (AttributeSearchCriterion prop in props)
            {
                Match match = prop.Pattern.Match(frame);
                if (!match.Success)
                    continue;

                metaFrame.AddOffset(
                    new LF2AttributeValueInArrayOffset(
                        prop.Attribute,
                        prop.ValueType,
                        frameOffset + match.Index,
                        match.Value.Length,
                        arrayIndex
                    )
                );
            }
        }
    }
}
