﻿using System.Text.RegularExpressions;

namespace Easier_Data_Editor.Forms.TextEditors
{
    partial class TextEditor
    {
        private static Regex REGEX_PATTERN_MATCH_START = new Regex(
            @"(^|^[^#]*?\s+)<frame>",
            RegexOptions.IgnoreCase
        );
        private static Regex REGEX_PATTERN_MATCH_END = new Regex(
            @"(^|^[^#]*?\s+)<frame_end>",
            RegexOptions.IgnoreCase
        );
        private static Regex REGEX_PATTERN_NEW_LINE = new Regex(@"\n", RegexOptions.IgnoreCase);

        // private static Regex REGEX_PATTERN_NUMBER = new Regex(@"-?\d+", RegexOptions.IgnoreCase);
        private static Regex REGEX_PATTERN_NUMBER_FLOAT = new Regex(
            @"-?\d+(\.\d+)?",
            RegexOptions.IgnoreCase
        );
    }
}
