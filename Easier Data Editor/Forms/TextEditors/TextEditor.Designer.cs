﻿
namespace Easier_Data_Editor.Forms.TextEditors
{
    partial class TextEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TextEditor));
            this.cmsActions = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmiCut = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiCopy = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiPaste = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiPasteAdaptIDs = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiPasteXY = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiSelectAll = new System.Windows.Forms.ToolStripMenuItem();
            this.tss1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmiFrameViewer = new System.Windows.Forms.ToolStripMenuItem();
            this.splitCon = new System.Windows.Forms.SplitContainer();
            this.scintilla1 = new Extensions.Scintilla.LF2Scintilla();
            this.btnSplit = new System.Windows.Forms.Button();
            this.cmsActions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)this.splitCon).BeginInit();
            this.splitCon.Panel1.SuspendLayout();
            this.splitCon.SuspendLayout();
            this.SuspendLayout();
            // 
            // cmsActions
            // 
            this.cmsActions.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { this.tsmiCut, this.tsmiCopy, this.tsmiPaste, this.tsmiPasteAdaptIDs, this.tsmiPasteXY, this.tsmiDelete, this.tsmiSelectAll, this.tss1, this.tsmiFrameViewer });
            this.cmsActions.Name = "CMS_actions";
            this.cmsActions.Size = new System.Drawing.Size(174, 186);
            // 
            // tsmiCut
            // 
            this.tsmiCut.Image = Properties.Resources.icon_cut;
            this.tsmiCut.Name = "tsmiCut";
            this.tsmiCut.Size = new System.Drawing.Size(173, 22);
            this.tsmiCut.Text = "Cut";
            this.tsmiCut.Click += this.TsmiCut_Click;
            // 
            // tsmiCopy
            // 
            this.tsmiCopy.Image = Properties.Resources.icon_copy;
            this.tsmiCopy.Name = "tsmiCopy";
            this.tsmiCopy.Size = new System.Drawing.Size(173, 22);
            this.tsmiCopy.Text = "Copy";
            this.tsmiCopy.Click += this.TsmiCopy_Click;
            // 
            // tsmiPaste
            // 
            this.tsmiPaste.Image = Properties.Resources.icon_paste;
            this.tsmiPaste.Name = "tsmiPaste";
            this.tsmiPaste.Size = new System.Drawing.Size(173, 22);
            this.tsmiPaste.Text = "Paste";
            this.tsmiPaste.Click += this.TsmiPaste_Click;
            // 
            // tsmiPasteAdaptIDs
            // 
            this.tsmiPasteAdaptIDs.Image = Properties.Resources.icon_paste_cool;
            this.tsmiPasteAdaptIDs.Name = "tsmiPasteAdaptIDs";
            this.tsmiPasteAdaptIDs.Size = new System.Drawing.Size(173, 22);
            this.tsmiPasteAdaptIDs.Text = "Paste && Adapt IDs";
            this.tsmiPasteAdaptIDs.Click += this.TsmiPasteAdaptIDs_Click;
            // 
            // tsmiPasteXY
            // 
            this.tsmiPasteXY.Name = "tsmiPasteXY";
            this.tsmiPasteXY.Size = new System.Drawing.Size(173, 22);
            this.tsmiPasteXY.Text = "Paste && Adapt X, Y";
            this.tsmiPasteXY.Click += this.TsmiPasteXY_Click;
            // 
            // tsmiDelete
            // 
            this.tsmiDelete.Image = Properties.Resources.icon_delete;
            this.tsmiDelete.Name = "tsmiDelete";
            this.tsmiDelete.Size = new System.Drawing.Size(173, 22);
            this.tsmiDelete.Text = "Delete";
            this.tsmiDelete.Click += this.TsmiDelete_Click;
            // 
            // tsmiSelectAll
            // 
            this.tsmiSelectAll.Image = Properties.Resources.icon_select_all;
            this.tsmiSelectAll.Name = "tsmiSelectAll";
            this.tsmiSelectAll.Size = new System.Drawing.Size(173, 22);
            this.tsmiSelectAll.Text = "Select All";
            this.tsmiSelectAll.Click += this.TsmiSelectAll_Click;
            // 
            // tss1
            // 
            this.tss1.Name = "tss1";
            this.tss1.Size = new System.Drawing.Size(170, 6);
            // 
            // tsmiFrameViewer
            // 
            this.tsmiFrameViewer.CheckOnClick = true;
            this.tsmiFrameViewer.Image = Properties.Resources.icon_character;
            this.tsmiFrameViewer.Name = "tsmiFrameViewer";
            this.tsmiFrameViewer.Size = new System.Drawing.Size(173, 22);
            this.tsmiFrameViewer.Text = "Frame Viewer";
            this.tsmiFrameViewer.Click += this.TsmiFrameViewer_Click;
            // 
            // splitCon
            // 
            this.splitCon.AllowDrop = true;
            this.splitCon.BackColor = System.Drawing.Color.Silver;
            this.splitCon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitCon.Location = new System.Drawing.Point(0, 0);
            this.splitCon.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.splitCon.Name = "splitCon";
            this.splitCon.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitCon.Panel1
            // 
            this.splitCon.Panel1.AllowDrop = true;
            this.splitCon.Panel1.Controls.Add(this.scintilla1);
            this.splitCon.Panel1.DragDrop += this.DragDropOnTextEditorControls;
            this.splitCon.Panel1.DragEnter += this.DragEnterOnTextEditorControls;
            this.splitCon.Panel1.DragOver += this.DragOverOnTextEditorControls;
            this.splitCon.Panel1MinSize = 0;
            // 
            // splitCon.Panel2
            // 
            this.splitCon.Panel2.AllowDrop = true;
            this.splitCon.Panel2.DragDrop += this.DragDropOnTextEditorControls;
            this.splitCon.Panel2.DragEnter += this.DragEnterOnTextEditorControls;
            this.splitCon.Panel2.DragOver += this.DragOverOnTextEditorControls;
            this.splitCon.Panel2Collapsed = true;
            this.splitCon.Panel2MinSize = 0;
            this.splitCon.Size = new System.Drawing.Size(933, 519);
            this.splitCon.SplitterDistance = 58;
            this.splitCon.SplitterWidth = 3;
            this.splitCon.TabIndex = 2;
            this.splitCon.SplitterMoved += this.SplitCon_SplitterMoved;
            this.splitCon.DragDrop += this.DragDropOnTextEditorControls;
            this.splitCon.DragEnter += this.DragEnterOnTextEditorControls;
            this.splitCon.DragOver += this.DragOverOnTextEditorControls;
            // 
            // scintilla1
            // 
            this.scintilla1.AdditionalSelectionTyping = true;
            this.scintilla1.AllowDrop = true;
            this.scintilla1.AutocompleteListSelectedBackColor = System.Drawing.Color.FromArgb(51, 153, 255);
            this.scintilla1.BlockUndoRedoActions = false;
            this.scintilla1.BorderStyle = ScintillaNET.BorderStyle.None;
            this.scintilla1.CaretLineBackColor = System.Drawing.Color.FromArgb(235, 235, 255);
            this.scintilla1.CaretLineVisibleAlways = true;
            this.scintilla1.ContextMenuStrip = this.cmsActions;
            this.scintilla1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scintilla1.LexerName = null;
            this.scintilla1.Location = new System.Drawing.Point(0, 0);
            this.scintilla1.Margin = new System.Windows.Forms.Padding(2);
            this.scintilla1.MouseSelectionRectangularSwitch = true;
            this.scintilla1.MultiPaste = ScintillaNET.MultiPaste.Each;
            this.scintilla1.MultipleSelection = true;
            this.scintilla1.Name = "scintilla1";
            this.scintilla1.ScrollWidth = 100;
            this.scintilla1.SelectionInactiveBackColor = System.Drawing.Color.Silver;
            this.scintilla1.Size = new System.Drawing.Size(933, 519);
            this.scintilla1.SuppressFoldingCheck = false;
            this.scintilla1.TabIndex = 0;
            this.scintilla1.VirtualSpaceOptions = ScintillaNET.VirtualSpace.RectangularSelection;
            this.scintilla1.WhitespaceSize = 3;
            this.scintilla1.WhitespaceTextColor = System.Drawing.Color.LightGray;
            this.scintilla1.DragDrop += this.DragDropOnTextEditorControls;
            this.scintilla1.DragEnter += this.DragEnterOnTextEditorControls;
            this.scintilla1.DragOver += this.DragOverOnTextEditorControls;
            // 
            // btnSplit
            // 
            this.btnSplit.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right;
            this.btnSplit.FlatAppearance.BorderSize = 0;
            this.btnSplit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSplit.Image = Properties.Resources.icon_split_horizontal;
            this.btnSplit.Location = new System.Drawing.Point(889, 0);
            this.btnSplit.Margin = new System.Windows.Forms.Padding(0);
            this.btnSplit.Name = "btnSplit";
            this.btnSplit.Size = new System.Drawing.Size(22, 22);
            this.btnSplit.TabIndex = 6;
            this.btnSplit.UseVisualStyleBackColor = true;
            this.btnSplit.MouseDown += this.BtnSplit_MouseDown;
            this.btnSplit.MouseMove += this.BtnSplit_MouseMove;
            this.btnSplit.MouseUp += this.BtnSplit_MouseUp;
            // 
            // TextEditor
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(933, 519);
            this.Controls.Add(this.btnSplit);
            this.Controls.Add(this.splitCon);
            this.Icon = (System.Drawing.Icon)resources.GetObject("$this.Icon");
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Name = "TextEditor";
            this.Text = "New file";
            this.cmsActions.ResumeLayout(false);
            this.splitCon.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)this.splitCon).EndInit();
            this.splitCon.ResumeLayout(false);
            this.ResumeLayout(false);
        }

        #endregion

        private Extensions.Scintilla.LF2Scintilla scintilla1;
        internal System.Windows.Forms.ContextMenuStrip cmsActions;
        internal System.Windows.Forms.ToolStripMenuItem tsmiCut;
        internal System.Windows.Forms.ToolStripMenuItem tsmiCopy;
        internal System.Windows.Forms.ToolStripMenuItem tsmiPaste;
        internal System.Windows.Forms.ToolStripMenuItem tsmiPasteAdaptIDs;
        internal System.Windows.Forms.ToolStripMenuItem tsmiPasteXY;
        internal System.Windows.Forms.ToolStripMenuItem tsmiDelete;
        internal System.Windows.Forms.ToolStripMenuItem tsmiSelectAll;
        internal System.Windows.Forms.ToolStripSeparator tss1;
        internal System.Windows.Forms.ToolStripMenuItem tsmiFrameViewer;
        private System.Windows.Forms.SplitContainer splitCon;
        internal System.Windows.Forms.Button btnSplit;
    }
}