﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Diagnostics;
using System.Linq;
using System.Media;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Easier_Data_Editor.Environments.FormatBlueprints;
using Easier_Data_Editor.Extensions.Scintilla;
using Easier_Data_Editor.Forms.ErrorDetections;
using Easier_Data_Editor.Forms.TextEditors.MetaInfoFrames;
using Easier_Data_Editor.Forms.TextEditors.Models;
using Easier_Data_Editor.Models;
using Easier_Data_Editor.Models.LittleFighter2;
using Easier_Data_Editor.Properties;
using Easier_Data_Editor.Serializers;
using Easier_Data_Editor.Tools.Reformatters;
using Easier.MultiLangSupport;
using Easier.MultiLangSupport.DockPanel.Forms;
using ScintillaNET;
using Scintilla = Easier_Data_Editor.Extensions.Scintilla.Scintilla;

namespace Easier_Data_Editor.Forms.TextEditors
{
    public partial class TextEditor : TranslatableDockContent, IEditor, ISavableEditor
    {
        private MetaInfoFrame? _metaFrame;

        private int _lastTextLength = 0;
        private int _lastTextLines = 0;
        private int _lastTextPos = 0;
        private int _lastSelectedChars = 0;
        private int _lastSelectedLines = 0;
        private string _path = "";
        private string _fileName = "";
        private SaveState? _lastSavedState = null;
        private readonly SaveState _currentState;

        public event EventHandler<bool>? HasFocusChanged;
        public event EventHandler? SaveStateChanged;
        public event EventHandler? UndoRedoStackChanged;
        public event EventHandler? TextLengthChanged;
        public event EventHandler? LinesCountChanged;
        public event EventHandler? TextPositionChanged;
        public event EventHandler? SelectionChanged;
        public event EventHandler? DocumentPropertiesChanged;

        public int ID { get; } = Environments.Application.RANDOM_GENERATOR.Next(1, int.MaxValue);

        public string FileName
        {
            get => this._fileName;
        }

        public string Path
        {
            get => this._path;
            set
            {
                if (this._path.Equals(value))
                    return;

                this._path = value;
                this._fileName = System.IO.Path.GetFileName(value);
                this.Text = this._fileName;
                this.ToolTipText = this._path;
            }
        }

        public bool IsSave =>
            this._lastSavedState != null && this._currentState.DataAreEquals(this._lastSavedState);

        public bool DocumentEncrypted => this._currentState.EncryptedDocument;

        public TextEncoding Encoding => this._currentState.Encoding;

        public ImmutableList<Tuple<int, int>> IncorrectSyntaxPositions =>
            this.GetActiveScintillaEditor().IncorrectSyntaxPositions;

        public string EditorText => this.scintilla1.Text;

        public TextEditor(string text, string path, bool wasEncrypted, TextEncoding encoding)
            : this(false)
        {
            this._currentState = new SaveState(encoding, wasEncrypted, true);
            this._lastSavedState = new SaveState(
                (TextEncoding)encoding.Clone(),
                wasEncrypted,
                true
            );
            this._fileName = this.GetVariable("NEW_FILE");

            this.InitializeComponent();
            this.AfterControlsInit();

            this.Path = path;
            this.SetTabNameFromSaveState();

            this.scintilla1.SetStartText(text);
            this.scintilla1.UpdateUI += this.Scintilla_UpdateUI;
            this.scintilla1.SavePointReached += (_, __) => this.SetIsReachedSavePoint(true);
            this.scintilla1.SavePointLeft += (_, __) => this.SetIsReachedSavePoint(false);
            this.scintilla1.UndoRedoStackChanged += (sender, e) =>
                this.UndoRedoStackChanged?.Invoke(this, EventArgs.Empty);
        }

        // for translatable
        private TextEditor(bool initializeComponents = true)
        {
            this.VariablesList.Add("NEW_FILE", "New File");
            this._currentState = new SaveState(
                new TextEncoding(ETextEncoding.UTF8, false),
                false,
                true
            );

            if (initializeComponents)
                this.InitializeComponent();
        }

        private static void SetClipboard(Frame frame, string text)
        {
            var dataObj = new DataObject();
            dataObj.SetText(text);
            dataObj.SetData("frame", frame);
            Clipboard.SetDataObject(dataObj);
        }

        private static MetaInfoFrame? SetMetaFrame(Scintilla scin)
        {
            int frameStartLine = GetFrameSectionStart(scin);
            if (frameStartLine < 0)
                return null;
            int frameEndLine = GetFrameSectionEnd(scin);
            if (frameEndLine < 0)
                return null;

            int frameOffset = scin.Lines[frameStartLine].Position;
            string frame = scin.GetTextRange(
                frameOffset,
                scin.Lines[frameEndLine].EndPosition - frameOffset
            );

            MetaInfoFrame metaFrame = Creator.GetMetaInfoFrameFromText(frame, frameOffset);

            return metaFrame;
        }

        private static int GetFrameSectionStart(Scintilla scin)
        {
            int scanStartLine = scin.CurrentLine;
            int frameStartLine = scanStartLine;

            while (frameStartLine >= 0)
            {
                Line line = scin.Lines[frameStartLine];
                if (frameStartLine != scanStartLine && REGEX_PATTERN_MATCH_END.IsMatch(line.Text))
                    return -1;

                if (REGEX_PATTERN_MATCH_START.IsMatch(line.Text))
                    return frameStartLine;

                --frameStartLine;
            }

            return -1;
        }

        private static int GetFrameSectionEnd(Scintilla scin)
        {
            int scanStartLine = scin.CurrentLine;
            int frameEndLine = scanStartLine;

            while (frameEndLine >= 0 && frameEndLine < scin.Lines.Count)
            {
                Line line = scin.Lines[frameEndLine];
                if (REGEX_PATTERN_MATCH_END.IsMatch(line.Text))
                    return frameEndLine;

                if (REGEX_PATTERN_MATCH_START.IsMatch(line.Text))
                    return -1;

                ++frameEndLine;
            }

            return -1;
        }

        private static bool IsDataTxt(string text) =>
            text.Contains("<object>")
            || text.Contains("<background>")
            || text.Contains("<file_editing>");

        private static bool IsStage(string text) =>
            text.Contains("<stage>") || text.Contains("<phase>") || text.Contains("bound:");

        private static bool IsBackground(string text) =>
            text.Contains("zboundary:") || text.Contains("shadowsize:") || text.Contains("layer:");

        public override void Translate(Language language)
        {
            bool isNewFile = string.IsNullOrEmpty(this._path);
            base.Translate(language);

            if (isNewFile)
                this._fileName = this.GetVariable("NEW_FILE");

            // necessary to call always because the translator has already translate the "Text" property
            this.SetTabNameFromSaveState();
        }

        public void DocumentSaved()
        {
            this._lastSavedState = new SaveState(
                (TextEncoding)this._currentState.Encoding.Clone(),
                this._currentState.EncryptedDocument,
                true
            );
            this._currentState.IsReachedSavePoint = true;

            this.scintilla1.SetSavePoint();
            this.UpdateSaveState();
        }

        public void SetHasFocus(bool focus, bool raiseEvent = true)
        {
            // TODO

            if (raiseEvent)
                HasFocusChanged?.Invoke(this, focus);
        }

        public void SetActiveState(bool state)
        {
            foreach (Scintilla scin in this.GetAllEditors())
                scin.Enabled = state;
        }

        public void ChangeEncoding(ETextEncoding encoding, bool bom)
        {
            if (
                this._currentState.Encoding.Encoding == encoding
                && this._currentState.Encoding.HasBOM == bom
            )
                return;

            this._currentState.Encoding.Encoding = encoding;
            this._currentState.Encoding.HasBOM = bom;

            this.DocumentPropertiesChanged?.Invoke(this, EventArgs.Empty);
            this.UpdateSaveState();
        }

        public void ChangeEncryptionState(bool isEncrypted)
        {
            if (this._currentState.EncryptedDocument == isEncrypted)
                return;

            this._currentState.EncryptedDocument = isEncrypted;

            this.DocumentPropertiesChanged?.Invoke(this, EventArgs.Empty);
            this.UpdateSaveState();
        }

        public void SetAnnotations()
        {
            foreach (LF2Scintilla editor in this.GetAllEditors())
            {
                editor.SetAnnotations();
                editor.DrawAnnotations();
            }
        }

        public void ClearAnnotations()
        {
            foreach (LF2Scintilla editor in this.GetAllEditors())
            {
                editor.ClearAnnotations();
                editor.DrawAnnotations();
            }
        }

        public void Reformat(bool onlySelectedAreas) =>
            new Reformatter(this.scintilla1, Environments.Application.FormatBlueprints).Reformat(
                onlySelectedAreas,
                this.GetReformatterFileType()
            );

        public int GetLineNumberFromPosition(int pos) => this.scintilla1.LineFromPosition(pos);

        public void FocusTextEditor()
        {
            this.GetActiveScintillaEditor().Focus();
        }

        public void AddErrors(IEnumerable<ErrorItem> errorItems)
        {
            foreach (ErrorItem item in errorItems)
            {
                scintilla1.IndicatorCurrent = (int)EIndicatorIndices.Error;
                scintilla1.IndicatorFillRange(item.Position, item.Length);
            }
        }

        public void RemoveErrors(IEnumerable<ErrorItem> errorItems)
        {
            int textLen = this.scintilla1.TextLength;
            foreach (ErrorItem item in errorItems.Where(i => i.Position < textLen))
            {
                scintilla1.IndicatorCurrent = (int)EIndicatorIndices.Error;
                scintilla1.IndicatorClearRange(item.Position, item.Length);
            }
        }

        protected override void SetNotTranslatableThings()
        {
            this.NotTranslatableControlsRecursiveList.Add(this.splitCon); // don't try to translate the scintilla controls
        }

        protected override void OnShown(EventArgs e)
        {
            base.OnShown(e);

            this.btnSplit.Location = this.GetDefaultPositionSplitButton();
        }

        private void UpdateSaveState()
        {
            this.SetTabNameFromSaveState();
            this.SaveStateChanged?.Invoke(this, EventArgs.Empty);
        }

        private void SetIsReachedSavePoint(bool value)
        {
            if (this._currentState.IsReachedSavePoint == value)
                return;

            this._currentState.IsReachedSavePoint = value;

            this.UpdateSaveState();
        }

        private void DragDropOnTextEditorControls(object? sender, DragEventArgs e) =>
            this.OnDragDrop(e);

        private void DragEnterOnTextEditorControls(object? sender, DragEventArgs e) =>
            this.OnDragEnter(e);

        private void DragOverOnTextEditorControls(object? sender, DragEventArgs e) =>
            this.OnDragOver(e);

        #region Default Editor Stuff (Undo, Redo, etc..)

        public int GetTextLength() => this._lastTextLength;

        public int GetLinesCount() => this._lastTextLines;

        public int GetPosition() => this._lastTextPos;

        public int GetLengthOfSelectedCharacters() => this._lastSelectedChars;

        public int GetLengthOfSelectedLines() => this._lastSelectedLines;

        public int GetCurrentLine() => this.GetActiveScintillaEditor().CurrentLine;

        public int GetCurrentFrame()
        {
            if (this._metaFrame == null)
                return -1;

            Frame frame = new Frame();
            LF2DataDeserializer.ParseObjectFromAttributes(
                frame,
                new StringBuilder(
                    this.scintilla1.GetTextRange(this._metaFrame.Offset, this._metaFrame.Length)
                )
            );

            return frame.ID;
        }

        public bool CanUndo() => this.GetActiveScintillaEditor().CanUndo;

        public bool CanRedo() => this.GetActiveScintillaEditor().CanRedo;

        public void Undo() => this.GetActiveScintillaEditor().Undo();

        public void Redo() => this.GetActiveScintillaEditor().Redo();

        public void Cut() => this.GetActiveScintillaEditor().Cut();

        public void Copy()
        {
            Frame? frame = null;
            if (this._metaFrame != null)
            {
                frame = new Frame();
                LF2DataDeserializer.ParseObjectFromAttributes(
                    frame,
                    new StringBuilder(
                        this.scintilla1.GetTextRange(this._metaFrame.Offset, this._metaFrame.Length)
                    )
                );
            }

            this.GetActiveScintillaEditor().Copy();
            if (frame != null)
                SetClipboard(frame, Clipboard.GetText());
        }

        public void Paste() => this.GetActiveScintillaEditor().Paste();

        public void PasteXY()
        {
            IDataObject? dataObj = Clipboard.GetDataObject();
            if (dataObj == null || !dataObj.GetDataPresent("frame") || this._metaFrame == null)
            {
                SystemSounds.Beep.Play();
                return;
            }

            Frame? clipboardFrame = (Frame?)dataObj.GetData("frame");
            Frame frame = new Frame();
            LF2DataDeserializer.ParseObjectFromAttributes(
                frame,
                new StringBuilder(
                    this.scintilla1.GetTextRange(this._metaFrame.Offset, this._metaFrame.Length)
                )
            );

            if (clipboardFrame == null)
            {
                SystemSounds.Beep.Play();
                return;
            }

            string clipboardText = Clipboard.GetText();
            string adjustedCode = Easier_Data_Editor.Tools.AdjusterXY.GetAdjustedXYValues(
                clipboardText,
                frame,
                clipboardFrame
            );

            Clipboard.SetText(adjustedCode);
            GetActiveScintillaEditor().Paste();
            SetClipboard(clipboardFrame, clipboardText);
        }

        public void PasteAdaptIDs()
        {
            if (!Clipboard.ContainsText())
            {
                SystemSounds.Beep.Play();
                return;
            }

            try
            {
                Scintilla scin = this.GetActiveScintillaEditor();
                string text = Clipboard.GetText();
                string dataObj = new Easier_Data_Editor.Tools.AdjusterIDs(
                    scin,
                    text
                ).GetAdjustedIDs();
                if (!string.IsNullOrEmpty(dataObj))
                {
                    Clipboard.SetText(dataObj);
                    scin.Paste();
                    Clipboard.SetText(text); // set the old clipboard text
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
        }

        public void Delete()
        {
            Scintilla activeScintilla = this.GetActiveScintillaEditor();
            activeScintilla.DeleteRange(
                activeScintilla.SelectionStart,
                activeScintilla.SelectionEnd - activeScintilla.SelectionStart
            );
        }

        public bool DeleteKey() => this.GetActiveScintillaEditor().DeleteKey();

        public void SelectAll() => this.GetActiveScintillaEditor().SelectAll();

        public void ExpandFoldingAll() =>
            this.GetActiveScintillaEditor().FoldAll(FoldAction.Expand);

        public void CollapseFoldingAll() =>
            this.GetActiveScintillaEditor().FoldAll(FoldAction.Contract);

        public string ExportToHTML() =>
            this.GetActiveScintillaEditor()
                .GetTextRangeAsHtml(0, this.GetActiveScintillaEditor().TextLength);

        public void GoToLine(int lineIndex) => this.GetActiveScintillaEditor().GoToLine(lineIndex);

        public bool GoToFrame(int frameID) => this.GetActiveScintillaEditor().GoToFrame(frameID);

        public void GoToPosition(int position) =>
            this.GetActiveScintillaEditor().GotoPosition(position);

        public void SetFirstVisibleLine(int lineIndex)
        {
            if (lineIndex >= 0 && lineIndex < this.scintilla1.Lines.Count)
                this.scintilla1.FirstVisibleLine = lineIndex;
        }

        public int GetCurrentPosition() => this.GetActiveScintillaEditor().CurrentPosition;

        public void ToggleBookmark() =>
            Scintilla.ToggleBookmark(
                this.scintilla1.Lines[this.GetActiveScintillaEditor().CurrentLine]
            );

        public void GoToNextBookmark() => this.GetActiveScintillaEditor().GoToNextBookmark();

        public void GoToPreviousBookmark() =>
            this.GetActiveScintillaEditor().GoToPreviousBookmark();

        public void ClearBookmarks() =>
            this.scintilla1.MarkerDeleteAll((int)EMarkerIndices.Bookmark);

        public void CutBookmarkedLines() => this.scintilla1.CutBookmarkedLines();

        public void CopyBookmarkedLines() => this.scintilla1.CopyBookmarkedLines();

        public void PasteToBookmarkedLines()
        {
            if (Clipboard.ContainsText())
                this.GetActiveScintillaEditor().PasteToBookmarkedLines(Clipboard.GetText());
        }

        public void RemoveBookmarkedLines() => this.scintilla1.RemoveBookmarkedLines();

        public void RemoveUnmarkedLines() => this.scintilla1.RemoveUnmarkedLines();

        public void InvertBookmarks() => this.scintilla1.InvertBookmarks();

        public void ReloadStyle()
        {
            foreach (Scintilla scin in this.GetAllEditors())
                scin.ReloadStyle();
        }

        public void IncreaseOrDecreaseSelectedNumber(bool increase)
        {
            Scintilla scin = this.GetActiveScintillaEditor();
            scin.BeginUndoAction();
            foreach (Selection sel in scin.Selections.Where(s => s.End - s.Start > 0))
            {
                string selText = scin.GetTextRange(sel.Start, sel.End - sel.Start);
                int selStart = sel.Start;
                MatchCollection matches = REGEX_PATTERN_NUMBER_FLOAT.Matches(selText);
                for (int i = matches.Count - 1; i >= 0; --i)
                {
                    Match match = matches[i];
                    int dotPos = match.Value.IndexOf('.');
                    string val = dotPos > 0 ? match.Value.Substring(0, dotPos) : match.Value;
                    if (!int.TryParse(val, out int inputNumber))
                        continue;

                    int oldLen = inputNumber.ToString().Length;

                    if (increase)
                        ++inputNumber;
                    else
                        --inputNumber;

                    int diffLen = inputNumber.ToString().Length - oldLen;
                    int newEnd = sel.End + diffLen;

                    scin.TargetStart = sel.Start + match.Index;
                    scin.TargetEnd = scin.TargetStart + val.Length;
                    scin.ReplaceTarget(inputNumber.ToString());
                    // replace reset selection pos (but it doesn't delete the selection...)
                    // so re-set the values
                    sel.Start = selStart;
                    sel.End = newEnd;
                }
            }
            scin.EndUndoAction();
        }

        public string GetContentToSave()
        {
            if (Environments.Application.UserSettings.TextEditorSettings.ReformattingOnSave)
                new Reformatter(
                    this.scintilla1,
                    Environments.Application.FormatBlueprints
                ).Reformat(false, this.GetReformatterFileType());

            return this.scintilla1.Text;
        }
        #endregion

        #region Events
        private void TsmiCut_Click(object sender, EventArgs e) => this.Cut();

        private void TsmiCopy_Click(object sender, EventArgs e) => this.Copy();

        private void TsmiPaste_Click(object sender, EventArgs e) => this.Paste();

        private void TsmiPasteAdaptIDs_Click(object sender, EventArgs e) => this.PasteAdaptIDs();

        private void TsmiPasteXY_Click(object sender, EventArgs e) => this.PasteXY();

        private void TsmiDelete_Click(object sender, EventArgs e) => this.Delete();

        private void TsmiSelectAll_Click(object sender, EventArgs e) => this.SelectAll();

        private void TsmiFrameViewer_Click(object sender, EventArgs e) =>
            throw new NotImplementedException();
        #endregion

        private LF2Scintilla GetActiveScintillaEditor()
        {
            if (
                !this.splitCon.Panel2Collapsed
                && this.splitCon.ActiveControl is LF2Scintilla scintilla2
            )
                return scintilla2;

            return this.scintilla1;
        }

        private List<LF2Scintilla> GetAllEditors()
        {
            List<LF2Scintilla> editors = new List<LF2Scintilla>() { this.scintilla1 };
            if (!this.splitCon.Panel2Collapsed)
                editors.AddRange(
                    this.splitCon.Panel2.Controls.Cast<Control>()
                        .Where(c => c is LF2Scintilla)
                        .Cast<LF2Scintilla>()
                );

            return editors;
        }

        private void Scintilla_UpdateUI(object? sender, UpdateUIEventArgs e)
        {
            if (!(sender is Scintilla scin) || scin.IsSetText)
                return;

            this.NotifySubscribers(scin);

            if (
                this._metaFrame != null
                && scin.CurrentPosition >= this._metaFrame.Offset
                && scin.CurrentPosition < this._metaFrame.Offset + this._metaFrame.Length
            )
                return;

            this._metaFrame = SetMetaFrame(scin);
        }

        private void NotifySubscribers(Scintilla scin)
        {
            if (this._lastTextLength != scin.TextLength)
            {
                this._lastTextLength = scin.TextLength;
                this.TextLengthChanged?.Invoke(this, EventArgs.Empty);
            }

            if (this._lastTextLines != scin.Lines.Count)
            {
                this._lastTextLines = scin.Lines.Count;
                this.LinesCountChanged?.Invoke(this, EventArgs.Empty);
            }

            if (this._lastTextPos != scin.CurrentPosition)
            {
                this._lastTextPos = scin.CurrentPosition;
                this.TextPositionChanged?.Invoke(this, EventArgs.Empty);
            }

            this.NotifySubscribersSelections(scin);
        }

        private void NotifySubscribersSelections(Scintilla scin)
        {
            int selectionChars = 0;
            int selectionLines = 0;
            foreach (Selection sel in scin.Selections)
            {
                selectionChars += sel.End - sel.Start;
                if (selectionChars == 0)
                    continue;

                string selText = scin.GetTextRange(sel.Start, sel.End - sel.Start);
                selectionLines += REGEX_PATTERN_NEW_LINE.Split(selText).Length;
            }

            if (
                this._lastSelectedChars != selectionChars
                || this._lastSelectedLines != selectionLines
            )
            {
                this._lastSelectedChars = selectionChars;
                this._lastSelectedLines = selectionLines;
                this.SelectionChanged?.Invoke(this, EventArgs.Empty);
            }
        }

        private MetaInfoFrame? SetMetaFrame() => SetMetaFrame(this.GetActiveScintillaEditor());

        private void SetTabNameFromSaveState()
        {
            if (!this.IsSave)
            {
                Text = $"{this._fileName}*";
                Icon = Resources.icon_red;
            }
            else
            {
                Text = this._fileName;
                Icon = Resources.icon;
            }
        }

        private EFileType GetReformatterFileType()
        {
            string text = this.scintilla1.Text.ToLower();
            if (IsDataTxt(text))
                return EFileType.DataTxt;
            else if (IsStage(text))
                return EFileType.Stage;
            else if (IsBackground(text))
                return EFileType.Background;

            return EFileType.NormalData;
        }
    }
}
