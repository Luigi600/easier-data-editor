﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Easier_Data_Editor.Extensions.Scintilla;
using Easier_Data_Editor.Forms.TextEditors.Models;
using Easier_Data_Editor.Properties;

namespace Easier_Data_Editor.Forms.TextEditors
{
    partial class TextEditor
    {
        private const int SPLITTER_MINIUM_HEIGHT = 50;
        private const int SPLITTER_BUTTON_PADDING_RIGHT = 16;
        private const int SPLITTER_BUTTON_DEFAULT_POSITION_Y = -1;
        private const int SPLITTER_CLOSING_TOLERANCE = 12;
        private int _splitAnchor = 0;

        public SplitMode SplitMode
        {
            get
            {
                if (this.splitCon.Panel2Collapsed)
                    return SplitMode.Clear;

                return this.splitCon.Orientation == Orientation.Horizontal
                    ? SplitMode.Horizontal
                    : SplitMode.Vertical;
            }
        }

        public void Split(SplitMode split, int splitDis = -1, bool setStyleFromSecondEditor = false)
        {
            // just change the orientation
            if (split != SplitMode.Clear)
            {
                this.splitCon.Orientation =
                    split == SplitMode.Horizontal ? Orientation.Horizontal : Orientation.Vertical;
                this.splitCon.SplitterDistance = this.GetPerfectSplitterDistance(splitDis);
                this.btnSplit.Image =
                    this.splitCon.Orientation == Orientation.Horizontal
                        ? Resources.icon_split_horizontal
                        : Resources.icon_split_vertical;

                if (!this.splitCon.Panel2Collapsed)
                    return;
            }

            if (split == SplitMode.Clear)
                this.ClearSecondEditor(setStyleFromSecondEditor);
            else
                this.CreateSecondEditor();

            this.splitCon.Panel2Collapsed = split == SplitMode.Clear;
            this.btnSplit.Location = this.GetDefaultPositionSplitButton();
            this.btnSplit.Visible = this.splitCon.Panel2Collapsed;
        }

        private Point GetDefaultPositionSplitButton() =>
            new Point(
                Width - this.btnSplit.Width - SPLITTER_BUTTON_PADDING_RIGHT,
                SPLITTER_BUTTON_DEFAULT_POSITION_Y
            );

        private void BtnSplit_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Left)
                return;

            Point minPos = this.GetDefaultPositionSplitButton();

            if (this.splitCon.Orientation == Orientation.Horizontal)
            {
                int newY = Math.Max(
                    minPos.Y,
                    Math.Min(
                        this.Height - this.btnSplit.Height,
                        PointToClient(MousePosition).Y - this._splitAnchor
                    )
                );

                this.btnSplit.Location = new Point(this.btnSplit.Location.X, newY);
            }
            else
            {
                int newX = Math.Max(
                    0,
                    Math.Min(minPos.X, PointToClient(MousePosition).X - this._splitAnchor)
                );

                this.btnSplit.Location = new Point(newX, this.btnSplit.Location.Y);
            }
        }

        private void BtnSplit_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Left)
                return;

            if (this.splitCon.Orientation == Orientation.Horizontal)
                this._splitAnchor = e.Y;
            else
                this._splitAnchor = e.X;
        }

        private void BtnSplit_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Left)
                return;

            Point defaultPos = this.GetDefaultPositionSplitButton();

            foreach (LF2Scintilla scin in this.GetAllEditors())
                scin.Enabled = false;

            if (
                this.splitCon.Orientation == Orientation.Horizontal
                && this.btnSplit.Location.Y > SPLITTER_CLOSING_TOLERANCE
            )
                this.Split(SplitMode.Horizontal, this.btnSplit.Location.Y);
            else if (
                this.splitCon.Orientation == Orientation.Vertical
                && (defaultPos.X - this.btnSplit.Location.X) > SPLITTER_CLOSING_TOLERANCE
            )
                this.Split(SplitMode.Vertical, this.btnSplit.Location.X);
            else
                this.btnSplit.Location = this.GetDefaultPositionSplitButton();

            foreach (LF2Scintilla scin in this.GetAllEditors())
                scin.Enabled = true;
        }

        private void SplitCon_SplitterMoved(object sender, SplitterEventArgs e)
        {
            if (this.splitCon.Panel2Collapsed)
                return;

            if (this.splitCon.SplitterDistance < SPLITTER_MINIUM_HEIGHT)
                Split(SplitMode.Clear, -1, true);
            else if (
                (
                    this.splitCon.Orientation == Orientation.Vertical
                    && this.splitCon.SplitterDistance
                        >= this.splitCon.Width - SPLITTER_MINIUM_HEIGHT + SPLITTER_CLOSING_TOLERANCE
                )
                || (
                    this.splitCon.Orientation == Orientation.Horizontal
                    && this.splitCon.SplitterDistance
                        >= this.splitCon.Height
                            - SPLITTER_MINIUM_HEIGHT
                            + SPLITTER_CLOSING_TOLERANCE
                )
            )
                Split(SplitMode.Clear, -1, false);

            foreach (LF2Scintilla scin in this.GetAllEditors())
                scin.Enabled = true;
        }

        private int GetPerfectSplitterDistance(int splitDis)
        {
            if (splitDis < 0)
            {
                if (this.splitCon.Orientation == Orientation.Horizontal)
                    splitDis = this.splitCon.Height / 2;
                else
                    splitDis = this.splitCon.Width / 2;
            }
            else if (splitDis < SPLITTER_MINIUM_HEIGHT)
            {
                splitDis = SPLITTER_MINIUM_HEIGHT;
            }

            return splitDis;
        }

        private void ClearSecondEditor(bool setStyleFromSecondEditor)
        {
            foreach (Scintilla scin in this.splitCon.Panel2.Controls.OfType<Scintilla>())
            {
                scin.UpdateUI -= this.Scintilla_UpdateUI;

                if (setStyleFromSecondEditor)
                {
                    this.scintilla1.CurrentPosition = scin.CurrentPosition;
                    this.scintilla1.FirstVisibleLine = scin.FirstVisibleLine;
                    this.scintilla1.ApplySelections(scin.Selections);
                }

                setStyleFromSecondEditor = false;
            }

            this.splitCon.Panel2.Controls.Clear();
        }

        private void CreateSecondEditor()
        {
            if (this.splitCon.Panel2.Controls.Count != 0)
                return;

            Scintilla scin = new LF2Scintilla()
            {
                Dock = DockStyle.Fill,
                BorderStyle = ScintillaNET.BorderStyle.None,
                Document = this.scintilla1.Document,
            };
            scin.UpdateUI += this.Scintilla_UpdateUI;
            scin.ContextMenuStrip = this.cmsActions;
            scin.AllowDrop = true;
            scin.DragDrop += this.DragDropOnTextEditorControls;
            scin.DragEnter += this.DragEnterOnTextEditorControls;
            scin.DragOver += this.DragOverOnTextEditorControls;
            this.splitCon.Panel2.Controls.Add(scin);

            scin.CurrentPosition = this.scintilla1.CurrentPosition;
            scin.FirstVisibleLine = this.scintilla1.FirstVisibleLine;
            scin.ApplySelections(this.scintilla1.Selections);
        }
    }
}
