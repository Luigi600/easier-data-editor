﻿using Easier_Data_Editor.Models;

namespace Easier_Data_Editor.Forms.TextEditors.Models
{
    public class SaveState : IDataEqualation
    {
        public TextEncoding Encoding { get; }
        public bool EncryptedDocument { get; set; }
        public bool IsReachedSavePoint { get; set; }

        public SaveState(TextEncoding encoding, bool encryptedDocument, bool isReachedSavePoint)
        {
            this.Encoding = encoding;
            this.EncryptedDocument = encryptedDocument;
            this.IsReachedSavePoint = isReachedSavePoint;
        }

        public bool DataAreEquals(object obj)
        {
            if (!(obj is SaveState saveState))
                return false;

            return saveState.EncryptedDocument == this.EncryptedDocument
                && saveState.IsReachedSavePoint == this.IsReachedSavePoint
                && saveState.Encoding.DataAreEquals(this.Encoding);
        }
    }
}
