﻿namespace Easier_Data_Editor.Forms.TextEditors.Models
{
    public enum SplitMode
    {
        Clear,
        Horizontal,
        Vertical,
    }
}
