﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Easier_Data_Editor.Forms
{
    public class FakeButton : Panel
    {
        private Bitmap? _imageDefault;
        private Bitmap? _imageDisabled;
        private Bitmap? _imageHover;

        private bool _isHover = false;

        public Bitmap? ImageDefault
        {
            get => this._imageDefault;
            set
            {
                this._imageDefault = value;
                this.SetButtonImage();
            }
        }

        public Bitmap? ImageDisabled
        {
            get => this._imageDisabled;
            set
            {
                this._imageDisabled = value;
                this.SetButtonImage();
            }
        }

        public Bitmap? ImageHover
        {
            get => this._imageHover;
            set
            {
                this._imageHover = value;
                this.SetButtonImage();
            }
        }

        public FakeButton()
        {
            this.BackgroundImageLayout = ImageLayout.Stretch;
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            this._isHover = true;
            SetButtonImage();
            base.OnMouseMove(e);
        }

        protected override void OnMouseLeave(EventArgs e)
        {
            this._isHover = false;
            SetButtonImage();
            base.OnMouseLeave(e);
        }

        protected override void OnEnabledChanged(EventArgs e)
        {
            SetButtonImage();
            base.OnEnabledChanged(e);
        }

        private void SetButtonImage()
        {
            if (!Enabled)
                this.BackgroundImage = this._imageDisabled;
            else if (this._isHover)
                this.BackgroundImage = this._imageHover;
            else
                this.BackgroundImage = this._imageDefault;
        }
    }
}
