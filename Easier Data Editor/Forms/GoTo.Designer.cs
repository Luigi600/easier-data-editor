﻿
namespace Easier_Data_Editor.Forms
{
    partial class GoTo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GoTo));
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnGoTo = new System.Windows.Forms.Button();
            this.nudVal = new System.Windows.Forms.NumericUpDown();
            this.lblMaximumValue = new System.Windows.Forms.Label();
            this.lblCurrentValue = new System.Windows.Forms.Label();
            this.lblMaximum = new System.Windows.Forms.Label();
            this.lblCurrent = new System.Windows.Forms.Label();
            this.lblTarget = new System.Windows.Forms.Label();
            this.rbFrame = new System.Windows.Forms.RadioButton();
            this.rbLine = new System.Windows.Forms.RadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.nudVal)).BeginInit();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCancel.Location = new System.Drawing.Point(27, 154);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(100, 23);
            this.btnCancel.TabIndex = 15;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // btnGoTo
            // 
            this.btnGoTo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGoTo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGoTo.Location = new System.Drawing.Point(287, 154);
            this.btnGoTo.Name = "btnGoTo";
            this.btnGoTo.Size = new System.Drawing.Size(100, 23);
            this.btnGoTo.TabIndex = 14;
            this.btnGoTo.Text = "Go To";
            this.btnGoTo.UseVisualStyleBackColor = true;
            this.btnGoTo.Click += new System.EventHandler(this.BtnGoTo_Click);
            // 
            // nudVal
            // 
            this.nudVal.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.nudVal.Location = new System.Drawing.Point(145, 85);
            this.nudVal.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.nudVal.Name = "nudVal";
            this.nudVal.Size = new System.Drawing.Size(242, 20);
            this.nudVal.TabIndex = 11;
            this.nudVal.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudVal.KeyDown += new System.Windows.Forms.KeyEventHandler(this.NudVal_KeyDown);
            // 
            // lblMaximumValue
            // 
            this.lblMaximumValue.AutoSize = true;
            this.lblMaximumValue.Location = new System.Drawing.Point(145, 113);
            this.lblMaximumValue.Name = "lblMaximumValue";
            this.lblMaximumValue.Size = new System.Drawing.Size(13, 13);
            this.lblMaximumValue.TabIndex = 6;
            this.lblMaximumValue.Text = "0";
            // 
            // lblCurrentValue
            // 
            this.lblCurrentValue.AutoSize = true;
            this.lblCurrentValue.Location = new System.Drawing.Point(145, 64);
            this.lblCurrentValue.Name = "lblCurrentValue";
            this.lblCurrentValue.Size = new System.Drawing.Size(13, 13);
            this.lblCurrentValue.TabIndex = 7;
            this.lblCurrentValue.Text = "0";
            // 
            // lblMaximum
            // 
            this.lblMaximum.AutoSize = true;
            this.lblMaximum.Location = new System.Drawing.Point(27, 113);
            this.lblMaximum.Name = "lblMaximum";
            this.lblMaximum.Size = new System.Drawing.Size(54, 13);
            this.lblMaximum.TabIndex = 8;
            this.lblMaximum.Text = "Maximum:";
            // 
            // lblCurrent
            // 
            this.lblCurrent.AutoSize = true;
            this.lblCurrent.Location = new System.Drawing.Point(27, 64);
            this.lblCurrent.Name = "lblCurrent";
            this.lblCurrent.Size = new System.Drawing.Size(44, 13);
            this.lblCurrent.TabIndex = 9;
            this.lblCurrent.Text = "Current:";
            // 
            // lblTarget
            // 
            this.lblTarget.AutoSize = true;
            this.lblTarget.Location = new System.Drawing.Point(27, 87);
            this.lblTarget.Name = "lblTarget";
            this.lblTarget.Size = new System.Drawing.Size(41, 13);
            this.lblTarget.TabIndex = 10;
            this.lblTarget.Text = "Target:";
            // 
            // rbFrame
            // 
            this.rbFrame.AutoSize = true;
            this.rbFrame.Checked = true;
            this.rbFrame.Location = new System.Drawing.Point(145, 27);
            this.rbFrame.Name = "rbFrame";
            this.rbFrame.Size = new System.Drawing.Size(54, 17);
            this.rbFrame.TabIndex = 13;
            this.rbFrame.TabStop = true;
            this.rbFrame.Text = "Frame";
            this.rbFrame.UseVisualStyleBackColor = true;
            // 
            // rbLine
            // 
            this.rbLine.AutoSize = true;
            this.rbLine.Location = new System.Drawing.Point(27, 27);
            this.rbLine.Name = "rbLine";
            this.rbLine.Size = new System.Drawing.Size(45, 17);
            this.rbLine.TabIndex = 12;
            this.rbLine.Text = "Line";
            this.rbLine.UseVisualStyleBackColor = true;
            // 
            // GoTo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(414, 196);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnGoTo);
            this.Controls.Add(this.nudVal);
            this.Controls.Add(this.lblMaximumValue);
            this.Controls.Add(this.lblCurrentValue);
            this.Controls.Add(this.lblMaximum);
            this.Controls.Add(this.lblCurrent);
            this.Controls.Add(this.lblTarget);
            this.Controls.Add(this.rbFrame);
            this.Controls.Add(this.rbLine);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "GoTo";
            this.Padding = new System.Windows.Forms.Padding(24, 24, 24, 16);
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Go To...";
            ((System.ComponentModel.ISupportInitialize)(this.nudVal)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Button btnCancel;
        internal System.Windows.Forms.Button btnGoTo;
        internal System.Windows.Forms.NumericUpDown nudVal;
        internal System.Windows.Forms.Label lblMaximumValue;
        internal System.Windows.Forms.Label lblCurrentValue;
        internal System.Windows.Forms.Label lblMaximum;
        internal System.Windows.Forms.Label lblCurrent;
        internal System.Windows.Forms.Label lblTarget;
        internal System.Windows.Forms.RadioButton rbFrame;
        internal System.Windows.Forms.RadioButton rbLine;
    }
}