﻿using System.ComponentModel;
using System.Windows.Forms;

namespace Easier_Data_Editor.Forms
{
    public class BindableToolStripMenuItem : ToolStripMenuItem, IBindableComponent
    {
        private readonly ControlBindingsCollection _dataBindings;

#pragma warning disable CS0108 // Member hides inherited member; missing new keyword (I just want set designer attributes)
        [Browsable(false)]
        public BindingContext? BindingContext { get; set; } = null;

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public ControlBindingsCollection DataBindings => _dataBindings;
#pragma warning restore CS0108 // Member hides inherited member; missing new keyword

        public BindableToolStripMenuItem()
        {
            this._dataBindings = new ControlBindingsCollection(this);
        }
    }
}
