﻿using System;
using System.ComponentModel;
using System.Media;
using System.Windows.Forms;
using Easier_Data_Editor.Forms.TextEditors;
using Easier_Data_Editor.Windows;

namespace Easier_Data_Editor.Forms
{
    public partial class GoTo : EscapeCloseableWindow
    {
        private TextEditor? _editor;

        public GoTo(IMainEditor main, TextEditor? editor, bool lineSelected)
            : this()
        {
            this.SetMode(lineSelected);

            this.rbLine.CheckedChanged += this.RatioButtons_CheckedChanged;
            this.rbFrame.CheckedChanged += this.RatioButtons_CheckedChanged;

            main.TextEditorChanged += (sender, e) => SetTextEditor(e);
            this.SetTextEditor(editor);
        }

        // for translatable lib
        private GoTo()
        {
            InitializeComponent();
        }

        public void SetMode(bool isLineOptionSelected)
        {
            this.rbLine.Checked = isLineOptionSelected;
            this.rbFrame.Checked = !isLineOptionSelected;

            this.nudVal.Select(0, this.nudVal.Value.ToString().Length);
        }

        protected override void SetNotTranslatableThings()
        {
            this.NotTranslatableControlsList.AddRange(
                new Component[] { this.lblCurrentValue, this.lblMaximumValue }
            );
        }

        protected override void OnActivated(EventArgs e)
        {
            base.OnActivated(e);

            this.SetStatsValue();
        }

        private void SetTextEditor(TextEditor? editor)
        {
            this._editor = editor;
            this.SetStatsValue();
        }

        private void SetStatsValue()
        {
            if (this._editor == null)
                return;

            if (this.rbLine.Checked)
            {
                this.lblCurrentValue.Text = (this._editor.GetCurrentLine() + 1).ToString(); // +1 coz lines are zero-based
                this.lblMaximumValue.Text = this._editor.GetLinesCount().ToString();
            }
            else
            {
                this.lblCurrentValue.Text = this._editor.GetCurrentFrame().ToString();
                this.lblMaximumValue.Text = LittleFighter2.Constants.MAXIMUM_FRAMES.ToString();
            }
        }

        private void NudVal_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode != Keys.Enter)
                return;

            e.Handled = true;
            e.SuppressKeyPress = true;
            this.btnGoTo.PerformClick();
        }

        private void RatioButtons_CheckedChanged(object? sender, EventArgs e)
        {
            if (this.rbLine.Checked)
            {
                if (this.nudVal.Value < 1)
                    this.nudVal.Value = 1;

                this.nudVal.Minimum = 1;
            }
            else
            {
                this.nudVal.Minimum = 0;
            }

            this.SetStatsValue();
        }

        private void BtnCancel_Click(object sender, EventArgs e) => this.Close();

        private void BtnGoTo_Click(object sender, EventArgs e)
        {
            if (this._editor == null)
                return;

            if (this.rbLine.Checked)
            {
                this._editor.GoToLine((int)this.nudVal.Value - 1); // -1 coz lines are zero-based
            }
            else if (!this._editor.GoToFrame((int)this.nudVal.Value))
            {
                SystemSounds.Exclamation.Play();
                return;
            }

            this.Close();
        }
    }
}
