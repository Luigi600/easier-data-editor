﻿using System;

namespace Easier_Data_Editor.Forms
{
    public interface IEditor
    {
        int ID { get; }
        string Path { get; set; }
        event EventHandler<bool>? HasFocusChanged;
        void SetHasFocus(bool focus, bool raiseEvent = true);
    }
}
