﻿using System;
using System.Runtime.InteropServices;

namespace Easier_Data_Editor.Natives.Communications
{
    public static class Instance
    {
        public const uint WM_SETTEXT = 0xC;
        public const uint SW_RESTORE = 0x9;

        [DllImport("user32.dll", SetLastError = true)]
        private static extern IntPtr SendMessage(
            IntPtr hWnd,
            uint msg,
            IntPtr wParam,
            string lParam
        );

        [DllImport("user32.dll", EntryPoint = "ShowWindow")]
        private static extern int ShowWindowAndSendMessage(IntPtr hWnd, uint msg);

        [DllImport("user32.dll", EntryPoint = "SetForegroundWindow")]
        private static extern bool SetHandleToForeground(IntPtr hWnd);

        public static IntPtr SendMessageToProcess(IntPtr hWnd, string message)
        {
            if (message == null)
                throw new ArgumentException("Message is null!");

            if (hWnd == IntPtr.Zero)
                throw new ArgumentException("Invalid handle!");

            return SendMessage(hWnd, WM_SETTEXT, IntPtr.Zero, message);
        }

        public static int ShowWindow(IntPtr hWnd, uint message)
        {
            if (hWnd == IntPtr.Zero)
                throw new ArgumentException("Invalid handle!");

            return ShowWindowAndSendMessage(hWnd, message);
        }

        public static void SetForegroundWindow(IntPtr hWnd)
        {
            if (hWnd == IntPtr.Zero)
                throw new ArgumentException("Invalid handle!");

            SetHandleToForeground(hWnd);
        }
    }
}
