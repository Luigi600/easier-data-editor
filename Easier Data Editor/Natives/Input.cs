﻿using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace Easier_Data_Editor.Natives
{
    public static class KeyUtil
    {
        [DllImport("User32.dll")]
        private static extern short GetKeyState(int vKey);

        public static bool IsKeyPressed(Keys key) =>
            BitConverter.GetBytes(GetKeyState((int)key))[0] > 0;
    }
}
