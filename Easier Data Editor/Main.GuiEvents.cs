﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Media;
using System.Windows.Forms;
using Easier_Data_Editor.Forms;
using Easier_Data_Editor.Forms.ErrorDetections;
using Easier_Data_Editor.Forms.TextEditors;
using Easier_Data_Editor.Forms.TextEditors.Models;
using Easier_Data_Editor.Forms.Tools;
using Easier_Data_Editor.Models;
using Easier_Data_Editor.Utilities;
using Easier_Data_Editor.Windows;
using Easier_Data_Editor.Windows.Settings;
using WeifenLuo.WinFormsUI.Docking;

namespace Easier_Data_Editor
{
    partial class Main
    {
        private GoTo? _gotoWindow = null;
        private ErrorList? _errorList = null;

        #region New

        private void TsmiFile_DropDownOpening(object sender, EventArgs e)
        {
            this.tsmiRecentFiles.Enabled =
                Environments.Application.UserSettings.GeneralSettings.RecentFiles.Count > 0;

            this.tsmiRecentFiles.DropDownItems.Clear();
            Console.WriteLine(Environments.Application.UserSettings.GeneralSettings.RecentFiles);
            foreach (
                string recentFile in Environments
                    .Application
                    .UserSettings
                    .GeneralSettings
                    .RecentFiles
            )
                this.tsmiRecentFiles.DropDownItems.Add(new ToolStripMenuItem(recentFile));

            foreach (ToolStripItem item in this.tsmiRecentFiles.DropDownItems)
                item.Click += (_, __) => OpenFile(item.Text ?? "");
        }

        private void TsmiSave_EnabledChanged(object sender, EventArgs e) =>
            this.tsmiIconSave.Enabled = ((ToolStripMenuItem)sender).Enabled;

        private void TsmiOpen_Click(object sender, EventArgs e)
        {
            if (this.ofdData.ShowDialog() != DialogResult.OK)
                return;

            foreach (string file in this.ofdData.FileNames)
                this.OpenFile(file);
        }

        private void TsmiSave_Click(object sender, EventArgs e)
        {
            if (this._currentTextEditor == null)
                return;

            this.SaveDataFile(this._currentTextEditor, this._currentTextEditor.Path);
        }

        private void TsmiSaveAll_Click(object sender, EventArgs e)
        {
            foreach (TextEditor editor in this.dockPalMain.Contents.OfType<TextEditor>())
                this.SaveDataFile(editor, editor.Path);
        }

        private void TsmiSaveAs_Click(object sender, EventArgs e)
        {
            if (this._currentTextEditor == null)
            {
                SystemSounds.Beep.Play();
                return;
            }

            if (this.sfdSaveAsData.ShowDialog() != DialogResult.OK)
                return;

            this.SaveDataFile(this._currentTextEditor, this.sfdSaveAsData.FileName);
            Environments.Application.UserSettings.GeneralSettings.RecentFiles.Add(
                this.sfdSaveAsData.FileName
            );
        }

        private void TsmiCloseAllFiles_Click(object sender, EventArgs e)
        {
            int index = 0;
            while (index < this.dockPalMain.Contents.Count)
            {
                IDockContent iContent = this.dockPalMain.Contents[index];
                if (!(iContent is IEditor editor) || !(iContent is DockContent content))
                {
                    ++index;
                    continue;
                }

                // TODO: save
                Debug.WriteLine(editor);

                content.HideOnClose = false;
                content.Close();
            }
        }

        private void TsmiExportHTML_Click(object sender, EventArgs e)
        {
            if (this._currentTextEditor == null)
            {
                SystemSounds.Beep.Play();
                return;
            }

            if (this.sfdHTML.ShowDialog() != DialogResult.OK)
                return;

            File.WriteAllText(this.sfdHTML.FileName, this._currentTextEditor.ExportToHTML());
            SystemSounds.Asterisk.Play();
        }

        private void TsmiQuit_Click(object sender, EventArgs e) => this.Close();
        #endregion


        #region Edit
        private void TsmiUndo_EnabledChanged(object sender, EventArgs e) =>
            this.tsmiIconUndo.Enabled = ((ToolStripMenuItem)sender).Enabled;

        private void TsmiRedo_EnabledChanged(object sender, EventArgs e) =>
            this.tsmiIconRedo.Enabled = ((ToolStripMenuItem)sender).Enabled;

        private void TsmiEdit_DropDownOpening(object sender, EventArgs e)
        {
            this.tsmiUndo.Enabled =
                this._currentTextEditor != null && this._currentTextEditor.CanUndo();
            this.tsmiRedo.Enabled =
                this._currentTextEditor != null && this._currentTextEditor.CanRedo();

            this.tsmiCollapseAllFoldings.Enabled = Environments
                .Application
                .UserSettings
                .TextEditorSettings
                .Folding;
            this.tsmiExpandAllFoldings.Enabled = Environments
                .Application
                .UserSettings
                .TextEditorSettings
                .Folding;
        }

        private void Undo_Click(object sender, EventArgs e) => this._currentTextEditor?.Undo();

        private void Redo_Click(object sender, EventArgs e) => this._currentTextEditor?.Redo();

        private void TsmiCut_Click(object sender, EventArgs e) => this._currentTextEditor?.Cut();

        private void TsmiCopy_Click(object sender, EventArgs e) => this._currentTextEditor?.Copy();

        private void TsmiPaste_Click(object sender, EventArgs e) =>
            this._currentTextEditor?.Paste();

        private void TsmiPasteIDs_Click(object sender, EventArgs e) =>
            this._currentTextEditor?.PasteAdaptIDs();

        private void TsmiPasteXY_Click(object sender, EventArgs e) =>
            this._currentTextEditor?.PasteXY();

        private void TsmiDelete_Click(object sender, EventArgs e) =>
            this._currentTextEditor?.Delete();

        private void TsmiSelectAll_Click(object sender, EventArgs e) =>
            this._currentTextEditor?.SelectAll();

        private void TsmiCloseFile_Click(object sender, EventArgs e)
        {
            if (!(this.dockPalMain.ActiveContent is DockContent dockCentent))
                return;

            if (dockCentent.HideOnClose)
                dockCentent.Hide();
            else
                dockCentent.Close();
        }

        private void TsmiExpandAllFoldings_Click(object sender, EventArgs e) =>
            this._currentTextEditor?.ExpandFoldingAll();

        private void TsmiCollapseAllFoldings_Click(object sender, EventArgs e) =>
            this._currentTextEditor?.CollapseFoldingAll();

        #endregion

        #region Search

        private void TsmiSearch_DropDownOpening(object sender, EventArgs e) =>
            this.tsmiBookmark.Enabled = Environments
                .Application
                .UserSettings
                .TextEditorSettings
                .Bookmarks;

        private void TsmiGoTo_Click(object sender, EventArgs e) => this.ShowGoToWindow(true);

        private void TsmiGoToLine_Click(object sender, EventArgs e) => this.ShowGoToWindow(false);

        private void TsmiGoToFrame_Click(object sender, EventArgs e) => this.ShowGoToWindow(true);

        private void ShowGoToWindow(bool isFrame)
        {
            if (this._gotoWindow != null)
            {
                this._gotoWindow.SetMode(!isFrame);
                this._gotoWindow.BringToFront();
            }
            else
            {
                this._gotoWindow = new GoTo(this, this.CurrentTextEditor, !isFrame);
                this._gotoWindow.FormClosed += (sender, e) => this._gotoWindow = null;
                this._gotoWindow.Show(this);
            }
        }

        #region Bookmarks

        private void TsmiToggleBookmark_Click(object sender, EventArgs e) =>
            this._currentTextEditor?.ToggleBookmark();

        private void TsmiNextBookmark_Click(object sender, EventArgs e) =>
            this._currentTextEditor?.GoToNextBookmark();

        private void TsmiPreviousBookmark_Click(object sender, EventArgs e) =>
            this._currentTextEditor?.GoToPreviousBookmark();

        private void TsmiClearBookmarks_Click(object sender, EventArgs e) =>
            this._currentTextEditor?.ClearBookmarks();

        private void TsmiCutBookmarkedLines_Click(object sender, EventArgs e) =>
            this._currentTextEditor?.CutBookmarkedLines();

        private void TsmiCopyBookmarkedLines_Click(object sender, EventArgs e) =>
            this._currentTextEditor?.CopyBookmarkedLines();

        private void TsmiPasteBookmarkedLines_Click(object sender, EventArgs e) =>
            this._currentTextEditor?.PasteToBookmarkedLines();

        private void TsmiRemoveBookmarkedLines_Click(object sender, EventArgs e) =>
            this._currentTextEditor?.RemoveBookmarkedLines();

        private void TsmiRemoveUnmarkedLines_Click(object sender, EventArgs e) =>
            this._currentTextEditor?.RemoveUnmarkedLines();

        private void TsmiInvertBookmarks_Click(object sender, EventArgs e) =>
            this._currentTextEditor?.InvertBookmarks();

        #endregion

        #endregion

        #region View

        private void TsmiView_DropDownOpening(object sender, EventArgs e)
        {
            this.tsmiHSplit.Checked =
                this._currentTextEditor != null
                && this._currentTextEditor.SplitMode == SplitMode.Horizontal;
            this.tsmiVSplit.Checked =
                this._currentTextEditor != null
                && this._currentTextEditor.SplitMode == SplitMode.Vertical;
        }

        private void TsmiHSplit_Click(object sender, EventArgs e) =>
            this._currentTextEditor?.Split(
                this.tsmiHSplit.Checked ? SplitMode.Horizontal : SplitMode.Clear
            );

        private void TsmiVSplit_Click(object sender, EventArgs e) =>
            this._currentTextEditor?.Split(
                this.tsmiVSplit.Checked ? SplitMode.Vertical : SplitMode.Clear
            );

        #endregion

        #region Windows
        private void TsmiErrorList_Click(object sender, EventArgs e)
        {
            if (this._errorList == null)
            {
                this._errorList = new ErrorList(this, this.CurrentTextEditor);
                this._errorList.Show(this.dockPalMain, DockState.Document);
            }
            else
            {
                this._errorList.Close();
                this._errorList = null;
            }
        }
        #endregion

        #region Tools

        private void TsmiReformatter_Click(object sender, EventArgs e)
        {
            new Reformatter(
                this.dockPalMain.Contents.OfType<DockContent>()
                    .Where(c => c is TextEditor)
                    .Cast<TextEditor>()
                    .ToArray()
            ).ShowDialog(this);
        }

        #endregion

        private void TsmiAbout_Click(object sender, EventArgs e) => new About().ShowDialog();

        private void TsmiSettings_Click(object sender, EventArgs e)
        {
            this._isSettingsActive = true;
            try
            {
                new Settings(
                    Environments.Application.UserSettings,
                    Environments.Application.FormatBlueprints
                ).ShowDialog();
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                throw;
            }
            finally
            {
                this._isSettingsActive = false;
            }
        }

        #region ToolStrip

        private void TsslTextEncryptionVal_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Right)
                return;

            this.cmsEncryption.Show(MousePosition);
        }

        private void TsslTextEncryptionVal_DoubleClick(object sender, EventArgs e) =>
            this.cmsEncryption.Show(MousePosition);

        private void TsslTextEncodingVal_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Right)
                return;

            this.cmsEncoding.Show(MousePosition);
        }

        private void TsslTextEncodingVal_DoubleClick(object sender, EventArgs e) =>
            this.cmsEncoding.Show(MousePosition);

        private void TsmiEncryptionTrue_Click(object sender, EventArgs e) =>
            this.SetEncryption(true);

        private void TsmiEncryptionFalse_Click(object sender, EventArgs e) =>
            this.SetEncryption(false);

        private void TsmiEncodingUTF8_Click(object sender, EventArgs e) =>
            this.SetEncoding(ETextEncoding.UTF8, false);

        private void TsmiEncodingUTF8BOM_Click(object sender, EventArgs e) =>
            this.SetEncoding(ETextEncoding.UTF8, true);

        private void TsmiEncodingUTF16BE_Click(object sender, EventArgs e) =>
            this.SetEncoding(ETextEncoding.UTF16BE, true);

        private void TsmiEncodingUTF16LE_Click(object sender, EventArgs e) =>
            this.SetEncoding(ETextEncoding.UTF16LE, true);

        private void TsmiEncodingUTF32BE_Click(object sender, EventArgs e) =>
            this.SetEncoding(ETextEncoding.UTF32BE, true);

        private void TsmiEncodingUTF32LE_Click(object sender, EventArgs e) =>
            this.SetEncoding(ETextEncoding.UTF32LE, true);

        private void SetEncoding(ETextEncoding encoding, bool bom)
        {
            if (this._currentTextEditor == null)
                return;

            this._currentTextEditor.ChangeEncoding(encoding, bom);
        }

        private void SetEncryption(bool isEncrypted)
        {
            if (this._currentTextEditor == null)
                return;

            if (
                MessageBox.Show(
                    GetVariable("CHANGE_ENCRYPTION"),
                    GetVariable("CHANGE_ENCRYPTION_TITLE"),
                    MessageBoxButtons.OKCancel,
                    MessageBoxIcon.Exclamation
                ) != DialogResult.OK
            )
                return;

            this._currentTextEditor.ChangeEncryptionState(isEncrypted);
        }

        #endregion

        private void SaveDataFile(TextEditor editor, string path)
        {
            if (editor.DocumentEncrypted)
            {
                using (BinaryWriter bw = new BinaryWriter(File.Open(path, FileMode.Create)))
                {
                    bw.Write(
                        LittleFighter2.Ciphers.Encryption.EncryptData(editor.GetContentToSave())
                    );
                }
            }
            else
            {
                using (
                    StreamWriter sw = new StreamWriter(
                        File.Open(path, FileMode.Create),
                        EncodingUtil.GetSystemEncodingFromTextEncoding(editor.Encoding)
                    )
                )
                {
                    sw.Write(editor.GetContentToSave());
                }
            }

            editor.Path = path;
            editor.DocumentSaved();
        }
    }
}
