﻿using System;
using System.Text.RegularExpressions;
using Easier_Data_Editor.Models.LittleFighter2;
using Easier_Data_Editor.Utilities;

namespace Easier_Data_Editor.Tools
{
    public static class AdjusterXY
    {
        public static string GetAdjustedXYValues(string text, Frame frame, Frame sourceFrame)
        {
            Regex regexX = RegexUtil.GetSingleWordIntegerNegativeRegexFromWordAndValuePattern("x");
            Regex regexY = RegexUtil.GetSingleWordIntegerNegativeRegexFromWordAndValuePattern("y");

            text = ChangeXOrYValues(regexX, text, sourceFrame.Center.X - frame.Center.X);
            text = ChangeXOrYValues(regexY, text, sourceFrame.Center.Y - frame.Center.Y);

            return text;
        }

        private static string ChangeXOrYValues(
            Regex xOrYRegex,
            string text,
            int diffBetweenSourceAndTargetFrame
        )
        {
            MatchCollection matches = xOrYRegex.Matches(text);
            for (int i = matches.Count - 1; i >= 0; i -= 1)
            {
                Match mtch = matches[i];
                try
                {
                    if (int.TryParse(mtch.Value, out int x))
                    {
                        text = xOrYRegex.Replace(
                            text,
                            (x - diffBetweenSourceAndTargetFrame).ToString(),
                            1,
                            mtch.Index
                        );
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }

            return text;
        }
    }
}
