﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text.RegularExpressions;
using Easier_Data_Editor.Utilities;
using ScintillaNET;

namespace Easier_Data_Editor.Tools
{
    public class AdjusterIDs
    {
        private static readonly string[] ATTRIBUTES_WITH_FRAME_IDS = new string[]
        {
            @"next",
            @"hit_[A-Za-z]+",
        };

        private static readonly Regex REGEX_ATTRIBUTES_WITH_FRAME_IDS = new Regex(
            $"({string.Join("|", ATTRIBUTES_WITH_FRAME_IDS.Select(i => RegexUtil.GetSingleWordPatternFromWord(i)).ToArray())}):\\s*(-?\\d+)",
            RegexOptions.IgnoreCase
        );

        private static readonly Regex REGEX_IS_JUST_A_NUMBER = new Regex(
            @"^-?\d+$",
            RegexOptions.IgnoreCase
        );
        private static readonly Regex REGEX_IS_HIT_ATTRIBUTE = new Regex(
            @"hit_[A-Za-z]+",
            RegexOptions.IgnoreCase
        );

        private readonly Scintilla _scintilla;
        private string _editedText;
        private readonly List<int> _unusedFrameList = new List<int>();
        private readonly MatchCollection _inputFrames;

        /// <param name="scintilla">The editor which is the paste target. To try find unused frame IDs of the current position and in a row.</param>
        /// <param name="text">The text which contains the frames, which should adjust.</param>
        public AdjusterIDs(Scintilla scintilla, string text)
        {
            this._scintilla = scintilla;
            this._editedText = text;

            if (string.IsNullOrEmpty(this._editedText))
                this._editedText = ""; // no return coz init _inputFrames

            this._inputFrames = Regex.Matches(
                this._editedText,
                Easier_Data_Editor.Constants.REGEX_PATTERN_FRAME_CONTENT,
                RegexOptions.IgnoreCase | RegexOptions.Singleline | RegexOptions.Multiline
            );

            if (this._inputFrames.Count <= 0)
                return;

            int frameAt = this.GetBeginOfFrameID(this._inputFrames[0].Value); // 0 exists, check if above

            if (frameAt == int.MinValue)
                return;

            _unusedFrameList.AddRange(
                IdsAnalyser.GetFreeIDsTryInRow(scintilla.Text, this._inputFrames.Count, frameAt)
            );
        }

        /// <summary>Adjusts the IDs of text which contains frames.</summary>
        /// <returns>The adjusted text if successful. Otherwise "rawText".</returns>
        public string GetAdjustedIDs()
        {
            if (
                string.IsNullOrEmpty(this._editedText)
                || _unusedFrameList.Count < this._inputFrames.Count
            )
                return this._editedText;

            ImmutableDictionary<int, int> idMapper = this.GetIDMapper();

            for (int i = this._inputFrames.Count - 1; i >= 0; --i)
            {
                Match frame = this._inputFrames[i];
                MatchCollection attributesWithIDs = REGEX_ATTRIBUTES_WITH_FRAME_IDS.Matches(
                    frame.Value
                );
                for (int j = attributesWithIDs.Count - 1; j >= 0; --j)
                {
                    Match attributeWithFrameID = attributesWithIDs[j];
                    ChangeSingleID(attributeWithFrameID, idMapper, frame.Index);
                }
            }

            return this._editedText;
        }

        private static int GetFrameID(
            string text,
            int startat = 0,
            RegexOptions options = RegexOptions.IgnoreCase
        )
        {
            Regex frameIDPattern = new Regex(@"(?<=<frame>\s*)\d+", options);
            Match mtchAt = frameIDPattern.Match(text, startat);
            try
            {
                if (mtchAt.Success && int.TryParse(mtchAt.Value.Trim(), out int frameID))
                    return frameID;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            return int.MinValue;
        }

        private int GetBeginOfFrameID(string fallbackFrame)
        {
            int frameAt = GetFrameID(
                this._scintilla.Text,
                this._scintilla.CurrentPosition,
                RegexOptions.IgnoreCase | RegexOptions.RightToLeft
            );
            if (frameAt == int.MinValue)
                frameAt = GetFrameID(fallbackFrame);

            return frameAt;
        }

        private ImmutableDictionary<int, int> GetIDMapper()
        {
            // key = old frame number, value = new frame number
            Dictionary<int, int> idMapper = new Dictionary<int, int>();
            int idIndex = this._inputFrames.Count - 1;
            for (int i = this._inputFrames.Count - 1; i >= 0; --i)
            {
                Match frame = this._inputFrames[i];
                int frameID = GetFrameID(frame.Value);

                if (frameID == int.MinValue)
                    continue;

                Regex frameIDPositionPattern = new Regex(
                    $"(?<=<frame>\\s*){frameID}",
                    RegexOptions.IgnoreCase
                );
                this._editedText = frameIDPositionPattern.Replace(
                    this._editedText,
                    this._unusedFrameList[idIndex].ToString(),
                    1,
                    frame.Index
                );

                if (!idMapper.ContainsKey(frameID))
                {
                    idMapper.Add(frameID, this._unusedFrameList[idIndex]);
                    --idIndex;
                }
            }

            return idMapper.ToImmutableDictionary();
        }

        private void ChangeSingleID(
            Match match,
            ImmutableDictionary<int, int> idMapper,
            int frameOffset
        )
        {
            try
            {
                Group? grp = match
                    .Groups.Cast<Group>()
                    .FirstOrDefault(m => REGEX_IS_JUST_A_NUMBER.IsMatch(m.Value.Trim()));

                if (
                    grp != null
                    && int.TryParse(grp.Value.Trim(), out int frameID)
                    && idMapper.ContainsKey(Math.Abs(frameID))
                )
                {
                    // special case: ignore hit_ attributes with frame id 0
                    // OR
                    // frame id is 999
                    if (
                        frameID == 0 && REGEX_IS_HIT_ATTRIBUTE.IsMatch(match.Value)
                        || frameID == Constants.FRAME_ID_DELETION
                    )
                        return;

                    int newID = idMapper[Math.Abs(frameID)];
                    if (frameID < 0)
                        newID *= -1;

                    this._editedText = REGEX_ATTRIBUTES_WITH_FRAME_IDS.Replace(
                        this._editedText,
                        $"$1: {newID}",
                        1,
                        frameOffset + match.Index
                    );
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
    }
}
