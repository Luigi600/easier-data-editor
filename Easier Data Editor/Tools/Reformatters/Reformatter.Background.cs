﻿using System.Linq;
using System.Text.RegularExpressions;
using Easier_Data_Editor.Tools.Reformatters.Builders;
using static Easier_Data_Editor.Tools.Reformatters.Constants;

namespace Easier_Data_Editor.Tools.Reformatters
{
    public partial class Reformatter
    {
        private void ReformatBackgrounds()
        {
            BlueprintBuilder? builder = (BlueprintBuilder?)
                this._blueprintBuilders.Find(b => b.TemplateName.Equals("background"))?.Clone();
            if (builder == null)
                return;

            MatchCollection layers = REGEX_GETTER_BACKGROUND_LAYERS.Matches(
                this._builder.ToString()
            );
            for (int i = layers.Count - 1; i >= 0; --i)
            {
                Match layer = layers[i];
                this._builder.Remove(layer.Index, layer.Length);
            }

            OriginalBuilder old = new OriginalBuilder(this._builder);

            ApplyReferenceToNew(
                old,
                builder,
                false,
                Easier_Data_Editor.Constants.CHAR_NEW_LINE_LF.ToString()
            );

            foreach (Match layer in layers)
            {
                this.ReformatSubStructure(
                    "backgroundLayer",
                    builder,
                    layer.Value,
                    (oldLayer, newLayer) =>
                    {
                        Match path = REGEX_GETTER_BACKGROUND_LAYER_PATH.Match(
                            oldLayer.Text.ToString()
                        );
                        if (!path.Success)
                            return;

                        newLayer.SetVariable(BLUEPRINT_VARIABLE_BACKGROUND_LAYER_FILE, path.Value);
                    }
                );
            }

            builder.RemoveUnusedStuff();

            this.AdjustSelection(0, this._builder.Length, builder.Text.Length);
            this._builder.Remove(0, this._builder.Length);
            this._builder.Insert(0, builder.Text.ToString());
        }
    }
}
