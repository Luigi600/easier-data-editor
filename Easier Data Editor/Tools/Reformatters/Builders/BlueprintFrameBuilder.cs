﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Easier_Data_Editor.Tools.Reformatters.Models;

namespace Easier_Data_Editor.Tools.Reformatters.Builders
{
    public class BlueprintFrameBuilder : BlueprintBuilder
    {
        private DataComment? _frameNameComment;

        public DataComment? FrameNameComment
        {
            get => this._frameNameComment;
        }

        public BlueprintFrameBuilder(string templateName, string template)
            : base(templateName, template) { }

        protected BlueprintFrameBuilder(
            string templateName,
            string template,
            List<DataAttribute> attrs,
            List<DataComment> comments,
            List<BlueprintVariable> vars,
            DataComment? frameNameComment
        )
            : base(templateName, template, attrs, comments, vars)
        {
            this._frameNameComment = frameNameComment;
        }

        protected override void SetComments()
        {
            Match commentHolder = Constants.REGEX_GETTER_FRAME_COMMENT_AFTER_NAME_POS.Match(
                this.Text.ToString()
            );
            base.SetComments();
            if (!commentHolder.Success || commentHolder.Groups.Count <= 2)
                return;

            this._frameNameComment = new DataComment(commentHolder, commentHolder.Groups[2]);
            int frameCommentIndex = this.Comments.FindIndex(c =>
                commentHolder.Index <= c.Index
                && commentHolder.Index + commentHolder.Length >= c.EndIndex
            );
            if (frameCommentIndex >= 0)
                this.Comments.RemoveAt(frameCommentIndex);
        }

        public override object Clone()
        {
            BlueprintFrameBuilder clone = new BlueprintFrameBuilder(
                this.TemplateName,
                this.Text.ToString(),
                this.Attributes.Select(a => (DataAttribute)a.Clone()).ToList(),
                this.Comments.Select(a => (DataComment)a.Clone()).ToList(),
                this.Variables.Select(a => (BlueprintVariable)a.Clone()).ToList(),
                this._frameNameComment
            );

            foreach (DataAttribute attr in clone.Attributes)
                clone.SetEventsForAttribute(attr);

            return clone;
        }
    }
}
