﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Easier_Data_Editor.Tools.Reformatters.Models;

namespace Easier_Data_Editor.Tools.Reformatters.Builders
{
    public class OriginalBuilder : Builder
    {
        public OriginalBuilder(string text)
            : this(new StringBuilder(text)) { }

        public OriginalBuilder(StringBuilder text)
            : base(text)
        {
            this.Init();
        }

        protected override void Init()
        {
            this.SetComments();
            this.SetAttributes();
        }

        protected virtual void SetComments()
        {
            foreach (
                Match comment in Constants
                    .REGEX_GETTER_COMMENT_ORIGINAL.Matches(this.Text.ToString())
                    .OfType<Match>()
                    .Where(g => g.Groups.Count > 2)
            )
            {
                this.Comments.Add(new DataComment(comment, comment.Groups[2]));
            }

            this.RemoveCommentsFromText();
        }

        protected virtual void SetAttributes()
        {
            foreach (
                Match attr in Constants
                    .REGEX_GETTER_ATTRIBUTES.Matches(this.Text.ToString())
                    .OfType<Match>()
                    .Where(m => m.Groups.Count > 2)
            )
                this.Attributes.Add(this.GetAttributeFromMatch(attr));
        }

        protected virtual DataAttribute GetAttributeFromMatch(Match attr)
        {
            List<DataAttributeValue> values = attr.Groups[2]
                .Captures.OfType<Capture>()
                .Select(c => new DataAttributeValue(c))
                .ToList();

            return new DataAttributeOriginal(
                attr,
                attr.Value,
                new DataAttributeValue(attr.Groups[1]),
                values
            );
        }
    }
}
