﻿using System.Text;
using System.Text.RegularExpressions;

namespace Easier_Data_Editor.Tools.Reformatters.Builders
{
    public class OriginalSpawnBuilder : OriginalBuilder
    {
        public OriginalSpawnBuilder(string text)
            : base(text) { }

        public OriginalSpawnBuilder(StringBuilder text)
            : base(text) { }

        protected override void SetAttributes()
        {
            base.SetAttributes();

            foreach (Match attr in Constants.REGEX_GETTER_TAG.Matches(this.Text.ToString()))
                this.Attributes.Add(this.GetAttributeFromMatch(attr));

            this.Attributes.Sort((attr1, attr2) => attr1.Index - attr2.Index);
        }
    }
}
