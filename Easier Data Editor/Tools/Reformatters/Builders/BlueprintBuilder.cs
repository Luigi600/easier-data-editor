﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Easier_Data_Editor.Tools.Reformatters.Models;

namespace Easier_Data_Editor.Tools.Reformatters.Builders
{
    public class BlueprintBuilder : Builder, ICloneable
    {
        public string TemplateName { get; }
        internal readonly List<BlueprintVariable> Variables;

        public BlueprintBuilder(string templateName, string template)
            : base(template)
        {
            this.TemplateName = templateName;
            this.Variables = new List<BlueprintVariable>();

            this.Init();
        }

        protected BlueprintBuilder(
            string templateName,
            string template,
            List<DataAttribute> attrs,
            List<DataComment> comments,
            List<BlueprintVariable> vars
        )
            : base(new StringBuilder(template), attrs, comments)
        {
            this.TemplateName = templateName;
            this.Variables = vars;
        }

        private static string GetLinesWithSpaceFromVariable(
            BlueprintVariable var,
            string textToAppend
        ) =>
            string.Join(
                $"{Easier_Data_Editor.Constants.CHAR_NEW_LINE_LF}{var.AppendChar}",
                Easier_Data_Editor.Constants.REGEX_NEW_LINE_SPLITTER.Split(textToAppend)
            );

        protected override void Init()
        {
            this.SetComments();
            this.SetAttributes();
            this.SetVariables();
        }

        public virtual DataAttributeBlueprint? GetBlueprintAttributeFromName(string name) =>
            (DataAttributeBlueprint?)
                this.Attributes.Find(a =>
                    a is DataAttributeBlueprint aBlue
                    && aBlue.Name.Value.Equals(name, StringComparison.OrdinalIgnoreCase)
                );

        public override void AddAttribute(DataAttribute attr)
        {
            base.AddAttribute(attr);

            foreach (DataAttributePart var in this.Variables)
                this.Adjust(var, attr.Index, attr.Length, true);

            this.SetEventsForAttribute(attr);
        }

        public override void RemoveAttribute(DataAttribute attr)
        {
            int oldLen = this.Attributes.Count;
            base.RemoveAttribute(attr);
            if (this.Attributes.Count == oldLen)
                return;

            foreach (DataAttributePart var in this.Variables)
                this.Adjust(var, attr.Index, -attr.Length);
        }

        public virtual void AddVariable(BlueprintVariable var)
        {
            foreach (DataAttributePart variable in this.Variables)
                this.Adjust(variable, var.Index, var.Length, true);

            foreach (DataAttributePart attr in this.Attributes)
                this.Adjust(attr, var.Index, var.Length, true);

            this.Variables.Add(var);
        }

        public virtual void RemoveVariable(BlueprintVariable var)
        {
            if (!this.Variables.Remove(var))
                return;

            int index = 0;
            while (index < this.Variables.Count)
            {
                DataAttributePart variable = this.Variables[index];
                if (!this.Adjust(variable, var.Index, -var.Length))
                    this.Variables.RemoveAt(index);
                else
                    ++index;
            }

            index = 0;
            while (index < this.Attributes.Count)
            {
                DataAttributePart dataAttr = this.Attributes[index];
                if (!this.Adjust(dataAttr, var.Index, -var.Length))
                    this.Attributes.RemoveAt(index);
                else
                    ++index;
            }
        }

        public virtual void InsertText(int index, string text)
        {
            this.Text.Insert(index, text);
            this.AdjustAll(index, text.Length, true);
        }

        public virtual void AppendAfterVariable(string varName, string textToAppend)
        {
            BlueprintVariable? var = this.Variables.Find(v =>
                v.Value.Equals(varName, StringComparison.OrdinalIgnoreCase)
            );
            if (var == null)
                return;

            this.InsertText(var.EndIndex, textToAppend);
        }

        public virtual void AppendBeforeVariable(
            string varName,
            string textToAppend,
            bool useSpaceFromVar
        )
        {
            BlueprintVariable? var = this.Variables.Find(v =>
                v.Value.Equals(varName, StringComparison.OrdinalIgnoreCase)
            );
            if (var == null)
                return;

            if (useSpaceFromVar)
                textToAppend += var.AppendChar;

            this.InsertText(var.Index, textToAppend);
        }

        public virtual void AppendBeforeVariableApplySpace(
            string varName,
            string textToAppend,
            string? charToAppendAfterNewText = null
        )
        {
            BlueprintVariable? var = this.Variables.Find(v =>
                v.Value.Equals(varName, StringComparison.OrdinalIgnoreCase)
            );
            if (var == null)
                return;

            if (var.IsSingleLine)
                textToAppend = GetLinesWithSpaceFromVariable(var, textToAppend);

            if (charToAppendAfterNewText != null)
                textToAppend += charToAppendAfterNewText;

            textToAppend += var.AppendChar;

            this.InsertText(var.Index, textToAppend);
        }

        public virtual void RemoveText(int index, int len)
        {
            this.Text.Remove(index, len);
            this.AdjustAll(index, -len); // * -1 coz removed
        }

        public virtual void RemoveComment(DataComment comment)
        {
            if (this.Comments.Find(c => c == comment) == null)
                return;

            this.GetRemovableArea(
                comment.Index,
                comment.Length,
                out int removeStart,
                out int removeEnd
            );

            int len = removeEnd - removeStart + 1;
            this.Text.Remove(removeStart, len);
            this.AdjustAll(removeStart, -len);
        }

        public virtual void ReplaceText(int index, int len, string val)
        {
            this.Text.Remove(index, len);
            this.Text.Insert(index, val);
            this.AdjustAll(index, val.Length - len, false);
        }

        public virtual void SetVariable(string varName, string newVal)
        {
            BlueprintVariable? var = this.Variables.Find(v =>
                v.Value.Equals(varName, StringComparison.OrdinalIgnoreCase)
            );
            if (var == null)
                return;

            string val = var.GetValuePadding(newVal);
            this.ReplaceText(var.Index, var.Length, val);
            var.IsSet = true;
        }

        public virtual void SetVariableApplySpace(string varName, string newVal)
        {
            BlueprintVariable? var = this.Variables.Find(v =>
                v.Value.Equals(varName, StringComparison.OrdinalIgnoreCase)
            );
            if (var == null)
                return;

            if (var.IsSingleLine)
                newVal = GetLinesWithSpaceFromVariable(var, newVal);

            string val = var.GetValuePadding(newVal);
            this.ReplaceText(var.Index, var.Length, val);
            var.IsSet = true;
        }

        public virtual void RemoveUnusedStuff()
        {
            BlueprintVariable? var;
            while ((var = this.Variables.Find(v => !v.IsSet)) != null)
            {
                this.GetRemovableArea(
                    var.Index,
                    var.Length,
                    out int removeStart,
                    out int removeEnd
                );

                int len = removeEnd - removeStart + 1;
                this.Text.Remove(removeStart, len);
                this.AdjustAll(removeStart, -len);
            }

            DataAttribute? var1;
            while (
                (
                    var1 = this.Attributes.Find(a =>
                        a is DataAttributeBlueprint aBlue && !aBlue.IsSet
                    )
                ) != null
            )
            {
                this.GetRemovableArea(
                    var1.Index,
                    var1.Length,
                    out int removeStart,
                    out int removeEnd
                );

                int len = removeEnd - removeStart + 1;
                this.Text.Remove(removeStart, len);
                this.AdjustAll(removeStart, -len);
            }
        }

        public virtual void ApplyPaddingToAttributeNames()
        {
            foreach (
                DataAttributeValueBlueprint attrBlueName in this
                    .Attributes.Where(a =>
                        a is DataAttributeBlueprint attrBlue
                        && attrBlue.IsSet
                        && attrBlue.Name.Padding > 0
                    )
                    .Select(a => a.Name)
                    .OfType<DataAttributeValueBlueprint>()
            )
            {
                this.ReplaceText(
                    attrBlueName.Index,
                    attrBlueName.Length,
                    attrBlueName.GetValuePadding(attrBlueName.Value)
                );
            }
        }

        public virtual object Clone()
        {
            BlueprintBuilder clone = new BlueprintBuilder(
                this.TemplateName,
                this.Text.ToString(),
                this.Attributes.Select(a => (DataAttribute)a.Clone()).ToList(),
                this.Comments.Select(a => (DataComment)a.Clone()).ToList(),
                this.Variables.Select(a => (BlueprintVariable)a.Clone()).ToList()
            );

            foreach (DataAttribute attr in clone.Attributes)
                clone.SetEventsForAttribute(attr);

            return clone;
        }

        protected override void AdjustAll(int index, int lenOffset, bool completelyNew = false)
        {
            base.AdjustAll(index, lenOffset, completelyNew);

            int elementIndex = 0;
            while (elementIndex < this.Variables.Count)
            {
                DataAttributePart dataAttr = this.Variables[elementIndex];
                if (!this.Adjust(dataAttr, index, lenOffset, completelyNew))
                    this.Variables.RemoveAt(elementIndex);
                else
                    ++elementIndex;
            }
        }

        protected virtual void SetComments()
        {
            foreach (
                Match comment in Constants
                    .REGEX_GETTER_COMMENT_BLUEPRINT.Matches(this.Text.ToString())
                    .OfType<Match>()
                    .Where(g => g.Groups.Count > 2)
            )
            {
                this.Comments.Add(new DataComment(comment, comment.Groups[2]));
            }

            this.RemoveCommentsFromText();
        }

        protected virtual void SetAttributes()
        {
            foreach (
                Match attr in Constants
                    .REGEX_GETTER_ATTRIBUTES_BLUEPRINT.Matches(this.Text.ToString())
                    .OfType<Match>()
                    .Where(m => m.Groups.Count > 2)
            )
            {
                DataAttribute dataAttr = this.GetAttributeFromMatch(attr);
                this.Attributes.Add(dataAttr);
                this.SetEventsForAttribute(dataAttr);
            }
        }

        protected virtual void SetVariables()
        {
            foreach (
                Match attr in Constants
                    .REGEX_GETTER_PLACEHOLDER_VARIABLE.Matches(this.Text.ToString())
                    .OfType<Match>()
                    .Where(m => m.Groups.Count > 2)
            )
            {
                this.Variables.Add(
                    BlueprintVariable.GetInstanceWithPadding(
                        attr.Groups[2],
                        attr.Groups[1].Value,
                        Utility.IsSingleLine(this.Text, attr.Index, attr.Index + attr.Length - 1)
                    )
                );
            }
        }

        protected virtual DataAttribute GetAttributeFromMatch(Match attr)
        {
            List<DataAttributeValueBlueprint> values = attr.Groups[2]
                .Captures.OfType<Capture>()
                .Select(c => DataAttributeValueBlueprint.GetInstanceWithPadding(c))
                .ToList();

            return new DataAttributeBlueprint(
                attr,
                DataAttributeValueBlueprint.GetInstanceWithPadding(attr.Groups[1]),
                values
            );
        }

        protected virtual void GetRemovableArea(
            int index,
            int len,
            out int removeStart,
            out int removeEnd
        )
        {
            removeStart = index;
            int reachLineEndIndex = this.GetRemovableAreaEnd(index, len, out removeEnd);

            if (index > 0 && this.IsRemoveEndBeforeComment(removeEnd))
                removeEnd = index + len - 1;

            if (reachLineEndIndex < 0 && removeEnd == this.Text.Length - 1)
                reachLineEndIndex = this.Text.Length - 1;

            if (reachLineEndIndex < 0)
                return;

            if (
                !this.GetRemovableAreaStart(index, len, out removeStart)
                && reachLineEndIndex != this.Text.Length - 1
            )
                removeEnd = reachLineEndIndex; // don't remove the new line chars coz the line has attributes before the current attribute (current attribute = index)
        }

        protected virtual int GetRemovableAreaEnd(int index, int len, out int removeEnd)
        {
            removeEnd = index + len - 1;
            int reachLineEndIndex = -1;

            for (int i = removeEnd + 1; i < this.Text.Length; ++i)
            {
                char chr = this.Text[i];
                if (Easier_Data_Editor.Constants.REGEX_IS_NEW_LINE.IsMatch(chr.ToString()))
                {
                    reachLineEndIndex = i - 1;
                    removeEnd = i;
                    if (++i < this.Text.Length)
                    {
                        char nextChr = this.Text[i];
                        if (
                            Easier_Data_Editor.Constants.REGEX_IS_NEW_LINE.IsMatch(
                                nextChr.ToString()
                            ) && !nextChr.Equals(chr)
                        )
                            removeEnd = i;
                    }
                    break;
                }
                else if (
                    !Easier_Data_Editor.Constants.REGEX_IS_SPACE_TAB_CHAR.IsMatch(chr.ToString())
                )
                {
                    removeEnd = i - 1;
                    break;
                }
            }

            return reachLineEndIndex;
        }

        protected virtual bool GetRemovableAreaStart(int index, int len, out int removeStart)
        {
            removeStart = index;

            for (int i = index - 1; i >= 0; --i)
            {
                string chr = this.Text[i].ToString();
                if (Easier_Data_Editor.Constants.REGEX_IS_SPACE_TAB_CHAR.IsMatch(chr))
                    removeStart = i;
                else if (!Easier_Data_Editor.Constants.REGEX_IS_NEW_LINE.IsMatch(chr))
                    return false;
                else
                    break;
            }

            return true;
        }

        protected virtual bool IsRemoveEndBeforeComment(int removeEnd)
        {
            int nextChar = removeEnd + 1;
            return this.Comments.Find((c) => nextChar >= c.Index && nextChar <= c.EndIndex) != null;
        }

        protected virtual void SetEventsForAttribute(DataAttribute attr)
        {
            attr.ValueAdded += this.DataAttribute_ValueAdded;
            attr.ValueRemoved += this.DataAttribute_ValueRemoved;
        }

        private void DataAttribute_ValueAdded(object? sender, AttributeAddEventArgs e)
        {
            if (!(sender is DataAttribute attr))
                return;

            foreach (DataAttribute attribute in this.Attributes.Where(a => a != attr))
                this.Adjust(attribute, e.Index, e.Value.Length, true);

            foreach (BlueprintVariable var in this.Variables)
                this.Adjust(var, e.Index, e.Value.Length, true);

            this.Text.Insert(e.Index, e.Value);
        }

        private void DataAttribute_ValueRemoved(object? sender, AttributeRemoveEventArgs e)
        {
            if (!(sender is DataAttribute attr))
                return;

            foreach (DataAttribute attribute in this.Attributes.Where(a => a != attr))
                this.Adjust(attribute, e.Index, -e.Length);

            foreach (BlueprintVariable var in this.Variables)
                this.Adjust(var, e.Index, -e.Length);

            this.Text.Remove(e.Index, e.Length);
        }
    }
}
