﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Easier_Data_Editor.Tools.Reformatters.Models;

namespace Easier_Data_Editor.Tools.Reformatters.Builders
{
    public class BlueprintSpawnBuilder : BlueprintBuilder
    {
        public BlueprintSpawnBuilder(string templateName, string template)
            : base(templateName, template) { }

        protected BlueprintSpawnBuilder(
            string templateName,
            string template,
            List<DataAttribute> attrs,
            List<DataComment> comments,
            List<BlueprintVariable> vars
        )
            : base(templateName, template, attrs, comments, vars) { }

        public override object Clone()
        {
            BlueprintSpawnBuilder clone = new BlueprintSpawnBuilder(
                this.TemplateName,
                this.Text.ToString(),
                this.Attributes.Select(a => (DataAttribute)a.Clone()).ToList(),
                this.Comments.Select(a => (DataComment)a.Clone()).ToList(),
                this.Variables.Select(a => (BlueprintVariable)a.Clone()).ToList()
            );

            foreach (DataAttribute attr in clone.Attributes)
                clone.SetEventsForAttribute(attr);

            return clone;
        }

        protected override void SetAttributes()
        {
            base.SetAttributes();

            foreach (
                Match attr in Constants.REGEX_GETTER_TAG_BLUEPRINT.Matches(this.Text.ToString())
            )
            {
                DataAttribute dataAttr = this.GetAttributeFromMatch(attr);
                this.Attributes.Add(dataAttr);
                this.SetEventsForAttribute(dataAttr);
            }

            this.Attributes.Sort((attr1, attr2) => attr1.Index - attr2.Index);
        }
    }
}
