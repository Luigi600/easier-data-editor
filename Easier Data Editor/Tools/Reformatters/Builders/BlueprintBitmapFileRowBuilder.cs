﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Easier_Data_Editor.Tools.Reformatters.Models;
using static Easier_Data_Editor.Tools.Reformatters.Constants;

namespace Easier_Data_Editor.Tools.Reformatters.Builders
{
    public class BlueprintBitmapFileRowBuilder : BlueprintBuilder
    {
        public BlueprintBitmapFileRowBuilder(string templateName, string template)
            : base(templateName, template) { }

        protected BlueprintBitmapFileRowBuilder(
            string templateName,
            string template,
            List<DataAttribute> attrs,
            List<DataComment> comments,
            List<BlueprintVariable> vars
        )
            : base(templateName, template, attrs, comments, vars) { }

        public override DataAttributeBlueprint? GetBlueprintAttributeFromName(string name)
        {
            if (REGEX_EQUALS_FILE.IsMatch(name))
                return (DataAttributeBlueprint?)
                    this.Attributes.Find(a =>
                        a is DataAttributeBlueprint aBlue
                        && REGEX_EQUALS_FILE.IsMatch(aBlue.Name.Value)
                    );

            return base.GetBlueprintAttributeFromName(name);
        }

        public override object Clone()
        {
            BlueprintBitmapFileRowBuilder clone = new BlueprintBitmapFileRowBuilder(
                this.TemplateName,
                this.Text.ToString(),
                this.Attributes.Select(a => (DataAttribute)a.Clone()).ToList(),
                this.Comments.Select(a => (DataComment)a.Clone()).ToList(),
                this.Variables.Select(a => (BlueprintVariable)a.Clone()).ToList()
            );

            foreach (DataAttribute attr in clone.Attributes)
                this.SetEventsForAttribute(attr);

            return clone;
        }

        public override void ApplyPaddingToAttributeNames()
        {
            // work around coz "%INDICES% variable ignores the new value because it is not updated internally.
            // Therefore, reset the name value of the attribute so that the padding is applied correctly
            string text = this.Text.ToString();
            foreach (
                DataAttributeValueBlueprint attrBlueName in this
                    .Attributes.Where(a =>
                        a is DataAttributeBlueprint attrBlue
                        && attrBlue.IsSet
                        && attrBlue.Name.Padding > 0
                    )
                    .Select(a => a.Name)
                    .OfType<DataAttributeValueBlueprint>()
            )
            {
                // cut of padding and anchor
                string realNewVal = Utility.GetPaddingInformationFromString(
                    text.Substring(attrBlueName.Index, attrBlueName.Length),
                    out EValueAnchor _,
                    out int __
                );
                attrBlueName.SetValue(realNewVal);
            }

            base.ApplyPaddingToAttributeNames();
        }

        protected override void SetVariables()
        {
            base.SetVariables();

            if (
                this.GetBlueprintAttributeFromName(BLUEPRINT_VARIABLE_BITMAP_HEADER_INDICES) != null
            )
                return;

            foreach (
                Match attr in REGEX_GETTER_PLACEHOLDER_VARIABLE_FILE_INDICES.Matches(
                    this.Text.ToString()
                )
            )
            {
                this.Variables.Add(
                    BlueprintVariable.GetInstanceWithPadding(
                        attr,
                        attr.Groups[1].Value,
                        Utility.IsSingleLine(this.Text, attr.Index, attr.Index + attr.Length - 1)
                    )
                );
            }
        }
    }
}
