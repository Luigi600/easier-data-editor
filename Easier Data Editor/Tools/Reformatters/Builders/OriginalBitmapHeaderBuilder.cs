﻿using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Easier_Data_Editor.Tools.Reformatters.Builders
{
    public class OriginalBitmapHeaderBuilder : OriginalBuilder
    {
        public OriginalBitmapHeaderBuilder(string text)
            : base(text) { }

        public OriginalBitmapHeaderBuilder(StringBuilder text)
            : base(text) { }

        public void AddSpecialAttributes(IEnumerable<Match> attributes)
        {
            foreach (Match attr in attributes)
                this.Attributes.Add(this.GetAttributeFromMatch(attr));
        }
    }
}
