﻿using System.Text;
using System.Text.RegularExpressions;
using Easier_Data_Editor.Tools.Reformatters.Models;

namespace Easier_Data_Editor.Tools.Reformatters.Builders
{
    public class OriginalFrameBuilder : OriginalBuilder
    {
        private DataComment? _frameNameComment;

        public DataComment? FrameNameComment
        {
            get => this._frameNameComment;
        }

        public OriginalFrameBuilder(string text)
            : base(text) { }

        public OriginalFrameBuilder(StringBuilder text)
            : base(text) { }

        protected override void SetComments()
        {
            Match frameComment = Constants.REGEX_GETTER_FRAME_COMMENT_AFTER_NAME_2.Match(
                this.Text.ToString()
            );
            base.SetComments();
            if (!frameComment.Success || frameComment.Groups.Count <= 2)
                return;

            this._frameNameComment = new DataComment(frameComment, frameComment.Groups[2]);
            int frameCommentIndex = this.Comments.FindIndex(c =>
                frameComment.Index <= c.Index
                && frameComment.Index + frameComment.Length >= c.EndIndex
            );
            if (frameCommentIndex >= 0)
                this.Comments.RemoveAt(frameCommentIndex);
        }
    }
}
