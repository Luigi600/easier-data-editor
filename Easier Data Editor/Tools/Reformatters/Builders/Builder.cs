﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Easier_Data_Editor.Tools.Reformatters.Models;

namespace Easier_Data_Editor.Tools.Reformatters.Builders
{
    public abstract class Builder
    {
        internal readonly List<DataAttribute> Attributes;
        internal readonly List<DataComment> Comments;

        public StringBuilder Text { get; }

        protected Builder(string text)
            : this(new StringBuilder(text)) { }

        protected Builder(StringBuilder text)
        {
            this.Text = text;

            this.Attributes = new List<DataAttribute>();
            this.Comments = new List<DataComment>();
        }

        protected Builder(StringBuilder text, List<DataAttribute> attrs, List<DataComment> comments)
        {
            this.Text = text;
            this.Attributes = attrs;
            this.Comments = comments;
        }

        protected abstract void Init();

        public virtual void RemoveAttribute(DataAttribute attr)
        {
            if (!this.Attributes.Remove(attr))
                return;

            int index = 0;
            while (index < this.Attributes.Count)
            {
                DataAttribute dataAttr = this.Attributes[index];
                if (!this.Adjust(dataAttr, attr.Index, -attr.Length))
                    this.Attributes.RemoveAt(index);
                else
                    ++index;
            }
        }

        public virtual void AddAttribute(DataAttribute attr)
        {
            foreach (DataAttribute dataAttr in this.Attributes)
                this.Adjust(dataAttr, attr.Index, attr.Length);

            this.Attributes.Add(attr);
        }

        public virtual DataAttribute? GetAttributeFromName(string name) =>
            this.Attributes.Find(a =>
                a.Name.Value.Equals(name, StringComparison.OrdinalIgnoreCase)
            );

        protected virtual void RemoveCommentsFromText()
        {
            this.Comments.Sort((c1, c2) => c2.Index - c1.Index);
            foreach (DataComment comment in this.Comments)
            {
                this.Text.Remove(comment.Index, comment.Length);
                this.Text.Insert(
                    comment.Index,
                    Easier_Data_Editor.Constants.CHAR_SPACE.ToString(),
                    comment.Length
                );
            }
            // restore comment
            this.Comments.Sort((c1, c2) => c1.Index - c2.Index);
        }

        protected virtual void AdjustAll(int index, int lenOffset, bool completelyNew = false)
        {
            int elementIndex = 0;
            while (elementIndex < this.Attributes.Count)
            {
                DataAttribute dataAttr = this.Attributes[elementIndex];
                if (!this.Adjust(dataAttr, index, lenOffset, completelyNew))
                    this.Attributes.RemoveAt(elementIndex);
                else
                    ++elementIndex;
            }

            elementIndex = 0;
            while (elementIndex < this.Comments.Count)
            {
                DataAttributePart dataAttr = this.Comments[elementIndex];
                if (!this.Adjust(dataAttr, index, lenOffset, completelyNew))
                {
                    this.Comments.RemoveAt(elementIndex);
                }
                else
                {
                    this.Adjust(
                        this.Comments[elementIndex].RealContent,
                        index,
                        lenOffset,
                        completelyNew
                    );
                    ++elementIndex;
                }
            }
        }

        protected virtual bool Adjust(
            DataAttribute part,
            int index,
            int lenOffset,
            bool completelyNew = false
        )
        {
            bool stillExists = this.Adjust(
                (DataAttributePart)part,
                index,
                lenOffset,
                completelyNew
            );
            if (!stillExists)
                return false;

            int elementIndex = 0;
            while (elementIndex < part.Values.Count)
            {
                DataAttributeValue val = part.Values[elementIndex];
                if (!this.Adjust(val, index, lenOffset, completelyNew))
                    part.RemoveValue(val);
                else
                    ++elementIndex;
            }

            this.Adjust(part.Name, index, lenOffset, completelyNew);

            return stillExists;
        }

        protected virtual bool Adjust(
            DataAttributePart part,
            int index,
            int lenOffset,
            bool completelyNew = false
        )
        {
            // len < 0 means that content was removed
            // the deletion starts BEFORE "part.Index" and ends AFTER "part.EndIndex"
            if (lenOffset < 0 && index <= part.Index && index - lenOffset >= part.EndIndex)
                return false; // => removes "part" from the list

            if (!completelyNew && index >= part.Index && index < part.EndIndex)
                part.UpdateLength(lenOffset);

            if (index < part.Index || completelyNew && index == part.Index)
                part.UpdateIndex(lenOffset);

            return true;
        }
    }
}
