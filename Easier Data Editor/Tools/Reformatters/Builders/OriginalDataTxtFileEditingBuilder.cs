﻿using System.Text.RegularExpressions;
using Easier_Data_Editor.Tools.Reformatters.Models;

namespace Easier_Data_Editor.Tools.Reformatters.Builders
{
    public class OriginalDataTxtFileEditingBuilder : OriginalBuilder
    {
        private DataCommentValue? _file;

        public DataCommentValue? File
        {
            get => this._file;
        }

        public OriginalDataTxtFileEditingBuilder(string text)
            : base(text) { }

        protected override void SetAttributes()
        {
            Match file = Easier_Data_Editor.Constants.REGEX_PATH.Match(this.Text.ToString());
            if (file.Success)
            {
                this.Text.Remove(file.Index, file.Length);
                this._file = new DataCommentValue(file);
            }

            base.SetAttributes();
        }
    }
}
