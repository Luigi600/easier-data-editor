﻿using System.Text.RegularExpressions;

namespace Easier_Data_Editor.Tools.Reformatters.Models
{
    /// <summary>
    /// Represents a value of an LF2 data attribute.
    /// </summary>
    public class DataAttributeValue : DataAttributePart
    {
        public virtual string Value { get; protected set; }

        public DataAttributeValue(Capture attr)
            : this(attr, attr.Value) { }

        public DataAttributeValue(Capture attr, string value)
            : base(attr)
        {
            this.Value = value;
        }

        public DataAttributeValue(int index, int len, string value)
            : base(index, len)
        {
            this.Value = value;
        }

        public virtual void SetValue(string val) => this.Value = val;

        public virtual void SetValueWithLength(string val)
        {
            int oldLen = this.Value.Length;
            this.Value = val;
            this.UpdateLength(val.Length - oldLen);
        }

        public override object Clone() =>
            new DataAttributeValue(this.Index, this.Length, this.Value);

        public override string ToString() => this.Value;
    }
}
