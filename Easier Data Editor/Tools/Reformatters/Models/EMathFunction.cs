﻿namespace Easier_Data_Editor.Tools.Reformatters.Models
{
    public enum EMathFunction
    {
        Ceiling,
        Floor,
        Truncate,
        Min,
        Max,
        Limit,
    }
}
