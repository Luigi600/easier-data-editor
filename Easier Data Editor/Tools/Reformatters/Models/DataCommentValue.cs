﻿using System.Text;
using System.Text.RegularExpressions;

namespace Easier_Data_Editor.Tools.Reformatters.Models
{
    public class DataCommentValue : DataAttributePart
    {
        internal StringBuilder Value { get; }

        public DataCommentValue(Capture capture)
            : base(capture) => this.Value = new StringBuilder(capture.Value);

        public DataCommentValue(int index, int len, string val)
            : this(index, len, new StringBuilder(val)) { }

        public DataCommentValue(int index, int len, StringBuilder val)
            : base(index, len) => this.Value = val;

        public override object Clone() => new DataCommentValue(this.Index, this.Length, this.Value);
    }
}
