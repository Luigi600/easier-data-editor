﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Easier_Data_Editor.Tools.Reformatters.Models
{
    /// <summary>
    /// Represents a whole attribute of LF2 data. This includes the name of the attribute, as well as the values (multiple possible).
    /// </summary>
    public class DataAttribute : DataAttributePart, ICloneable
    {
        private readonly DataAttributeValue _name;

        public DataAttributeValue Name
        {
            get => this._name;
        }
        protected DataAttributeValueComparer Comparer { get; } = new DataAttributeValueComparer();
        internal readonly List<DataAttributeValue> Values;

        public event EventHandler<AttributeAddEventArgs>? ValueAdded;
        public event EventHandler<AttributeRemoveEventArgs>? ValueRemoved;

        public DataAttribute(
            Capture capture,
            DataAttributeValue name,
            List<DataAttributeValue> values
        )
            : base(capture)
        {
            this._name = name;
            this.Values = values;

            this.Values.Sort(this.Comparer);
        }

        protected DataAttribute(
            int index,
            int len,
            DataAttributeValue name,
            List<DataAttributeValue> attrs
        )
            : base(index, len)
        {
            this._name = name;
            this.Values = attrs;

            this.Values.Sort(this.Comparer);
        }

        public virtual void AddValue(DataAttributeValue val, string textToAdd)
        {
            this.AddValue(val, textToAdd, val.Index);
        }

        public virtual void AddValue(DataAttributeValue val, string textToAdd, int index)
        {
            foreach (DataAttributeValue value in this.Values.Where(v => v.Index > index))
                value.UpdateIndex(textToAdd.Length);

            this.Values.Add(val);
            this.Values.Sort(this.Comparer);
            this.UpdateLength(textToAdd.Length);
            this.OnValueAdded(new AttributeAddEventArgs(val, index, textToAdd));
        }

        public virtual bool RemoveValue(DataAttributeValue val)
        {
            int removeIndex = this.Values.FindIndex(v => v == val);
            if (removeIndex < 0)
                return false;

            DataAttributeValue? lastVal = removeIndex > 0 ? this.Values[removeIndex - 1] : null;
            int startIndex = lastVal != null ? lastVal.EndIndex : val.Index;
            int lenToDelete = val.EndIndex - startIndex;

            this.Values.Remove(val);
            foreach (DataAttributeValue value in this.Values.Where(v => v.Index > startIndex))
                value.UpdateIndex(-lenToDelete);

            this.UpdateLength(-lenToDelete);
            this.OnValueRemoved(new AttributeRemoveEventArgs(val, startIndex, lenToDelete));

            return true;
        }

        public override object Clone() =>
            new DataAttribute(
                this.Index,
                this.Length,
                (DataAttributeValue)this.Name.Clone(),
                this.Values.ConvertAll(v => (DataAttributeValue)v.Clone())
            );

        protected virtual void OnValueAdded(AttributeAddEventArgs e) =>
            this.ValueAdded?.Invoke(this, e);

        protected virtual void OnValueRemoved(AttributeRemoveEventArgs e) =>
            this.ValueRemoved?.Invoke(this, e);
    }
}
