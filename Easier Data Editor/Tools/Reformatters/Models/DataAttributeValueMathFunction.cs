﻿using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Easier_Data_Editor.Tools.Reformatters.Models
{
    public class DataAttributeValueMathFunction : DataAttributeValue
    {
        /// <summary>
        /// Specifies the index of math function with the name (<seealso cref="DataAttributeValueMathFunction.Index"/> contains only the first bracket position).
        /// </summary>
        public int FunctionStartIndex { get; }
        public EMathFunction Type { get; }
        internal List<DataAttributeValueMathFunction> InnernFunctions =
            new List<DataAttributeValueMathFunction>();

        public DataAttributeValueMathFunction(
            Capture attr,
            string value,
            EMathFunction type,
            int functionStart
        )
            : base(attr, value)
        {
            this.Type = type;
            this.FunctionStartIndex = functionStart;
        }

        public DataAttributeValueMathFunction(
            int index,
            int len,
            string value,
            EMathFunction type,
            int functionStart
        )
            : base(index, len, value)
        {
            this.Type = type;
            this.FunctionStartIndex = functionStart;
        }

        public DataAttributeValueMathFunction(
            int index,
            int len,
            string value,
            EMathFunction type,
            List<DataAttributeValueMathFunction> functions,
            int functionStart
        )
            : base(index, len, value)
        {
            this.Type = type;
            this.InnernFunctions = functions;
            this.FunctionStartIndex = functionStart;
        }

        public static DataAttributeValueMathFunction? GetMathFunctionFromString(
            string fullText,
            string mathName,
            int mathFunctionIndex
        )
        {
            EMathFunction? functionType = Utility.GetMathFunctionEnumFromString(mathName);
            if (functionType == null)
                return null;

            List<bool> bracketBuffer = new List<bool>();
            int firstBracket = -1;
            for (int i = mathFunctionIndex + mathName.Length; i < fullText.Length; ++i)
            {
                char chr = fullText[i];
                if (chr == '(')
                {
                    if (bracketBuffer.Count == 0)
                        firstBracket = i;

                    bracketBuffer.Add(true);
                }
                else if (chr == ')')
                {
                    if (bracketBuffer.Count == 0)
                        return null;

                    bracketBuffer.RemoveAt(bracketBuffer.Count - 1);

                    if (bracketBuffer.Count == 0)
                    {
                        int len = i - firstBracket + 1; // +1 coz otherwise endIndex
                        return new DataAttributeValueMathFunction(
                            firstBracket,
                            len,
                            fullText.Substring(firstBracket, len),
                            (EMathFunction)functionType,
                            mathFunctionIndex
                        );
                    }
                }
            }

            return null;
        }

        public override object Clone() =>
            new DataAttributeValueMathFunction(
                this.Index,
                this.Length,
                this.Value,
                this.Type,
                this.InnernFunctions.ConvertAll(f => (DataAttributeValueMathFunction)f.Clone()),
                this.FunctionStartIndex
            );
    }
}
