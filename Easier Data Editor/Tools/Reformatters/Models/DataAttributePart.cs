﻿using System;
using System.Text.RegularExpressions;

namespace Easier_Data_Editor.Tools.Reformatters.Models
{
    /// <summary>
    /// Represents a part of a LF2 data attribute, like the name "pic:" or value "5".
    /// </summary>
    public abstract class DataAttributePart : ICloneable
    {
        private int _index;
        private int _length;

        public virtual int Index
        {
            get => this._index;
        }
        public virtual int EndIndex
        {
            get => this._index + this._length;
        }
        public virtual int Length
        {
            get => this._length;
        }

        protected DataAttributePart(Capture capture)
            : this(capture.Index, capture.Length) { }

        protected DataAttributePart(int index, int len)
        {
            this._index = index;
            this._length = len;
        }

        public void UpdateIndex(int offset) => this._index += offset;

        public void UpdateLength(int offset) => this._length += offset;

        public abstract object Clone();
    }
}
