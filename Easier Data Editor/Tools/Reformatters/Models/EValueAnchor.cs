﻿namespace Easier_Data_Editor.Tools.Reformatters.Models
{
    public enum EValueAnchor
    {
        Left,
        Center,
        Right,
    }
}
