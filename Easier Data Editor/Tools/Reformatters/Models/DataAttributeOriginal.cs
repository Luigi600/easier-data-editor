﻿using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Easier_Data_Editor.Tools.Reformatters.Models
{
    public class DataAttributeOriginal : DataAttribute
    {
        public string Value { get; }

        public DataAttributeOriginal(
            Capture capture,
            string value,
            DataAttributeValue name,
            List<DataAttributeValue> values
        )
            : base(capture, name, values)
        {
            this.Value = value;
        }

        protected DataAttributeOriginal(
            int index,
            int len,
            string value,
            DataAttributeValue name,
            List<DataAttributeValue> attrs
        )
            : base(index, len, name, attrs)
        {
            this.Value = value;
        }

        public override object Clone() =>
            new DataAttributeOriginal(
                this.Index,
                this.Length,
                this.Value,
                (DataAttributeValue)this.Name.Clone(),
                this.Values.ConvertAll(a => (DataAttributeValue)a.Clone())
            );
    }
}
