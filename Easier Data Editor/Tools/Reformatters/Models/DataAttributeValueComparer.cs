﻿using System.Collections.Generic;

namespace Easier_Data_Editor.Tools.Reformatters.Models
{
    public class DataAttributeValueComparer : Comparer<DataAttributeValue>
    {
        public override int Compare(DataAttributeValue? x, DataAttributeValue? y)
        {
            if (x is null && y is null)
                return 0;

            // TODO: right order when "null"?
            if (x is null)
                return -1;
            if (y is null)
                return 1;

            return x.Index - y.Index;
        }
    }
}
