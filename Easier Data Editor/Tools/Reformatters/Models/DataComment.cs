﻿using System.Text;
using System.Text.RegularExpressions;

namespace Easier_Data_Editor.Tools.Reformatters.Models
{
    public class DataComment : DataCommentValue
    {
        public DataCommentValue RealContent { get; }

        public DataComment(Capture wholeComment, Capture realContent)
            : base(wholeComment)
        {
            this.RealContent = new DataCommentValue(realContent);
        }

        public DataComment(int index, int len, string val, DataCommentValue realContent)
            : this(index, len, new StringBuilder(val), realContent) { }

        public DataComment(int index, int len, StringBuilder val, DataCommentValue realContent)
            : base(index, len, val)
        {
            this.RealContent = realContent;
        }

        public override object Clone() =>
            new DataComment(
                this.Index,
                this.Length,
                this.Value,
                (DataCommentValue)this.RealContent.Clone()
            );
    }
}
