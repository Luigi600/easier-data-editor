﻿using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Easier_Data_Editor.Tools.Reformatters.Models
{
    public class BlueprintVariable : DataAttributeValueBlueprint
    {
        public string AppendChar { get; }
        public bool IsSingleLine { get; }

        public BlueprintVariable(
            Capture capture,
            string value,
            string appendChar,
            EValueAnchor anchor = EValueAnchor.Left,
            int padding = 0,
            bool isSingleLine = false
        )
            : this(capture.Index, capture.Length, value, appendChar, anchor, padding, isSingleLine)
        {
            this.IsSingleLine = isSingleLine;
        }

        protected BlueprintVariable(
            int index,
            int len,
            string value,
            string appendChar,
            EValueAnchor anchor,
            int padding,
            bool isSingleLine
        )
            : base(index, len, value, anchor, padding, new List<DataAttributeValueMathFunction>())
        {
            this.AppendChar = appendChar;
            this.IsSingleLine = isSingleLine;
        }

        public static BlueprintVariable GetInstanceWithPadding(
            Capture capture,
            string appendChar,
            bool isSingleLine
        )
        {
            string realPlaceholder = Utility.GetPaddingInformationFromString(
                capture.Value,
                out EValueAnchor anchor,
                out int padding
            );

            return new BlueprintVariable(
                capture,
                realPlaceholder,
                appendChar,
                anchor,
                padding,
                isSingleLine
            );
        }

        public override object Clone() =>
            new BlueprintVariable(
                this.Index,
                this.Length,
                this.Value,
                this.AppendChar,
                this.Anchor,
                this.Padding,
                this.IsSingleLine
            );
    }
}
