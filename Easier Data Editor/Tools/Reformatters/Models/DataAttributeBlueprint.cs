﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Easier_Data_Editor.Tools.Reformatters.Models
{
    public class DataAttributeBlueprint : DataAttribute
    {
        private readonly DataAttributeValueBlueprint _name;

        public bool IsSet
        {
            get => this.GetValues().TrueForAll(v => v.IsSet);
        }
        public new DataAttributeValueBlueprint Name
        {
            get => this._name;
        }

        public DataAttributeBlueprint(
            Capture capture,
            DataAttributeValueBlueprint name,
            List<DataAttributeValueBlueprint> values
        )
            : this(capture.Index, capture.Length, name, values) { }

        protected DataAttributeBlueprint(
            int index,
            int len,
            DataAttributeValueBlueprint name,
            List<DataAttributeValueBlueprint> values
        )
            : base(index, len, name, values.ConvertAll(v => (DataAttributeValue)v))
        {
            this._name = name;
        }

        internal List<DataAttributeValueBlueprint> GetValues() =>
            this
                .Values.Where(v => v is DataAttributeValueBlueprint)
                .Cast<DataAttributeValueBlueprint>()
                .ToList();

        public override object Clone() =>
            new DataAttributeBlueprint(
                this.Index,
                this.Length,
                (DataAttributeValueBlueprint)this.Name.Clone(),
                this.GetValues().ConvertAll(v => (DataAttributeValueBlueprint)v.Clone())
            );
    }
}
