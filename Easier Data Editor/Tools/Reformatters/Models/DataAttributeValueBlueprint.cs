﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Text.RegularExpressions;

namespace Easier_Data_Editor.Tools.Reformatters.Models
{
    public class DataAttributeValueBlueprint : DataAttributeValue
    {
        private readonly List<DataAttributeValueMathFunction> _mathFunctions;

        public EValueAnchor Anchor { get; }
        public int Padding { get; }
        public bool IsSet { get; set; } = false;
        public bool IsOptional { get; }

        public DataAttributeValueBlueprint(
            Capture capture,
            string value,
            EValueAnchor anchor = EValueAnchor.Left,
            int padding = 0,
            List<DataAttributeValueMathFunction>? functions = null
        )
            : this(capture.Index, capture.Length, value, anchor, padding, functions) { }

        protected DataAttributeValueBlueprint(
            int index,
            int len,
            string value,
            EValueAnchor anchor,
            int padding,
            List<DataAttributeValueMathFunction>? functions
        )
            : base(index, len, value)
        {
            this.Anchor = anchor;
            this.Padding = padding;
            this._mathFunctions = functions ?? new List<DataAttributeValueMathFunction>();

            this.IsOptional = value.Contains(Constants.BLUEPRINT_PLACEHOLDER_OPTIONAL_VALUE);
        }

        public static DataAttributeValueBlueprint GetInstanceWithPadding(Capture capture)
        {
            string realPlaceholder = Utility.GetPaddingInformationFromString(
                capture.Value,
                out EValueAnchor anchor,
                out int padding
            );

            return new DataAttributeValueBlueprint(
                capture,
                realPlaceholder,
                anchor,
                padding,
                GetMathFunctionsFromString(realPlaceholder)
            );
        }

        private static List<DataAttributeValueMathFunction> GetMathFunctionsFromString(string val)
        {
            List<DataAttributeValueMathFunction> list = new List<DataAttributeValueMathFunction>();

            foreach (Match mathFunction in Constants.REGEX_GETTER_MATH_FUNCTION.Matches(val))
            {
                DataAttributeValueMathFunction? func =
                    DataAttributeValueMathFunction.GetMathFunctionFromString(
                        val,
                        mathFunction.Groups[1].Value,
                        mathFunction.Index
                    );

                if (func == null)
                    continue;

                DataAttributeValueMathFunction? parent = null;
                foreach (DataAttributeValueMathFunction mathFunc in list)
                {
                    if (func.Index > mathFunc.Index && func.EndIndex < mathFunc.EndIndex)
                    {
                        parent = mathFunc;
                        break;
                    }
                }

                if (parent == null)
                    list.Add(func);
                else
                    parent.InnernFunctions.Add(func);
            }

            return list;
        }

        private static void Calculate(
            DataAttributeValueMathFunction func,
            StringBuilder completeText,
            string attrVal,
            DataAttributeValueMathFunction? parent = null
        )
        {
            foreach (DataAttributeValueMathFunction innernFunc in func.InnernFunctions)
                Calculate(innernFunc, completeText, attrVal, func);

            object? calculated = Calculator.CalculateFromAttribute(func, attrVal);

            if (calculated == null)
                return;

            string newVal;
            if (func.Type == EMathFunction.Ceiling)
                newVal = Calculator.CalculateCeiling(calculated);
            else if (func.Type == EMathFunction.Floor)
                newVal = Calculator.CalculateFloor(calculated);
            else if (func.Type == EMathFunction.Truncate)
                newVal = Calculator.CalculateTruncate(calculated);
            else
                newVal = calculated.ToString() ?? "";

            completeText.Remove(
                func.FunctionStartIndex,
                func.Index - func.FunctionStartIndex + func.Length
            );
            completeText.Insert(func.FunctionStartIndex, newVal);

            parent?.SetValueWithLength($"({newVal})");
        }

        public string? GetCalculatedValueWithPaddingOptionalNull(string val)
        {
            string? calculated = this.Calculate(val);
            if (calculated != null)
                val = calculated;

            try
            {
                if (Utilities.NumberShitUtil.IsNumeric(val) && int.Parse(val) == 0)
                    return null;
            }
            catch (Exception exception)
            {
                // val is no number, is it possible?
                Debug.WriteLine(exception.ToString());
            }

            return this.GetValuePadding(val);
        }

        public string GetCalculatedValueWithPadding(string val)
        {
            string? calculated = this.Calculate(val);
            if (calculated != null)
                val = calculated;

            return this.GetValuePadding(val);
        }

        public string GetValuePadding(string val)
        {
            if (val.Length > this.Padding)
                return val;

            if (this.Anchor == EValueAnchor.Left)
                return val.PadRight(this.Padding, Easier_Data_Editor.Constants.CHAR_SPACE);

            if (this.Anchor == EValueAnchor.Right)
                return val.PadLeft(this.Padding, Easier_Data_Editor.Constants.CHAR_SPACE);

            float half = (this.Padding - val.Length) / 2.0f;
            int padLeft = (int)Math.Floor(half);
            int padRight = (int)Math.Ceiling(half);

            return val.PadLeft(val.Length + padLeft, Easier_Data_Editor.Constants.CHAR_SPACE)
                .PadRight(val.Length + padLeft + padRight, Easier_Data_Editor.Constants.CHAR_SPACE);
        }

        public override object Clone() =>
            new DataAttributeValueBlueprint(
                this.Index,
                this.Length,
                this.Value,
                this.Anchor,
                this.Padding,
                this._mathFunctions.ConvertAll(f => (DataAttributeValueMathFunction)f.Clone())
            );

        private string? Calculate(string val)
        {
            if (!Utilities.NumberShitUtil.IsNumeric(val))
                return null;

            if (this._mathFunctions.Count > 0)
            {
                StringBuilder valHolder = new StringBuilder(this.Value);
                foreach (DataAttributeValueMathFunction mathFunc in this._mathFunctions)
                    Calculate(mathFunc, valHolder, val);

                object? calculatedValHolder = Calculator.Calculate(valHolder.ToString());
                if (calculatedValHolder != null)
                    return calculatedValHolder.ToString();

                return valHolder.ToString();
            }
            else if (this.Value.Length > 1)
            {
                object? calculated = Calculator.Calculate(
                    Constants.REGEX_GETTER_SYMBOL_PLACEHOLDER.Replace(this.Value, val)
                );

                return calculated?.ToString();
            }

            return null;
        }
    }
}
