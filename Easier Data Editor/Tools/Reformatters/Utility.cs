﻿using System;
using System.Diagnostics;
using System.Text;
using Easier_Data_Editor.Tools.Reformatters.Models;

namespace Easier_Data_Editor.Tools.Reformatters
{
    internal static class Utility
    {
        internal static bool IsSingleLine(StringBuilder text, int matchStart, int matchEnd)
        {
            bool isLineStart = IsSingleLineBefore(text, matchStart);

            if (!isLineStart)
                return false;

            return IsSingleLineEnd(text, matchEnd);
        }

        internal static string GetPaddingInformationFromString(
            string attrBlueprintVal,
            out EValueAnchor anchor,
            out int paddingLength
        )
        {
            // set default behavior
            anchor = EValueAnchor.Left;
            paddingLength = 0;

            string[] infos = attrBlueprintVal.Split(
                Constants.BLUEPRINT_PLACEHOLDER_SPECIAL_FUNCTION_SEPARATOR
            );

            if (infos.Length == 1)
                return infos[0];

            try
            {
                _ = int.TryParse(infos[1], out paddingLength);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }

            if (infos.Length == 3)
            {
                char anchorChar = infos[2].ToLower()[0];
                if (anchorChar.Equals('r'))
                    anchor = EValueAnchor.Right;
                else if (anchorChar.Equals('l'))
                    anchor = EValueAnchor.Left;
                else if (anchorChar.Equals('c'))
                    anchor = EValueAnchor.Center;
            }

            return infos[0];
        }

        internal static EMathFunction? GetMathFunctionEnumFromString(string name)
        {
            name = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(
                name.ToLower()
            );
            try
            {
                EMathFunction result = (EMathFunction)Enum.Parse(typeof(EMathFunction), name);
                if (Enum.IsDefined(typeof(EMathFunction), result))
                    return result;
            }
            catch (ArgumentException exception)
            {
                Debug.WriteLine(exception);
            }

            return null;
        }

        private static bool IsSingleLineBefore(StringBuilder text, int matchStart)
        {
            for (int i = matchStart - 1; i >= 0; --i)
            {
                string chr = text[i].ToString();
                if (Easier_Data_Editor.Constants.REGEX_IS_NEW_LINE.IsMatch(chr))
                    return true;
                else if (!Easier_Data_Editor.Constants.REGEX_IS_SPACE_TAB_CHAR.IsMatch(chr))
                    return false;
            }

            return true;
        }

        private static bool IsSingleLineEnd(StringBuilder text, int matchEnd)
        {
            for (int i = matchEnd + 1; i < text.Length; ++i)
            {
                string chr = text[i].ToString();
                if (Easier_Data_Editor.Constants.REGEX_IS_NEW_LINE.IsMatch(chr))
                    return true;
                else if (!Easier_Data_Editor.Constants.REGEX_IS_SPACE_TAB_CHAR.IsMatch(chr))
                    return false;
            }

            return true;
        }
    }
}
