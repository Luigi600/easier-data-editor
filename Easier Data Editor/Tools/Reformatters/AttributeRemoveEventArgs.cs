﻿using System;
using Easier_Data_Editor.Tools.Reformatters.Models;

namespace Easier_Data_Editor.Tools.Reformatters
{
    public class AttributeRemoveEventArgs : EventArgs
    {
        public DataAttributeValue DataAttributeValue { get; }
        public int Index { get; }
        public int Length { get; }

        public AttributeRemoveEventArgs(DataAttributeValue val, int index, int len)
        {
            this.DataAttributeValue = val;
            this.Index = index;
            this.Length = len;
        }
    }
}
