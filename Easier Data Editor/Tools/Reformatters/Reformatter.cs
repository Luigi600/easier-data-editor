﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Easier_Data_Editor.Environments.FormatBlueprints;
using Easier_Data_Editor.Tools.Reformatters.Builders;
using Easier_Data_Editor.Tools.Reformatters.Models;
using ScintillaNET;
using static Easier_Data_Editor.Tools.Reformatters.Constants;

namespace Easier_Data_Editor.Tools.Reformatters
{
    public partial class Reformatter
    {
        private sealed class SelectionArea
        {
            public int Anchor { get; set; }
            public int AnchorVirtualSpace { get; set; }
            public int Caret { get; set; }
            public int CaretVirtualSpace { get; set; }

            public SelectionArea(Selection sel)
            {
                this.Anchor = sel.Anchor;
                this.AnchorVirtualSpace = sel.AnchorVirtualSpace;
                this.Caret = sel.Caret;
                this.CaretVirtualSpace = sel.CaretVirtualSpace;
            }
        }

        private static readonly Dictionary<
            string,
            string
        > SUB_STRUCTURE_TO_PLACEHOLDER_VARIABLE_NAME_RESOLVER = new Dictionary<string, string>()
        {
            { "bpoint", "BPOINT" },
            { "cpoint", "CPOINT" },
            { "opoint", "OPOINT" },
            { "wpoint", "WPOINT" },
            { "itr", "ITRS" },
            { "bdy", "BODIES" },
            { "bitmapHeaderFileRows", "FILE_ROWS" },
            { "weaponStrengthListEntry", "ENTRIES" },
            { "backgroundLayer", "LAYERS" },
            { "phase", "PHASES" },
            { "spawn", "SPAWNS" },
            { "objects", "OBJECTS" },
            { "fileEditing", "FILE_EDITING" },
            { "backgrounds", "BACKGROUNDS" },
        };

        private static readonly Dictionary<string, string> SUB_STRUCTURE_TO_BUILDER_NAME_RESOLVER =
            new Dictionary<string, string>()
            {
                { "bpoint", "frameBloodPoint" },
                { "cpoint", "frameCatchPoint" },
                { "opoint", "frameObjectPoint" },
                { "wpoint", "frameWeaponPoint" },
                { "itr", "frameItr" },
                { "bdy", "frameBody" },
                { "bitmapHeaderFileRows", "bitmapFileRows" },
                { "weaponStrengthListEntry", "weaponStrengthListEntry" },
                { "backgroundLayer", "backgroundLayer" },
                { "phase", "phase" },
                { "spawn", "spawn" },
                { "objects", "objects" },
                { "fileEditing", "fileEditing" },
                { "backgrounds", "backgrounds" },
            };

        private static readonly Dictionary<string, string> SUB_STRUCTURE_MULTIPLE_USING =
            new Dictionary<string, string>()
            {
                { "ITRS", "\n" },
                { "BODIES", "\n" },
                { "FILE_ROWS", "\n" },
                { "ENTRIES", "\n" },
                { "LAYERS", "\n" },
                { "PHASES", "\n" },
                { "SPAWNS", "\n" },
                { "OBJECTS", "\n" },
                { "FILE_EDITING", "\n" },
                { "BACKGROUNDS", "\n" },
            };

        private readonly List<BlueprintBuilder> _blueprintBuilders = new List<BlueprintBuilder>();

        private readonly Extensions.Scintilla.Scintilla _editor;
        private readonly StringBuilder _builder;
        private readonly Blueprints _blueprints;

        private List<SelectionArea> _selections = new List<SelectionArea>();
        private int _firstVisLine = -1;

        public Reformatter(Extensions.Scintilla.Scintilla editor, Blueprints blueprints)
        {
            this._editor = editor;
            this._builder = new StringBuilder(this._editor.Text);
            this._blueprints = blueprints;

            this._blueprintBuilders.Add(
                new BlueprintFrameBuilder("frame", this._blueprints.GetFrame())
            );
            this._blueprintBuilders.Add(
                new BlueprintBuilder("frameBloodPoint", this._blueprints.GetFrameBloodPoint())
            );
            this._blueprintBuilders.Add(
                new BlueprintBuilder("frameCatchPoint", this._blueprints.GetFrameCatchPoint())
            );
            this._blueprintBuilders.Add(
                new BlueprintBuilder("frameObjectPoint", this._blueprints.GetFrameObjectPoint())
            );
            this._blueprintBuilders.Add(
                new BlueprintBuilder("frameWeaponPoint", this._blueprints.GetFrameWeaponPoint())
            );
            this._blueprintBuilders.Add(
                new BlueprintBuilder("frameBody", this._blueprints.GetFrameBody())
            );
            this._blueprintBuilders.Add(
                new BlueprintBuilder("frameItr", this._blueprints.GetFrameItr())
            );
            this._blueprintBuilders.Add(
                new BlueprintBuilder("bitmap", this._blueprints.GetBitmapHeader())
            );
            this._blueprintBuilders.Add(
                new BlueprintBitmapFileRowBuilder(
                    "bitmapFileRows",
                    this._blueprints.GetBitmapFileRow()
                )
            );
            this._blueprintBuilders.Add(
                new BlueprintBuilder("weaponStrengthList", this._blueprints.GetWeaponStrengthList())
            );
            this._blueprintBuilders.Add(
                new BlueprintBuilder(
                    "weaponStrengthListEntry",
                    this._blueprints.GetWeaponStrengthListEntry()
                )
            );
            this._blueprintBuilders.Add(
                new BlueprintBuilder("background", this._blueprints.GetBackground())
            );
            this._blueprintBuilders.Add(
                new BlueprintBuilder("backgroundLayer", this._blueprints.GetBackgroundLayer())
            );
            this._blueprintBuilders.Add(
                new BlueprintBuilder("stage", this._blueprints.GetStageStage())
            );
            this._blueprintBuilders.Add(
                new BlueprintBuilder("phase", this._blueprints.GetStagePhase())
            );
            this._blueprintBuilders.Add(
                new BlueprintSpawnBuilder("spawn", this._blueprints.GetStageSpawn())
            );
            this._blueprintBuilders.Add(
                new BlueprintBuilder("dataTxt", this._blueprints.GetDataTxtDataTxt())
            );
            this._blueprintBuilders.Add(
                new BlueprintBuilder("objects", this._blueprints.GetDataTxtObject())
            );
            this._blueprintBuilders.Add(
                new BlueprintBuilder("fileEditing", this._blueprints.GetDataTxtFileEditing())
            );
            this._blueprintBuilders.Add(
                new BlueprintBuilder("backgrounds", this._blueprints.GetDataTxtBackground())
            );
        }

        private static void ApplyCommentsFromReferenceToNew(
            OriginalBuilder old,
            BlueprintBuilder @new
        )
        {
            if (old.Comments.Count <= 0)
            {
                List<DataComment> dataComments = new List<DataComment>(@new.Comments);
                dataComments.Sort((c1, c2) => c2.Index - c1.Index);
                foreach (DataComment comment in dataComments)
                    @new.RemoveComment(comment);

                return;
            }

            string comments = string.Join(
                Easier_Data_Editor.Constants.CHAR_SPACE.ToString(),
                old.Comments.Select(c => c.RealContent.Value.ToString()).ToArray()
            );

            if (@new.Comments.Count > 0)
            {
                DataComment firstComment = @new.Comments[0];
                @new.ReplaceText(
                    firstComment.Index,
                    firstComment.Length,
                    firstComment.Value.ToString()
                );
                @new.InsertText(firstComment.RealContent.EndIndex, $" {comments}");
            }
            else
            {
                @new.AppendAfterVariable(BLUEPRINT_VARIABLE_NAME_OTHER, $"# {comments}");
            }
        }

        private static void ApplyReferenceToNew(
            OriginalBuilder old,
            BlueprintBuilder @new,
            bool removeUnusedStuff = true,
            string? appendChar = null
        )
        {
            ApplyAttributesFromReferenceToNew(old, @new, appendChar);
            @new.ApplyPaddingToAttributeNames();
            ApplyCommentsFromReferenceToNew(old, @new);

            if (removeUnusedStuff)
                @new.RemoveUnusedStuff();
        }

        private static void ApplyAttributesFromReferenceToNew(
            OriginalBuilder old,
            BlueprintBuilder @new,
            string? appendChar = null
        )
        {
            foreach (DataAttribute attr in old.Attributes)
            {
                DataAttributeBlueprint? newAttr = @new.GetBlueprintAttributeFromName(
                    attr.Name.Value
                );
                if (newAttr == null)
                {
                    InsertAttributeFromReferenceToNew(attr, @new, appendChar);
                }
                else
                {
                    EqualizeAttributePlaceholders(attr, newAttr);
                    ReplaceAttributeFromReferenceToNew(attr, newAttr, @new);
                }
            }
        }

        private static void InsertAttributeFromReferenceToNew(
            DataAttribute attr,
            BlueprintBuilder @new,
            string? appendChar = null
        )
        {
            if (attr is not DataAttributeOriginal attrOriginal)
                return;

            if (appendChar == null)
                @new.AppendBeforeVariable(BLUEPRINT_VARIABLE_NAME_OTHER, attrOriginal.Value, true);
            else
                @new.AppendBeforeVariable(
                    BLUEPRINT_VARIABLE_NAME_OTHER,
                    attrOriginal.Value + appendChar,
                    true
                );
        }

        private static void ReplaceAttributeFromReferenceToNew(
            DataAttribute attr,
            DataAttributeBlueprint newAttr,
            BlueprintBuilder @new
        )
        {
            List<DataAttributeValueBlueprint> placeholderValues = newAttr.GetValues();
            for (int i = 0; i < newAttr.Values.Count; ++i)
            {
                DataAttributeValueBlueprint placeholder = placeholderValues[i];
                DataAttributeValue realValue = attr.Values[i];
                if (newAttr.Values.Count == 1 && placeholder.IsOptional)
                {
                    string? newVal = placeholder.GetCalculatedValueWithPaddingOptionalNull(
                        realValue.Value
                    );
                    if (newVal == null)
                        break;

                    @new.ReplaceText(placeholder.Index, placeholder.Length, newVal);
                }
                else
                {
                    @new.ReplaceText(
                        placeholder.Index,
                        placeholder.Length,
                        placeholder.GetCalculatedValueWithPadding(realValue.Value)
                    );
                }

                placeholder.IsSet = true;
            }
        }

        private static void EqualizeAttributePlaceholders(
            DataAttribute attr,
            DataAttributeBlueprint newAttr
        )
        {
            if (newAttr.Values.Count == attr.Values.Count)
                return;

            if (newAttr.Values.Count > attr.Values.Count)
            {
                while (newAttr.Values.Count > attr.Values.Count)
                {
                    DataAttributeValue val = newAttr.Values[newAttr.Values.Count - 1];
                    newAttr.RemoveValue(val);
                }
            }
            else if (newAttr.Values.Count < attr.Values.Count)
            {
                int amountOfSpaces =
                    attr.Values.Count > 1
                        ? attr.Values[attr.Values.Count - 1].Index
                            - attr.Values[attr.Values.Count - 2].EndIndex
                        : 1;

                string spaces = "".PadLeft(amountOfSpaces, Easier_Data_Editor.Constants.CHAR_SPACE);

                while (newAttr.Values.Count < attr.Values.Count)
                {
                    DataAttributeValue lastVal = newAttr.Values[newAttr.Values.Count - 1];
                    DataAttributeValue val = (DataAttributeValueBlueprint)lastVal.Clone();
                    val.UpdateIndex(lastVal.Length + spaces.Length);
                    newAttr.AddValue(val, $"{spaces}{lastVal.Value}", lastVal.EndIndex);
                }
            }
        }

        public void Reformat(bool onlySelected, EFileType type)
        {
            this._selections = this
                ._editor.Selections.OfType<Selection>()
                .Select(s => new SelectionArea(s))
                .ToList();
            this._firstVisLine = this._editor.FirstVisibleLine;

            this._editor.BlockUndoRedoActions = true;

            Stopwatch watcher = new Stopwatch();
            watcher.Start();

            if (type == EFileType.Background)
            {
                this.ReformatBackgrounds();
            }
            else if (type == EFileType.Stage)
            {
                this.ReformatStages(onlySelected);
            }
            else if (type == EFileType.DataTxt)
            {
                this.ReformatDataTxt();
            }
            else
            {
                this.ReformatFrames(onlySelected);
                this.ReformatBitmapHeaders(onlySelected);
                this.ReformatWeaponStrengthLists(onlySelected);
            }
            watcher.Stop();
            Console.WriteLine("REFORMATTER {0}", watcher.ElapsedMilliseconds);

            // it faster to set the completely text than to use the ReplaceTarget function (idk why)
            this._editor.Text = this._builder.ToString();

            this._editor.BlockUndoRedoActions = false;

            this._editor.ClearSelections();
            foreach (SelectionArea sel in this._selections)
                this._editor.AddSelection(
                    sel.Caret,
                    sel.Anchor,
                    sel.CaretVirtualSpace,
                    sel.AnchorVirtualSpace
                );

            this._editor.DropSelection(0);

            this._editor.FirstVisibleLine = this._firstVisLine;
        }

        private void ReformatSubStructure(
            string subStructName,
            BlueprintBuilder @new,
            string refText,
            Action<OriginalBuilder, BlueprintBuilder>? beforeSet = null
        )
        {
            this.ReformatSubStructure(subStructName, @new, new OriginalBuilder(refText), beforeSet);
        }

        private void ReformatSubStructure(
            string subStructName,
            BlueprintBuilder @new,
            StringBuilder refText,
            Action<OriginalBuilder, BlueprintBuilder>? beforeSet = null
        )
        {
            this.ReformatSubStructure(subStructName, @new, new OriginalBuilder(refText), beforeSet);
        }

        private void ReformatSubStructure(
            string subStructName,
            BlueprintBuilder @new,
            OriginalBuilder subStructBuilderOld,
            Action<OriginalBuilder, BlueprintBuilder>? beforeSet = null
        )
        {
            if (
                !SUB_STRUCTURE_TO_PLACEHOLDER_VARIABLE_NAME_RESOLVER.TryGetValue(
                    subStructName,
                    out string? placeholderVariableName
                )
                || !SUB_STRUCTURE_TO_BUILDER_NAME_RESOLVER.TryGetValue(
                    subStructName,
                    out string? templateName
                )
            )
                return;

            BlueprintBuilder? subStructBuilderNew = (BlueprintBuilder?)
                this._blueprintBuilders.Find(b => b.TemplateName.Equals(templateName))?.Clone();

            if (subStructBuilderNew == null)
                return;

            beforeSet?.Invoke(subStructBuilderOld, subStructBuilderNew);
            ApplyReferenceToNew(subStructBuilderOld, subStructBuilderNew);

            if (
                SUB_STRUCTURE_MULTIPLE_USING.TryGetValue(
                    placeholderVariableName,
                    out string? appendChar
                )
            )
                @new.AppendBeforeVariableApplySpace(
                    $"%{placeholderVariableName}%",
                    subStructBuilderNew.Text.ToString(),
                    appendChar
                );
            else
                @new.SetVariableApplySpace(
                    $"%{placeholderVariableName}%",
                    subStructBuilderNew.Text.ToString()
                );
        }

        private void AdjustSelection(int oldIndex, int oldLength, int newLength)
        {
            int diff = newLength - oldLength;
            if (diff == 0)
                return;

            foreach (SelectionArea sel in this._selections)
            {
                if (
                    (
                        oldIndex > sel.Anchor
                        && oldIndex > sel.AnchorVirtualSpace
                        && oldIndex > sel.Caret
                        && oldIndex > sel.CaretVirtualSpace
                    ) || (sel.Anchor == 0 && sel.Caret == 0)
                )
                    continue;

                if (sel.Anchor >= oldIndex && oldIndex > sel.Caret)
                {
                    sel.Anchor += diff;
                    sel.Caret += diff;
                }
            }
        }
    }
}
