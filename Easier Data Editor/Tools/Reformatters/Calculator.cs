﻿using System;
using System.Data;
using Easier_Data_Editor.Tools.Reformatters.Models;

namespace Easier_Data_Editor.Tools.Reformatters
{
    internal static class Calculator
    {
        private static readonly DataTable DATA_TABLE = new DataTable();

        internal static object? CalculateFromAttribute(
            DataAttributeValueMathFunction func,
            string attrVal
        )
        {
            if (func.Type == EMathFunction.Min || func.Type == EMathFunction.Max)
            {
                string funcVal = func.Value.Substring(1, func.Value.Length - 2); // remove first and last bracket "(" / ")"
                string[] values = funcVal.Split(';');
                if (values.Length < 2)
                    return null;

                object? val1 = Calculate(
                    Constants.REGEX_GETTER_SYMBOL_PLACEHOLDER.Replace(values[0], attrVal)
                );
                object? val2 = Calculate(
                    Constants.REGEX_GETTER_SYMBOL_PLACEHOLDER.Replace(values[1], attrVal)
                );

                if (val1 == null || val2 == null)
                    return null;

                if (func.Type == EMathFunction.Min)
                    return CalculateMin(val1, val2);
                else
                    return CalculateMax(val1, val2);
            }
            else
            {
                return Calculate(
                    Constants.REGEX_GETTER_SYMBOL_PLACEHOLDER.Replace(func.Value, attrVal)
                );
            }
        }

        internal static string CalculateCeiling(object result)
        {
            if (result is float resultFloat)
                return Math.Ceiling(resultFloat).ToString();
            else if (result is double resultDbl)
                return Math.Ceiling(resultDbl).ToString();
            else if (result is decimal resultDec)
                return Math.Ceiling(resultDec).ToString();

            return result.ToString() ?? "";
        }

        internal static string CalculateFloor(object result)
        {
            if (result is float resultFloat)
                return Math.Floor(resultFloat).ToString();
            else if (result is double resultDbl)
                return Math.Floor(resultDbl).ToString();
            else if (result is decimal resultDec)
                return Math.Floor(resultDec).ToString();

            return result.ToString() ?? "";
        }

        internal static string CalculateTruncate(object result)
        {
            if (result is float resultFloat)
                return Math.Truncate(resultFloat).ToString();
            else if (result is double resultDbl)
                return Math.Truncate(resultDbl).ToString();
            else if (result is decimal resultDec)
                return Math.Truncate(resultDec).ToString();

            return result.ToString() ?? "";
        }

        internal static object CalculateMin(object result1, object result2)
        {
            if (result1 is decimal || result2 is decimal)
                return Math.Min((decimal)result1, (decimal)result2).ToString();
            else if (result1 is double || result2 is double)
                return Math.Min((double)result1, (double)result2).ToString();
            else if (result1 is float || result2 is float)
                return Math.Min((float)result1, (float)result2).ToString();
            else if (result1 is int || result2 is int)
                return Math.Min((int)result1, (int)result2).ToString();

            return 0;
        }

        internal static object CalculateMax(object result1, object result2)
        {
            if (result1 is decimal || result2 is decimal)
                return Math.Max((decimal)result1, (decimal)result2).ToString();
            else if (result1 is double || result2 is double)
                return Math.Max((double)result1, (double)result2).ToString();
            else if (result1 is float || result2 is float)
                return Math.Max((float)result1, (float)result2).ToString();
            else if (result1 is int || result2 is int)
                return Math.Max((int)result1, (int)result2).ToString();

            return 0;
        }

        internal static object? Calculate(string expression)
        {
            try
            {
                return CalculateWithException(expression);
            }
            catch (Exception)
            {
                return null;
            }
        }

        internal static object CalculateWithException(string expression) =>
            DATA_TABLE.Compute(expression, default);
    }
}
