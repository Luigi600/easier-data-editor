﻿using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Easier_Data_Editor.Tools.Reformatters.Builders;
using static Easier_Data_Editor.Tools.Reformatters.Constants;

namespace Easier_Data_Editor.Tools.Reformatters
{
    public partial class Reformatter
    {
        private void ReformatStages(bool onlySelected)
        {
            // clone later in reformatStage!!!
            BlueprintBuilder? builder = this._blueprintBuilders.Find(b =>
                b.TemplateName.Equals("stage")
            );
            if (builder == null)
                return;

            MatchCollection stagesMatch = REGEX_GETTER_STAGE.Matches(this._builder.ToString());

            for (int i = stagesMatch.Count - 1; i >= 0; --i)
            {
                Match stageMatch = stagesMatch[i];
                if (
                    onlySelected
                    && !this._selections.Exists(sel =>
                        stageMatch.Index >= sel.Anchor && stageMatch.Index <= sel.Caret
                    )
                )
                    continue; // only in selection and no selection contains the frame -> skip THIS frame

                this.ReformatStage(stageMatch, builder);
            }
        }

        private void ReformatStage(Match stage, BlueprintBuilder builder)
        {
            StringBuilder originalStage = new StringBuilder(stage.Value);
            BlueprintBuilder @new = (BlueprintBuilder)builder.Clone();

            // remove phases
            MatchCollection phases = REGEX_GETTER_PHASE.Matches(originalStage.ToString());
            for (int i = phases.Count - 1; i >= 0; --i)
            {
                Match phase = phases[i];
                originalStage.Remove(phase.Index, phase.Length);
            }

            OriginalBuilder old = new OriginalBuilder(originalStage);

            ApplyReferenceToNew(old, @new, false);

            foreach (Match phase in phases)
            {
                this.ReformatPhase(phase, @new);
            }

            @new.RemoveUnusedStuff();

            this.AdjustSelection(stage.Index, stage.Length, @new.Text.Length);
            this._builder.Remove(stage.Index, stage.Length);
            this._builder.Insert(stage.Index, @new.Text.ToString());
        }

        private void ReformatPhase(Match phase, BlueprintBuilder @new)
        {
            StringBuilder phaseText = new StringBuilder(phase.Value);
            MatchCollection spawns = REGEX_GETTER_SPAWN.Matches(phase.Value);
            for (int i = spawns.Count - 1; i >= 0; --i)
            {
                Match point = spawns[i];
                phaseText.Remove(point.Index, point.Length);
            }

            this.ReformatSubStructure(
                "phase",
                @new,
                phaseText,
                (oldPhase, newPhase) =>
                {
                    foreach (Match spawn in spawns)
                    {
                        this.ReformatSubStructure(
                            "spawn",
                            newPhase,
                            new OriginalSpawnBuilder(spawn.Value)
                        );
                    }
                }
            );
        }
    }
}
