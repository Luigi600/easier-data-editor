﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Easier_Data_Editor.Tools.Reformatters.Builders;
using static Easier_Data_Editor.Tools.Reformatters.Constants;

namespace Easier_Data_Editor.Tools.Reformatters
{
    public partial class Reformatter
    {
        /// <summary>
        /// Gets the special attributes of the bitmap header and removes them.
        ///
        /// This step is necessary because LF2 does not read a "file(%d-%d)"-line line by line. To have "single-line" support in the reformatter,
        /// the attributes in the header which do not have colons must be handled separately. Otherwise they will be counted as attribute value.
        /// </summary>
        /// <param name="originalHeader"></param>
        /// <returns></returns>
        private static List<Match> GetAndRemoveAllSpecialAttributes(
            ref StringBuilder originalHeader
        )
        {
            List<Match> specialAttributes = REGEX_GETTER_ATTRIBUTES_BITMAP_HEADER
                .Matches(originalHeader.ToString())
                .OfType<Match>()
                .Where(m => m.Groups.Count > 2)
                .Reverse()
                .ToList();
            foreach (Match attr in specialAttributes)
                originalHeader.Remove(attr.Index, attr.Length);

            specialAttributes.Reverse();
            return specialAttributes;
        }

        private void ReformatBitmapHeaders(bool onlySelected)
        {
            BlueprintBuilder? builder = this._blueprintBuilders.Find(b =>
                b.TemplateName.Equals("bitmap")
            );
            if (builder == null)
                return;

            MatchCollection headers = REGEX_GETTER_BITMAP_HEADERS.Matches(this._builder.ToString());

            for (int i = headers.Count - 1; i >= 0; --i)
            {
                Match header = headers[i];
                if (
                    onlySelected
                    && !this._selections.Exists(sel =>
                        header.Index >= sel.Anchor && header.Index <= sel.Caret
                    )
                )
                    continue; // only in selection and no selection contains the frame -> skip THIS frame

                this.ReformatBitmapHeader(header, builder);
            }
        }

        private void ReformatBitmapHeader(Match header, BlueprintBuilder builder)
        {
            StringBuilder originalHeader = new StringBuilder(header.Value);
            BlueprintBuilder @new = (BlueprintBuilder)builder.Clone();

            List<Match> specialAttributes = GetAndRemoveAllSpecialAttributes(ref originalHeader);

            // remove file rows
            MatchCollection fileRows = REGEX_GETTER_BITMAP_HEADER_FILE.Matches(
                originalHeader.ToString()
            );
            for (int i = fileRows.Count - 1; i >= 0; --i)
            {
                Match @struct = fileRows[i];
                originalHeader.Remove(@struct.Index, @struct.Length);
            }

            OriginalBitmapHeaderBuilder old = new OriginalBitmapHeaderBuilder(originalHeader);
            old.AddSpecialAttributes(specialAttributes);

            ApplyReferenceToNew(
                old,
                @new,
                false,
                Easier_Data_Editor.Constants.CHAR_NEW_LINE_LF.ToString()
            );

            foreach (Match fileRow in fileRows)
            {
                this.ReformatSubStructure(
                    "bitmapHeaderFileRows",
                    @new,
                    fileRow.Value,
                    (oldRow, newRow) =>
                    {
                        Match infos = REGEX_GETTER_BITMAP_HEADER_FILE_ROW_INFO.Match(
                            oldRow.Text.ToString()
                        );
                        if (!infos.Success || infos.Groups.Count <= 1)
                            return;

                        newRow.SetVariable(
                            BLUEPRINT_VARIABLE_BITMAP_HEADER_INDICES,
                            infos.Groups[1].Value
                        );
                    }
                );
            }

            @new.RemoveUnusedStuff();

            this.AdjustSelection(header.Index, header.Length, @new.Text.Length);
            this._builder.Remove(header.Index, header.Length);
            this._builder.Insert(header.Index, @new.Text.ToString());
        }
    }
}
