﻿using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Easier_Data_Editor.Tools.Reformatters.Builders;
using Easier_Data_Editor.Utilities;
using static Easier_Data_Editor.Tools.Reformatters.Constants;

namespace Easier_Data_Editor.Tools.Reformatters
{
    public partial class Reformatter
    {
        private static void HandleSpecialBlueprintVariablesFrameName(
            ref OriginalFrameBuilder originalFrame,
            ref BlueprintFrameBuilder newFrame
        )
        {
            string frameName = RegexUtil.GetRegexValue(
                originalFrame.Text.ToString(),
                Easier_Data_Editor.Constants.REGEX_PATTERN_FRAME_NAME,
                RegexOptions.IgnoreCase
            );

            if (originalFrame.FrameNameComment != null)
            {
                if (newFrame.FrameNameComment != null)
                {
                    newFrame.ReplaceText(
                        newFrame.FrameNameComment.Index,
                        newFrame.FrameNameComment.Length,
                        newFrame.FrameNameComment.Value.ToString()
                    );
                    newFrame.InsertText(
                        newFrame.FrameNameComment.RealContent.EndIndex,
                        originalFrame.FrameNameComment.RealContent.Value.ToString()
                    );
                }
                else
                {
                    newFrame.AppendAfterVariable(
                        BLUEPRINT_VARIABLE_FRAME_NAME,
                        $" #{originalFrame.FrameNameComment.RealContent.Value}"
                    );
                }
            }
            else if (newFrame.FrameNameComment != null)
            {
                newFrame.RemoveText(
                    newFrame.FrameNameComment.Index,
                    newFrame.FrameNameComment.Length
                );
            }

            newFrame.SetVariable(BLUEPRINT_VARIABLE_FRAME_NAME, frameName);
        }

        private static void HandleSpecialBlueprintVariablesFrameID(
            ref BlueprintFrameBuilder newFrame,
            int frameID
        )
        {
            newFrame.SetVariable(BLUEPRINT_VARIABLE_FRAME_ID, frameID.ToString());
        }

        private void ReformatFrames(bool onlySelected)
        {
            BlueprintBuilder? builder = this._blueprintBuilders.Find(b =>
                b.TemplateName.Equals("frame")
            );
            if (!(builder is BlueprintFrameBuilder frameBuilder))
                return;

            MatchCollection framesMatch = Regex.Matches(
                this._builder.ToString(),
                Easier_Data_Editor.Constants.REGEX_PATTERN_FRAME_CONTENT,
                RegexOptions.IgnoreCase | RegexOptions.Singleline
            );

            for (int i = framesMatch.Count - 1; i >= 0; --i)
            {
                Match frameMatch = framesMatch[i];
                if (
                    onlySelected
                    && !this._selections.Exists(sel =>
                        frameMatch.Index >= sel.Anchor && frameMatch.Index <= sel.Caret
                    )
                )
                    continue; // only in selection and no selection contains the frame -> skip THIS frame

                int frameID = RegexUtil.GetRegexValueInteger(
                    frameMatch.Value,
                    Easier_Data_Editor.Constants.REGEX_PATTERN_FRAME_ID,
                    RegexOptions.IgnoreCase,
                    -1
                );
                if (frameID >= 0)
                    this.ReformatFrame(frameMatch, frameID, frameBuilder);
            }
        }

        private void ReformatFrame(Match frame, int frameID, BlueprintFrameBuilder builder)
        {
            StringBuilder originalFrame = new StringBuilder(frame.Value);
            BlueprintFrameBuilder @new = (BlueprintFrameBuilder)builder.Clone();

            // remove sub structures from frame
            MatchCollection points = REGEX_GETTER_FRAME_ELEMENTS.Matches(originalFrame.ToString());
            for (int i = points.Count - 1; i >= 0; --i)
            {
                Match point = points[i];
                originalFrame.Remove(point.Index, point.Length);
            }

            OriginalFrameBuilder old = new OriginalFrameBuilder(originalFrame);

            HandleSpecialBlueprintVariablesFrameName(ref old, ref @new);
            HandleSpecialBlueprintVariablesFrameID(ref @new, frameID);

            ApplyReferenceToNew(old, @new, false);

            foreach (Match match in points)
            {
                string subStructName = match.Groups[1].Value;
                this.ReformatSubStructure(subStructName, @new, match.Value);
            }

            @new.RemoveUnusedStuff();

            this.AdjustSelection(frame.Index, frame.Length, @new.Text.Length);
            this._builder.Remove(frame.Index, frame.Length);
            this._builder.Insert(frame.Index, @new.Text.ToString());
        }
    }
}
