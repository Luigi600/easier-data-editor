﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Easier_Data_Editor.Models;
using Easier_Data_Editor.Tools.Reformatters.Builders;
using Easier_Data_Editor.Utilities;

namespace Easier_Data_Editor.Tools.Reformatters
{
    public partial class Reformatter
    {
        private static readonly Regex REGEX_ELEMENT = new Regex(
            @"\s*?<[A-z_\-]+>",
            RegexOptions.IgnoreCase
        );

        public void ReformatDataTxt()
        {
            BlueprintBuilder? builder = (BlueprintBuilder?)
                this._blueprintBuilders.Find(b => b.TemplateName.Equals("dataTxt"))?.Clone();
            if (builder == null)
                return;

            string text = this._builder.ToString();
            MatchCollection objects = Constants.REGEX_GETTER_DATA_TXT_OBJECTS.Matches(text);
            MatchCollection fileEditings = Constants.REGEX_GETTER_DATA_TXT_FILE_EDITING.Matches(
                text
            );
            MatchCollection backgrounds = Constants.REGEX_GETTER_DATA_TXT_BACKGROUNDS.Matches(text);

            List<Match> allStuff = new List<Match>();
            allStuff.AddRange(objects.OfType<Match>());
            allStuff.AddRange(fileEditings.OfType<Match>());
            allStuff.AddRange(backgrounds.OfType<Match>());

            allStuff.Sort((m1, m2) => m1.Index - m2.Index);

            for (int i = allStuff.Count - 1; i >= 0; --i)
            {
                Match stuff = allStuff[i];
                this._builder.Remove(stuff.Index, stuff.Length);
            }

            OriginalBuilder old = new OriginalBuilder(this._builder);
            ApplyReferenceToNew(
                old,
                builder,
                false,
                Easier_Data_Editor.Constants.CHAR_NEW_LINE_LF.ToString()
            );

            this.ReformatDataTxtList("objects", objects, builder);
            this.ReformatDataTxtListFileEditing(fileEditings, builder);
            this.ReformatDataTxtList("backgrounds", backgrounds, builder);

            builder.RemoveUnusedStuff();

            this.AdjustSelection(0, this._builder.Length, builder.Text.Length);
            this._builder.Remove(0, this._builder.Length);
            this._builder.Insert(0, builder.Text.ToString());
        }

        private void ReformatDataTxtList(
            string subStructName,
            MatchCollection objects,
            BlueprintBuilder builder
        )
        {
            foreach (Match obj in objects)
            {
                List<FakeMatch> lines = RegexUtil
                    .GetLinesWithEmptyLines(obj.Value)
                    .Where(m => !REGEX_ELEMENT.IsMatch(m.Value))
                    .ToList();

                foreach (FakeMatch line in lines)
                {
                    this.ReformatSubStructure(subStructName, builder, line.Value);
                }
            }
        }

        private void ReformatDataTxtListFileEditing(
            MatchCollection objects,
            BlueprintBuilder builder
        )
        {
            foreach (Match obj in objects)
            {
                StringBuilder objVal = new StringBuilder(obj.Value);
                List<FakeMatch> lines = RegexUtil
                    .GetLinesWithEmptyLines(obj.Value)
                    .Where(m => !REGEX_ELEMENT.IsMatch(m.Value))
                    .ToList();

                for (int i = lines.Count - 1; i >= 0; --i)
                {
                    FakeMatch line = lines[i];
                    objVal.Remove(line.Index, line.Length);
                }

                foreach (FakeMatch line in lines)
                {
                    this.ReformatSubStructure(
                        "fileEditing",
                        builder,
                        new OriginalDataTxtFileEditingBuilder(line.Value),
                        (oldFileEditing, newFileEditing) =>
                        {
                            if (
                                oldFileEditing
                                    is OriginalDataTxtFileEditingBuilder fileEditingBuilder
                                && fileEditingBuilder.File != null
                            )
                                newFileEditing.SetVariable(
                                    Constants.BLUEPRINT_VARIABLE_DATA_TXT_FILE_EDITING_FILE_PATH,
                                    fileEditingBuilder.File.Value.ToString()
                                );
                        }
                    );
                }
            }
        }
    }
}
