﻿using System;
using Easier_Data_Editor.Tools.Reformatters.Models;

namespace Easier_Data_Editor.Tools.Reformatters
{
    public sealed class AttributeAddEventArgs : EventArgs
    {
        public DataAttributeValue DataAttributeValue { get; }
        public int Index { get; }
        public string Value { get; }

        public AttributeAddEventArgs(DataAttributeValue val, int index, string value)
        {
            this.DataAttributeValue = val;
            this.Index = index;
            this.Value = value;
        }
    }
}
