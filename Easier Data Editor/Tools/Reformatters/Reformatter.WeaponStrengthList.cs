﻿using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Easier_Data_Editor.Tools.Reformatters.Builders;
using static Easier_Data_Editor.Tools.Reformatters.Constants;

namespace Easier_Data_Editor.Tools.Reformatters
{
    public partial class Reformatter
    {
        private void ReformatWeaponStrengthLists(bool onlySelected)
        {
            BlueprintBuilder? builder = this._blueprintBuilders.Find(b =>
                b.TemplateName.Equals("weaponStrengthList")
            );
            if (builder == null)
                return;

            MatchCollection lists = REGEX_GETTER_WEAPON_STRENGTH_LIST.Matches(
                this._builder.ToString()
            );

            for (int i = lists.Count - 1; i >= 0; --i)
            {
                Match list = lists[i];
                if (
                    onlySelected
                    && !this._selections.Exists(sel =>
                        list.Index >= sel.Anchor && list.Index <= sel.Caret
                    )
                )
                    continue; // only in selection and no selection contains the frame -> skip THIS frame

                this.ReformatWeaponStrengthList(list, builder);
            }
        }

        private void ReformatWeaponStrengthList(Match list, BlueprintBuilder builder)
        {
            StringBuilder originalList = new StringBuilder(list.Value);
            BlueprintBuilder @new = (BlueprintBuilder)builder.Clone();

            // remove sub structures from header
            MatchCollection entries = REGEX_GETTER_WEAPON_STRENGTH_LIST_ENTRIES.Matches(list.Value);
            for (int i = entries.Count - 1; i >= 0; --i)
            {
                Match entry = entries[i];
                originalList.Remove(entry.Index, entry.Length);
            }

            OriginalBuilder old = new OriginalBuilder(originalList);

            // FindAndWriteAttributes(ref originalHeader, ref newHeader, Easier_Data_Editor.Constants.CHAR_NEW_LINE_LF);
            ApplyReferenceToNew(
                old,
                @new,
                false,
                Easier_Data_Editor.Constants.CHAR_NEW_LINE_LF.ToString()
            );

            foreach (Match entry in entries)
            {
                this.ReformatSubStructure("weaponStrengthListEntry", @new, entry.Value);
            }

            @new.RemoveUnusedStuff();

            this.AdjustSelection(list.Index, list.Length, @new.Text.Length);
            this._builder.Remove(list.Index, list.Length);
            this._builder.Insert(list.Index, @new.Text.ToString());
        }
    }
}
