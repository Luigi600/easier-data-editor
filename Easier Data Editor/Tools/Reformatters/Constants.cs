﻿using System.Text.RegularExpressions;

namespace Easier_Data_Editor.Tools.Reformatters
{
    internal static class Constants
    {
        internal const string BLUEPRINT_PLACEHOLDER_VALUE = "$";
        internal const string BLUEPRINT_PLACEHOLDER_OPTIONAL_VALUE = "?";
        internal const char BLUEPRINT_PLACEHOLDER_SPECIAL_FUNCTION_SEPARATOR = '|';
        internal const string BLUEPRINT_VARIABLE_FRAME_NAME = "%FRAME_NAME%";
        internal const string BLUEPRINT_VARIABLE_FRAME_ID = "%FRAME_ID%";
        internal const string BLUEPRINT_VARIABLE_BITMAP_HEADER_INDICES = "%INDICES%";
        internal const string BLUEPRINT_VARIABLE_BACKGROUND_LAYER_FILE = "%LAYER_IMAGE%";
        internal const string BLUEPRINT_VARIABLE_DATA_TXT_FILE_EDITING_FILE_PATH = "%FILE%";
        internal const string BLUEPRINT_VARIABLE_NAME_OTHER = "%others%";

        internal const string REGEX_PATTERN_ATTRIBUTE_NAME = @"[A-z_-]{1,}\d*|file\([^\(\)]+\)";
        internal const string REGEX_PATTERN_ATTRIBUTE_VALUE = @"[^:<>\s\n\r]+";
        internal const string REGEX_PATTERN_TAG = @"\<(?:boss|soldier)\>";
        internal const string REGEX_PATTERN_OPTIONAL_PADDING_OPTION = @"(?:\|\d+(?:\|[LRClrc])?)?";
        internal const string REGEX_PATTERN_ATTRIBUTE_VALUE_BLUEPRINT =
            @"(?:(?!%)"
            + REGEX_PATTERN_ATTRIBUTE_VALUE
            + "(?<!%)"
            + REGEX_PATTERN_OPTIONAL_PADDING_OPTION
            + ")"; // template string doesn't work...

        internal const string REGEX_PATTERN_GETTER_BITMAP_HEADER_FILE =
            @"file\([^()]*?\):((?!file).)*";

        /// <summary>
        /// Specifies a regex consisting of two groups. The first group contains the attribute name, with colon if present. The second group has the values of the attribute as "captures".
        /// </summary>
        //                                                                                     | - first group: contains the attribute name (must have a length > 0, ":" is optional (see bitmap header))
        //                                                                                     |
        //                                                                                     |
        //                                                                                     |                                 | - each attribute value must be preceded by a space, but the space is not a part of result (non-capturing group)
        //                                                                                     |                                 |
        //                                                                                     |                                 |                         | - allow many chars coz sound file
        //                                                                                     |                                 |                         |
        //                                                                                     |                                 |                         |                           | - must end with a new line/space OR the end of the file
        internal static readonly Regex REGEX_GETTER_ATTRIBUTES = new Regex(
            $"(?<=^|\\s)\\b((?:{REGEX_PATTERN_ATTRIBUTE_NAME}):?)(?:(?:[^\\S\\r\\n]+)({REGEX_PATTERN_ATTRIBUTE_VALUE}))+(?:(?=[\\n\\r\\s])|$)",
            RegexOptions.IgnoreCase
        );
        internal static readonly Regex REGEX_GETTER_ATTRIBUTES_BLUEPRINT = new Regex(
            $"(?<=^|\\s)\\b((?:{REGEX_PATTERN_ATTRIBUTE_NAME}):?{REGEX_PATTERN_OPTIONAL_PADDING_OPTION})(?:(?:[^\\S\\r\\n]+)({REGEX_PATTERN_ATTRIBUTE_VALUE_BLUEPRINT}))+(?:(?=[\\n\\r\\s])|$)",
            RegexOptions.IgnoreCase
        );

        //                                                                                                           | - attribute name must have an underscore
        //                                                                                                           |
        //                                                                                                           |                                                            | - just ONE value => NO "+"
        internal static readonly Regex REGEX_GETTER_ATTRIBUTES_BITMAP_HEADER = new Regex(
            $"(?<=^|\\s)\\b([A-z]+(?:_[A-z]+)+)(?:(?:[^\\S\\r\\n]+)({REGEX_PATTERN_ATTRIBUTE_VALUE}))(?:(?=[\\n\\r\\s])|$)",
            RegexOptions.IgnoreCase
        );

        internal static readonly Regex REGEX_GETTER_TAG = new Regex(
            $"(?<=^|\\s)((?:{REGEX_PATTERN_TAG}))(?:(?=[\\n\\r\\s])|$)",
            RegexOptions.IgnoreCase
        );
        internal static readonly Regex REGEX_GETTER_TAG_BLUEPRINT = new Regex(
            $"(?<=^|\\s)((?:{REGEX_PATTERN_TAG}){REGEX_PATTERN_OPTIONAL_PADDING_OPTION})(?:(?=[\\n\\r\\s])|$)",
            RegexOptions.IgnoreCase
        );

        internal static readonly Regex REGEX_GETTER_PLACEHOLDER_VARIABLE = new Regex(
            $"(?:^|([^\\S\\n\\r]+))(%[^%\\s\\n\\r]+%{REGEX_PATTERN_OPTIONAL_PADDING_OPTION})(?=(?:$|\\s))",
            RegexOptions.IgnoreCase | RegexOptions.Multiline
        );
        internal static readonly Regex REGEX_GETTER_PLACEHOLDER_VARIABLE_FILE_INDICES = new Regex(
            BLUEPRINT_VARIABLE_BITMAP_HEADER_INDICES,
            RegexOptions.IgnoreCase | RegexOptions.Multiline
        );
        internal static readonly Regex REGEX_GETTER_MATH_FUNCTION = new Regex(
            @"(CEILING|FLOOR|TRUNCATE|MIN|MAX|LIMIT)\(",
            RegexOptions.IgnoreCase
        );
        internal static readonly Regex REGEX_GETTER_SYMBOL_PLACEHOLDER = new Regex(
            @"(\$|\?)",
            RegexOptions.IgnoreCase
        );

        internal static readonly Regex REGEX_EQUALS_FILE = new Regex(
            @"file\([^\(\)]+\)",
            RegexOptions.IgnoreCase
        );

        internal static readonly Regex REGEX_GETTER_COMMENT_BLUEPRINT = new Regex(
            @"([^\S\n\r]+)(#[^\n\r]*)",
            RegexOptions.IgnoreCase
        );
        internal static readonly Regex REGEX_GETTER_COMMENT_ORIGINAL = new Regex(
            @"((?:[^\S\n\r]+|^)#[^\S\n\r]*)([^\n\r]*)",
            RegexOptions.IgnoreCase
        );

        internal static readonly Regex REGEX_GETTER_FRAME_COMMENT_AFTER_NAME_2 = new Regex(
            @"(?<=\<frame\>[^#]+)([^\S\r\n]+#)([\^S\n\r]*[^\n\r]+)",
            RegexOptions.IgnoreCase
        );

        internal static readonly Regex REGEX_GETTER_FRAME_ELEMENTS = new Regex(
            @"\s*(bpoint|wpoint|cpoint|opoint|bdy|itr):([^_]+?)(?:\s+[^_]+_end:(?=[\n\r]|$))",
            RegexOptions.IgnoreCase | RegexOptions.Singleline
        );

        internal static readonly Regex REGEX_GETTER_FRAME_COMMENT_AFTER_NAME_POS = new Regex(
            $"(?<={BLUEPRINT_VARIABLE_FRAME_NAME}{REGEX_PATTERN_OPTIONAL_PADDING_OPTION})([^\\S\\n\\r]*)(#[^\\n\\r]*)",
            RegexOptions.IgnoreCase
        );

        internal static readonly Regex REGEX_GETTER_BITMAP_HEADERS = new Regex(
            @"<bmp_begin>[^<>]*?<bmp_end>",
            RegexOptions.IgnoreCase | RegexOptions.Singleline
        );
        internal static readonly Regex REGEX_GETTER_BITMAP_HEADER_FILE = new Regex(
            REGEX_PATTERN_GETTER_BITMAP_HEADER_FILE,
            RegexOptions.IgnoreCase
        );
        internal static readonly Regex REGEX_GETTER_BITMAP_HEADER_FILE_ROW_INFO = new Regex(
            @"file\(([^\(\)]*)\):"
        );

        internal static readonly Regex REGEX_GETTER_WEAPON_STRENGTH_LIST = new Regex(
            @"<weapon_strength_list>[^<>]*?<weapon_strength_list_end>",
            RegexOptions.IgnoreCase | RegexOptions.Singleline
        );
        internal static readonly Regex REGEX_GETTER_WEAPON_STRENGTH_LIST_ENTRIES = new Regex(
            @"entry:[^<>]+?(?=entry:|<|[\n\r]$)",
            RegexOptions.IgnoreCase | RegexOptions.Singleline
        );

        internal static readonly Regex REGEX_GETTER_BACKGROUND_LAYERS = new Regex(
            @"layer:.*?layer_end",
            RegexOptions.IgnoreCase | RegexOptions.Singleline
        );
        internal static readonly Regex REGEX_GETTER_BACKGROUND_LAYER_PATH = new Regex(
            @"(?<=layer:.*?[\n\r]+.*?)[^\s\n\r]+\.[^\n\r]{1,5}",
            RegexOptions.IgnoreCase
        );

        internal static readonly Regex REGEX_GETTER_STAGE = new Regex(
            @"<stage>.*?<stage_end>",
            RegexOptions.IgnoreCase | RegexOptions.Singleline
        );
        internal static readonly Regex REGEX_GETTER_PHASE = new Regex(
            @"<phase>.*?<phase_end>",
            RegexOptions.IgnoreCase | RegexOptions.Singleline
        );
        internal static readonly Regex REGEX_GETTER_SPAWN = new Regex(
            @"[^\n\r]*id:\s+-?\d+[^\n\r]+",
            RegexOptions.IgnoreCase
        );

        internal static readonly Regex REGEX_GETTER_DATA_TXT_OBJECTS = new Regex(
            @"<object>.*?<object_end>",
            RegexOptions.IgnoreCase | RegexOptions.Singleline
        );
        internal static readonly Regex REGEX_GETTER_DATA_TXT_FILE_EDITING = new Regex(
            @"<file_editing>.*?<file_editing_end>",
            RegexOptions.IgnoreCase | RegexOptions.Singleline
        );
        internal static readonly Regex REGEX_GETTER_DATA_TXT_BACKGROUNDS = new Regex(
            @"<background>.*?<background_end>",
            RegexOptions.IgnoreCase | RegexOptions.Singleline
        );
    }
}
