﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Easier_Data_Editor.Tools
{
    public static class IdsAnalyser
    {
        public static Dictionary<int, int> GetUsedFrameIDs(string text)
        {
            Dictionary<int, int> result = new Dictionary<int, int>();
            foreach (
                Match mtch in Regex.Matches(
                    text,
                    @"(?<=(^(?!#)|\s+)<frame>\s*)([0-9]+)",
                    RegexOptions.IgnoreCase | RegexOptions.Multiline
                )
            )
            {
                try
                {
                    if (mtch.Groups.Count >= 1 && int.TryParse(mtch.Groups[0].Value, out int id))
                        result.Add(mtch.Index, id);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }
            }

            return new Dictionary<int, int>(result);
        }

        public static HashSet<int> GetUniqueUsedFrameIDs(string text)
        {
            List<int> result = GetUsedFrameIDs(text).Values.ToList();
            result.Sort((item1, item2) => item1 - item2);

            return new HashSet<int>(result);
        }

        public static HashSet<int> GetFreeIDs(string text) =>
            new HashSet<int>(
                Enumerable
                    .Range(0, LittleFighter2.Constants.MAXIMUM_FRAMES)
                    .Except(GetUniqueUsedFrameIDs(text))
            );

        public static HashSet<int> GetFreeIDsTryInRow(
            string text,
            int countOfFrames,
            int startFrame = 0
        )
        {
            List<Tuple<int, int>> freeIDs = GetFreeIDsAndLengthInRow(text);
            // try to get free ids in row
            Tuple<int, int>? freeID =
                freeIDs.Find(f => f.Item1 >= startFrame && f.Item2 >= countOfFrames)
                ?? freeIDs.LastOrDefault(f => f.Item2 >= countOfFrames);
            if (freeID != null)
                return new HashSet<int>(Enumerable.Range(freeID.Item1, freeID.Item2));

            // get free ids in parts (try to start by frame "startFrame")
            int startID = GetBestPossibleFreeIDsFromID(freeIDs, countOfFrames, startFrame);

            // get all free ids and return
            List<int> result = new List<int>();
            foreach (Tuple<int, int> tuple in freeIDs.Where(f => f.Item1 >= startID))
            {
                result.AddRange(Enumerable.Range(tuple.Item1, tuple.Item2));
                if (result.Count >= countOfFrames)
                    break;
            }

            return new HashSet<int>(result);
        }

        public static List<Tuple<int, int>> GetFreeIDsAndLengthInRow(string text)
        {
            List<Tuple<int, int>> result = new List<Tuple<int, int>>();

            foreach (int freeID in GetFreeIDs(text))
            {
                if (result.Count > 0)
                {
                    Tuple<int, int> lastItem = result[result.Count - 1];
                    if (lastItem.Item1 + lastItem.Item2 == freeID)
                    {
                        result[result.Count - 1] = new Tuple<int, int>(
                            lastItem.Item1,
                            lastItem.Item2 + 1
                        );
                        continue;
                    }
                }

                result.Add(new Tuple<int, int>(freeID, 1));
            }

            return result;
        }

        public static int GetBestPossibleFreeIDsFromID(
            List<Tuple<int, int>> freeIDs,
            int countOfFrames,
            int startFrame = 0
        )
        {
            int startID = startFrame;
            List<Tuple<int, int>> freeIDsAfterStart = freeIDs
                .Where(f => f.Item1 >= startFrame)
                .ToList();
            int length = freeIDsAfterStart.Select(f => f.Item2).Sum();
            // length is too small, add also previous ids from the list
            if (length < countOfFrames)
            {
                List<Tuple<int, int>> reverseFreeIDs = new List<Tuple<int, int>>(freeIDs);
                reverseFreeIDs.Sort((item1, item2) => item2.Item1 - item1.Item1);

                foreach (Tuple<int, int> tuple in reverseFreeIDs.Except(freeIDsAfterStart))
                {
                    startID = tuple.Item1;
                    length += tuple.Item2;

                    // enough ids -> STOP
                    if (length >= countOfFrames)
                        break;
                }
            }

            return startID;
        }
    }
}
