﻿using System;
using System.Text.RegularExpressions;
using Easier_Data_Editor.Utilities;

namespace Easier_Data_Editor.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class LF2DataWordPropertyAttribute : LF2DataRegexPropertyAttribute
    {
        public LF2DataWordPropertyAttribute(string word)
            : this(word, @"[^\s]+") { }

        public LF2DataWordPropertyAttribute(string word, RegexOptions regexOptions)
            : this(word, @"[^\s]+", regexOptions) { }

        public LF2DataWordPropertyAttribute(
            string word,
            string pattern,
            RegexOptions regexOptions = RegexOptions.IgnoreCase
        )
            : base(RegexUtil.GetSingleWordRegexFromWordAndValuePattern(word, pattern, regexOptions))
        { }
    }
}
