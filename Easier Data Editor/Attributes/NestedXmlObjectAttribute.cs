﻿using System;

namespace Easier_Data_Editor.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class NestedXmlObjectAttribute : Attribute
    {
        public NestedXmlObjectAttribute() { }
    }
}
