﻿using System;
using Easier_Data_Editor.Attributes.Interfaces;

namespace Easier_Data_Editor.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class LF2DataTransformAttribute : Attribute, ITransform
    {
        public string TransformMethod { get; set; }

        public LF2DataTransformAttribute(string method)
        {
            this.TransformMethod = method;
        }
    }
}
