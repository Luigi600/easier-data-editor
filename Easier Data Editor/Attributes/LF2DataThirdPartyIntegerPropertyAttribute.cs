﻿using System;
using System.Text.RegularExpressions;
using Easier_Data_Editor.Utilities;

namespace Easier_Data_Editor.Attributes
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = true)]
    public class LF2DataThirdPartyIntegerPropertyAttribute : LF2DataThirdPartyRegexPropertyAttribute
    {
        public LF2DataThirdPartyIntegerPropertyAttribute(
            string word,
            bool negativePossible = false,
            RegexOptions options = RegexOptions.IgnoreCase
        )
            : base(
                negativePossible
                    ? RegexUtil.GetSingleWordIntegerNegativeRegexFromWordAndValuePattern(
                        word,
                        options
                    )
                    : RegexUtil.GetSingleWordIntegerRegexFromWordAndValuePattern(word, options)
            ) { }
    }
}
