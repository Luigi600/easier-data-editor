﻿using System;
using System.Text.RegularExpressions;

namespace Easier_Data_Editor.Attributes
{
    public abstract class LF2DataPropertyAttribute : Attribute
    {
        public Regex Pattern { get; protected set; }

        public Type? TargetType { get; protected set; } = null;

        protected LF2DataPropertyAttribute(Regex pattern)
        {
            this.Pattern = pattern;
        }
    }
}
