﻿using System;
using System.Text.RegularExpressions;
using Easier_Data_Editor.Utilities;

namespace Easier_Data_Editor.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class LF2DataIntegerPropertyAttribute : LF2DataRegexPropertyAttribute
    {
        public LF2DataIntegerPropertyAttribute(
            string word,
            bool negativePossible = false,
            RegexOptions options = RegexOptions.IgnoreCase
        )
            : base(
                negativePossible
                    ? RegexUtil.GetSingleWordIntegerNegativeRegexFromWordAndValuePattern(
                        word,
                        options
                    )
                    : RegexUtil.GetSingleWordIntegerRegexFromWordAndValuePattern(word, options)
            ) { }
    }
}
