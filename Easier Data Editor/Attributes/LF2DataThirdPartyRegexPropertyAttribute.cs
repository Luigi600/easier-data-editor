﻿using System;
using System.Text.RegularExpressions;
using Easier_Data_Editor.Attributes.Interfaces;

namespace Easier_Data_Editor.Attributes
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = true)]
    public class LF2DataThirdPartyRegexPropertyAttribute
        : LF2DataPropertyAttribute,
            IDynamicTargetType,
            ITargetProperty,
            ITransform
    {
        public string? DynamicTargetTypeMethod { get; set; } = null;

        public string? TargetProperty { get; set; } = null;

        public string? TransformMethod { get; set; } = null;

        public LF2DataThirdPartyRegexPropertyAttribute(Regex pattern)
            : base(pattern) { }
    }
}
