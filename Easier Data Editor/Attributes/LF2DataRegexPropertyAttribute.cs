﻿using System;
using System.Text.RegularExpressions;

namespace Easier_Data_Editor.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class LF2DataRegexPropertyAttribute : LF2DataPropertyAttribute
    {
        public LF2DataRegexPropertyAttribute(string pattern, RegexOptions regexOptions)
            : this(new Regex(pattern, regexOptions)) { }

        public LF2DataRegexPropertyAttribute(Regex pattern)
            : base(pattern) { }
    }
}
