﻿using System;
using Easier_Data_Editor.Attributes.Interfaces;

namespace Easier_Data_Editor.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class LF2DataDynamicTargetTypeAttribute : Attribute, IDynamicTargetType
    {
        public string DynamicTargetTypeMethod { get; set; }

        public LF2DataDynamicTargetTypeAttribute(string method)
        {
            this.DynamicTargetTypeMethod = method;
        }
    }
}
