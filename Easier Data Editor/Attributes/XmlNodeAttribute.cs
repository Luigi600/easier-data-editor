﻿using System;

namespace Easier_Data_Editor.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class XmlNodeAttribute : Attribute
    {
        public virtual string XMLNodeName { get; }

        public XmlNodeAttribute(string xmlNodeName)
        {
            this.XMLNodeName = xmlNodeName;
        }
    }
}
