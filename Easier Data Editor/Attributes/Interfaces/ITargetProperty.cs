﻿namespace Easier_Data_Editor.Attributes.Interfaces
{
    public interface ITargetProperty
    {
        string? TargetProperty { get; }
    }
}
