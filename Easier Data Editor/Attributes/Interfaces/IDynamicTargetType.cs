﻿namespace Easier_Data_Editor.Attributes.Interfaces
{
    public interface IDynamicTargetType
    {
        string? DynamicTargetTypeMethod { get; }
    }
}
