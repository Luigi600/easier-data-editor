﻿namespace Easier_Data_Editor.Attributes.Interfaces
{
    public interface ITransform
    {
        string? TransformMethod { get; }
    }
}
