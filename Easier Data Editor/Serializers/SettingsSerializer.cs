﻿using System.Xml;
using Easier_Data_Editor.Environments;

namespace Easier_Data_Editor.Serializers
{
    public static class SettingsSerializer
    {
        public static void WriteToXML(UserSettings settings)
        {
            XmlWriter xmlWriter = XmlWriter.Create(
                Application.SETTINGS_FILE_PATH,
                new XmlWriterSettings() { Indent = true, OmitXmlDeclaration = false }
            );
            xmlWriter.WriteStartDocument();
            xmlWriter.WriteStartElement(Application.SETTINGS_XML_NODE);
            xmlWriter.WriteAttributeString(
                Application.SETTINGS_XML_VERSION_ATTRIBUTE,
                Application.SETTINGS_XML_VERSION.ToString()
            );
            XmlSerializer.WriteToXML(settings, ref xmlWriter);
            xmlWriter.WriteEndElement();
            xmlWriter.WriteEndDocument();
            xmlWriter.Close();
        }
    }
}
