﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Xml;
using Easier_Data_Editor.Attributes;
using Easier_Data_Editor.Utilities;

namespace Easier_Data_Editor.Serializers
{
    static class XmlDeserializer
    {
        public static void LoadFromXML(object? settings, XmlNode myRootNode)
        {
            PropertyInfo[] properties =
                settings?.GetType().GetProperties() ?? Array.Empty<PropertyInfo>();
            Dictionary<string, PropertyInfo> nodeNamePropDic = properties.ToDictionary(
                prop => SerializationUtil.GetXMLName(prop),
                prop => prop
            );

            foreach (XmlNode node in myRootNode.ChildNodes)
            {
                KeyValuePair<string, PropertyInfo>? entry = nodeNamePropDic
                    .Where(e => e.Key.Equals(node.Name))
                    .Select(e => (KeyValuePair<string, PropertyInfo>?)e)
                    .FirstOrDefault();
                if (entry == null)
                    continue;

                PropertyInfo propInfo = entry.Value.Value;

                if (
                    propInfo.GetCustomAttribute(typeof(NestedXmlObjectAttribute))
                    is NestedXmlObjectAttribute
                )
                {
                    LoadFromXML(propInfo.GetValue(settings), node);
                    continue;
                }

                try
                {
                    if (typeof(List<string>).IsAssignableFrom(propInfo.PropertyType))
                    {
                        foreach (XmlNode subNode in node.ChildNodes)
                            propInfo
                                .PropertyType.GetMethod("Add")
                                ?.Invoke(
                                    propInfo.GetValue(settings),
                                    new object[] { subNode.InnerText }
                                );
                    }
                    else
                    {
                        propInfo.SetValue(
                            settings,
                            DeserializationUtil.ParseStringValue(
                                propInfo.PropertyType,
                                node.InnerText
                            )
                        );
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("Exception during parsing settings {0}", ex);
                }
            }
        }
    }
}
