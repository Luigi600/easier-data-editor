﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using Easier_Data_Editor.Attributes;
using Easier_Data_Editor.Serializers.Models;
using Easier_Data_Editor.Utilities;

namespace Easier_Data_Editor.Serializers
{
    public static class LF2DataDeserializer
    {
        public static void ParseObjectFromAttributes(object obj, StringBuilder text)
        {
            ParseObject(obj, text);
        }

        private static bool IsList(Type type) =>
            type is IList
            || type.IsGenericType && type.GetGenericTypeDefinition() == typeof(List<>);

        private static Type GetPropertyType(Type propType)
        {
            if (IsList(propType))
            {
                return propType.GetGenericArguments().Single();
            }

            return propType;
        }

        private static void ParseObject(object obj, StringBuilder text)
        {
            ParsePropertiesFromAttributes(obj, text);
        }

        private static void ParsePropertiesFromAttributes(object obj, StringBuilder text)
        {
            List<Tuple<PropertyInfo, List<DeserializationProperty>>> parsedAttributes =
                new List<Tuple<PropertyInfo, List<DeserializationProperty>>>();

            foreach (PropertyInfo prop in obj.GetType().GetProperties())
            {
                List<DeserializationProperty> attrs = GetDeserializationPropertiesFromField(
                    prop,
                    obj.GetType()
                );

                if (attrs.Count == 0)
                    continue;

                parsedAttributes.Add(
                    new Tuple<PropertyInfo, List<DeserializationProperty>>(prop, attrs)
                );
            }

            foreach (Tuple<PropertyInfo, List<DeserializationProperty>> tuple in parsedAttributes)
                ParsePropertyFromAttributes(obj, text, tuple.Item1, tuple.Item2);
        }

#pragma warning disable S3011 // Reflection should not be used to increase accessibility of classes, methods, or fields
        private static List<DeserializationProperty> GetDeserializationPropertiesFromField(
            PropertyInfo prop,
            Type holder
        )
        {
            List<DeserializationProperty> result = new List<DeserializationProperty>();
            DeserializationProperty? simpleProp = GetDeserializationPropertyFromSimpleAttribute(
                prop,
                holder
            );
            List<LF2DataThirdPartyRegexPropertyAttribute> attrs = prop.GetCustomAttributes(
                    typeof(LF2DataThirdPartyRegexPropertyAttribute),
                    true
                )
                .Cast<LF2DataThirdPartyRegexPropertyAttribute>()
                .ToList();

            if (simpleProp is not null)
                result.Add(simpleProp);

            result.AddRange(
                attrs.Select(attr =>
                {
                    PropertyInfo? thirdProperty = prop.PropertyType.GetProperty(
                        attr.TargetProperty ?? ""
                    );
                    if (thirdProperty is null)
                        throw new ArgumentException("Third party property is unknown!");

                    return new DeserializationProperty(
                        attr.Pattern,
                        attr.TargetProperty,
                        attr.TargetType ?? GetPropertyType(thirdProperty.PropertyType),
                        !(attr.DynamicTargetTypeMethod is null)
                            ? holder.GetMethod(
                                attr.DynamicTargetTypeMethod,
                                BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static
                            )
                            : null,
                        !(attr.TransformMethod is null)
                            ? holder.GetMethod(
                                attr.TransformMethod,
                                BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static
                            )
                            : null
                    );
                })
            );

            return result;
        }

        private static DeserializationProperty? GetDeserializationPropertyFromSimpleAttribute(
            PropertyInfo prop,
            Type holder
        )
        {
            LF2DataDynamicTargetTypeAttribute? dynMethod = (LF2DataDynamicTargetTypeAttribute?)
                prop.GetCustomAttribute(typeof(LF2DataDynamicTargetTypeAttribute), true);
            LF2DataTransformAttribute? transformMethod = (LF2DataTransformAttribute?)
                prop.GetCustomAttribute(typeof(LF2DataTransformAttribute), true);
            LF2DataRegexPropertyAttribute? attr = (LF2DataRegexPropertyAttribute?)
                prop.GetCustomAttribute(typeof(LF2DataRegexPropertyAttribute), true);

            if (attr is not null)
            {
                return new DeserializationProperty(
                    attr.Pattern,
                    null,
                    GetPropertyType(prop.PropertyType),
                    dynMethod is not null
                        ? holder.GetMethod(
                            dynMethod.DynamicTargetTypeMethod,
                            BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static
                        )
                        : null,
                    transformMethod is not null
                        ? holder.GetMethod(
                            transformMethod.TransformMethod,
                            BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static
                        )
                        : null
                );
            }

            return null;
        }
#pragma warning restore S3011 // Reflection should not be used to increase accessibility of classes, methods, or fields

        private static void ParsePropertyFromAttributes(
            object obj,
            StringBuilder text,
            PropertyInfo info,
            List<DeserializationProperty> attributes
        )
        {
            int arrayIndex = 0;
            foreach (DeserializationProperty attr in attributes)
            {
                MatchCollection matches = attr.Pattern.Matches(text.ToString());
                List<string> values = new List<string>();

                bool breakAfterFirstIteration =
                    !IsList(info.PropertyType) && !info.PropertyType.IsArray;
                foreach (Match mtch in matches)
                {
                    // remove found matches so that subsequent searches cannot parse this range again
                    text.Remove(mtch.Index, mtch.Length);
                    text.Insert(mtch.Index, " ", mtch.Length);

                    values.Add(mtch.Value);

                    if (breakAfterFirstIteration)
                        break;
                }

                foreach (string val in values)
                {
                    ParsePropertyFromAttribute(attr, val, obj, info, ref arrayIndex);
                }
            }
        }

        private static void ParsePropertyFromAttribute(
            DeserializationProperty prop,
            string value,
            object obj,
            PropertyInfo propInfo,
            ref int arrayIndex
        )
        {
            // developer wants access to another class
            if (prop.ComplexTargetProperty is not null)
            {
                ParsePropertyFromAttributeThirdPartyType(
                    prop,
                    value,
                    obj,
                    propInfo,
                    ref arrayIndex
                );
            }
            // the target is complex type
            else if (
                !prop.FinalTargetType.IsPrimitive
                && prop.FinalTargetType.FullName?.Equals("System.String") != true
            )
            {
                ParsePropertyFromAnnotationComplexType(prop, value, obj, propInfo, ref arrayIndex);
            }
            // just a very simple value like number or string
            else
            {
                object? result = prop.TransformMethod is not null
                    ? prop.TransformMethod.Invoke(null, new object[] { value })
                    : value;

                if (
                    propInfo.PropertyType.FullName?.Equals("System.String") != true
                    && result?.GetType().FullName?.Equals("System.String") == true
                )
                    result = NumberShitUtil.ParseStringToNumberOrBoolean(
                        propInfo.PropertyType,
                        (string)result
                    );

                SetTarget(propInfo, obj, result, ref arrayIndex);
            }
        }

        private static void ParsePropertyFromAttributeThirdPartyType(
            DeserializationProperty prop,
            string match,
            object obj,
            PropertyInfo propInfo,
            ref int arrayIndex
        )
        {
            // try to get the object that contains the target class
            object? objToSet =
                IsList(propInfo.PropertyType) || propInfo.PropertyType.IsArray
                    ? null
                    : propInfo.GetValue(obj);
            // does not have an instance, create a new instance
            if (objToSet is null)
            {
                objToSet = GetObjectFrom(prop, match, prop.FinalTargetType);
            }

            if (prop.ComplexTargetProperty is null)
                throw new ArgumentNullException("ComplexTargetProperty should not be 'null'");

            // get the new property/field from (eventually newly generated) instance that developer actually wants
            PropertyInfo? targetProp = objToSet.GetType().GetProperty(prop.ComplexTargetProperty);

            if (targetProp is null)
                throw new ArgumentNullException("targetProp should not be 'null'");

            // recursion (call the function that calls this function again)
            ParsePropertyFromAttribute(
                new DeserializationProperty(
                    prop.Pattern,
                    null,
                    prop.FinalTargetType,
                    prop.TargetDeterminerMethod,
                    prop.TransformMethod
                ),
                match,
                objToSet,
                targetProp,
                ref arrayIndex
            );

            // in case of an array or primitive type => set the value of the old field to the (eventually newly generated) instance
            SetTarget(propInfo, obj, objToSet, ref arrayIndex);
        }

        private static void ParsePropertyFromAnnotationComplexType(
            DeserializationProperty prop,
            string match,
            object obj,
            PropertyInfo propInfo,
            ref int arrayIndex
        )
        {
            // check if complex object already exists
            object? objToSet =
                IsList(propInfo.PropertyType) || propInfo.PropertyType.IsArray
                    ? null
                    : propInfo.GetValue(obj);
            // does not have an instance, create a new instance
            if (objToSet is null)
            {
                objToSet = GetObjectFrom(prop, match, prop.FinalTargetType);
            }

            // create a new string builder that contains only the complex type template
            StringBuilder innerText = new StringBuilder(match);
            ParseObject(objToSet, innerText);
            SetTarget(propInfo, obj, objToSet, ref arrayIndex);
        }

        private static void SetTarget(
            PropertyInfo info,
            object obj,
            object? value,
            ref int arrayIndex
        )
        {
            if (
                info.PropertyType is IList
                || info.PropertyType.IsGenericType
                    && info.PropertyType.GetGenericTypeDefinition() == typeof(List<>)
            )
            {
                (info.GetValue(obj) as IList)?.Add(value);
            }
            else if (info.PropertyType.IsArray)
            {
                object[]? array = info.GetValue(obj) as object[];
                if (array != null && arrayIndex < array.Length)
                {
                    array[arrayIndex] = obj;
                    ++arrayIndex;
                }
            }
            else
            {
                info.SetValue(obj, value);
            }
        }

        private static object GetObjectFrom(
            DeserializationProperty prop,
            string input,
            Type? fallbackType
        )
        {
            Type? clsToCreate = fallbackType;
            if (prop.TargetDeterminerMethod is not null)
            {
                clsToCreate = (Type?)
                    prop.TargetDeterminerMethod.Invoke(null, new object[] { input });
            }

            if (clsToCreate is null)
                throw new ArgumentException("Property type unknown!");

            ConstructorInfo[] ctors = clsToCreate
                .GetConstructors()
                .Where(c => c.GetParameters().Length == 0)
                .ToArray();
            if (ctors.Length != 1)
                throw new ArgumentException("No model constructor with 0 parameters found!");

            return ctors[0].Invoke(Array.Empty<object>());
        }
    }
}
