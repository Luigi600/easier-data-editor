﻿using System.Xml;
using Easier_Data_Editor.Environments;
using Easier_Data_Editor.Environments.FormatBlueprints;

namespace Easier_Data_Editor.Serializers
{
    public static class BlueprintsSerializer
    {
        public static void WriteToXML(Blueprints prints)
        {
            XmlWriter xmlWriter = XmlWriter.Create(
                Application.FORMAT_BLUEPRINTS_FILE_PATH,
                new XmlWriterSettings() { Indent = true, OmitXmlDeclaration = false }
            );
            xmlWriter.WriteStartDocument();
            xmlWriter.WriteStartElement(Application.FORMAT_BLUEPRINTS_XML_NODE);
            xmlWriter.WriteAttributeString(
                Application.FORMAT_BLUEPRINTS_XML_VERSION_ATTRIBUTE,
                Application.FORMAT_BLUEPRINTS_XML_VERSION.ToString()
            );
            XmlSerializer.WriteToXML(prints, ref xmlWriter);
            xmlWriter.WriteEndElement();
            xmlWriter.WriteEndDocument();
            xmlWriter.Close();
        }
    }
}
