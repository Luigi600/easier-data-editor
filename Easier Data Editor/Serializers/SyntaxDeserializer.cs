﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Xml;
using Easier_Data_Editor.Extensions.Scintilla.SyntaxHighlighting;
using Easier_Data_Editor.Utilities;

namespace Easier_Data_Editor.Serializers
{
    public static class SyntaxDeserializer
    {
        private const string XML_NODE_NAME_DOCUMENT_ROOT = "SyntaxDefinition";
        private const string XML_NODE_NAME_DEFAULT_RULES = "Default";
        private const string XML_NODE_NAME_REGEX_RULES = "Regex";
        private const string XML_NODE_NAME_KEYWORD_RULES = "Keywords";
        private const string XML_NODE_NAME_WORD_REGEX = "SingleWordRegex";
        private const string XML_NODE_NAME_SINGLE_WORD = "Word";
        private const string XML_NODE_NAME_PATTERN = "Pattern";

        private const string XML_ATTRIBUTE_NAME_FORE_COLOR = "forecol";
        private const string XML_ATTRIBUTE_NAME_BACK_COLOR = "backcol";
        private const string XML_ATTRIBUTE_NAME_NAME = "name";
        private const string XML_ATTRIBUTE_NAME_PATTERN = "pattern";
        private const string XML_ATTRIBUTE_NAME_BOLD = "bold";
        private const string XML_ATTRIBUTE_NAME_ITALIC = "italic";

        private sealed class SyntaxItem : Item
        {
            public SyntaxItem(
                Color? foreColor = null,
                Color? backColor = null,
                bool isBold = false,
                bool isItalic = false
            )
                : base(foreColor, backColor, isBold, isItalic) { }
        }

        public static Syntax LoadSyntax(string xmlContent)
        {
            List<ItemRegex> regexSpecials = new List<ItemRegex>();
            List<ItemRegex> regexSingleWord = new List<ItemRegex>();
            List<ItemKeywordList> keywordLists = new List<ItemKeywordList>();

            Color? defaultBackColor = null;
            Color? defaultForeColor = null;

            XmlDocument xmlFile = new XmlDocument();
            xmlFile.LoadXml(xmlContent);
            if (xmlFile.DocumentElement?.Name.EndsWith(XML_NODE_NAME_DOCUMENT_ROOT) != true)
                throw new InvalidDataException();

            foreach (XmlNode node in xmlFile.DocumentElement.ChildNodes)
            {
                if (node.Name.Equals(XML_NODE_NAME_DEFAULT_RULES))
                {
                    if (node.Attributes is null)
                        continue;

                    XmlUtil.GetAttributeValueAndSet(
                        node.Attributes,
                        XML_ATTRIBUTE_NAME_FORE_COLOR,
                        ref defaultForeColor
                    );
                    XmlUtil.GetAttributeValueAndSet(
                        node.Attributes,
                        XML_ATTRIBUTE_NAME_BACK_COLOR,
                        ref defaultBackColor
                    );
                }
                else if (node.Name.Equals(XML_NODE_NAME_REGEX_RULES))
                {
                    if (GetItemRegexFromRegexSpecial(node) is ItemRegex item)
                        regexSpecials.Add(item);
                }
                else if (node.Name.Equals(XML_NODE_NAME_KEYWORD_RULES))
                {
                    if (GetItemKeywordList(node) is ItemKeywordList item)
                        keywordLists.Add(item);
                }
                else if (node.Name.Equals(XML_NODE_NAME_WORD_REGEX))
                {
                    regexSingleWord.AddRange(GetItemRegexFromSingleWord(node));
                }
            }

            if (defaultBackColor == null || defaultForeColor == null)
                throw new InvalidDataException();

            Syntax syntax = new Syntax((Color)defaultForeColor, (Color)defaultBackColor);
            syntax.AddRegexSpecial(regexSpecials);
            syntax.AddRegexSingleWord(regexSingleWord);
            syntax.AddKeywordList(keywordLists);
            return syntax;
        }

        private static ItemRegex? GetItemRegexFromRegexSpecial(XmlNode node)
        {
            if (node.Attributes is null)
                return null;

            SyntaxItem syntaxItem = GetSyntaxItem(node.Attributes);
            string? name = null;
            string? pattern = null;
            XmlUtil.GetAttributeValueAndSet(node.Attributes, XML_ATTRIBUTE_NAME_NAME, ref name);
            XmlUtil.GetAttributeValueAndSet(
                node.Attributes,
                XML_ATTRIBUTE_NAME_PATTERN,
                ref pattern
            );
            if (pattern != null)
                return new ItemRegex(
                    name ?? "",
                    pattern,
                    syntaxItem.ForeColor,
                    syntaxItem.BackColor,
                    syntaxItem.IsBold,
                    syntaxItem.IsItalic
                );

            return null;
        }

        private static ItemKeywordList? GetItemKeywordList(XmlNode node)
        {
            if (node.Attributes is null)
                return null;

            SyntaxItem syntaxItem = GetSyntaxItem(node.Attributes);
            ItemKeywordList ky = new ItemKeywordList(
                syntaxItem.ForeColor,
                syntaxItem.BackColor,
                syntaxItem.IsBold,
                syntaxItem.IsItalic
            );
            foreach (
                XmlNode subNode in node
                    .ChildNodes.Cast<XmlNode>()
                    .Where(n => n.Name.Equals(XML_NODE_NAME_SINGLE_WORD) && n.InnerText.Length > 0)
            )
                ky.AddKeyword(subNode.InnerText.ToLower());

            if (ky.Keywords.Count > 0)
                return ky;

            return null;
        }

        private static ItemRegex[] GetItemRegexFromSingleWord(XmlNode node)
        {
            if (node.Attributes is null)
                return Array.Empty<ItemRegex>();

            SyntaxItem syntaxItem = GetSyntaxItem(node.Attributes);
            string? name = null;
            XmlUtil.GetAttributeValueAndSet(node.Attributes, XML_ATTRIBUTE_NAME_NAME, ref name);
            if (name == null)
                name = "";

            List<ItemRegex> result = new List<ItemRegex>();
            foreach (
                XmlNode subNode in node
                    .ChildNodes.Cast<XmlNode>()
                    .Where(n => n.Name.Equals(XML_NODE_NAME_PATTERN))
            )
                result.Add(
                    new ItemRegex(
                        name,
                        subNode.InnerText,
                        syntaxItem.ForeColor,
                        syntaxItem.BackColor,
                        syntaxItem.IsBold,
                        syntaxItem.IsItalic
                    )
                );

            return result.ToArray();
        }

        private static SyntaxItem GetSyntaxItem(XmlAttributeCollection attrs)
        {
            Color? backColor = null;
            Color? foreColor = null;
            bool isBold = false;
            bool isItalic = false;

            XmlUtil.GetAttributeValueAndSet(attrs, XML_ATTRIBUTE_NAME_FORE_COLOR, ref foreColor);
            XmlUtil.GetAttributeValueAndSet(attrs, XML_ATTRIBUTE_NAME_BACK_COLOR, ref backColor);

            XmlUtil.GetAttributeValueAndSet(attrs, XML_ATTRIBUTE_NAME_BOLD, ref isBold);
            XmlUtil.GetAttributeValueAndSet(attrs, XML_ATTRIBUTE_NAME_ITALIC, ref isItalic);

            return new SyntaxItem(foreColor, backColor, isBold, isItalic);
        }
    }
}
