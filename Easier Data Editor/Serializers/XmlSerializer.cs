﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Xml;
using Easier_Data_Editor.Attributes;
using Easier_Data_Editor.Utilities;

namespace Easier_Data_Editor.Serializers
{
    public static class XmlSerializer
    {
        public static void WriteToXML(object? settings, ref XmlWriter writer)
        {
            foreach (
                PropertyInfo info in settings?.GetType().GetProperties()
                    ?? Array.Empty<PropertyInfo>()
            )
            {
                if (
                    info.GetCustomAttribute(typeof(SkipSerializationAttribute))
                    is SkipSerializationAttribute
                )
                    continue;

                if (
                    info.GetCustomAttribute(typeof(SkipNullSerializationAttribute))
                        is SkipNullSerializationAttribute
                    && info.GetValue(settings) == null
                )
                    continue;

                if (
                    info.GetCustomAttribute(typeof(NestedXmlObjectAttribute))
                    is NestedXmlObjectAttribute
                )
                {
                    string nodeName = SerializationUtil.GetXMLName(info);
                    writer.WriteStartElement(nodeName);

                    WriteToXML(info.GetValue(settings), ref writer);
                    writer.WriteEndElement();
                }
                else if (typeof(List<string>).IsAssignableFrom(info.PropertyType))
                {
                    List<string>? items = (List<string>?)info.GetValue(settings);
                    if (items is null)
                        continue;

                    writer.WriteStartElement(info.Name);
                    items.Reverse();

                    foreach (string item in items)
                        writer.WriteElementString("Item", item);

                    writer.WriteEndElement();
                }
                else
                {
                    writer.WriteElementString(
                        info.Name,
                        SerializationUtil.GetStringValue(info.GetValue(settings))
                    );
                }
            }
        }
    }
}
