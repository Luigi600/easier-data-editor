﻿using System;
using System.Reflection;
using System.Text.RegularExpressions;

namespace Easier_Data_Editor.Serializers.Models
{
    internal sealed class DeserializationProperty
    {
        public readonly Regex Pattern;
        public readonly string? ComplexTargetProperty;
        public readonly Type FinalTargetType;
        public readonly MethodInfo? TargetDeterminerMethod;
        public readonly MethodInfo? TransformMethod;

        public DeserializationProperty(
            Regex pattern,
            string? complexTargetProperty,
            Type finalTargetType,
            MethodInfo? targetDeterminerMethod,
            MethodInfo? transformMethod
        )
        {
            this.Pattern = pattern;
            this.ComplexTargetProperty = complexTargetProperty;
            this.FinalTargetType = finalTargetType;
            this.TargetDeterminerMethod = targetDeterminerMethod;
            this.TransformMethod = transformMethod;
        }
    }
}
