﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Windows.Forms;

namespace Easier_Data_Editor
{
    /**
     * Based on https://docs.microsoft.com/en-us/dotnet/api/system.windows.forms.application.threadexception?view=windowsdesktop-6.0#examples
     */
    internal static class ExceptionHandler
    {
        internal static void UnhandledException(object sender, UnhandledExceptionEventArgs args)
        {
            Exception e = (Exception)args.ExceptionObject;
            Debug.WriteLine("Try to save settings!!"); // TODO: do it!
            ShowExceptionDialog("Easier Data Editor - Unexpected Error", e);
        }

        internal static void UIThreadException(object sender, ThreadExceptionEventArgs t)
        {
            DialogResult result = DialogResult.Cancel;
            try
            {
                result = ShowExceptionDialog(
                    "Easier Data Editor - Windows Forms Error",
                    t.Exception
                );
            }
            catch
            {
                try
                {
                    MessageBox.Show(
                        "Fatal Windows Forms Error",
                        "Fatal Windows Forms Error",
                        MessageBoxButtons.AbortRetryIgnore,
                        MessageBoxIcon.Stop
                    );
                }
                finally
                {
                    Application.Exit();
                }
            }

            // Exits the program when the user clicks Abort.
            if (result == DialogResult.Abort)
                Application.Exit();
        }

        internal static DialogResult ShowExceptionDialog(string title, Exception e)
        {
            string errorMsg =
                "An application error occurred. Please contact the developers "
                + "with the following information:\n\n";
            errorMsg = errorMsg + e.Message + "\n\nStack Trace:\n" + e.StackTrace;
            return MessageBox.Show(
                errorMsg,
                title,
                MessageBoxButtons.AbortRetryIgnore,
                MessageBoxIcon.Stop
            );
        }
    }
}
