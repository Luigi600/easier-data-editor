﻿using System.Linq;
using Easier_Data_Editor.Models;

namespace Easier_Data_Editor.Collections
{
    public class LimitedListWithEvents<T> : ListWithEvents<T>, IDataEqualation
    {
        private int m_capacity = 20;

        public new int Capacity
        {
            get => m_capacity;
            set
            {
                if (value >= 0 && value < m_capacity)
                {
                    while (Count > value)
                        this.RemoveAt(Count - 1);
                }

                m_capacity = value;
            }
        }

        /// <summary>Adds an object to the end of the List and triggers events, if RasieEvents on true.</summary>
        /// <param name="item">The object to be added to the end of the List. The value can be null for reference types.</param>
        public override void Add(T item)
        {
            if (RasieEvents)
                this.OnItemAdd(item);

            if (this.Contains(item))
                this.Remove(item); // remove old item and add at position 0
            this.Insert(0, item);

            if (Capacity >= 0)
            {
                while (Count > Capacity)
                    this.RemoveAt(Count - 1);
            }

            if (RasieEvents)
                this.OnItemAdded(item);
        }

        /// <summary>Removes a range of elements from the List and triggers events, if RasieEvents on true. <b>Important:</b> this function calls RemoveAt(int index). So it triggers <paramref name="counter"/> times one/two events.</summary>
        /// <param name="index">The zero-based starting index of the range of elements to remove.</param>
        /// <param name="counter">The number of elements to remove.</param>
        /// <exception cref="ArgumentOutOfRangeException">
        /// index is less than 0. <br />-or-<br />
        /// count is less than 0.
        /// </exception>
        /// <exception cref="ArgumentException">Index and count do not denote a valid range of elements in the List.</exception>
        public new void RemoveRange(int index, int counter)
        {
            while (index < Count && counter > 0)
            {
                this.RemoveAt(index);
                --counter;
            }
        }

        public bool DataAreEquals(object obj)
        {
            if (!(obj is LimitedListWithEvents<T> list))
                return false;

            return this.SequenceEqual(list);
        }
    }
}
