﻿using System;
using System.Collections.Generic;

namespace Easier_Data_Editor.Collections
{
    public class ListWithEvents<T> : List<T>
    {
        /// <summary>Event before an item will be added to list. Triggered by "Add".</summary>
        public event EventHandler<T>? ItemAdd;

        /// <summary>Event after an item is added to list. Triggered by "Add".</summary>
        public event EventHandler<T>? ItemAdded;

        /// <summary>Event before items will be added to list. Triggered by "AddRange".</summary>
        public event EventHandler<IEnumerable<T>>? ItemsAdd;

        /// <summary>Event after items are added to list. Triggered by "AddRange".</summary>
        public event EventHandler<IEnumerable<T>>? ItemsAdded;

        /// <summary>Event before an item will be removed from the list. Triggered by "Remove" and "RemoveAt".</summary>
        public event EventHandler<T>? ItemRemove;

        /// <summary>Event after an item is removed from the list. Triggered by "Remove" and "RemoveAt".</summary>
        public event EventHandler<T>? ItemRemoved;

        /// <include file="Documentation\Main.xml" path="Parts/summary[@id='itemsClear']" />
        public event EventHandler? ItemsClear;

        /// <include file="Documentation\Main.xml" path="Parts/summary[@id='itemsCleared']" />
        public event EventHandler? ItemsCleared;

        /// <include file="Documentation\Main.xml" path="Parts/summary[@id='raiseEvents']" />
        public virtual bool RasieEvents { get; set; } = true; // false = NONE raiseEvent

        /// <summary>Adds an object to the end of the List and triggers events, if RasieEvents on true.</summary>
        /// <param name="item">The object to be added to the end of the List. The value can be null for reference types.</param>
        public virtual new void Add(T item)
        {
            if (RasieEvents)
                ItemAdd?.Invoke(this, item);

            base.Add(item);

            if (RasieEvents)
                ItemAdded?.Invoke(this, item);
        }

        /// <summary>Adds the elements of the specified collection to the end of the List and triggers events, if RasieEvents on true.</summary>
        /// <param name="items">The collection whose elements should be added to the end of the List. The collection itself cannot be null, but it can contain elements that are null, if type T is a reference type.</param>
        public virtual new void AddRange(IEnumerable<T> items)
        {
            if (RasieEvents)
                ItemsAdd?.Invoke(this, items);

            base.AddRange(items);

            if (RasieEvents)
                ItemsAdded?.Invoke(this, items);
        }

        /// <summary>Removes the first occurrence of a specific object from the List and triggers events, if RasieEvents on true.</summary>
        /// <param name="item">The object to remove from the List. The value can be null for reference types.</param>
        public virtual new void Remove(T item)
        {
            if (RasieEvents)
                ItemRemove?.Invoke(this, item);

            base.Remove(item);

            if (RasieEvents)
                ItemRemoved?.Invoke(this, item);
        }

        /// <summary>Removes the element at the specified index of the List and triggers events, if RasieEvents on true.</summary>
        /// <param name="index">The zero-based index of the element to remove.</param>
        public virtual new void RemoveAt(int index)
        {
            if (index >= Count)
                return;

            T item = this[index];
            if (RasieEvents)
                ItemRemove?.Invoke(this, item);

            base.RemoveAt(index);

            if (RasieEvents)
                ItemRemoved?.Invoke(this, item);
        }

        /// <summary>Removes all elements from the List and triggers events, if RasieEvents on true.</summary>
        public new void Clear()
        {
            if (RasieEvents)
                ItemsClear?.Invoke(this, EventArgs.Empty);

            base.Clear();

            if (RasieEvents)
                ItemsCleared?.Invoke(this, EventArgs.Empty);
        }

        protected virtual void OnItemAdd(T item) => this.ItemAdd?.Invoke(this, item);

        protected virtual void OnItemAdded(T item) => this.ItemAdded?.Invoke(this, item);

        protected virtual void OnItemsAdd(IEnumerable<T> items) =>
            this.ItemsAdded?.Invoke(this, items);

        protected virtual void OnItemsAdded(IEnumerable<T> items) =>
            this.ItemsAdded?.Invoke(this, items);

        protected virtual void OnItemRemove(T item) => this.ItemRemove?.Invoke(this, item);

        protected virtual void OnItemRemoved(T item) => this.ItemRemoved?.Invoke(this, item);

        protected virtual void OnItemsClear(EventArgs e) => this.ItemsClear?.Invoke(this, e);

        protected virtual void OnItemsCleared(EventArgs e) => this.ItemsCleared?.Invoke(this, e);
    }
}
