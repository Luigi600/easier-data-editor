﻿
using DarkModeForms;

namespace Easier_Data_Editor
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.ssInfos = new System.Windows.Forms.StatusStrip();
            this.tsslTextEncryption = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsslTextEncryptionVal = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsslTextLength = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsslTextLengthVal = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsslTextLines = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsslTextLinesVal = new System.Windows.Forms.ToolStripStatusLabel();
            this.tss_13 = new System.Windows.Forms.ToolStripSeparator();
            this.tsslTextPos = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsslTextPosVal = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsslTextSel = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsslTextSelVal = new System.Windows.Forms.ToolStripStatusLabel();
            this.tss_14 = new System.Windows.Forms.ToolStripSeparator();
            this.tsslTextEncoding = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsslTextEncodingVal = new System.Windows.Forms.ToolStripStatusLabel();
            this.tss_15 = new System.Windows.Forms.ToolStripSeparator();
            this.tsslCopyright = new System.Windows.Forms.ToolStripStatusLabel();
            this.linkCookiesoft = new System.Windows.Forms.ToolStripStatusLabel();
            this.msIcons = new System.Windows.Forms.MenuStrip();
            this.tsmiIconNew = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiIconOpen = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiIconSave = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiIconSaveAll = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiIconClose = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiIconUndo = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiIconRedo = new System.Windows.Forms.ToolStripMenuItem();
            this.tsCombFrames = new FlatComboBox();
            this.tsCombLF = new System.Windows.Forms.ToolStripComboBox();
            this.tsmiRun = new System.Windows.Forms.ToolStripMenuItem();
            this.msMain = new System.Windows.Forms.MenuStrip();
            this.tsmiFile = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiNew = new System.Windows.Forms.ToolStripMenuItem();
            this.tss_1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmiOpen = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiRecentFiles = new System.Windows.Forms.ToolStripMenuItem();
            this.tss_2 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmiSave = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiSaveAll = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiSaveAs = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiExportHTML = new System.Windows.Forms.ToolStripMenuItem();
            this.tss_3 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmiCloseFile = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiCloseAllFiles = new System.Windows.Forms.ToolStripMenuItem();
            this.tss_4 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmiQuit = new System.Windows.Forms.ToolStripMenuItem();
            this.DEBUG_DO_ERRORS = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiEdit = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiUndo = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiRedo = new System.Windows.Forms.ToolStripMenuItem();
            this.tss_5 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmiCut = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiCopy = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiPaste = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiPasteIDs = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiPasteXY = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiSelectAll = new System.Windows.Forms.ToolStripMenuItem();
            this.tss_7 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmiExpandAllFoldings = new Easier_Data_Editor.Forms.BindableToolStripMenuItem();
            this.tsmiCollapseAllFoldings = new Easier_Data_Editor.Forms.BindableToolStripMenuItem();
            this.tsmiSearch = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiFind = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiFindNext = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiFindPrevious = new System.Windows.Forms.ToolStripMenuItem();
            this.tss_6 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmiReplace = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiReplaceNext = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiReplacePrevious = new System.Windows.Forms.ToolStripMenuItem();
            this.tss_12 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmiMark = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiMultiFind = new System.Windows.Forms.ToolStripMenuItem();
            this.tss_10 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmiGoTo = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiGoToLine = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiGoToFrame = new System.Windows.Forms.ToolStripMenuItem();
            this.tss_11 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmiBookmark = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiToggleBookmark = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiNextBookmark = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiPreviousBookmark = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiClearBookmarks = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiCutBookmarkedLines = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiCopyBookmarkedLines = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiPasteBookmarkedLines = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiRemoveBookmarkedLines = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiRemoveUnmarkedLines = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiInvertBookmarks = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiView = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiLineNumbers = new Easier_Data_Editor.Forms.BindableToolStripMenuItem();
            this.tsmiMarkLine = new Easier_Data_Editor.Forms.BindableToolStripMenuItem();
            this.tsmiWhitespaces = new Easier_Data_Editor.Forms.BindableToolStripMenuItem();
            this.tsmiHSplit = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiVSplit = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiWrap = new System.Windows.Forms.ToolStripMenuItem();
            this.tsCombWrapMode = new Easier_Data_Editor.Forms.BindableToolStripComboBox();
            this.tsmiWindow = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiFrameViewer = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiUnusedFrameIDs = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiErrorList = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiTools = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiReformatter = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiMirrorFrame = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiTagAdder = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiIDChanger = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiSort = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiEquation = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiEquationSetOptions = new System.Windows.Forms.ToolStripMenuItem();
            this.tss_8 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmiEquationCenter = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiEquationBodies = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiEquationItrs = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiEquationBpoi = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiEquationCpoi = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiEquationOpoi = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiEquationWpoi = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiEquationWait = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiEquationState = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiEquationNext = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiEquationHit = new System.Windows.Forms.ToolStripMenuItem();
            this.tss_9 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmiEquationApply = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiSetter = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiSettings = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiHiddenTools = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiHiddenToolsShowFrameViewer = new System.Windows.Forms.ToolStripMenuItem();
            this.DEBUG_BeginUndo = new System.Windows.Forms.ToolStripMenuItem();
            this.DEBUG_EndUndo = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiHiddenToolsToggleBSprite = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiHiddenToolsNextFrame = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiHiddenToolsPreviousFrame = new System.Windows.Forms.ToolStripMenuItem();
            this.dockPalMain = new WeifenLuo.WinFormsUI.Docking.DockPanel();
            this.ofdData = new System.Windows.Forms.OpenFileDialog();
            this.sfdHTML = new System.Windows.Forms.SaveFileDialog();
            this.cmsEncryption = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmiEncryptionTrue = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiEncryptionFalse = new System.Windows.Forms.ToolStripMenuItem();
            this.cmsEncoding = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmiEncodingUTF8 = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiEncodingUTF8BOM = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiEncodingUTF16BE = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiEncodingUTF16LE = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiEncodingUTF32BE = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiEncodingUTF32LE = new System.Windows.Forms.ToolStripMenuItem();
            this.timAnnotations = new System.Windows.Forms.Timer(this.components);
            this.sfdSaveAsData = new System.Windows.Forms.SaveFileDialog();
            this.ssInfos.SuspendLayout();
            this.msIcons.SuspendLayout();
            this.msMain.SuspendLayout();
            this.cmsEncryption.SuspendLayout();
            this.cmsEncoding.SuspendLayout();
            this.SuspendLayout();
            // 
            // ssInfos
            // 
            this.ssInfos.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.ssInfos.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsslTextEncryption,
            this.tsslTextEncryptionVal,
            this.tsslTextLength,
            this.tsslTextLengthVal,
            this.tsslTextLines,
            this.tsslTextLinesVal,
            this.tss_13,
            this.tsslTextPos,
            this.tsslTextPosVal,
            this.tsslTextSel,
            this.tsslTextSelVal,
            this.tss_14,
            this.tsslTextEncoding,
            this.tsslTextEncodingVal,
            this.tss_15,
            this.tsslCopyright,
            this.linkCookiesoft});
            this.ssInfos.Location = new System.Drawing.Point(0, 481);
            this.ssInfos.Name = "ssInfos";
            this.ssInfos.Size = new System.Drawing.Size(957, 23);
            this.ssInfos.TabIndex = 6;
            // 
            // tsslTextEncryption
            // 
            this.tsslTextEncryption.Name = "tsslTextEncryption";
            this.tsslTextEncryption.Size = new System.Drawing.Size(67, 18);
            this.tsslTextEncryption.Text = "Encryption:";
            // 
            // tsslTextEncryptionVal
            // 
            this.tsslTextEncryptionVal.DoubleClickEnabled = true;
            this.tsslTextEncryptionVal.Name = "tsslTextEncryptionVal";
            this.tsslTextEncryptionVal.Size = new System.Drawing.Size(31, 18);
            this.tsslTextEncryptionVal.Text = "false";
            this.tsslTextEncryptionVal.DoubleClick += new System.EventHandler(this.TsslTextEncryptionVal_DoubleClick);
            this.tsslTextEncryptionVal.MouseUp += new System.Windows.Forms.MouseEventHandler(this.TsslTextEncryptionVal_MouseUp);
            // 
            // tsslTextLength
            // 
            this.tsslTextLength.Name = "tsslTextLength";
            this.tsslTextLength.Size = new System.Drawing.Size(215, 18);
            this.tsslTextLength.Spring = true;
            this.tsslTextLength.Text = "Text Length:";
            this.tsslTextLength.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tsslTextLengthVal
            // 
            this.tsslTextLengthVal.Name = "tsslTextLengthVal";
            this.tsslTextLengthVal.Padding = new System.Windows.Forms.Padding(0, 0, 24, 0);
            this.tsslTextLengthVal.Size = new System.Drawing.Size(37, 18);
            this.tsslTextLengthVal.Text = "0";
            this.tsslTextLengthVal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tsslTextLines
            // 
            this.tsslTextLines.Name = "tsslTextLines";
            this.tsslTextLines.Size = new System.Drawing.Size(37, 18);
            this.tsslTextLines.Text = "Lines:";
            // 
            // tsslTextLinesVal
            // 
            this.tsslTextLinesVal.Name = "tsslTextLinesVal";
            this.tsslTextLinesVal.Padding = new System.Windows.Forms.Padding(0, 0, 24, 0);
            this.tsslTextLinesVal.Size = new System.Drawing.Size(37, 18);
            this.tsslTextLinesVal.Text = "0";
            this.tsslTextLinesVal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tss_13
            // 
            this.tss_13.Name = "tss_13";
            this.tss_13.Size = new System.Drawing.Size(6, 23);
            // 
            // tsslTextPos
            // 
            this.tsslTextPos.Name = "tsslTextPos";
            this.tsslTextPos.Padding = new System.Windows.Forms.Padding(24, 0, 0, 0);
            this.tsslTextPos.Size = new System.Drawing.Size(53, 18);
            this.tsslTextPos.Text = "Pos:";
            this.tsslTextPos.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tsslTextPosVal
            // 
            this.tsslTextPosVal.Name = "tsslTextPosVal";
            this.tsslTextPosVal.Padding = new System.Windows.Forms.Padding(0, 0, 24, 0);
            this.tsslTextPosVal.Size = new System.Drawing.Size(37, 18);
            this.tsslTextPosVal.Text = "0";
            this.tsslTextPosVal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tsslTextSel
            // 
            this.tsslTextSel.Name = "tsslTextSel";
            this.tsslTextSel.Size = new System.Drawing.Size(25, 18);
            this.tsslTextSel.Text = "Sel:";
            this.tsslTextSel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tsslTextSelVal
            // 
            this.tsslTextSelVal.Name = "tsslTextSelVal";
            this.tsslTextSelVal.Padding = new System.Windows.Forms.Padding(0, 0, 28, 0);
            this.tsslTextSelVal.Size = new System.Drawing.Size(41, 18);
            this.tsslTextSelVal.Text = "0";
            this.tsslTextSelVal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tss_14
            // 
            this.tss_14.ForeColor = System.Drawing.SystemColors.ControlText;
            this.tss_14.Name = "tss_14";
            this.tss_14.Size = new System.Drawing.Size(6, 23);
            // 
            // tsslTextEncoding
            // 
            this.tsslTextEncoding.Name = "tsslTextEncoding";
            this.tsslTextEncoding.Padding = new System.Windows.Forms.Padding(24, 0, 0, 0);
            this.tsslTextEncoding.Size = new System.Drawing.Size(84, 18);
            this.tsslTextEncoding.Text = "Encoding:";
            this.tsslTextEncoding.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tsslTextEncodingVal
            // 
            this.tsslTextEncodingVal.DoubleClickEnabled = true;
            this.tsslTextEncodingVal.Name = "tsslTextEncodingVal";
            this.tsslTextEncodingVal.Padding = new System.Windows.Forms.Padding(0, 0, 28, 0);
            this.tsslTextEncodingVal.Size = new System.Drawing.Size(40, 18);
            this.tsslTextEncodingVal.Text = "-";
            this.tsslTextEncodingVal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.tsslTextEncodingVal.DoubleClick += new System.EventHandler(this.TsslTextEncodingVal_DoubleClick);
            this.tsslTextEncodingVal.MouseUp += new System.Windows.Forms.MouseEventHandler(this.TsslTextEncodingVal_MouseUp);
            // 
            // tss_15
            // 
            this.tss_15.ForeColor = System.Drawing.SystemColors.ControlText;
            this.tss_15.Name = "tss_15";
            this.tss_15.Size = new System.Drawing.Size(6, 23);
            // 
            // tsslCopyright
            // 
            this.tsslCopyright.AutoSize = false;
            this.tsslCopyright.Name = "tsslCopyright";
            this.tsslCopyright.Size = new System.Drawing.Size(125, 18);
            this.tsslCopyright.Text = "Copyright of:";
            this.tsslCopyright.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // linkCookiesoft
            // 
            this.linkCookiesoft.IsLink = true;
            this.linkCookiesoft.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline;
            this.linkCookiesoft.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.linkCookiesoft.Name = "linkCookiesoft";
            this.linkCookiesoft.Size = new System.Drawing.Size(64, 18);
            this.linkCookiesoft.Text = "Cookiesoft";
            this.linkCookiesoft.VisitedLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            // 
            // msIcons
            // 
            this.msIcons.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(242)))));
            this.msIcons.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiIconNew,
            this.tsmiIconOpen,
            this.tsmiIconSave,
            this.tsmiIconSaveAll,
            this.tsmiIconClose,
            this.tsmiIconUndo,
            this.tsmiIconRedo,
            this.tsCombFrames,
            this.tsCombLF,
            this.tsmiRun});
            this.msIcons.Location = new System.Drawing.Point(0, 24);
            this.msIcons.Name = "msIcons";
            this.msIcons.Size = new System.Drawing.Size(957, 27);
            this.msIcons.TabIndex = 5;
            this.msIcons.Text = "Icons";
            // 
            // tsmiIconNew
            // 
            this.tsmiIconNew.Image = global::Easier_Data_Editor.Properties.Resources.icon_new;
            this.tsmiIconNew.Name = "tsmiIconNew";
            this.tsmiIconNew.Size = new System.Drawing.Size(28, 23);
            // 
            // tsmiIconOpen
            // 
            this.tsmiIconOpen.Image = global::Easier_Data_Editor.Properties.Resources.icon_open;
            this.tsmiIconOpen.Name = "tsmiIconOpen";
            this.tsmiIconOpen.Size = new System.Drawing.Size(28, 23);
            this.tsmiIconOpen.Click += new System.EventHandler(this.TsmiOpen_Click);
            // 
            // tsmiIconSave
            // 
            this.tsmiIconSave.Enabled = false;
            this.tsmiIconSave.Image = global::Easier_Data_Editor.Properties.Resources.icon_save;
            this.tsmiIconSave.Name = "tsmiIconSave";
            this.tsmiIconSave.Size = new System.Drawing.Size(28, 23);
            this.tsmiIconSave.Click += new System.EventHandler(this.TsmiSave_Click);
            // 
            // tsmiIconSaveAll
            // 
            this.tsmiIconSaveAll.Enabled = false;
            this.tsmiIconSaveAll.Image = global::Easier_Data_Editor.Properties.Resources.icon_save_all;
            this.tsmiIconSaveAll.Name = "tsmiIconSaveAll";
            this.tsmiIconSaveAll.Size = new System.Drawing.Size(28, 23);
            this.tsmiIconSaveAll.Click += new System.EventHandler(this.TsmiSaveAll_Click);
            // 
            // tsmiIconClose
            // 
            this.tsmiIconClose.Enabled = false;
            this.tsmiIconClose.Image = global::Easier_Data_Editor.Properties.Resources.icon_close;
            this.tsmiIconClose.Name = "tsmiIconClose";
            this.tsmiIconClose.Size = new System.Drawing.Size(28, 23);
            this.tsmiIconClose.Click += new System.EventHandler(this.TsmiCloseFile_Click);
            // 
            // tsmiIconUndo
            // 
            this.tsmiIconUndo.Enabled = false;
            this.tsmiIconUndo.Image = global::Easier_Data_Editor.Properties.Resources.icon_undo;
            this.tsmiIconUndo.Name = "tsmiIconUndo";
            this.tsmiIconUndo.Size = new System.Drawing.Size(28, 23);
            this.tsmiIconUndo.Click += new System.EventHandler(this.Undo_Click);
            // 
            // tsmiIconRedo
            // 
            this.tsmiIconRedo.Enabled = false;
            this.tsmiIconRedo.Image = global::Easier_Data_Editor.Properties.Resources.icon_redo;
            this.tsmiIconRedo.Name = "tsmiIconRedo";
            this.tsmiIconRedo.Size = new System.Drawing.Size(28, 23);
            this.tsmiIconRedo.Click += new System.EventHandler(this.Redo_Click);
            // 
            // tsCombFrames
            // 
            this.tsCombFrames.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.tsCombFrames.Name = "tsCombFrames";
            this.tsCombFrames.Size = new System.Drawing.Size(130, 23);
            // 
            // tsCombLF
            // 
            this.tsCombLF.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.tsCombLF.Margin = new System.Windows.Forms.Padding(32, 0, 1, 0);
            this.tsCombLF.Name = "tsCombLF";
            this.tsCombLF.Size = new System.Drawing.Size(200, 23);
            // 
            // tsmiRun
            // 
            this.tsmiRun.Enabled = false;
            this.tsmiRun.Image = global::Easier_Data_Editor.Properties.Resources.icon_start;
            this.tsmiRun.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.tsmiRun.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsmiRun.Margin = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.tsmiRun.Name = "tsmiRun";
            this.tsmiRun.Padding = new System.Windows.Forms.Padding(20, 0, 10, 0);
            this.tsmiRun.Size = new System.Drawing.Size(85, 23);
            this.tsmiRun.Text = "   Run";
            // 
            // msMain
            // 
            this.msMain.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(242)))));
            this.msMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiFile,
            this.tsmiEdit,
            this.tsmiSearch,
            this.tsmiView,
            this.tsmiWindow,
            this.tsmiTools,
            this.tsmiSettings,
            this.tsmiAbout,
            this.tsmiHiddenTools});
            this.msMain.Location = new System.Drawing.Point(0, 0);
            this.msMain.Name = "msMain";
            this.msMain.Size = new System.Drawing.Size(957, 24);
            this.msMain.TabIndex = 7;
            this.msMain.Text = "Menu Strip Main";
            // 
            // tsmiFile
            // 
            this.tsmiFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiNew,
            this.tss_1,
            this.tsmiOpen,
            this.tsmiRecentFiles,
            this.tss_2,
            this.tsmiSave,
            this.tsmiSaveAll,
            this.tsmiSaveAs,
            this.tsmiExportHTML,
            this.tss_3,
            this.tsmiCloseFile,
            this.tsmiCloseAllFiles,
            this.tss_4,
            this.tsmiQuit,
            this.DEBUG_DO_ERRORS});
            this.tsmiFile.Name = "tsmiFile";
            this.tsmiFile.Size = new System.Drawing.Size(37, 20);
            this.tsmiFile.Text = "File";
            this.tsmiFile.DropDownOpening += new System.EventHandler(this.TsmiFile_DropDownOpening);
            // 
            // tsmiNew
            // 
            this.tsmiNew.Image = global::Easier_Data_Editor.Properties.Resources.icon_new;
            this.tsmiNew.Name = "tsmiNew";
            this.tsmiNew.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.tsmiNew.Size = new System.Drawing.Size(248, 22);
            this.tsmiNew.Text = "New";
            // 
            // tss_1
            // 
            this.tss_1.Name = "tss_1";
            this.tss_1.Size = new System.Drawing.Size(245, 6);
            // 
            // tsmiOpen
            // 
            this.tsmiOpen.Image = global::Easier_Data_Editor.Properties.Resources.icon_open;
            this.tsmiOpen.Name = "tsmiOpen";
            this.tsmiOpen.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.tsmiOpen.Size = new System.Drawing.Size(248, 22);
            this.tsmiOpen.Text = "Open";
            this.tsmiOpen.Click += new System.EventHandler(this.TsmiOpen_Click);
            // 
            // tsmiRecentFiles
            // 
            this.tsmiRecentFiles.Enabled = false;
            this.tsmiRecentFiles.Image = global::Easier_Data_Editor.Properties.Resources.icon_recent;
            this.tsmiRecentFiles.Name = "tsmiRecentFiles";
            this.tsmiRecentFiles.Size = new System.Drawing.Size(248, 22);
            this.tsmiRecentFiles.Text = "Recent Files";
            // 
            // tss_2
            // 
            this.tss_2.Name = "tss_2";
            this.tss_2.Size = new System.Drawing.Size(245, 6);
            // 
            // tsmiSave
            // 
            this.tsmiSave.Enabled = false;
            this.tsmiSave.Image = global::Easier_Data_Editor.Properties.Resources.icon_save;
            this.tsmiSave.Name = "tsmiSave";
            this.tsmiSave.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.tsmiSave.Size = new System.Drawing.Size(248, 22);
            this.tsmiSave.Text = "Save";
            this.tsmiSave.Click += new System.EventHandler(this.TsmiSave_Click);
            this.tsmiSave.EnabledChanged += new System.EventHandler(this.TsmiSave_EnabledChanged);
            // 
            // tsmiSaveAll
            // 
            this.tsmiSaveAll.Enabled = false;
            this.tsmiSaveAll.Image = global::Easier_Data_Editor.Properties.Resources.icon_save_all;
            this.tsmiSaveAll.Name = "tsmiSaveAll";
            this.tsmiSaveAll.Size = new System.Drawing.Size(248, 22);
            this.tsmiSaveAll.Text = "Save All";
            this.tsmiSaveAll.Click += new System.EventHandler(this.TsmiSaveAll_Click);
            // 
            // tsmiSaveAs
            // 
            this.tsmiSaveAs.Enabled = false;
            this.tsmiSaveAs.Image = global::Easier_Data_Editor.Properties.Resources.icon_save_as;
            this.tsmiSaveAs.Name = "tsmiSaveAs";
            this.tsmiSaveAs.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.S)));
            this.tsmiSaveAs.Size = new System.Drawing.Size(248, 22);
            this.tsmiSaveAs.Text = "Save As...";
            this.tsmiSaveAs.Click += new System.EventHandler(this.TsmiSaveAs_Click);
            // 
            // tsmiExportHTML
            // 
            this.tsmiExportHTML.Enabled = false;
            this.tsmiExportHTML.Name = "tsmiExportHTML";
            this.tsmiExportHTML.Size = new System.Drawing.Size(248, 22);
            this.tsmiExportHTML.Text = "Export to HTML";
            this.tsmiExportHTML.Click += new System.EventHandler(this.TsmiExportHTML_Click);
            // 
            // tss_3
            // 
            this.tss_3.Name = "tss_3";
            this.tss_3.Size = new System.Drawing.Size(245, 6);
            // 
            // tsmiCloseFile
            // 
            this.tsmiCloseFile.Enabled = false;
            this.tsmiCloseFile.Image = global::Easier_Data_Editor.Properties.Resources.icon_close;
            this.tsmiCloseFile.Name = "tsmiCloseFile";
            this.tsmiCloseFile.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.W)));
            this.tsmiCloseFile.Size = new System.Drawing.Size(248, 22);
            this.tsmiCloseFile.Text = "Close File";
            this.tsmiCloseFile.Click += new System.EventHandler(this.TsmiCloseFile_Click);
            // 
            // tsmiCloseAllFiles
            // 
            this.tsmiCloseAllFiles.Name = "tsmiCloseAllFiles";
            this.tsmiCloseAllFiles.Size = new System.Drawing.Size(248, 22);
            this.tsmiCloseAllFiles.Text = "Close All Files";
            this.tsmiCloseAllFiles.Click += new System.EventHandler(this.TsmiCloseAllFiles_Click);
            // 
            // tss_4
            // 
            this.tss_4.Name = "tss_4";
            this.tss_4.Size = new System.Drawing.Size(245, 6);
            // 
            // tsmiQuit
            // 
            this.tsmiQuit.Image = global::Easier_Data_Editor.Properties.Resources.icon_quit;
            this.tsmiQuit.Name = "tsmiQuit";
            this.tsmiQuit.Size = new System.Drawing.Size(248, 22);
            this.tsmiQuit.Text = "Quit";
            this.tsmiQuit.Click += new System.EventHandler(this.TsmiQuit_Click);
            // 
            // DEBUG_DO_ERRORS
            // 
            this.DEBUG_DO_ERRORS.Name = "DEBUG_DO_ERRORS";
            this.DEBUG_DO_ERRORS.Size = new System.Drawing.Size(248, 22);
            this.DEBUG_DO_ERRORS.Text = "Throw Error";
            // 
            // tsmiEdit
            // 
            this.tsmiEdit.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiUndo,
            this.tsmiRedo,
            this.tss_5,
            this.tsmiCut,
            this.tsmiCopy,
            this.tsmiPaste,
            this.tsmiPasteIDs,
            this.tsmiPasteXY,
            this.tsmiDelete,
            this.tsmiSelectAll,
            this.tss_7,
            this.tsmiExpandAllFoldings,
            this.tsmiCollapseAllFoldings});
            this.tsmiEdit.Enabled = false;
            this.tsmiEdit.Name = "tsmiEdit";
            this.tsmiEdit.Size = new System.Drawing.Size(39, 20);
            this.tsmiEdit.Text = "Edit";
            this.tsmiEdit.DropDownOpening += new System.EventHandler(this.TsmiEdit_DropDownOpening);
            // 
            // tsmiUndo
            // 
            this.tsmiUndo.Image = global::Easier_Data_Editor.Properties.Resources.icon_undo;
            this.tsmiUndo.Name = "tsmiUndo";
            this.tsmiUndo.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Z)));
            this.tsmiUndo.Size = new System.Drawing.Size(295, 22);
            this.tsmiUndo.Text = "Undo";
            this.tsmiUndo.Click += new System.EventHandler(this.Undo_Click);
            this.tsmiUndo.EnabledChanged += new System.EventHandler(this.TsmiUndo_EnabledChanged);
            // 
            // tsmiRedo
            // 
            this.tsmiRedo.Image = global::Easier_Data_Editor.Properties.Resources.icon_redo;
            this.tsmiRedo.Name = "tsmiRedo";
            this.tsmiRedo.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Y)));
            this.tsmiRedo.Size = new System.Drawing.Size(295, 22);
            this.tsmiRedo.Text = "Redo";
            this.tsmiRedo.Click += new System.EventHandler(this.Redo_Click);
            this.tsmiRedo.EnabledChanged += new System.EventHandler(this.TsmiRedo_EnabledChanged);
            // 
            // tss_5
            // 
            this.tss_5.Name = "tss_5";
            this.tss_5.Size = new System.Drawing.Size(292, 6);
            // 
            // tsmiCut
            // 
            this.tsmiCut.Name = "tsmiCut";
            this.tsmiCut.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X)));
            this.tsmiCut.Size = new System.Drawing.Size(295, 22);
            this.tsmiCut.Text = "Cut";
            this.tsmiCut.Click += new System.EventHandler(this.TsmiCut_Click);
            // 
            // tsmiCopy
            // 
            this.tsmiCopy.Image = global::Easier_Data_Editor.Properties.Resources.icon_copy;
            this.tsmiCopy.Name = "tsmiCopy";
            this.tsmiCopy.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this.tsmiCopy.Size = new System.Drawing.Size(295, 22);
            this.tsmiCopy.Text = "Copy";
            this.tsmiCopy.Click += new System.EventHandler(this.TsmiCopy_Click);
            // 
            // tsmiPaste
            // 
            this.tsmiPaste.Image = global::Easier_Data_Editor.Properties.Resources.icon_paste;
            this.tsmiPaste.Name = "tsmiPaste";
            this.tsmiPaste.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V)));
            this.tsmiPaste.Size = new System.Drawing.Size(295, 22);
            this.tsmiPaste.Text = "Paste";
            this.tsmiPaste.Click += new System.EventHandler(this.TsmiPaste_Click);
            // 
            // tsmiPasteIDs
            // 
            this.tsmiPasteIDs.AutoToolTip = true;
            this.tsmiPasteIDs.Name = "tsmiPasteIDs";
            this.tsmiPasteIDs.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.V)));
            this.tsmiPasteIDs.Size = new System.Drawing.Size(295, 22);
            this.tsmiPasteIDs.Text = "Paste && Adapt IDs";
            this.tsmiPasteIDs.ToolTipText = "While pasting it changes the frame IDs to available IDs. Also changes the \"next\" " +
    "values.";
            this.tsmiPasteIDs.Click += new System.EventHandler(this.TsmiPasteIDs_Click);
            // 
            // tsmiPasteXY
            // 
            this.tsmiPasteXY.Name = "tsmiPasteXY";
            this.tsmiPasteXY.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.V)));
            this.tsmiPasteXY.Size = new System.Drawing.Size(295, 22);
            this.tsmiPasteXY.Text = "Paste && Adapt X, Y";
            this.tsmiPasteXY.Click += new System.EventHandler(this.TsmiPasteXY_Click);
            // 
            // tsmiDelete
            // 
            this.tsmiDelete.Image = global::Easier_Data_Editor.Properties.Resources.icon_delete;
            this.tsmiDelete.Name = "tsmiDelete";
            this.tsmiDelete.ShortcutKeys = System.Windows.Forms.Keys.Delete;
            this.tsmiDelete.Size = new System.Drawing.Size(295, 22);
            this.tsmiDelete.Text = "Delete";
            this.tsmiDelete.Click += new System.EventHandler(this.TsmiDelete_Click);
            // 
            // tsmiSelectAll
            // 
            this.tsmiSelectAll.Image = global::Easier_Data_Editor.Properties.Resources.icon_select_all;
            this.tsmiSelectAll.Name = "tsmiSelectAll";
            this.tsmiSelectAll.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.A)));
            this.tsmiSelectAll.Size = new System.Drawing.Size(295, 22);
            this.tsmiSelectAll.Text = "Select All";
            this.tsmiSelectAll.Click += new System.EventHandler(this.TsmiSelectAll_Click);
            // 
            // tss_7
            // 
            this.tss_7.Name = "tss_7";
            this.tss_7.Size = new System.Drawing.Size(292, 6);
            // 
            // tsmiExpandAllFoldings
            // 
            this.tsmiExpandAllFoldings.Image = global::Easier_Data_Editor.Properties.Resources.icon_expand;
            this.tsmiExpandAllFoldings.Name = "tsmiExpandAllFoldings";
            this.tsmiExpandAllFoldings.Size = new System.Drawing.Size(295, 22);
            this.tsmiExpandAllFoldings.Text = "Expand All Foldings";
            this.tsmiExpandAllFoldings.Click += new System.EventHandler(this.TsmiExpandAllFoldings_Click);
            // 
            // tsmiCollapseAllFoldings
            // 
            this.tsmiCollapseAllFoldings.Image = global::Easier_Data_Editor.Properties.Resources.icon_collapse;
            this.tsmiCollapseAllFoldings.Name = "tsmiCollapseAllFoldings";
            this.tsmiCollapseAllFoldings.Size = new System.Drawing.Size(295, 22);
            this.tsmiCollapseAllFoldings.Text = "Collapse All Foldings";
            this.tsmiCollapseAllFoldings.Click += new System.EventHandler(this.TsmiCollapseAllFoldings_Click);
            // 
            // tsmiSearch
            // 
            this.tsmiSearch.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiFind,
            this.tsmiFindNext,
            this.tsmiFindPrevious,
            this.tss_6,
            this.tsmiReplace,
            this.tsmiReplaceNext,
            this.tsmiReplacePrevious,
            this.tss_12,
            this.tsmiMark,
            this.tsmiMultiFind,
            this.tss_10,
            this.tsmiGoTo,
            this.tsmiGoToLine,
            this.tsmiGoToFrame,
            this.tss_11,
            this.tsmiBookmark});
            this.tsmiSearch.Enabled = false;
            this.tsmiSearch.Name = "tsmiSearch";
            this.tsmiSearch.Size = new System.Drawing.Size(54, 20);
            this.tsmiSearch.Text = "Search";
            this.tsmiSearch.DropDownOpening += new System.EventHandler(this.TsmiSearch_DropDownOpening);
            // 
            // tsmiFind
            // 
            this.tsmiFind.Name = "tsmiFind";
            this.tsmiFind.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F)));
            this.tsmiFind.Size = new System.Drawing.Size(247, 22);
            this.tsmiFind.Text = "Find...";
            // 
            // tsmiFindNext
            // 
            this.tsmiFindNext.Name = "tsmiFindNext";
            this.tsmiFindNext.ShortcutKeys = System.Windows.Forms.Keys.F3;
            this.tsmiFindNext.Size = new System.Drawing.Size(247, 22);
            this.tsmiFindNext.Text = "Find Next";
            // 
            // tsmiFindPrevious
            // 
            this.tsmiFindPrevious.Name = "tsmiFindPrevious";
            this.tsmiFindPrevious.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Shift | System.Windows.Forms.Keys.F3)));
            this.tsmiFindPrevious.Size = new System.Drawing.Size(247, 22);
            this.tsmiFindPrevious.Text = "Find Previous";
            // 
            // tss_6
            // 
            this.tss_6.Name = "tss_6";
            this.tss_6.Size = new System.Drawing.Size(244, 6);
            // 
            // tsmiReplace
            // 
            this.tsmiReplace.Name = "tsmiReplace";
            this.tsmiReplace.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.R)));
            this.tsmiReplace.Size = new System.Drawing.Size(247, 22);
            this.tsmiReplace.Text = "Replace...";
            // 
            // tsmiReplaceNext
            // 
            this.tsmiReplaceNext.Name = "tsmiReplaceNext";
            this.tsmiReplaceNext.Size = new System.Drawing.Size(247, 22);
            this.tsmiReplaceNext.Text = "Replace Next";
            // 
            // tsmiReplacePrevious
            // 
            this.tsmiReplacePrevious.Name = "tsmiReplacePrevious";
            this.tsmiReplacePrevious.Size = new System.Drawing.Size(247, 22);
            this.tsmiReplacePrevious.Text = "Replace Previous";
            // 
            // tss_12
            // 
            this.tss_12.Name = "tss_12";
            this.tss_12.Size = new System.Drawing.Size(244, 6);
            // 
            // tsmiMark
            // 
            this.tsmiMark.Name = "tsmiMark";
            this.tsmiMark.Size = new System.Drawing.Size(247, 22);
            this.tsmiMark.Text = "Mark";
            // 
            // tsmiMultiFind
            // 
            this.tsmiMultiFind.Name = "tsmiMultiFind";
            this.tsmiMultiFind.Size = new System.Drawing.Size(247, 22);
            this.tsmiMultiFind.Text = "Multi Find and Replace";
            this.tsmiMultiFind.Visible = false;
            // 
            // tss_10
            // 
            this.tss_10.Name = "tss_10";
            this.tss_10.Size = new System.Drawing.Size(244, 6);
            // 
            // tsmiGoTo
            // 
            this.tsmiGoTo.Name = "tsmiGoTo";
            this.tsmiGoTo.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.G)));
            this.tsmiGoTo.Size = new System.Drawing.Size(247, 22);
            this.tsmiGoTo.Text = "Go to...";
            this.tsmiGoTo.Click += new System.EventHandler(this.TsmiGoTo_Click);
            // 
            // tsmiGoToLine
            // 
            this.tsmiGoToLine.Name = "tsmiGoToLine";
            this.tsmiGoToLine.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.J)));
            this.tsmiGoToLine.Size = new System.Drawing.Size(247, 22);
            this.tsmiGoToLine.Text = "Go to Line...";
            this.tsmiGoToLine.Click += new System.EventHandler(this.TsmiGoToLine_Click);
            // 
            // tsmiGoToFrame
            // 
            this.tsmiGoToFrame.Name = "tsmiGoToFrame";
            this.tsmiGoToFrame.Size = new System.Drawing.Size(247, 22);
            this.tsmiGoToFrame.Text = "Go to Frame...";
            this.tsmiGoToFrame.Click += new System.EventHandler(this.TsmiGoToFrame_Click);
            // 
            // tss_11
            // 
            this.tss_11.Name = "tss_11";
            this.tss_11.Size = new System.Drawing.Size(244, 6);
            // 
            // tsmiBookmark
            // 
            this.tsmiBookmark.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiToggleBookmark,
            this.tsmiNextBookmark,
            this.tsmiPreviousBookmark,
            this.tsmiClearBookmarks,
            this.tsmiCutBookmarkedLines,
            this.tsmiCopyBookmarkedLines,
            this.tsmiPasteBookmarkedLines,
            this.tsmiRemoveBookmarkedLines,
            this.tsmiRemoveUnmarkedLines,
            this.tsmiInvertBookmarks});
            this.tsmiBookmark.Name = "tsmiBookmark";
            this.tsmiBookmark.Size = new System.Drawing.Size(247, 22);
            this.tsmiBookmark.Text = "Bookmark";
            // 
            // tsmiToggleBookmark
            // 
            this.tsmiToggleBookmark.Name = "tsmiToggleBookmark";
            this.tsmiToggleBookmark.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F2)));
            this.tsmiToggleBookmark.Size = new System.Drawing.Size(278, 22);
            this.tsmiToggleBookmark.Text = "Toggle Bookmark";
            this.tsmiToggleBookmark.Click += new System.EventHandler(this.TsmiToggleBookmark_Click);
            // 
            // tsmiNextBookmark
            // 
            this.tsmiNextBookmark.Name = "tsmiNextBookmark";
            this.tsmiNextBookmark.ShortcutKeys = System.Windows.Forms.Keys.F2;
            this.tsmiNextBookmark.Size = new System.Drawing.Size(278, 22);
            this.tsmiNextBookmark.Text = "Next Bookmark";
            this.tsmiNextBookmark.Click += new System.EventHandler(this.TsmiNextBookmark_Click);
            // 
            // tsmiPreviousBookmark
            // 
            this.tsmiPreviousBookmark.Name = "tsmiPreviousBookmark";
            this.tsmiPreviousBookmark.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Shift | System.Windows.Forms.Keys.F2)));
            this.tsmiPreviousBookmark.Size = new System.Drawing.Size(278, 22);
            this.tsmiPreviousBookmark.Text = "Previous Bookmark";
            this.tsmiPreviousBookmark.Click += new System.EventHandler(this.TsmiPreviousBookmark_Click);
            // 
            // tsmiClearBookmarks
            // 
            this.tsmiClearBookmarks.Name = "tsmiClearBookmarks";
            this.tsmiClearBookmarks.Size = new System.Drawing.Size(278, 22);
            this.tsmiClearBookmarks.Text = "Clear All Bookmarks";
            this.tsmiClearBookmarks.Click += new System.EventHandler(this.TsmiClearBookmarks_Click);
            // 
            // tsmiCutBookmarkedLines
            // 
            this.tsmiCutBookmarkedLines.Name = "tsmiCutBookmarkedLines";
            this.tsmiCutBookmarkedLines.Size = new System.Drawing.Size(278, 22);
            this.tsmiCutBookmarkedLines.Text = "Cut Bookmarked Lines";
            this.tsmiCutBookmarkedLines.Click += new System.EventHandler(this.TsmiCutBookmarkedLines_Click);
            // 
            // tsmiCopyBookmarkedLines
            // 
            this.tsmiCopyBookmarkedLines.Name = "tsmiCopyBookmarkedLines";
            this.tsmiCopyBookmarkedLines.Size = new System.Drawing.Size(278, 22);
            this.tsmiCopyBookmarkedLines.Text = "Copy Bookmarked Lines";
            this.tsmiCopyBookmarkedLines.Click += new System.EventHandler(this.TsmiCopyBookmarkedLines_Click);
            // 
            // tsmiPasteBookmarkedLines
            // 
            this.tsmiPasteBookmarkedLines.Name = "tsmiPasteBookmarkedLines";
            this.tsmiPasteBookmarkedLines.Size = new System.Drawing.Size(278, 22);
            this.tsmiPasteBookmarkedLines.Text = "Paste to (Replace) Bookmarked Lines";
            this.tsmiPasteBookmarkedLines.Click += new System.EventHandler(this.TsmiPasteBookmarkedLines_Click);
            // 
            // tsmiRemoveBookmarkedLines
            // 
            this.tsmiRemoveBookmarkedLines.Name = "tsmiRemoveBookmarkedLines";
            this.tsmiRemoveBookmarkedLines.Size = new System.Drawing.Size(278, 22);
            this.tsmiRemoveBookmarkedLines.Text = "Remove Bookmarked Lines";
            this.tsmiRemoveBookmarkedLines.Click += new System.EventHandler(this.TsmiRemoveBookmarkedLines_Click);
            // 
            // tsmiRemoveUnmarkedLines
            // 
            this.tsmiRemoveUnmarkedLines.Name = "tsmiRemoveUnmarkedLines";
            this.tsmiRemoveUnmarkedLines.Size = new System.Drawing.Size(278, 22);
            this.tsmiRemoveUnmarkedLines.Text = "Remove Unmarked Lines";
            this.tsmiRemoveUnmarkedLines.Click += new System.EventHandler(this.TsmiRemoveUnmarkedLines_Click);
            // 
            // tsmiInvertBookmarks
            // 
            this.tsmiInvertBookmarks.Name = "tsmiInvertBookmarks";
            this.tsmiInvertBookmarks.Size = new System.Drawing.Size(278, 22);
            this.tsmiInvertBookmarks.Text = "Invert Bookmarks";
            this.tsmiInvertBookmarks.Click += new System.EventHandler(this.TsmiInvertBookmarks_Click);
            // 
            // tsmiView
            // 
            this.tsmiView.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiLineNumbers,
            this.tsmiMarkLine,
            this.tsmiWhitespaces,
            this.tsmiHSplit,
            this.tsmiVSplit,
            this.tsmiWrap});
            this.tsmiView.Enabled = false;
            this.tsmiView.Name = "tsmiView";
            this.tsmiView.Size = new System.Drawing.Size(44, 20);
            this.tsmiView.Text = "View";
            this.tsmiView.DropDownOpening += new System.EventHandler(this.TsmiView_DropDownOpening);
            // 
            // tsmiLineNumbers
            // 
            this.tsmiLineNumbers.CheckOnClick = true;
            this.tsmiLineNumbers.Image = global::Easier_Data_Editor.Properties.Resources.icon_line_numbers;
            this.tsmiLineNumbers.Name = "tsmiLineNumbers";
            this.tsmiLineNumbers.Size = new System.Drawing.Size(180, 22);
            this.tsmiLineNumbers.Text = "Show Line Numbers";
            // 
            // tsmiMarkLine
            // 
            this.tsmiMarkLine.CheckOnClick = true;
            this.tsmiMarkLine.Name = "tsmiMarkLine";
            this.tsmiMarkLine.Size = new System.Drawing.Size(180, 22);
            this.tsmiMarkLine.Text = "Mark Current Line";
            // 
            // tsmiWhitespaces
            // 
            this.tsmiWhitespaces.CheckOnClick = true;
            this.tsmiWhitespaces.Name = "tsmiWhitespaces";
            this.tsmiWhitespaces.Size = new System.Drawing.Size(180, 22);
            this.tsmiWhitespaces.Text = "Show Whitespaces";
            // 
            // tsmiHSplit
            // 
            this.tsmiHSplit.CheckOnClick = true;
            this.tsmiHSplit.Image = global::Easier_Data_Editor.Properties.Resources.icon_split_horizontal;
            this.tsmiHSplit.Name = "tsmiHSplit";
            this.tsmiHSplit.Size = new System.Drawing.Size(180, 22);
            this.tsmiHSplit.Text = "Split View";
            this.tsmiHSplit.Click += new System.EventHandler(this.TsmiHSplit_Click);
            // 
            // tsmiVSplit
            // 
            this.tsmiVSplit.CheckOnClick = true;
            this.tsmiVSplit.Image = global::Easier_Data_Editor.Properties.Resources.icon_split_vertical;
            this.tsmiVSplit.Name = "tsmiVSplit";
            this.tsmiVSplit.Size = new System.Drawing.Size(180, 22);
            this.tsmiVSplit.Text = "Split View";
            this.tsmiVSplit.Click += new System.EventHandler(this.TsmiVSplit_Click);
            // 
            // tsmiWrap
            // 
            this.tsmiWrap.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsCombWrapMode});
            this.tsmiWrap.Name = "tsmiWrap";
            this.tsmiWrap.Size = new System.Drawing.Size(180, 22);
            this.tsmiWrap.Text = "Wrap";
            // 
            // tsCombWrapMode
            // 
            this.tsCombWrapMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.tsCombWrapMode.DropDownWidth = 230;
            this.tsCombWrapMode.FlatStyle = System.Windows.Forms.FlatStyle.Standard;
            this.tsCombWrapMode.Items.AddRange(new object[] {
            "None",
            "Word",
            "Char",
            "Whitespace"});
            this.tsCombWrapMode.Name = "tsCombWrapMode";
            this.tsCombWrapMode.Size = new System.Drawing.Size(180, 23);
            // 
            // tsmiWindow
            // 
            this.tsmiWindow.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiFrameViewer,
            this.tsmiUnusedFrameIDs,
            this.tsmiErrorList});
            this.tsmiWindow.Name = "tsmiWindow";
            this.tsmiWindow.Size = new System.Drawing.Size(63, 20);
            this.tsmiWindow.Text = "Window";
            // 
            // tsmiFrameViewer
            // 
            this.tsmiFrameViewer.Enabled = false;
            this.tsmiFrameViewer.Name = "tsmiFrameViewer";
            this.tsmiFrameViewer.Size = new System.Drawing.Size(169, 22);
            this.tsmiFrameViewer.Text = "Frame Viewer";
            // 
            // tsmiUnusedFrameIDs
            // 
            this.tsmiUnusedFrameIDs.CheckOnClick = true;
            this.tsmiUnusedFrameIDs.Name = "tsmiUnusedFrameIDs";
            this.tsmiUnusedFrameIDs.Size = new System.Drawing.Size(169, 22);
            this.tsmiUnusedFrameIDs.Text = "Unused Frame IDs";
            // 
            // tsmiErrorList
            // 
            this.tsmiErrorList.CheckOnClick = true;
            this.tsmiErrorList.Image = global::Easier_Data_Editor.Properties.Resources.icon_error_list;
            this.tsmiErrorList.Name = "tsmiErrorList";
            this.tsmiErrorList.Size = new System.Drawing.Size(169, 22);
            this.tsmiErrorList.Text = "Error List";
            this.tsmiErrorList.Click += new System.EventHandler(this.TsmiErrorList_Click);
            // 
            // tsmiTools
            // 
            this.tsmiTools.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiReformatter,
            this.tsmiMirrorFrame,
            this.tsmiTagAdder,
            this.tsmiIDChanger,
            this.tsmiSort,
            this.tsmiEquation,
            this.tsmiSetter});
            this.tsmiTools.Name = "tsmiTools";
            this.tsmiTools.Size = new System.Drawing.Size(46, 20);
            this.tsmiTools.Text = "Tools";
            // 
            // tsmiReformatter
            // 
            this.tsmiReformatter.Name = "tsmiReformatter";
            this.tsmiReformatter.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.T)));
            this.tsmiReformatter.Size = new System.Drawing.Size(327, 22);
            this.tsmiReformatter.Text = "Reformatter";
            this.tsmiReformatter.Click += new System.EventHandler(this.TsmiReformatter_Click);
            // 
            // tsmiMirrorFrame
            // 
            this.tsmiMirrorFrame.Name = "tsmiMirrorFrame";
            this.tsmiMirrorFrame.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.M)));
            this.tsmiMirrorFrame.Size = new System.Drawing.Size(327, 22);
            this.tsmiMirrorFrame.Text = "Mirror Frame (with data)";
            // 
            // tsmiTagAdder
            // 
            this.tsmiTagAdder.Name = "tsmiTagAdder";
            this.tsmiTagAdder.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.K)));
            this.tsmiTagAdder.Size = new System.Drawing.Size(327, 22);
            this.tsmiTagAdder.Text = "Tag Adder (Frames must selected)";
            this.tsmiTagAdder.ToolTipText = "Adds to all selected frames a custom tag.";
            // 
            // tsmiIDChanger
            // 
            this.tsmiIDChanger.Name = "tsmiIDChanger";
            this.tsmiIDChanger.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.J)));
            this.tsmiIDChanger.Size = new System.Drawing.Size(327, 22);
            this.tsmiIDChanger.Text = "Auto ID Changer (Frames must selected)";
            // 
            // tsmiSort
            // 
            this.tsmiSort.Name = "tsmiSort";
            this.tsmiSort.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.tsmiSort.Size = new System.Drawing.Size(327, 22);
            this.tsmiSort.Text = "Sort Frames by Frame ID";
            // 
            // tsmiEquation
            // 
            this.tsmiEquation.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiEquationSetOptions,
            this.tss_8,
            this.tsmiEquationCenter,
            this.tsmiEquationBodies,
            this.tsmiEquationItrs,
            this.tsmiEquationBpoi,
            this.tsmiEquationCpoi,
            this.tsmiEquationOpoi,
            this.tsmiEquationWpoi,
            this.tsmiEquationWait,
            this.tsmiEquationState,
            this.tsmiEquationNext,
            this.tsmiEquationHit,
            this.tss_9,
            this.tsmiEquationApply});
            this.tsmiEquation.Name = "tsmiEquation";
            this.tsmiEquation.Size = new System.Drawing.Size(327, 22);
            this.tsmiEquation.Text = "Set Values to All Frames with Same Pic ID";
            // 
            // tsmiEquationSetOptions
            // 
            this.tsmiEquationSetOptions.Enabled = false;
            this.tsmiEquationSetOptions.Name = "tsmiEquationSetOptions";
            this.tsmiEquationSetOptions.Size = new System.Drawing.Size(231, 22);
            this.tsmiEquationSetOptions.Text = "Set Options";
            // 
            // tss_8
            // 
            this.tss_8.Name = "tss_8";
            this.tss_8.Size = new System.Drawing.Size(228, 6);
            // 
            // tsmiEquationCenter
            // 
            this.tsmiEquationCenter.Checked = true;
            this.tsmiEquationCenter.CheckOnClick = true;
            this.tsmiEquationCenter.CheckState = System.Windows.Forms.CheckState.Checked;
            this.tsmiEquationCenter.Name = "tsmiEquationCenter";
            this.tsmiEquationCenter.Size = new System.Drawing.Size(231, 22);
            this.tsmiEquationCenter.Text = "Center X/Y";
            // 
            // tsmiEquationBodies
            // 
            this.tsmiEquationBodies.Checked = true;
            this.tsmiEquationBodies.CheckOnClick = true;
            this.tsmiEquationBodies.CheckState = System.Windows.Forms.CheckState.Checked;
            this.tsmiEquationBodies.Name = "tsmiEquationBodies";
            this.tsmiEquationBodies.Size = new System.Drawing.Size(231, 22);
            this.tsmiEquationBodies.Text = "Bodies";
            // 
            // tsmiEquationItrs
            // 
            this.tsmiEquationItrs.Checked = true;
            this.tsmiEquationItrs.CheckOnClick = true;
            this.tsmiEquationItrs.CheckState = System.Windows.Forms.CheckState.Checked;
            this.tsmiEquationItrs.Name = "tsmiEquationItrs";
            this.tsmiEquationItrs.Size = new System.Drawing.Size(231, 22);
            this.tsmiEquationItrs.Text = "Itrs";
            // 
            // tsmiEquationBpoi
            // 
            this.tsmiEquationBpoi.Checked = true;
            this.tsmiEquationBpoi.CheckOnClick = true;
            this.tsmiEquationBpoi.CheckState = System.Windows.Forms.CheckState.Checked;
            this.tsmiEquationBpoi.Name = "tsmiEquationBpoi";
            this.tsmiEquationBpoi.Size = new System.Drawing.Size(231, 22);
            this.tsmiEquationBpoi.Text = "BPoint";
            // 
            // tsmiEquationCpoi
            // 
            this.tsmiEquationCpoi.Checked = true;
            this.tsmiEquationCpoi.CheckOnClick = true;
            this.tsmiEquationCpoi.CheckState = System.Windows.Forms.CheckState.Checked;
            this.tsmiEquationCpoi.Name = "tsmiEquationCpoi";
            this.tsmiEquationCpoi.Size = new System.Drawing.Size(231, 22);
            this.tsmiEquationCpoi.Text = "CPoint";
            // 
            // tsmiEquationOpoi
            // 
            this.tsmiEquationOpoi.Checked = true;
            this.tsmiEquationOpoi.CheckOnClick = true;
            this.tsmiEquationOpoi.CheckState = System.Windows.Forms.CheckState.Checked;
            this.tsmiEquationOpoi.Name = "tsmiEquationOpoi";
            this.tsmiEquationOpoi.Size = new System.Drawing.Size(231, 22);
            this.tsmiEquationOpoi.Text = "OPoint";
            // 
            // tsmiEquationWpoi
            // 
            this.tsmiEquationWpoi.Checked = true;
            this.tsmiEquationWpoi.CheckOnClick = true;
            this.tsmiEquationWpoi.CheckState = System.Windows.Forms.CheckState.Checked;
            this.tsmiEquationWpoi.Name = "tsmiEquationWpoi";
            this.tsmiEquationWpoi.Size = new System.Drawing.Size(231, 22);
            this.tsmiEquationWpoi.Text = "WPoint";
            // 
            // tsmiEquationWait
            // 
            this.tsmiEquationWait.CheckOnClick = true;
            this.tsmiEquationWait.Name = "tsmiEquationWait";
            this.tsmiEquationWait.Size = new System.Drawing.Size(231, 22);
            this.tsmiEquationWait.Text = "Wait";
            // 
            // tsmiEquationState
            // 
            this.tsmiEquationState.CheckOnClick = true;
            this.tsmiEquationState.Name = "tsmiEquationState";
            this.tsmiEquationState.Size = new System.Drawing.Size(231, 22);
            this.tsmiEquationState.Text = "State";
            // 
            // tsmiEquationNext
            // 
            this.tsmiEquationNext.CheckOnClick = true;
            this.tsmiEquationNext.Name = "tsmiEquationNext";
            this.tsmiEquationNext.Size = new System.Drawing.Size(231, 22);
            this.tsmiEquationNext.Text = "Next";
            // 
            // tsmiEquationHit
            // 
            this.tsmiEquationHit.CheckOnClick = true;
            this.tsmiEquationHit.Name = "tsmiEquationHit";
            this.tsmiEquationHit.Size = new System.Drawing.Size(231, 22);
            this.tsmiEquationHit.Text = "Hit_";
            this.tsmiEquationHit.Visible = false;
            // 
            // tss_9
            // 
            this.tss_9.Name = "tss_9";
            this.tss_9.Size = new System.Drawing.Size(228, 6);
            // 
            // tsmiEquationApply
            // 
            this.tsmiEquationApply.Name = "tsmiEquationApply";
            this.tsmiEquationApply.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.R)));
            this.tsmiEquationApply.Size = new System.Drawing.Size(231, 22);
            this.tsmiEquationApply.Text = "Apply";
            // 
            // tsmiSetter
            // 
            this.tsmiSetter.Name = "tsmiSetter";
            this.tsmiSetter.Size = new System.Drawing.Size(327, 22);
            this.tsmiSetter.Text = "Set Same Attributes to Same Frame ID";
            // 
            // tsmiSettings
            // 
            this.tsmiSettings.Name = "tsmiSettings";
            this.tsmiSettings.Size = new System.Drawing.Size(61, 20);
            this.tsmiSettings.Text = "Settings";
            this.tsmiSettings.Click += new System.EventHandler(this.TsmiSettings_Click);
            // 
            // tsmiAbout
            // 
            this.tsmiAbout.Name = "tsmiAbout";
            this.tsmiAbout.Size = new System.Drawing.Size(52, 20);
            this.tsmiAbout.Text = "About";
            this.tsmiAbout.Click += new System.EventHandler(this.TsmiAbout_Click);
            // 
            // tsmiHiddenTools
            // 
            this.tsmiHiddenTools.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiHiddenToolsShowFrameViewer,
            this.DEBUG_BeginUndo,
            this.DEBUG_EndUndo,
            this.tsmiHiddenToolsToggleBSprite,
            this.tsmiHiddenToolsNextFrame,
            this.tsmiHiddenToolsPreviousFrame});
            this.tsmiHiddenTools.Name = "tsmiHiddenTools";
            this.tsmiHiddenTools.Size = new System.Drawing.Size(88, 20);
            this.tsmiHiddenTools.Text = "Hidden Tools";
            this.tsmiHiddenTools.Visible = false;
            // 
            // tsmiHiddenToolsShowFrameViewer
            // 
            this.tsmiHiddenToolsShowFrameViewer.Name = "tsmiHiddenToolsShowFrameViewer";
            this.tsmiHiddenToolsShowFrameViewer.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.H)));
            this.tsmiHiddenToolsShowFrameViewer.Size = new System.Drawing.Size(315, 22);
            this.tsmiHiddenToolsShowFrameViewer.Text = "Show Frame Viewer from Current File";
            // 
            // DEBUG_BeginUndo
            // 
            this.DEBUG_BeginUndo.Enabled = false;
            this.DEBUG_BeginUndo.Name = "DEBUG_BeginUndo";
            this.DEBUG_BeginUndo.ShortcutKeys = System.Windows.Forms.Keys.F1;
            this.DEBUG_BeginUndo.Size = new System.Drawing.Size(315, 22);
            this.DEBUG_BeginUndo.Text = "BeginUndo";
            // 
            // DEBUG_EndUndo
            // 
            this.DEBUG_EndUndo.Enabled = false;
            this.DEBUG_EndUndo.Name = "DEBUG_EndUndo";
            this.DEBUG_EndUndo.ShortcutKeys = System.Windows.Forms.Keys.F2;
            this.DEBUG_EndUndo.Size = new System.Drawing.Size(315, 22);
            this.DEBUG_EndUndo.Text = "EndUndo";
            // 
            // tsmiHiddenToolsToggleBSprite
            // 
            this.tsmiHiddenToolsToggleBSprite.Name = "tsmiHiddenToolsToggleBSprite";
            this.tsmiHiddenToolsToggleBSprite.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Q)));
            this.tsmiHiddenToolsToggleBSprite.Size = new System.Drawing.Size(315, 22);
            this.tsmiHiddenToolsToggleBSprite.Text = "B-Sprite Toggle";
            // 
            // tsmiHiddenToolsNextFrame
            // 
            this.tsmiHiddenToolsNextFrame.Name = "tsmiHiddenToolsNextFrame";
            this.tsmiHiddenToolsNextFrame.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.Down)));
            this.tsmiHiddenToolsNextFrame.Size = new System.Drawing.Size(315, 22);
            this.tsmiHiddenToolsNextFrame.Text = "Next Frame Text Editor";
            // 
            // tsmiHiddenToolsPreviousFrame
            // 
            this.tsmiHiddenToolsPreviousFrame.Name = "tsmiHiddenToolsPreviousFrame";
            this.tsmiHiddenToolsPreviousFrame.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.Up)));
            this.tsmiHiddenToolsPreviousFrame.Size = new System.Drawing.Size(315, 22);
            this.tsmiHiddenToolsPreviousFrame.Text = "Previous Frame Text Editor";
            // 
            // dockPalMain
            // 
            this.dockPalMain.AllowDrop = true;
            this.dockPalMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dockPalMain.Location = new System.Drawing.Point(0, 51);
            this.dockPalMain.Name = "dockPalMain";
            this.dockPalMain.Size = new System.Drawing.Size(957, 430);
            this.dockPalMain.TabIndex = 8;
            this.dockPalMain.DragDrop += new System.Windows.Forms.DragEventHandler(this.DragDropOnApp);
            this.dockPalMain.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterOnApp);
            this.dockPalMain.DragOver += new System.Windows.Forms.DragEventHandler(this.DragOverOnApp);
            // 
            // ofdData
            // 
            this.ofdData.Filter = "Files|*.dat;*.txt|LF2-Data File|*.dat|Text Files|*.txt";
            this.ofdData.Multiselect = true;
            this.ofdData.Title = "Test";
            // 
            // sfdHTML
            // 
            this.sfdHTML.Filter = "HTML-File|*.html;*.htm";
            // 
            // cmsEncryption
            // 
            this.cmsEncryption.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiEncryptionTrue,
            this.tsmiEncryptionFalse});
            this.cmsEncryption.Name = "cmsEncryption";
            this.cmsEncryption.Size = new System.Drawing.Size(101, 48);
            // 
            // tsmiEncryptionTrue
            // 
            this.tsmiEncryptionTrue.Name = "tsmiEncryptionTrue";
            this.tsmiEncryptionTrue.Size = new System.Drawing.Size(100, 22);
            this.tsmiEncryptionTrue.Text = "True";
            this.tsmiEncryptionTrue.Click += new System.EventHandler(this.TsmiEncryptionTrue_Click);
            // 
            // tsmiEncryptionFalse
            // 
            this.tsmiEncryptionFalse.Name = "tsmiEncryptionFalse";
            this.tsmiEncryptionFalse.Size = new System.Drawing.Size(100, 22);
            this.tsmiEncryptionFalse.Text = "False";
            this.tsmiEncryptionFalse.Click += new System.EventHandler(this.TsmiEncryptionFalse_Click);
            // 
            // cmsEncoding
            // 
            this.cmsEncoding.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiEncodingUTF8,
            this.tsmiEncodingUTF8BOM,
            this.tsmiEncodingUTF16BE,
            this.tsmiEncodingUTF16LE,
            this.tsmiEncodingUTF32BE,
            this.tsmiEncodingUTF32LE});
            this.cmsEncoding.Name = "cmsEncoding";
            this.cmsEncoding.Size = new System.Drawing.Size(166, 136);
            // 
            // tsmiEncodingUTF8
            // 
            this.tsmiEncodingUTF8.Name = "tsmiEncodingUTF8";
            this.tsmiEncodingUTF8.Size = new System.Drawing.Size(165, 22);
            this.tsmiEncodingUTF8.Text = "UTF-8";
            this.tsmiEncodingUTF8.Click += new System.EventHandler(this.TsmiEncodingUTF8_Click);
            // 
            // tsmiEncodingUTF8BOM
            // 
            this.tsmiEncodingUTF8BOM.Name = "tsmiEncodingUTF8BOM";
            this.tsmiEncodingUTF8BOM.Size = new System.Drawing.Size(165, 22);
            this.tsmiEncodingUTF8BOM.Text = "UTF-8 (BOM)";
            this.tsmiEncodingUTF8BOM.Click += new System.EventHandler(this.TsmiEncodingUTF8BOM_Click);
            // 
            // tsmiEncodingUTF16BE
            // 
            this.tsmiEncodingUTF16BE.Name = "tsmiEncodingUTF16BE";
            this.tsmiEncodingUTF16BE.Size = new System.Drawing.Size(165, 22);
            this.tsmiEncodingUTF16BE.Text = "UTF-16 BE (BOM)";
            this.tsmiEncodingUTF16BE.Click += new System.EventHandler(this.TsmiEncodingUTF16BE_Click);
            // 
            // tsmiEncodingUTF16LE
            // 
            this.tsmiEncodingUTF16LE.Name = "tsmiEncodingUTF16LE";
            this.tsmiEncodingUTF16LE.Size = new System.Drawing.Size(165, 22);
            this.tsmiEncodingUTF16LE.Text = "UTF-16 LE (BOM)";
            this.tsmiEncodingUTF16LE.Click += new System.EventHandler(this.TsmiEncodingUTF16LE_Click);
            // 
            // tsmiEncodingUTF32BE
            // 
            this.tsmiEncodingUTF32BE.Name = "tsmiEncodingUTF32BE";
            this.tsmiEncodingUTF32BE.Size = new System.Drawing.Size(165, 22);
            this.tsmiEncodingUTF32BE.Text = "UTF-32 BE (BOM)";
            this.tsmiEncodingUTF32BE.Click += new System.EventHandler(this.TsmiEncodingUTF32BE_Click);
            // 
            // tsmiEncodingUTF32LE
            // 
            this.tsmiEncodingUTF32LE.Name = "tsmiEncodingUTF32LE";
            this.tsmiEncodingUTF32LE.Size = new System.Drawing.Size(165, 22);
            this.tsmiEncodingUTF32LE.Text = "UTF-32 LE (BOM)";
            this.tsmiEncodingUTF32LE.Click += new System.EventHandler(this.TsmiEncodingUTF32LE_Click);
            // 
            // timAnnotations
            // 
            this.timAnnotations.Enabled = true;
            this.timAnnotations.Tick += new System.EventHandler(this.TimAnnotations_Tick);
            // 
            // sfdSaveAsData
            // 
            this.sfdSaveAsData.Filter = "Files|*.dat;*.txt|LF2-Data File|*.dat|Text Files|*.txt";
            // 
            // Main
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(957, 504);
            this.Controls.Add(this.dockPalMain);
            this.Controls.Add(this.ssInfos);
            this.Controls.Add(this.msIcons);
            this.Controls.Add(this.msMain);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(800, 400);
            this.Name = "Main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Easier Data Editor";
            this.DragDrop += new System.Windows.Forms.DragEventHandler(this.DragDropOnApp);
            this.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterOnApp);
            this.DragOver += new System.Windows.Forms.DragEventHandler(this.DragOverOnApp);
            this.ssInfos.ResumeLayout(false);
            this.ssInfos.PerformLayout();
            this.msIcons.ResumeLayout(false);
            this.msIcons.PerformLayout();
            this.msMain.ResumeLayout(false);
            this.msMain.PerformLayout();
            this.cmsEncryption.ResumeLayout(false);
            this.cmsEncoding.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.StatusStrip ssInfos;
        internal System.Windows.Forms.ToolStripStatusLabel tsslTextLength;
        internal System.Windows.Forms.ToolStripStatusLabel tsslTextLengthVal;
        internal System.Windows.Forms.ToolStripStatusLabel tsslTextLines;
        internal System.Windows.Forms.ToolStripStatusLabel tsslTextLinesVal;
        internal System.Windows.Forms.ToolStripStatusLabel tsslCopyright;
        internal System.Windows.Forms.ToolStripStatusLabel linkCookiesoft;
        internal System.Windows.Forms.MenuStrip msIcons;
        internal System.Windows.Forms.ToolStripMenuItem tsmiIconNew;
        internal System.Windows.Forms.ToolStripMenuItem tsmiIconOpen;
        internal System.Windows.Forms.ToolStripMenuItem tsmiIconSave;
        internal System.Windows.Forms.ToolStripMenuItem tsmiIconSaveAll;
        internal System.Windows.Forms.ToolStripMenuItem tsmiIconClose;
        internal System.Windows.Forms.ToolStripMenuItem tsmiIconUndo;
        internal System.Windows.Forms.ToolStripMenuItem tsmiIconRedo;
        internal System.Windows.Forms.ToolStripComboBox tsCombFrames;
        internal System.Windows.Forms.ToolStripComboBox tsCombLF;
        internal System.Windows.Forms.ToolStripMenuItem tsmiRun;
        internal System.Windows.Forms.MenuStrip msMain;
        internal System.Windows.Forms.ToolStripMenuItem tsmiFile;
        internal System.Windows.Forms.ToolStripMenuItem tsmiNew;
        internal System.Windows.Forms.ToolStripSeparator tss_1;
        internal System.Windows.Forms.ToolStripMenuItem tsmiOpen;
        internal System.Windows.Forms.ToolStripMenuItem tsmiRecentFiles;
        internal System.Windows.Forms.ToolStripSeparator tss_2;
        internal System.Windows.Forms.ToolStripMenuItem tsmiSave;
        internal System.Windows.Forms.ToolStripMenuItem tsmiSaveAll;
        internal System.Windows.Forms.ToolStripMenuItem tsmiSaveAs;
        internal System.Windows.Forms.ToolStripMenuItem tsmiExportHTML;
        internal System.Windows.Forms.ToolStripSeparator tss_3;
        internal System.Windows.Forms.ToolStripMenuItem tsmiCloseFile;
        internal System.Windows.Forms.ToolStripMenuItem tsmiCloseAllFiles;
        internal System.Windows.Forms.ToolStripSeparator tss_4;
        internal System.Windows.Forms.ToolStripMenuItem tsmiQuit;
        internal System.Windows.Forms.ToolStripMenuItem DEBUG_DO_ERRORS;
        internal System.Windows.Forms.ToolStripMenuItem tsmiEdit;
        internal System.Windows.Forms.ToolStripMenuItem tsmiUndo;
        internal System.Windows.Forms.ToolStripMenuItem tsmiRedo;
        internal System.Windows.Forms.ToolStripSeparator tss_5;
        internal System.Windows.Forms.ToolStripMenuItem tsmiCut;
        internal System.Windows.Forms.ToolStripMenuItem tsmiCopy;
        internal System.Windows.Forms.ToolStripMenuItem tsmiPaste;
        internal System.Windows.Forms.ToolStripMenuItem tsmiPasteIDs;
        internal System.Windows.Forms.ToolStripMenuItem tsmiPasteXY;
        internal System.Windows.Forms.ToolStripMenuItem tsmiDelete;
        internal System.Windows.Forms.ToolStripMenuItem tsmiSelectAll;
        internal System.Windows.Forms.ToolStripSeparator tss_7;
        internal Forms.BindableToolStripMenuItem tsmiExpandAllFoldings;
        internal Forms.BindableToolStripMenuItem tsmiCollapseAllFoldings;
        internal System.Windows.Forms.ToolStripMenuItem tsmiSearch;
        internal System.Windows.Forms.ToolStripMenuItem tsmiFind;
        internal System.Windows.Forms.ToolStripMenuItem tsmiFindNext;
        internal System.Windows.Forms.ToolStripMenuItem tsmiFindPrevious;
        internal System.Windows.Forms.ToolStripSeparator tss_6;
        internal System.Windows.Forms.ToolStripMenuItem tsmiReplace;
        internal System.Windows.Forms.ToolStripMenuItem tsmiReplaceNext;
        internal System.Windows.Forms.ToolStripMenuItem tsmiReplacePrevious;
        internal System.Windows.Forms.ToolStripSeparator tss_12;
        internal System.Windows.Forms.ToolStripMenuItem tsmiMark;
        internal System.Windows.Forms.ToolStripMenuItem tsmiMultiFind;
        internal System.Windows.Forms.ToolStripSeparator tss_10;
        internal System.Windows.Forms.ToolStripMenuItem tsmiGoTo;
        internal System.Windows.Forms.ToolStripMenuItem tsmiGoToLine;
        internal System.Windows.Forms.ToolStripMenuItem tsmiGoToFrame;
        internal System.Windows.Forms.ToolStripSeparator tss_11;
        internal System.Windows.Forms.ToolStripMenuItem tsmiBookmark;
        internal System.Windows.Forms.ToolStripMenuItem tsmiToggleBookmark;
        internal System.Windows.Forms.ToolStripMenuItem tsmiNextBookmark;
        internal System.Windows.Forms.ToolStripMenuItem tsmiPreviousBookmark;
        internal System.Windows.Forms.ToolStripMenuItem tsmiClearBookmarks;
        internal System.Windows.Forms.ToolStripMenuItem tsmiCutBookmarkedLines;
        internal System.Windows.Forms.ToolStripMenuItem tsmiCopyBookmarkedLines;
        internal System.Windows.Forms.ToolStripMenuItem tsmiPasteBookmarkedLines;
        internal System.Windows.Forms.ToolStripMenuItem tsmiRemoveBookmarkedLines;
        internal System.Windows.Forms.ToolStripMenuItem tsmiRemoveUnmarkedLines;
        internal System.Windows.Forms.ToolStripMenuItem tsmiInvertBookmarks;
        internal System.Windows.Forms.ToolStripMenuItem tsmiView;
        internal Forms.BindableToolStripMenuItem tsmiLineNumbers;
        internal Forms.BindableToolStripMenuItem tsmiMarkLine;
        internal Forms.BindableToolStripMenuItem tsmiWhitespaces;
        internal System.Windows.Forms.ToolStripMenuItem tsmiHSplit;
        internal System.Windows.Forms.ToolStripMenuItem tsmiVSplit;
        internal System.Windows.Forms.ToolStripMenuItem tsmiWrap;
        internal Forms.BindableToolStripComboBox tsCombWrapMode;
        internal System.Windows.Forms.ToolStripMenuItem tsmiWindow;
        internal System.Windows.Forms.ToolStripMenuItem tsmiFrameViewer;
        internal System.Windows.Forms.ToolStripMenuItem tsmiUnusedFrameIDs;
        internal System.Windows.Forms.ToolStripMenuItem tsmiErrorList;
        internal System.Windows.Forms.ToolStripMenuItem tsmiTools;
        internal System.Windows.Forms.ToolStripMenuItem tsmiReformatter;
        internal System.Windows.Forms.ToolStripMenuItem tsmiMirrorFrame;
        internal System.Windows.Forms.ToolStripMenuItem tsmiTagAdder;
        internal System.Windows.Forms.ToolStripMenuItem tsmiIDChanger;
        internal System.Windows.Forms.ToolStripMenuItem tsmiSort;
        internal System.Windows.Forms.ToolStripMenuItem tsmiEquation;
        internal System.Windows.Forms.ToolStripMenuItem tsmiEquationSetOptions;
        internal System.Windows.Forms.ToolStripSeparator tss_8;
        internal System.Windows.Forms.ToolStripMenuItem tsmiEquationCenter;
        internal System.Windows.Forms.ToolStripMenuItem tsmiEquationBodies;
        internal System.Windows.Forms.ToolStripMenuItem tsmiEquationItrs;
        internal System.Windows.Forms.ToolStripMenuItem tsmiEquationBpoi;
        internal System.Windows.Forms.ToolStripMenuItem tsmiEquationCpoi;
        internal System.Windows.Forms.ToolStripMenuItem tsmiEquationOpoi;
        internal System.Windows.Forms.ToolStripMenuItem tsmiEquationWpoi;
        internal System.Windows.Forms.ToolStripMenuItem tsmiEquationWait;
        internal System.Windows.Forms.ToolStripMenuItem tsmiEquationState;
        internal System.Windows.Forms.ToolStripMenuItem tsmiEquationNext;
        internal System.Windows.Forms.ToolStripMenuItem tsmiEquationHit;
        internal System.Windows.Forms.ToolStripSeparator tss_9;
        internal System.Windows.Forms.ToolStripMenuItem tsmiEquationApply;
        internal System.Windows.Forms.ToolStripMenuItem tsmiSetter;
        internal System.Windows.Forms.ToolStripMenuItem tsmiSettings;
        internal System.Windows.Forms.ToolStripMenuItem tsmiAbout;
        internal System.Windows.Forms.ToolStripMenuItem tsmiHiddenTools;
        internal System.Windows.Forms.ToolStripMenuItem tsmiHiddenToolsShowFrameViewer;
        internal System.Windows.Forms.ToolStripMenuItem DEBUG_BeginUndo;
        internal System.Windows.Forms.ToolStripMenuItem DEBUG_EndUndo;
        internal System.Windows.Forms.ToolStripMenuItem tsmiHiddenToolsToggleBSprite;
        internal System.Windows.Forms.ToolStripMenuItem tsmiHiddenToolsNextFrame;
        internal System.Windows.Forms.ToolStripMenuItem tsmiHiddenToolsPreviousFrame;
        private WeifenLuo.WinFormsUI.Docking.DockPanel dockPalMain;
        internal System.Windows.Forms.OpenFileDialog ofdData;
        internal System.Windows.Forms.ToolStripSeparator tss_13;
        private System.Windows.Forms.ToolStripStatusLabel tsslTextSel;
        private System.Windows.Forms.ToolStripStatusLabel tsslTextSelVal;
        private System.Windows.Forms.ToolStripStatusLabel tsslTextPos;
        private System.Windows.Forms.ToolStripStatusLabel tsslTextPosVal;
        internal System.Windows.Forms.ToolStripSeparator tss_14;
        internal System.Windows.Forms.SaveFileDialog sfdHTML;
        private System.Windows.Forms.ContextMenuStrip cmsEncryption;
        private System.Windows.Forms.ToolStripMenuItem tsmiEncryptionTrue;
        private System.Windows.Forms.ToolStripMenuItem tsmiEncryptionFalse;
        private System.Windows.Forms.ToolStripStatusLabel tsslTextEncryption;
        private System.Windows.Forms.ToolStripStatusLabel tsslTextEncryptionVal;
        private System.Windows.Forms.ToolStripStatusLabel tsslTextEncoding;
        internal System.Windows.Forms.ToolStripSeparator tss_15;
        private System.Windows.Forms.ToolStripStatusLabel tsslTextEncodingVal;
        private System.Windows.Forms.ContextMenuStrip cmsEncoding;
        private System.Windows.Forms.ToolStripMenuItem tsmiEncodingUTF8;
        private System.Windows.Forms.ToolStripMenuItem tsmiEncodingUTF8BOM;
        private System.Windows.Forms.ToolStripMenuItem tsmiEncodingUTF16BE;
        private System.Windows.Forms.ToolStripMenuItem tsmiEncodingUTF16LE;
        private System.Windows.Forms.ToolStripMenuItem tsmiEncodingUTF32BE;
        private System.Windows.Forms.ToolStripMenuItem tsmiEncodingUTF32LE;
        private System.Windows.Forms.Timer timAnnotations;
        private System.Windows.Forms.SaveFileDialog sfdSaveAsData;
    }
}

