![Easier Data Editor - Logo](.gitlab/doc/imgs/logo_xl.png)

## Summary

Little Fighter 2 (or LF2 for short) is a beat 'em up game and was created by Marti Wong and Starsky Wong. To edit game
data, it is necessary to decode it.

The "Easier Data Editor" decodes the files, supports the reading of the language by syntax highlighting and visualizing
the frames. However, this editor is aimed at amateurs and advanced "DCers" who already know the basics of
"Data Changing" (`DC`). A very good introduction to this topic can be found on
[LF Empire](https://lf-empire.de/lf2-empire/data-changing). The peculiarity of this data editor is that it was developed
in 2018, which makes it one of the newest. Furthermore, many unique features have been implemented.

**Note:** This repository only starts with version 1.4. Earlier versions were created with VB.NET and were published in
parts on GitHub. The version 1.0 on GitHub represents my first repository. Due to numerous errors caused by unknowing,
the repository has been replaced.

<br />
<br />

## Background

The Little Fighter 2 data files are encrypted with a simple byte shift (a kind of Caesar cipher on byte level with a
plaintext string as password). Data editors for Little Fighter 2 are available several times in different versions. This
data editor was developed by Luigi600 **for** STM93, a person from the LF2 community. STM93 wasn't fully satisfied with
the current DC editing tools, owing to either a lack of certain useful features or certain quirks that made them
annoying to use. Initially, the publication of this editor was not planned, but due to requests from various users, it
was released.

The "Easier Data Editor" focuses on the simple editing of data files for LF2. Project structures, animating frames,
editing at runtime via memory are not supported. This range of functionality will be implemented in the "LF2 Modding
Environment" (see [development blog](https://lf2me.lf-archive.de/) for more information), which is still in development.

<br />
<br />

## Screenshots

<details>
  <summary>Easier Data Editor in ACTION!</summary>

![](https://img.picload.org/image/dcdrcoow/main.png)
![](https://img.picload.org/image/dcdrcolw/sync.gif)
![](https://img.picload.org/image/dcoclcic/3d_itr2.png)

</details>

<br />
<br />

## Download

Go to [GitLab Releases](https://gitlab.com/Luigi600/easier-data-editor/-/releases) for stable releases or download
the [latest build](https://gitlab.com/Luigi600/easier-data-editor/-/jobs/artifacts/dev/v1-4-0-0/download?job=build_debug) (beta).

<br />
<br />

## Requirements

- Windows
- [.NET 8](https://dotnet.microsoft.com/en-us/download/dotnet/8.0) or download a version with `(without host dependencies)` in the name

<br />
<br />

## Build

**Note:** This information is only interesting for developers who want to work on the software.

The software includes several dependencies. As a management tool nuget is used (the most popular for .NET libraries).
Some of the dependencies are available on the official nuget website, but some are from Luigi600's own development.
These libraries are managed on the package registry of GitLab.

```bash
# add gitlab project package registry to nuget for the dependencies "ColorDialog2", "EasierMultiLangSupport", "EasierMultiLangSupport.DockPanelSuiteSupport"
dotnet nuget add source "https://gitlab.com/api/v4/projects/29611136/packages/nuget/index.json" -n "GitLab_EasierDataEditorRepo"

dotnet publish -c Release -o out -r win-x86 --self-contained false /p:EnableWindowsTargeting=true
```

<br />
<br />

## Authors

* Luigi600

<br />
<br />

## Credits

* Leaf.F
    * translation of the entire application in Chinese!
* STM93
    * tester
    * LF2 know-how
* RazenBrenday
    * for the [design of the LF2 font](https://lf-empire.de/forum/showthread.php?tid=10897) that was used to create the logo

<br />
<br />

## Donation

If you like my work, you can donate for me. By doing so, you show that my work is valued. A donation is not mandatory,
but I would be very happy :)

| Service / Currency  | Address                                    | Link / QR Code                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |
|---------------------|--------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Bitcoin             | 39S7NWad3FyGVN43etz4K45s9MBaYj8sbp         | ![Bitcoin Donation Address](.gitlab/doc/imgs/donation_bitcoin.png)                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
| Ethereum            | 0xBf6722333D72Bd1C0B3C6B3ad970310D1Ea6E83B | ![Ethereum Donation Address](.gitlab/doc/imgs/donation_ethereum.png)                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| Litecoin            | MCi3b77HVTvJFAcDZ3wASsmJRJrmxGgzKj         | ![Litecoin Donation Address](.gitlab/doc/imgs/donation_litecoin.png)                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| PayPal              | payment [at] lui-studio.net                | ![PayPal Donation Address](.gitlab/doc/imgs/donation_paypal.png) <br /> <form action="https://www.paypal.com/donate" method="post" target="_top"><input type="hidden" name="hosted_button_id" value="MBKLD79X3SCT2" /><input type="image" src="https://www.paypalobjects.com/en_US/DE/i/btn/btn_donateCC_LG.gif" border="0" name="submit" title="PayPal - The safer, easier way to pay online!" alt="Donate with PayPal button" /><img alt="" border="0" src="https://www.paypal.com/en_DE/i/scr/pixel.gif" width="1" height="1" /></form> |
