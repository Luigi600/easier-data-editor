#!/bin/sh

ZIP_NAME="${1}"
BIN_OUTPUT="${2:-''}"
PACKAGE_REGISTRY_URL="${3:-"${PACKAGE_REGISTRY_URL}"}"

# cd shit so that no parent folder is created during the process
create_zip()
{
  mv "${BIN_OUTPUT}" "${ZIP_NAME}"
  cd "./${ZIP_NAME}"
  zip -r "../${ZIP_NAME}.zip" "./"
  cd ../
  
  curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file "${ZIP_NAME}.zip" ${PACKAGE_REGISTRY_URL}
}

create_zip
